000100**************************************************************/   36370967
000200*  PROGRAM: PPAPTFAB                                         */   36370967
000300*  RELEASE: ___0967______ SERVICE REQUEST(S): _____3637____  */   36370967
000400*  NAME:_______PXP_______ MODIFICATION DATE:  __03/10/95____ */   36370967
000500*  DESCRIPTION:                                              */   36370967
000600**************************************************************/   36370967
000700 IDENTIFICATION DIVISION.                                         PPAPTFAB
000800 PROGRAM-ID. PPAPTFAB.                                            PPAPTFAB
000900 ENVIRONMENT DIVISION.                                            PPAPTFAB
001000 CONFIGURATION SECTION.                                           PPAPTFAB
001100 SOURCE-COMPUTER.                                                 PPAPTFAB
001200 COPY  CPOTXUCS.                                                  PPAPTFAB
001300 DATA DIVISION.                                                   PPAPTFAB
001400 WORKING-STORAGE SECTION.                                         PPAPTFAB
001500                                                                  PPAPTFAB
001600 01  WS-MISC.                                                     PPAPTFAB
001700     05  WS-START                          PIC X(36) VALUE        PPAPTFAB
001800         'PPAPTFAB WORKING STORAGE STARTS HERE'.                  PPAPTFAB
001900     05  WS-THIS-PROGRAM                   PIC X(08)              PPAPTFAB
002000                                               VALUE 'PPAPTFAB'.  PPAPTFAB
002100     05  WS-CALL-PGM                       PIC X(08) VALUE SPACES.PPAPTFAB
002200                                                                  PPAPTFAB
002300 01  UCWSPMSG         EXTERNAL.                                   PPAPTFAB
002400     COPY UCWSPMSG.                                               PPAPTFAB
002500                                                                  PPAPTFAB
002600                                                                  PPAPTFAB
002700 01  CPWSWORK         EXTERNAL.                                   PPAPTFAB
002800     EXEC SQL                                                     PPAPTFAB
002900        INCLUDE CPWSWORK                                          PPAPTFAB
003000     END-EXEC                                                     PPAPTFAB
003100         10  CPWSWORK-PPAPTFAB-WORK-AREA                          PPAPTFAB
003200                             REDEFINES CPWSWORK-PROGRAM-WORK-AREA.PPAPTFAB
003300             15  CPWSWORK-WA-INITIALIZED-IND   PIC X(16).         PPAPTFAB
003400                 88  CPWSWORK-WA-INITIALIZED                      PPAPTFAB
003500                     VALUE '>>??PPAPTFAB??<<'.                    PPAPTFAB
003600             15  CPWSWORK-WA-FILLER            PIC X(484).        PPAPTFAB
003700                                                                  PPAPTFAB
003800                                                                  PPAPTFAB
003900 01  UCCOMMON  EXTERNAL.                                          PPAPTFAB
004000     COPY UCCOMMON.                                               PPAPTFAB
004100                                                                  PPAPTFAB
004200                                                                  PPAPTFAB
004300     EXEC SQL                                                     PPAPTFAB
004400       INCLUDE SQLCA                                              PPAPTFAB
004500     END-EXEC.                                                    PPAPTFAB
004600                                                                  PPAPTFAB
004700                                                                  PPAPTFAB
004800 LINKAGE SECTION.                                                 PPAPTFAB
004900                                                                  PPAPTFAB
005000                                                                  PPAPTFAB
005100 PROCEDURE DIVISION.                                              PPAPTFAB
005200                                                                  PPAPTFAB
005300 0000-MAIN-LINE    SECTION.                                       PPAPTFAB
005400**************************************************************    PPAPTFAB
005500*  THIS IS THE MAIN LINE PROCESSING SECTION                  *    PPAPTFAB
005600**************************************************************    PPAPTFAB
005700                                                                  PPAPTFAB
005800     EXEC SQL                                                     PPAPTFAB
005900          INCLUDE CPPDXE99                                        PPAPTFAB
006000     END-EXEC.                                                    PPAPTFAB
006100                                                                  PPAPTFAB
006200     IF NOT CPWSWORK-WA-INITIALIZED                               PPAPTFAB
006300       INITIALIZE CPWSWORK-PPAPTFAB-WORK-AREA                     PPAPTFAB
006400       SET CPWSWORK-WA-INITIALIZED TO TRUE                        PPAPTFAB
006500     END-IF.                                                      PPAPTFAB
006600                                                                  PPAPTFAB
006700                                                                  PPAPTFAB
006800     GOBACK.                                                      PPAPTFAB
006900                                                                  PPAPTFAB
007000 0000-EXIT.                                                       PPAPTFAB
007100     EXIT.                                                        PPAPTFAB
007200                                                                  PPAPTFAB
007300***************    END OF SOURCE - PPAPTFAB    *******************PPAPTFAB
