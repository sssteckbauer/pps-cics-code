000100**************************************************************/   36430911
000200*  PROGRAM: PPAPPIIH                                         */   36430911
000300*  RELEASE # ___0911___   SERVICE REQUEST NO(S)____3643______*/   36430911
000400*  NAME ____MLG________   MODIFICATION DATE ____06/30/94_____*/   36430911
000500*  DESCRIPTION                                               */   36430911
000600*                                                            */   36430911
000700*     PAYROLL PAR INQUIRY INTERPRET HEADER/FOOTER PROGRAM    */   36430911
000800*     THIS IS A COMPLETE REPLACEMENT OF PPAPPIIH             */   36430911
000900*          AS RELEASED IN RELEASE 795                        */   36430911
001000**************************************************************/   36430911
001000 IDENTIFICATION DIVISION.                                         PPAPPIIH
001100 PROGRAM-ID. PPAPPIIH.                                            PPAPPIIH
001200 ENVIRONMENT DIVISION.                                            PPAPPIIH
001210 CONFIGURATION SECTION.                                           PPAPPIIH
001220 SOURCE-COMPUTER.                                                 PPAPPIIH
001230 COPY  CPOTXUCS.                                                  PPAPPIIH
001300 DATA DIVISION.                                                   PPAPPIIH
001400 WORKING-STORAGE SECTION.                                         PPAPPIIH
001700                                                                  PPAPPIIH
001800 01  WS-COMN.                                                     PPAPPIIH
001903     05  WS-START                          PIC X(27) VALUE        PPAPPIIH
002200         'WORKING STORAGE STARTS HERE'.                           PPAPPIIH
002300     05  WS-KEY-COUNT                    PIC 9(02) VALUE ZEROS.   PPAPPIIH
002000                                                                  PPAPPIIH
002100 01  WS-MESS-NOS.                                                 PPAPPIIH
002200     05 MP0006                           PIC X(5)  VALUE 'P0006'. PPAPPIIH
002700     05 MU0030                           PIC X(5)  VALUE 'U0030'. PPAPPIIH
002300                                                                  PPAPPIIH
010900 01  UCFTRARE  EXTERNAL.                                          PPAPPIIH
010910     COPY UCFTRARE.                                               PPAPPIIH
010913                                                                  PPAPPIIH
003200 01  WS-PPPIFT0 REDEFINES UCFTRARE.                               PPAPPIIH
003300     COPY PPPIFT0.                                                PPAPPIIH
011300                                                                  PPAPPIIH
003500 01  UCCOMMON               EXTERNAL.                             PPAPPIIH
003600     COPY UCCOMMON.                                               PPAPPIIH
011420                                                                  PPAPPIIH
003800 01  UCPIROIH               EXTERNAL.                             PPAPPIIH
003900     COPY UCPIROIH.                                               PPAPPIIH
011423                                                                  PPAPPIIH
004100 01  CPWSWORK               EXTERNAL.                             PPAPPIIH
004200     COPY CPWSWORK.                                               PPAPPIIH
004300                                                                  PPAPPIIH
004400 01  CPWSFOOT               EXTERNAL.                             PPAPPIIH
004500     EXEC SQL                                                     PPAPPIIH
004600          INCLUDE CPWSFOOT                                        PPAPPIIH
004700     END-EXEC.                                                    PPAPPIIH
004800                                                                  PPAPPIIH
004900 01  UCWSABND               EXTERNAL.                             PPAPPIIH
011440     COPY UCWSABND.                                               PPAPPIIH
011450                                                                  PPAPPIIH
005200     COPY DFHAID.                                                 PPAPPIIH
012200                                                                  PPAPPIIH
012300                                                                  PPAPPIIH
012900 LINKAGE SECTION.                                                 PPAPPIIH
013000                                                                  PPAPPIIH
013100     EJECT                                                        PPAPPIIH
013300                                                                  PPAPPIIH
013400 PROCEDURE DIVISION.                                              PPAPPIIH
013600                                                                  PPAPPIIH
006100 0000-MAINLINE                        SECTION.                    PPAPPIIH
013710**************************************************************    PPAPPIIH
006300*  MAIN LINE PROCESSING                                      *    PPAPPIIH
013750**************************************************************    PPAPPIIH
013800                                                                  PPAPPIIH
015100     SET UCCOMMON-RETURN-NO-ERRORS TO TRUE.                       PPAPPIIH
006700     MOVE COMMANDI TO UCCOMMON-COMMAND-DATA.                      PPAPPIIH
006800     MOVE NEXT-FUNCI TO CPWSFOOT-ENTERED-FUNC.                    PPAPPIIH
006900     SET UCPIROIH-KEY-NOT-ENTERED TO TRUE.                        PPAPPIIH
017100                                                                  PPAPPIIH
007100     IF UCCOMMON-UPD-IN-PROGRESS                                  PPAPPIIH
007200      AND UCCOMMON-FUNC-TYPE-INQUIRY                              PPAPPIIH
007300        MOVE MU0030 TO UCCOMMON-HOT-MSG-NUMBER                    PPAPPIIH
007400     END-IF.                                                      PPAPPIIH
007500                                                                  PPAPPIIH
007600     IF EIBAID = DFHENTER                                         PPAPPIIH
007700        PERFORM 1000-CHECK-KEY-COUNT                              PPAPPIIH
007800        IF UCCOMMON-RETURN-NO-ERRORS                              PPAPPIIH
007900           PERFORM 2000-CHECK-DATA-ENTERED                        PPAPPIIH
008000        END-IF                                                    PPAPPIIH
008100     END-IF.                                                      PPAPPIIH
008200                                                                  PPAPPIIH
034530     GOBACK.                                                      PPAPPIIH
034550                                                                  PPAPPIIH
034551 0000-EXIT.                                                       PPAPPIIH
034552     EXIT.                                                        PPAPPIIH
034553                                                                  PPAPPIIH
034554     EJECT                                                        PPAPPIIH
034555                                                                  PPAPPIIH
034556 1000-CHECK-KEY-COUNT                 SECTION.                    PPAPPIIH
034557**************************************************************    PPAPPIIH
009200*  COUNT KEY FIELDS ENTERED. IF MORE THAN 1, IT'S AN ERROR   *    PPAPPIIH
034561**************************************************************    PPAPPIIH
034562                                                                  PPAPPIIH
034563     MOVE ZERO TO WS-KEY-COUNT.                                   PPAPPIIH
034564                                                                  PPAPPIIH
009700     IF FTR-IDL > ZERO AND                                        PPAPPIIH
009800        FTR-IDI NOT = SPACES                                      PPAPPIIH
034567        ADD 1 TO WS-KEY-COUNT                                     PPAPPIIH
034568     END-IF.                                                      PPAPPIIH
034569                                                                  PPAPPIIH
010200     IF FTR-NAMEL > ZERO AND                                      PPAPPIIH
010300        FTR-NAMEI NOT = SPACES                                    PPAPPIIH
034572        ADD 1 TO WS-KEY-COUNT                                     PPAPPIIH
034573     END-IF.                                                      PPAPPIIH
034574                                                                  PPAPPIIH
010700     IF FTR-SSNL > ZERO AND                                       PPAPPIIH
010800        FTR-SSNI NOT = SPACES                                     PPAPPIIH
034577        ADD 1 TO WS-KEY-COUNT                                     PPAPPIIH
034578     END-IF.                                                      PPAPPIIH
034579                                                                  PPAPPIIH
034580     IF WS-KEY-COUNT > 1                                          PPAPPIIH
034581        SET UCCOMMON-RETURN-ERRORS-FOUND TO TRUE                  PPAPPIIH
034582        MOVE MP0006 TO  UCCOMMON-MSG-NUMBER                       PPAPPIIH
011500        SET UCCOMMON-REDISPLAY-FUNC TO TRUE                       PPAPPIIH
011600        SET CPWSWORK-REDISPLAY-EEID TO TRUE                       PPAPPIIH
011700        SET CPWSWORK-REDISPLAY-NAME TO TRUE                       PPAPPIIH
011800        SET CPWSWORK-REDISPLAY-SSN TO TRUE                        PPAPPIIH
034583     END-IF.                                                      PPAPPIIH
034584                                                                  PPAPPIIH
034585 1000-EXIT.                                                       PPAPPIIH
034586     EXIT.                                                        PPAPPIIH
034587                                                                  PPAPPIIH
034589                                                                  PPAPPIIH
034590 2000-CHECK-DATA-ENTERED              SECTION.                    PPAPPIIH
034591**************************************************************    PPAPPIIH
012700*  MOVE ENTERED KEY DATA TO CPWSFOOT                         *    PPAPPIIH
034595**************************************************************    PPAPPIIH
034596                                                                  PPAPPIIH
013000     IF (FTR-IDI NOT = SPACES)                                    PPAPPIIH
013100       AND (FTR-IDI NOT = LOW-VALUES)                             PPAPPIIH
013200         MOVE FTR-IDI TO CPWSFOOT-ENTERED-ID                      PPAPPIIH
013300         SET UCPIROIH-KEY-ENTERED TO TRUE                         PPAPPIIH
013400         MOVE UCCOMMON-ATR-ENTRY TO FTR-IDA                       PPAPPIIH
013500     END-IF.                                                      PPAPPIIH
034598                                                                  PPAPPIIH
013700     IF (FTR-NAMEI NOT = SPACES)                                  PPAPPIIH
013800       AND (FTR-NAMEI NOT = LOW-VALUES)                           PPAPPIIH
013900         MOVE FTR-NAMEI TO CPWSFOOT-ENTERED-NAME                  PPAPPIIH
034602         SET UCPIROIH-KEY-ENTERED TO TRUE                         PPAPPIIH
014100         MOVE UCCOMMON-ATR-ENTRY TO FTR-NAMEA                     PPAPPIIH
034604     END-IF.                                                      PPAPPIIH
034605                                                                  PPAPPIIH
014400     IF (FTR-SSNI NOT = SPACES)                                   PPAPPIIH
014500       AND (FTR-SSNI NOT = LOW-VALUES)                            PPAPPIIH
014600         MOVE FTR-SSNI TO CPWSFOOT-ENTERED-SSN                    PPAPPIIH
034609         SET UCPIROIH-KEY-ENTERED TO TRUE                         PPAPPIIH
014800         MOVE UCCOMMON-ATR-ENTRY TO FTR-SSNA                      PPAPPIIH
034611     END-IF.                                                      PPAPPIIH
034612                                                                  PPAPPIIH
015100     IF (FTR-NEXT-PAY-CYCLEI NOT = SPACES)                        PPAPPIIH
015200       AND (FTR-NEXT-PAY-CYCLEI NOT = LOW-VALUES)                 PPAPPIIH
015300         MOVE FTR-NEXT-PAY-CYCLEI TO CPWSFOOT-ENTERED-PAY-CYCLE   PPAPPIIH
034616         SET UCPIROIH-KEY-ENTERED TO TRUE                         PPAPPIIH
015500         MOVE UCCOMMON-ATR-ENTRY TO FTR-NEXT-PAY-CYCLEA           PPAPPIIH
034618     END-IF.                                                      PPAPPIIH
034619                                                                  PPAPPIIH
015800     IF (FTR-NEXT-CHECK-DATEI NOT = SPACES)                       PPAPPIIH
015900       AND (FTR-NEXT-CHECK-DATEI NOT = LOW-VALUES)                PPAPPIIH
016000         MOVE FTR-NEXT-CHECK-DATEI TO CPWSFOOT-ENTERED-CHECK-DATE PPAPPIIH
016100         SET UCPIROIH-KEY-ENTERED TO TRUE                         PPAPPIIH
016200         MOVE UCCOMMON-ATR-ENTRY TO FTR-NEXT-CHECK-DATEA          PPAPPIIH
016300     END-IF.                                                      PPAPPIIH
016400                                                                  PPAPPIIH
034627 2000-EXIT.                                                       PPAPPIIH
034628     EXIT.                                                        PPAPPIIH
034629                                                                  PPAPPIIH
034630     EJECT                                                        PPAPPIIH
016900                                                                  PPAPPIIH
034631 9999-ABEND    SECTION.                                           PPAPPIIH
034632**************************************************************    PPAPPIIH
034633*  COPIED PROCEDURE DIVISION CODE FOR ABEND HANDLING         *    PPAPPIIH
034634**************************************************************    PPAPPIIH
034636     MOVE 'PPAPPIIH' TO UCWSABND-ABENDING-PGM.                    PPAPPIIH
034637                                                                  PPAPPIIH
034638     COPY UCPDABND.                                               PPAPPIIH
034640                                                                  PPAPPIIH
034700 9999-EXIT.                                                       PPAPPIIH
034800***************    END OF SOURCE - PPAPPIIH    *******************PPAPPIIH
