000100**************************************************************/   36430911
000200*  PROGRAM: PPOROVRD                                         */   36430911
000300*  RELEASE # ___0911___   SERVICE REQUEST NO(S)____3643______*/   36430911
000400*  NAME ____SRS________   MODIFICATION DATE ____06/30/94_____*/   36430911
000500*  DESCRIPTION                                               */   36430911
000600*   REPLACEMENT MODULE FOR AIOOE ARCHITECTURE                */   36430911
000700**************************************************************/   36430911
001000 IDENTIFICATION DIVISION.                                         PPOROVRD
001100 PROGRAM-ID. PPOROVRD.                                            PPOROVRD
001200 ENVIRONMENT DIVISION.                                            PPOROVRD
001300 DATA DIVISION.                                                   PPOROVRD
001400 WORKING-STORAGE SECTION.                                         PPOROVRD
001800 01  WS-MISC-DATA.                                                PPOROVRD
002200     05  WS-COUNT                PIC S9(08) COMP SYNC VALUE ZERO. PPOROVRD
002300     05  WS-SUB                  PIC S9(04) COMP SYNC VALUE ZERO. PPOROVRD
002400     05  WS-MSSG-PAGE            PIC S9(04) COMP SYNC VALUE ZERO. PPOROVRD
001700     05  MP0507                  PIC X(5) VALUE 'P0507'.          PPOROVRD
001800     05  MP0598                  PIC X(5) VALUE 'P0598'.          PPOROVRD
001900 01  ABEYANCE-ORCA-RECORD. COPY CPWSABEY.                         PPOROVRD
007100     03  WS-C1-TRANS-IMAGE REDEFINES ABEY-TRANSACTION-IMAGE.      PPOROVRD
007200         COPY CPWSXC1T.                                           PPOROVRD
008510     03  WS-C3-TRANS-IMAGE REDEFINES ABEY-TRANSACTION-IMAGE.      PPOROVRD
008520         COPY CPWSXC3T.                                           PPOROVRD
002400 01  PPDB2MSG-INTERFACE.            COPY CPLNKDB2.                PPOROVRD
002500 01  UCCOMMON        EXTERNAL.                                    PPOROVRD
002600      EXEC SQL                                                    PPOROVRD
002700         INCLUDE UCCOMMON                                         PPOROVRD
002800      END-EXEC.                                                   PPOROVRD
002900 01  UCPISCRN        EXTERNAL.                                    PPOROVRD
003000      EXEC SQL                                                    PPOROVRD
003100         INCLUDE UCPISCRN                                         PPOROVRD
003200      END-EXEC.                                                   PPOROVRD
003300 01  CPWSSPEC        EXTERNAL.                                    PPOROVRD
003400     EXEC SQL                                                     PPOROVRD
003500          INCLUDE CPWSSPEC                                        PPOROVRD
003600     END-EXEC.                                                    PPOROVRD
003700 01 UCWSABND         EXTERNAL.                                    PPOROVRD
003800      EXEC SQL                                                    PPOROVRD
003900         INCLUDE UCWSABND                                         PPOROVRD
004000      END-EXEC.                                                   PPOROVRD
010400 01  WS-ABE.                                                      PPOROVRD
010500     EXEC SQL                                                     PPOROVRD
010600          INCLUDE PPPVZABE                                        PPOROVRD
010700     END-EXEC.                                                    PPOROVRD
013710     EXEC SQL                                                     PPOROVRD
013720          INCLUDE SQLCA                                           PPOROVRD
013730     END-EXEC.                                                    PPOROVRD
015491      EXEC SQL DECLARE                                            PPOROVRD
015492            ABE-CURS CURSOR FOR                                   PPOROVRD
015515            SELECT EMPLOYEE_ID,                                   PPOROVRD
015516                   PAR_CONTROL_NUMBER,                            PPOROVRD
015517                   TRANSACTION_CODE,                              PPOROVRD
015518                   ENTRY_SEQUENCE_NO,                             PPOROVRD
015524                   TRANSACTION_IMAGE                              PPOROVRD
015525            FROM PPPVZABE_ABE                                     PPOROVRD
005600            WHERE EMPLOYEE_ID        = :SPEC-ENTERED-ID           PPOROVRD
005700              AND PAR_CONTROL_NUMBER = :SPEC-ENTERED-PAR          PPOROVRD
015528              AND (   TRANSACTION_CODE = 'O0'                     PPOROVRD
015529                   OR TRANSACTION_CODE = 'O1'                     PPOROVRD
015530                   OR TRANSACTION_CODE = 'O3'                     PPOROVRD
015531                   OR TRANSACTION_CODE = 'O4' )                   PPOROVRD
015532            ORDER BY TRANSACTION_CODE,                            PPOROVRD
015533                     ENTRY_SEQUENCE_NO                            PPOROVRD
015534      END-EXEC.                                                   PPOROVRD
015540                                                                  PPOROVRD
006600 PROCEDURE DIVISION.                                              PPOROVRD
015700                                                                  PPOROVRD
016000     EXEC SQL                                                     PPOROVRD
006900        INCLUDE CPPDXE99                                          PPOROVRD
016200     END-EXEC.                                                    PPOROVRD
007100     PERFORM 3000-UPDATE-ABEYANCE.                                PPOROVRD
016300                                                                  PPOROVRD
019100 0100-END.                                                        PPOROVRD
019300     GOBACK.                                                      PPOROVRD
019400                                                                  PPOROVRD
034400 3000-UPDATE-ABEYANCE.                                            PPOROVRD
034500                                                                  PPOROVRD
034900      PERFORM 3100-OPEN-ABE-CURS.                                 PPOROVRD
007900      PERFORM 3200-FETCH-ABE-CURS.                                PPOROVRD
035030      IF SQLCODE = 100                                            PPOROVRD
008100         ADD 1 TO SPEC-MSSG-PTR                                   PPOROVRD
008200         MOVE MP0507 TO SPEC-MSSG-NO (SPEC-MSSG-PTR)              PPOROVRD
008300         MOVE 1 TO SPEC-MSSG-PAGE (SPEC-MSSG-PTR)                 PPOROVRD
035070         GO TO 0100-END                                           PPOROVRD
035080      END-IF.                                                     PPOROVRD
035091      IF ABEY-TRANSACTION-CODE-2 NOT = '0'                        PPOROVRD
008700         ADD 1 TO SPEC-MSSG-PTR                                   PPOROVRD
008800         MOVE MP0598 TO SPEC-MSSG-NO (SPEC-MSSG-PTR)              PPOROVRD
008900         MOVE 1 TO SPEC-MSSG-PAGE (SPEC-MSSG-PTR)                 PPOROVRD
035095         GO TO 9999-ABEND                                         PPOROVRD
035096      END-IF.                                                     PPOROVRD
009200      IF SPEC-CURR-PRIOR-YR = 'C'                                 PPOROVRD
035099         MOVE 'CURR ' TO ABEY-EDB-PROCESS-YEAR                    PPOROVRD
035100      ELSE                                                        PPOROVRD
035101         MOVE 'PRIOR' TO ABEY-EDB-PROCESS-YEAR                    PPOROVRD
035102      END-IF.                                                     PPOROVRD
009700      MOVE SPEC-YTD-BAL-IND TO ABEY-TRANS-YTD-BAL-IND.            PPOROVRD
009800      PERFORM 3300-UPDATE-ONE-ROW.                                PPOROVRD
009900      PERFORM 3200-FETCH-ABE-CURS.                                PPOROVRD
010000      PERFORM UNTIL SQLCODE NOT = ZERO                            PPOROVRD
010100       IF ABEY-TRANSACTION-CODE-2 = '1'                           PPOROVRD
010200          MOVE SPEC-YTD-BAL-IND TO XC1T-YEAR-IND                  PPOROVRD
010300          IF XC1T-YEAR-IND = 'N'                                  PPOROVRD
010400             MOVE '0' TO XC1T-QTR-CD                              PPOROVRD
010500          END-IF                                                  PPOROVRD
010600       ELSE                                                       PPOROVRD
010700          MOVE SPEC-YTD-BAL-IND TO XC3T-YEAR                      PPOROVRD
010800          IF XC3T-YEAR = 'N'                                      PPOROVRD
010900             MOVE '0' TO XC3T-QTR-CD                              PPOROVRD
011000          END-IF                                                  PPOROVRD
011100       END-IF                                                     PPOROVRD
011200       PERFORM 3300-UPDATE-ONE-ROW                                PPOROVRD
011300       PERFORM 3200-FETCH-ABE-CURS                                PPOROVRD
011400      END-PERFORM.                                                PPOROVRD
011500      PERFORM 3250-CLOSE-ABE-CURS.                                PPOROVRD
035103                                                                  PPOROVRD
104060 3100-OPEN-ABE-CURS.                                              PPOROVRD
104061                                                                  PPOROVRD
011900     MOVE '3100-OPEN-ABE-CURS' TO DB2MSG-TAG.                     PPOROVRD
104070     EXEC SQL                                                     PPOROVRD
104080       OPEN ABE-CURS                                              PPOROVRD
104090     END-EXEC.                                                    PPOROVRD
104091                                                                  PPOROVRD
104121 3200-FETCH-ABE-CURS.                                             PPOROVRD
104122                                                                  PPOROVRD
012600     MOVE '3200-FETCH-ABE-CURS' TO DB2MSG-TAG.                    PPOROVRD
104123     EXEC SQL                                                     PPOROVRD
104124       FETCH ABE-CURS                                             PPOROVRD
104129         INTO :EMPLOYEE-ID,                                       PPOROVRD
104130              :PAR-CONTROL-NUMBER,                                PPOROVRD
104131              :TRANSACTION-CODE,                                  PPOROVRD
104132              :ENTRY-SEQUENCE-NO,                                 PPOROVRD
104138              :TRANSACTION-IMAGE                                  PPOROVRD
104139     END-EXEC.                                                    PPOROVRD
104141     IF SQLCODE = ZERO                                            PPOROVRD
104142        MOVE WS-ABE TO ABEYANCE-ORCA-RECORD                       PPOROVRD
104161     END-IF.                                                      PPOROVRD
104162                                                                  PPOROVRD
013900 3250-CLOSE-ABE-CURS.                                             PPOROVRD
104252                                                                  PPOROVRD
014100     MOVE '3250-CLOSE-ABE-CURS' TO DB2MSG-TAG.                    PPOROVRD
014200     EXEC SQL                                                     PPOROVRD
014300       CLOSE ABE-CURS                                             PPOROVRD
014400     END-EXEC.                                                    PPOROVRD
104253                                                                  PPOROVRD
104254 3300-UPDATE-ONE-ROW.                                             PPOROVRD
104255                                                                  PPOROVRD
014800     MOVE '3300-UPDATE-ONE-ROW' TO DB2MSG-TAG.                    PPOROVRD
104257     MOVE ABEYANCE-ORCA-RECORD TO WS-ABE.                         PPOROVRD
104259     EXEC SQL                                                     PPOROVRD
104260          UPDATE PPPVZABE_ABE                                     PPOROVRD
104261             SET TRANSACTION_IMAGE = :TRANSACTION-IMAGE           PPOROVRD
104262             WHERE EMPLOYEE_ID        = :EMPLOYEE-ID              PPOROVRD
104263               AND PAR_CONTROL_NUMBER = :PAR-CONTROL-NUMBER       PPOROVRD
104264               AND TRANSACTION_CODE   = :TRANSACTION-CODE         PPOROVRD
104265               AND ENTRY_SEQUENCE_NO  = :ENTRY-SEQUENCE-NO        PPOROVRD
104266     END-EXEC.                                                    PPOROVRD
104267                                                                  PPOROVRD
104291 9999-ABEND.                                                      PPOROVRD
016000**************************************************************    PPOROVRD
016100*  COPIED PROCEDURE DIVISION CODE FOR ABEND HANDLING         *    PPOROVRD
016200**************************************************************    PPOROVRD
016300     MOVE 'PPOROVRD' TO UCWSABND-ABENDING-PGM.                    PPOROVRD
016400     COPY UCPDABND.                                               PPOROVRD
104292                                                                  PPOROVRD
016600*999999-SQL-ERROR.                                                PPOROVRD
016700**************************************************************    PPOROVRD
016800*  COPIED PROCEDURE DIVISION CODE FOR SQL ERROR HANDLING     *    PPOROVRD
016900**************************************************************    PPOROVRD
017000     EXEC SQL                                                     PPOROVRD
017100        INCLUDE CPPDXP99                                          PPOROVRD
017200     END-EXEC.                                                    PPOROVRD
017300     PERFORM 9999-ABEND.                                          PPOROVRD
