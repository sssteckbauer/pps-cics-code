000100**************************************************************/   32031184
000200*  PROGRAM: PPAPDPNF                                         */   32031184
000300*  RELEASE # ___1184___   SERVICE REQUEST NO(S)____13203_____*/   32031184
000400*  NAME __SRS__________   MODIFICATION DATE ____01/07/98_____*/   32031184
000500*  DESCRIPTION                                               */   32031184
000600*  DEPARTMENTAL PAR NEW FUNCTION SETUP APPLICATION PROCESSOR */   32031184
000610**************************************************************/   32031184
000900 IDENTIFICATION DIVISION.                                         PPAPDPNF
001000 PROGRAM-ID. PPAPDPNF.                                            PPAPDPNF
001100 ENVIRONMENT DIVISION.                                            PPAPDPNF
001200 CONFIGURATION SECTION.                                           PPAPDPNF
001300 SOURCE-COMPUTER.                                                 PPAPDPNF
001400 COPY CPOTXUCS.                                                   PPAPDPNF
001500                                                                  PPAPDPNF
001600 DATA DIVISION.                                                   PPAPDPNF
001700 WORKING-STORAGE SECTION.                                         PPAPDPNF
001800                                                                  PPAPDPNF
001900 01  WS-COMN.                                                     PPAPDPNF
002000   05  WS-START                            PIC X(27) VALUE        PPAPDPNF
002100       'WORKING STORAGE STARTS HERE'.                             PPAPDPNF
002200   05  WS-RESP                             PIC S9(8) COMP.        PPAPDPNF
002300   05    PGM-PPINITWK                      PIC X(08)              PPAPDPNF
002400                                               VALUE 'PPINITWK'.  PPAPDPNF
002500                                                                  PPAPDPNF
002600 01  WS-MESS-NOS.                                                 PPAPDPNF
002700     05  MU0030                            PIC X(5) VALUE 'U0030'.PPAPDPNF
002800                                                                  PPAPDPNF
002900 01  UCWSABND    EXTERNAL.                                        PPAPDPNF
003000     COPY UCWSABND.                                               PPAPDPNF
003100                                                                  PPAPDPNF
003200 01  UCPISCRN    EXTERNAL.                                        PPAPDPNF
003300     COPY UCPISCRN.                                               PPAPDPNF
003400                                                                  PPAPDPNF
003500 01  CPWSFOOT    EXTERNAL.                                        PPAPDPNF
003600     COPY CPWSFOOT.                                               PPAPDPNF
003700                                                                  PPAPDPNF
003800 01  CPWSWORK    EXTERNAL.                                        PPAPDPNF
003900     COPY CPWSWORK.                                               PPAPDPNF
004000                                                                  PPAPDPNF
004100 01  CPWSKEYS    EXTERNAL.                                        PPAPDPNF
004200     COPY CPWSKEYS.                                               PPAPDPNF
004300                                                                  PPAPDPNF
004400 01  UCCOMMON   EXTERNAL.                                         PPAPDPNF
004500     EXEC SQL                                                     PPAPDPNF
004600      INCLUDE UCCOMMON                                            PPAPDPNF
004700     END-EXEC.                                                    PPAPDPNF
004800                                                                  PPAPDPNF
004900 01  EUD-ROW       EXTERNAL.                                      PPAPDPNF
005000     EXEC SQL                                                     PPAPDPNF
005100          INCLUDE PPPVZEUD                                        PPAPDPNF
005200     END-EXEC.                                                    PPAPDPNF
005300                                                                  PPAPDPNF
005400 LINKAGE SECTION.                                                 PPAPDPNF
005500                                                                  PPAPDPNF
005600                                                                  PPAPDPNF
005700 EJECT                                                            PPAPDPNF
005800 PROCEDURE DIVISION.                                              PPAPDPNF
005900                                                                  PPAPDPNF
006000 0000-MAIN-LINE   SECTION.                                        PPAPDPNF
006100**************************************************************    PPAPDPNF
006200*  MAIN LINE PROCESSING                                      *    PPAPDPNF
006300**************************************************************    PPAPDPNF
006400                                                                  PPAPDPNF
006500     MOVE ZERO TO UCCOMMON-RETURN-CODE.                           PPAPDPNF
006600     MOVE 'N' TO CPWSWORK-ALL-EXTERNS-LOADED-SW.                  PPAPDPNF
006700                                                                  PPAPDPNF
006800     IF CPWSWORK-EMPLOYEE-NOT-FOUND                               PPAPDPNF
006900        SET CPWSWORK-EMPLOYEE-FOUND TO TRUE                       PPAPDPNF
007000        MOVE SPACES TO CPWSWORK-UNIQUE-EE-ID                      PPAPDPNF
007100                       CPWSKEYS-EMPLOYEE-ID                       PPAPDPNF
007200     END-IF.                                                      PPAPDPNF
007300                                                                  PPAPDPNF
007400     IF UCCOMMON-UPD-IN-PROGRESS                                  PPAPDPNF
007500        IF UCCOMMON-FUNC-TYPE-INQUIRY                             PPAPDPNF
007600         OR UCCOMMON-FUNC-TYPE-SELE                               PPAPDPNF
007700            MOVE MU0030 TO UCCOMMON-HOT-MSG-NUMBER                PPAPDPNF
007800        END-IF                                                    PPAPDPNF
007900     END-IF.                                                      PPAPDPNF
008000                                                                  PPAPDPNF
008100     IF CPWSFOOT-ENTERED-FUNC = SPACES OR LOW-VALUES              PPAPDPNF
008200        IF (UCPISCRN-MENU-FUNCTION-SELECT NOT = SPACES)           PPAPDPNF
008300         AND (UCPISCRN-MENU-FUNCTION-SELECT NOT = LOW-VALUES)     PPAPDPNF
008400           MOVE UCPISCRN-MENU-FUNCTION-SELECT                     PPAPDPNF
008500                                     TO CPWSFOOT-ENTERED-FUNC     PPAPDPNF
008600        END-IF                                                    PPAPDPNF
008700     END-IF.                                                      PPAPDPNF
008800                                                                  PPAPDPNF
008900**************************************************************    PPAPDPNF
009000*  IF IT'S A NEW SUBSYSTEM, AND THE EDB EXTERNALS DO NOT     *    PPAPDPNF
009100*  MATCH THE KEY IN CPWSKEYS, THE EXTERNALS MUST BE LOADED   *    PPAPDPNF
009200*  BY SETTING UCCOMMON-SWITCH-KEYS                           *    PPAPDPNF
009300**************************************************************    PPAPDPNF
009400*****IF (UCCOMMON-SUBSYSTEM-INIT OR CPWSWORK-SWITCH-FROM-BRS)     36430943
009410     IF CPWSWORK-SWITCH-FROM-BRS                                  36430943
009411         OR                                                       36430943
009420     (UCCOMMON-SUBSYSTEM-INIT                                     36430943
009500      AND (CPWSKEYS-EMPLOYEE-ID NOT = EMPLOYEE-ID OF EUD-ROW)     PPAPDPNF
009600      AND (CPWSKEYS-EMPLOYEE-ID NOT = SPACES)                     PPAPDPNF
009700      AND (CPWSKEYS-EMPLOYEE-ID NOT = LOW-VALUES)                 PPAPDPNF
009800******AND (NOT UCCOMMON-OLD-SYSTEM-FUNCTION)                      36430951
009900      AND (UCCOMMON-FUNCTION NOT = 'IBRS')                        PPAPDPNF
009910      )                                                           36430943
010000        MOVE CPWSKEYS-EMPLOYEE-ID TO CPWSWORK-UNIQUE-EE-ID        PPAPDPNF
010100        SET CPWSWORK-RELOAD-EXTERNALS TO TRUE                     PPAPDPNF
010200        SET UCCOMMON-SWITCH-KEYS TO TRUE                          PPAPDPNF
010300     END-IF.                                                      PPAPDPNF
010400                                                                  PPAPDPNF
010410     IF CPWSWORK-SWITCH-FROM-BRS AND CPWSWORK-RELOAD-EXTERNALS    36430943
010420        CONTINUE                                                  36430943
010430     ELSE                                                         36430943
010440        MOVE SPACES TO CPWSWORK-CYCLE-END-DATE                    36430943
010450                       CPWSWORK-CYCLE-CODE                        36430943
010460                       CPWSWORK-RECORD-TYPE                       36430943
010470                       CPWSWORK-PRI-GROSS-CTL                     36430943
010480     END-IF.                                                      36430943
010490                                                                  36430943
010500     IF (CPWSWORK-UNIQUE-EE-ID = SPACES OR LOW-VALUES)            PPAPDPNF
010600      AND (UCCOMMON-FUNCTION-TYPE NOT = 'M')                      PPAPDPNF
010700        MOVE 'IBRS' TO UCCOMMON-FUNCTION-REQUESTED                PPAPDPNF
010800        SET UCCOMMON-SWITCH-FUNCTIONS TO TRUE                     PPAPDPNF
010900        IF UCCOMMON-SUBSYSTEM-INIT                                PPAPDPNF
011000           PERFORM 1000-INITIALIZE-SUBSYSTEM                      PPAPDPNF
011100        END-IF                                                    PPAPDPNF
011200     ELSE                                                         PPAPDPNF
011300        IF UCCOMMON-SUBSYSTEM-INIT                                PPAPDPNF
011400           PERFORM 1000-INITIALIZE-SUBSYSTEM                      PPAPDPNF
011500        END-IF                                                    PPAPDPNF
011600     END-IF.                                                      PPAPDPNF
011700                                                                  PPAPDPNF
011800     MOVE SPACES TO CPWSWORK-SWITCH-FROM-BRS-SW.                  PPAPDPNF
011900                                                                  PPAPDPNF
012000     IF UCCOMMON-FUNCTION = 'IDCS' OR 'IDC2'                      PPAPDPNF
012100        CONTINUE                                                  PPAPDPNF
012200     ELSE                                                         PPAPDPNF
012300        MOVE ZEROES TO CPWSWORK-CURRENT-PAGE                      PPAPDPNF
012400                       CPWSWORK-TOTAL-PAGES                       PPAPDPNF
012500     END-IF.                                                      PPAPDPNF
012600                                                                  PPAPDPNF
012610     IF UCCOMMON-FUNCTION NOT = 'IBRS'                            36430955
012620        MOVE SPACES TO CPWSWORK-BROWSE-RETURN-FUNC                36430955
012630     END-IF.                                                      36430955
012640                                                                  36430955
012700     GOBACK.                                                      PPAPDPNF
012800                                                                  PPAPDPNF
012900 0000-EXIT.                                                       PPAPDPNF
013000     EXIT.                                                        PPAPDPNF
013100                                                                  PPAPDPNF
013200                                                                  PPAPDPNF
013300 EJECT                                                            PPAPDPNF
013400                                                                  PPAPDPNF
013500 1000-INITIALIZE-SUBSYSTEM   SECTION.                             PPAPDPNF
013600**************************************************************    PPAPDPNF
013700*  INITIALIZE WORK AREAS                                     *    PPAPDPNF
013800**************************************************************    PPAPDPNF
013900                                                                  PPAPDPNF
014000     IF NOT CPWSWORK-INITIALIZED                                  PPAPDPNF
014100        PERFORM 1100-INIT-CPWSWORK                                PPAPDPNF
014200     END-IF.                                                      PPAPDPNF
014300                                                                  PPAPDPNF
014400     IF UCCOMMON-SWITCH-KEYS                                      PPAPDPNF
014500        MOVE CPWSKEYS-EMPLOYEE-ID TO CPWSWORK-UNIQUE-EE-ID        PPAPDPNF
014600        SET CPWSWORK-RELOAD-EXTERNALS TO TRUE                     PPAPDPNF
014700     END-IF.                                                      PPAPDPNF
014800                                                                  PPAPDPNF
014900 1000-EXIT.                                                       PPAPDPNF
015000     EXIT.                                                        PPAPDPNF
015100                                                                  PPAPDPNF
015200 EJECT                                                            PPAPDPNF
015300                                                                  PPAPDPNF
015400 1100-INIT-CPWSWORK   SECTION.                                    PPAPDPNF
015500**************************************************************    PPAPDPNF
015600*  INITIALIZE CPWSWORK WORK AREA                             *    PPAPDPNF
015700**************************************************************    PPAPDPNF
015800                                                                  PPAPDPNF
015900     CALL PGM-PPINITWK USING DFHEIBLK                             PPAPDPNF
016000       ON EXCEPTION                                               PPAPDPNF
016100          STRING 'ERROR CALLING PROGRAM: ' DELIMITED BY SIZE      PPAPDPNF
016200                 PGM-PPINITWK              DELIMITED BY SIZE      PPAPDPNF
016300                      INTO UCWSABND-ABEND-DATA (1)                PPAPDPNF
016400          END-STRING                                              PPAPDPNF
016500          PERFORM 9999-ABEND                                      PPAPDPNF
016600     END-CALL.                                                    PPAPDPNF
016700                                                                  PPAPDPNF
016800                                                                  PPAPDPNF
016900 1100-EXIT.                                                       PPAPDPNF
017000     EXIT.                                                        PPAPDPNF
017100                                                                  PPAPDPNF
017200 EJECT                                                            PPAPDPNF
017300                                                                  PPAPDPNF
017400 9999-ABEND    SECTION.                                           PPAPDPNF
017500**************************************************************    PPAPDPNF
017600*  COPIED PROCEDURE DIVISION CODE                            *    PPAPDPNF
017700**************************************************************    PPAPDPNF
017800                                                                  PPAPDPNF
017900     MOVE 'PPAPDPNF' TO UCWSABND-ABENDING-PGM.                    PPAPDPNF
018000                                                                  PPAPDPNF
018100     COPY UCPDABND.                                               PPAPDPNF
018200                                                                  PPAPDPNF
018300 9999-EXIT.                                                       PPAPDPNF
018400     EXIT.                                                        PPAPDPNF
018500                                                                  PPAPDPNF
018600                                                                  PPAPDPNF
018700 EJECT                                                            PPAPDPNF
018800***************    END OF SOURCE - PPAPDPNF    *******************PPAPDPNF
