000100**************************************************************/   36510832
000200*  PROGRAM: UCA2SASR                                         */   36510832
000300*  RELEASE: ___0832______ SERVICE REQUEST(S): _____3651____  */   36510832
000400*  NAME:_______DDM_______ MODIFICATION DATE:  __11/12/93____ */   36510832
000500*  DESCRIPTION: DSS - DISTRIBUTED SYSTEM SERVICES            */   36510832
000600*                   - PRINT FUNCTION DATA                    */   36510832
000700*    STORE/RESTORE TS QUEUES FOR DSS PSEUDO-CONVERSATION.    */   36510832
000800**************************************************************/   36510832
000900 IDENTIFICATION DIVISION.                                         UCA2SASR
001000 PROGRAM-ID. UCA2SASR.                                            UCA2SASR
001100 ENVIRONMENT DIVISION.                                            UCA2SASR
001200 CONFIGURATION SECTION.                                           UCA2SASR
001300 SOURCE-COMPUTER.                                                 UCA2SASR
001400 COPY  CPOTXUCS.                                                  UCA2SASR
001500 DATA DIVISION.                                                   UCA2SASR
001600 WORKING-STORAGE SECTION.                                         UCA2SASR
001700                                                                  UCA2SASR
001800 01  WS-COMN.                                                     UCA2SASR
001900     05  WS-START                          PIC X(27) VALUE        UCA2SASR
002000         'WORKING STORAGE STARTS HERE'.                           UCA2SASR
002100     05  WS-RESP                           PIC S9(8) COMP.        UCA2SASR
002200     05  WS-LENGTH                         PIC S9(08) COMP.       UCA2SASR
002300     05  WS-START-POSITION                 PIC S9(08) COMP.       UCA2SASR
002400     05  TS-QUEUE-ID.                                             UCA2SASR
002500         10  TS-QUEUE-NAME-SS              PIC X(02).             UCA2SASR
002600         10  TS-QUEUE-NAME-N1              PIC 9(01).             UCA2SASR
002700         10  TS-QUEUE-NAME-N2              PIC 9(01).             UCA2SASR
002800         10  TS-QUEUE-TERMID               PIC X(04).             UCA2SASR
002900     05  TS-QUEUE-LENGTH                   PIC S9(04) COMP.       UCA2SASR
003000     05  TS-QUEUE-ITEM                     PIC S9(04) COMP.       UCA2SASR
003100     05  TS-AUDIT-QUEUE-ID.                                       UCA2SASR
003200         10  TS-AUDIT-Q-SS-NAME            PIC X(02).             UCA2SASR
003320         10  TS-AUDIT-Q-VARIABLE           PIC X(02).             UCA2SASR
003330             88  TS-AUDIT-Q-VAR-REGULAR              VALUE 'AU'.  UCA2SASR
003340             88  TS-AUDIT-Q-VAR-NESTED               VALUE 'AN'.  UCA2SASR
003400         10  TS-AUDIT-Q-TERM-ID            PIC X(04).             UCA2SASR
003500     05  TS-AUDIT-QUEUE-LENGTH             PIC S9(04) COMP        UCA2SASR
003600                                                      VALUE 8.    UCA2SASR
003700     05  TS-AUDIT-QUEUE-DATA.                                     UCA2SASR
003800         10  TS-AUDIT-Q-QUEUE-NAME         PIC X(08).             UCA2SASR
003900*                                                                 UCA2SASR
004000 01  WS-TS-AREA                            PIC X(24000).          UCA2SASR
004500*                                                                 UCA2SASR
004600 01  WS-MESS-NOS.                                                 UCA2SASR
004700     05  U0000                             PIC X(5) VALUE 'U0000'.UCA2SASR
004800     EJECT                                                        UCA2SASR
004900 01  PPDB2MSG-INTERFACE.                                          UCA2SASR
005000     COPY CPLNKDB2.                                               UCA2SASR
005100     EJECT                                                        UCA2SASR
005200 01  UCCOMMON   EXTERNAL.                                         UCA2SASR
005300     COPY UCCOMMON.                                               UCA2SASR
005400     EJECT                                                        UCA2SASR
005500 01  UCPIROSR   EXTERNAL.                                         UCA2SASR
005600     COPY UCPIROSR.                                               UCA2SASR
005700     EJECT                                                        UCA2SASR
005800 01  CPWSPRNT   EXTERNAL.                                         UCA2SASR
005900     COPY CPWSPRNT.                                               UCA2SASR
006300     EJECT                                                        UCA2SASR
006400 01  UCWSABND    EXTERNAL.                                        UCA2SASR
006500     COPY UCWSABND.                                               UCA2SASR
007300     EJECT                                                        UCA2SASR
007400 LINKAGE SECTION.                                                 UCA2SASR
007500     EJECT                                                        UCA2SASR
007600 PROCEDURE DIVISION.                                              UCA2SASR
007700****************************************************************  UCA2SASR
007800*            P R O C E D U R E  D I V I S I O N                   UCA2SASR
007900****************************************************************  UCA2SASR
008400 0000-MAINLINE SECTION.                                           UCA2SASR
008500**************************************************************    UCA2SASR
008600*  MAIN LINE PROCESSING SECTION                              *    UCA2SASR
008700*  PERFORM STORE OR RESTORE FUNCTIONS DEPENDING ON REQUEST   *    UCA2SASR
008800*  CODE.                                                     *    UCA2SASR
008900*  IF LOGICAL FUNCTION IS CANCEL, IT IS A REQUEST TO         *    UCA2SASR
009000*  RESTORE THE DATA FROM A HOLD AREA.                        *    UCA2SASR
009100*                                                            *    UCA2SASR
009200*  THE FOLLOWING CONVENTIONS ARE USED FOR TEMP STORAGE QUEUE *    UCA2SASR
009300*  NAMES: SSXYTTTT WHERE:                                    *    UCA2SASR
009400*    SS = SUBSYSTEM ID PASSED FROM UCROUTER                  *    UCA2SASR
009500*    X  = NESTING LEVEL PASSED FROM UCROUTER                 *    UCA2SASR
009600*    Y  = QUEUE IDENTIFIER                                   *    UCA2SASR
009700*    TTTT = TERMINAL ID                                      *    UCA2SASR
009800*                                                            *    UCA2SASR
009900*  THE VALUES FOR THE QUEUE IDENTIFIER ARE AS FOLLOWS:       *    UCA2SASR
010000*                                                            *    UCA2SASR
010100*    QUEUE ID=1 - PRNT - DATA FOR PRINT FUNCTIONS            *    UCA2SASR
010200**************************************************************    UCA2SASR
010800     MOVE ZERO TO UCCOMMON-RETURN-CODE                            UCA2SASR
010900*                                                                 UCA2SASR
011100     EVALUATE TRUE                                                UCA2SASR
011200       WHEN UCCOMMON-STORE-DATA                                   UCA2SASR
011300         PERFORM 1000-STORE-DATA                                  UCA2SASR
011400       WHEN OTHER                                                 UCA2SASR
011500         PERFORM 2000-RESTORE-DATA                                UCA2SASR
012000     END-EVALUATE                                                 UCA2SASR
012200*                                                                 UCA2SASR
012300*                                                                 UCA2SASR
012400     GOBACK.                                                      UCA2SASR
012500*                                                                 UCA2SASR
012600 0000-EXIT.                                                       UCA2SASR
012700     EXIT.                                                        UCA2SASR
012800     EJECT                                                        UCA2SASR
012900 1000-STORE-DATA   SECTION.                                       UCA2SASR
013000**************************************************************    UCA2SASR
013120*  THE FIRST TIME THROUGH, WRITE THE CPWSPRNT QUEUE NAME     *    UCA2SASR
013130*  TO THE AUDIT TSQ FOR LATER CLEANUP BY ROUTER.             *    UCA2SASR
013200**************************************************************    UCA2SASR
013400     MOVE EIBTRMID TO TS-QUEUE-TERMID.                            UCA2SASR
013600     MOVE UCCOMMON-SUBSYSTEM-ID TO TS-QUEUE-NAME-SS.              UCA2SASR
013700     MOVE UCPIROSR-NESTED-FUNCTION-SW TO TS-QUEUE-NAME-N1.        UCA2SASR
014200     MOVE 1 TO TS-QUEUE-NAME-N2.                                  UCA2SASR
014210     MOVE 1 TO TS-QUEUE-ITEM.                                     UCA2SASR
014220     MOVE LENGTH OF CPWSPRNT TO TS-QUEUE-LENGTH.                  UCA2SASR
014300*                                                                 UCA2SASR
014818        PERFORM 9200-WRITE-AUDIT-TSQ                              UCA2SASR
014821        PERFORM 3000-DELETE-QUEUE                                 UCA2SASR
014823*                                                                 UCA2SASR
014824        EXEC CICS                                                 UCA2SASR
014825             WRITEQ                                               UCA2SASR
014826             QUEUE  (TS-QUEUE-ID)                                 UCA2SASR
014827             FROM   (CPWSPRNT)                                    UCA2SASR
014828             ITEM   (TS-QUEUE-ITEM)                               UCA2SASR
014829             LENGTH (TS-QUEUE-LENGTH)                             UCA2SASR
014830             NOHANDLE                                             UCA2SASR
014831             RESP   (WS-RESP)                                     UCA2SASR
014832        END-EXEC                                                  UCA2SASR
014834*                                                                 UCA2SASR
014835     IF  NOT  WS-RESP  = DFHRESP(NORMAL)                          UCA2SASR
014840        PERFORM 9999-ABEND                                        UCA2SASR
014850     END-IF.                                                      UCA2SASR
016200*                                                                 UCA2SASR
016300 1000-EXIT.                                                       UCA2SASR
016400     EXIT.                                                        UCA2SASR
018500     EJECT                                                        UCA2SASR
018600 2000-RESTORE-DATA   SECTION.                                     UCA2SASR
018700**************************************************************    UCA2SASR
018800*  READ TEMP STORAGE QUEUES AND POPULATE EXTERNAL AREAS      *    UCA2SASR
018900**************************************************************    UCA2SASR
019000*                                                                 UCA2SASR
019100     MOVE EIBTRMID TO TS-QUEUE-TERMID.                            UCA2SASR
019200*                                                                 UCA2SASR
019300     MOVE UCCOMMON-SUBSYSTEM-ID TO TS-QUEUE-NAME-SS.              UCA2SASR
019400     MOVE UCPIROSR-NESTED-FUNCTION-SW TO TS-QUEUE-NAME-N1.        UCA2SASR
019500*                                                                 UCA2SASR
019600**************************************************************    UCA2SASR
019700*  QUEUE 1 CONTAINS PRNT - DATA FOR PRINT FUNCTIONS               UCA2SASR
019800**************************************************************    UCA2SASR
019900     MOVE 1 TO TS-QUEUE-NAME-N2.                                  UCA2SASR
020000*                                                                 UCA2SASR
020100     MOVE 1 TO TS-QUEUE-ITEM.                                     UCA2SASR
020200*                                                                 UCA2SASR
020300     EXEC CICS                                                    UCA2SASR
020400          READQ                                                   UCA2SASR
020500          QUEUE  (TS-QUEUE-ID)                                    UCA2SASR
020600          INTO   (CPWSPRNT)                                       UCA2SASR
020700          ITEM   (TS-QUEUE-ITEM)                                  UCA2SASR
020800          NOHANDLE                                                UCA2SASR
020900          RESP   (WS-RESP)                                        UCA2SASR
021000     END-EXEC.                                                    UCA2SASR
021100*                                                                 UCA2SASR
021200     IF WS-RESP = DFHRESP(NORMAL)                                 UCA2SASR
021300        CONTINUE                                                  UCA2SASR
021400     ELSE                                                         UCA2SASR
021500        PERFORM 9999-ABEND                                        UCA2SASR
021600     END-IF.                                                      UCA2SASR
021700*                                                                 UCA2SASR
021800 2000-EXIT.                                                       UCA2SASR
021900     EXIT.                                                        UCA2SASR
022000     EJECT                                                        UCA2SASR
022100 3000-DELETE-QUEUE   SECTION.                                     UCA2SASR
022200**************************************************************    UCA2SASR
022300***DELETE THE TEMP STORAGE QUEUE                             *    UCA2SASR
022400**************************************************************    UCA2SASR
022500*                                                                 UCA2SASR
022600     EXEC CICS                                                    UCA2SASR
022700          DELETEQ TS                                              UCA2SASR
022800          QUEUE (TS-QUEUE-ID)                                     UCA2SASR
022900          NOHANDLE                                                UCA2SASR
023000          RESP(WS-RESP)                                           UCA2SASR
023100     END-EXEC.                                                    UCA2SASR
023200*                                                                 UCA2SASR
023300 3000-EXIT.                                                       UCA2SASR
023310     EXIT.                                                        UCA2SASR
023400     EJECT                                                        UCA2SASR
023500 9200-WRITE-AUDIT-TSQ  SECTION.                                   UCA2SASR
023600**************************************************************    UCA2SASR
023700*  WRITE THE TEMP STORAGE AUDIT QUEUE CONTAINING THE NAME    *    UCA2SASR
023800*  OF A TEMP STORAGE QUEUE BEING WRITTEN.                    *    UCA2SASR
023900**************************************************************    UCA2SASR
023901     MOVE UCCOMMON-SUBSYSTEM-ID TO TS-AUDIT-Q-SS-NAME.            UCA2SASR
023910     IF UCPIROSR-NESTED-FUNCTION                                  UCA2SASR
023920        SET TS-AUDIT-Q-VAR-NESTED TO TRUE                         UCA2SASR
023930     ELSE                                                         UCA2SASR
023940        SET TS-AUDIT-Q-VAR-REGULAR TO TRUE                        UCA2SASR
023950     END-IF.                                                      UCA2SASR
024000*                                                                 UCA2SASR
024200     MOVE EIBTRMID TO TS-AUDIT-Q-TERM-ID.                         UCA2SASR
024300     MOVE TS-QUEUE-ID TO TS-AUDIT-Q-QUEUE-NAME.                   UCA2SASR
024400*                                                                 UCA2SASR
024500     EXEC CICS                                                    UCA2SASR
024600          WRITEQ TS                                               UCA2SASR
024700          QUEUE  (TS-AUDIT-QUEUE-ID)                              UCA2SASR
024800          FROM   (TS-AUDIT-QUEUE-DATA)                            UCA2SASR
024900          LENGTH (TS-AUDIT-QUEUE-LENGTH)                          UCA2SASR
025000          NOHANDLE                                                UCA2SASR
025100          RESP   (WS-RESP)                                        UCA2SASR
025200     END-EXEC.                                                    UCA2SASR
025300*                                                                 UCA2SASR
025400     IF WS-RESP NOT = DFHRESP(NORMAL)                             UCA2SASR
025500        PERFORM 9999-ABEND                                        UCA2SASR
025600     END-IF.                                                      UCA2SASR
025700*                                                                 UCA2SASR
025800 9200-EXIT.                                                       UCA2SASR
025900     EXIT.                                                        UCA2SASR
026000     EJECT                                                        UCA2SASR
026100 9999-ABEND    SECTION.                                           UCA2SASR
026200**************************************************************    UCA2SASR
026300*  COPIED PROCEDURE DIVISION CODE                            *    UCA2SASR
026400**************************************************************    UCA2SASR
026500*                                                                 UCA2SASR
026600     MOVE 'UCA2SASR' TO UCWSABND-ABENDING-PGM.                    UCA2SASR
026700*                                                                 UCA2SASR
026800     COPY UCPDABND.                                               UCA2SASR
026900*                                                                 UCA2SASR
027000 9999-EXIT.                                                       UCA2SASR
027100     EXIT.                                                        UCA2SASR
