000000**************************************************************/   36430935
000001*  PROGRAM: UCAPIDFE                                         */   36430935
000002*  RELEASE: ___0935______ SERVICE REQUEST(S): _____3643____  */   36430935
000003*  NAME:_______MLG_______ MODIFICATION DATE:  __10/20/94____ */   36430935
000004*  DESCRIPTION:                                              */   36430935
000005*    CORRECT EDIT ERROR HANDLING                             */   36430935
000007**************************************************************/   36430935
000010**************************************************************/   36430889
000020*  PROGRAM: UCAPIDFE                                         */   36430889
000030*  RELEASE: ___0889______ SERVICE REQUEST(S): _____3643____  */   36430889
000040*  NAME:_______STC_______ MODIFICATION DATE:  __04/08/94____ */   36430889
000050*  DESCRIPTION:                                              */   36430889
000060*                                                            */   36430889
000070*     CHANGE TO HANDLE ERROR FROM RESOL. REQ PROCESSING      */   36430889
000080*                                                            */   36430889
000090**************************************************************/   36430889
000100**************************************************************/   36430821
000200*  PROGRAM: UCAPIDFE                                         */   36430821
000300*  RELEASE: ___0821______ SERVICE REQUEST(S): _____3643____  */   36430821
000400*  NAME:_______STC_______ MODIFICATION DATE:  __10/05/93____ */   36430821
000500*  DESCRIPTION:                                              */   36430821
000600*                                                            */   36430821
000700*     ID APPLICATION PROCESSOR = 'FINAL-EDIT'                */   36430821
000800*                                                            */   36430821
000900**************************************************************/   36430821
001000 IDENTIFICATION DIVISION.                                         UCAPIDFE
001100 PROGRAM-ID. UCAPIDFE.                                            UCAPIDFE
001200 ENVIRONMENT DIVISION.                                            UCAPIDFE
001300 CONFIGURATION SECTION.                                           UCAPIDFE
001400 SOURCE-COMPUTER.                                  COPY CPOTXUCS. UCAPIDFE
001500 OBJECT-COMPUTER.                                  COPY CPOTXOBJ. UCAPIDFE
001600 DATA DIVISION.                                                   UCAPIDFE
001700 WORKING-STORAGE SECTION.                                         UCAPIDFE
001800                                                                  UCAPIDFE
001900 01  UCCOMMON   EXTERNAL.                                         UCAPIDFE
002000     COPY UCCOMMON.                                               UCAPIDFE
002100*                                                                 UCAPIDFE
002200 01  UCWSABND    EXTERNAL.                                        UCAPIDFE
002300     COPY UCWSABND.                                               UCAPIDFE
002400*                                                                 UCAPIDFE
002500 01  UCIDWSWK    EXTERNAL.                                        UCAPIDFE
002600     COPY UCIDWSWK.                                               UCAPIDFE
002700     EJECT                                                        UCAPIDFE
002800*                                                                 UCAPIDFE
002900 01  UCIDWS10    EXTERNAL.                                        UCAPIDFE
003000     COPY UCIDWS10.                                               UCAPIDFE
003100     EJECT                                                        UCAPIDFE
003200*                                                                 UCAPIDFE
003300 01  UCIDWSID    EXTERNAL.                                        UCAPIDFE
003400     COPY UCIDWSID.                                               UCAPIDFE
003500     EJECT                                                        UCAPIDFE
003600*                                                                 UCAPIDFE
003700 01  UCIDWSBS    EXTERNAL.                                        UCAPIDFE
003800     COPY UCIDWSBS.                                               UCAPIDFE
003900     EJECT                                                        UCAPIDFE
004000*                                                                 UCAPIDFE
004100 01  MISC-WORK-AREA.                                              UCAPIDFE
004200     05  WS-IDB-UTILITY          PIC X(08)   VALUE 'UCIID100'.    UCAPIDFE
004300     05  WS-IXB-UTILITY          PIC X(08)   VALUE 'UCIID200'.    UCAPIDFE
004400     05  WS-ASN-UTILITY          PIC X(08)   VALUE 'UCIIDASN'.    UCAPIDFE
004500     05  WS-MNT-UTILITY          PIC X(08)   VALUE 'UCIIDMNT'.    UCAPIDFE
004600                                                                  UCAPIDFE
004700 PROCEDURE DIVISION.                                              UCAPIDFE
004800                                                                  UCAPIDFE
004900 0000-MAIN-LINE   SECTION.                                        UCAPIDFE
005000                                                                  UCAPIDFE
005001**** SET UCCOMMON-NO-EDIT-ERRORS                                  36430889
005002****                             TO TRUE.                         36430889
005010     IF UCCOMMON-MSG-NUMBER NOT = SPACES                          36430889
005100        SET UCCOMMON-EDIT-ERRORS-FOUND                            36430935
005110********SET UCCOMMON-NO-EDIT-ERRORS                               36430935
005200                                 TO TRUE                          36430889
005201     ELSE                                                         36430889
005202        SET UCCOMMON-NO-EDIT-ERRORS                               36430935
005210********SET UCCOMMON-EDIT-ERRORS-FOUND                            36430935
005220                                 TO TRUE                          36430889
005230     END-IF.                                                      36430889
005300                                                                  UCAPIDFE
005400     IF UCIDWSWK-IDAS-RESOL-NEEDED                                UCAPIDFE
005500        SET UCCOMMON-EDIT-ERRORS-FOUND                            UCAPIDFE
005600                                 TO TRUE                          UCAPIDFE
005700        MOVE 'IDBS'                                               UCAPIDFE
005800                                 TO UCCOMMON-FUNCTION-REQUESTED   UCAPIDFE
005900        SET UCCOMMON-SWITCH-FUNCTIONS                             UCAPIDFE
006000                                 TO TRUE                          UCAPIDFE
006100     END-IF.                                                      UCAPIDFE
006200                                                                  UCAPIDFE
006300     GOBACK.                                                      UCAPIDFE
006400                                                                  UCAPIDFE
006500 0000-EXIT.                                                       UCAPIDFE
006600     EXIT.                                                        UCAPIDFE
