000100**************************************************************/   36430875
000200*  PROGRAM: UCAPIDAB                                         */   36430875
000300*  RELEASE: ___0875______ SERVICE REQUEST(S): _____3643____  */   36430875
000400*  NAME:_______DXS_______ MODIFICATION DATE:  __02/24/94____ */   36430875
000500*  DESCRIPTION: ABEND PROCESSOR FOR THE ID SUBSYSTEM         */   36430875
000600*    THIS PROGRAM WILL BACKOUT CHANGES IN THE ID SUBSYSTEM   */   36430875
000700*    IN THE EVENT OF AN ABEND.                               */   36430875
000800**************************************************************/   36430875
000900 IDENTIFICATION DIVISION.                                         UCAPIDAB
001000 PROGRAM-ID. UCAPIDAB.                                            UCAPIDAB
001100 ENVIRONMENT DIVISION.                                            UCAPIDAB
001200 CONFIGURATION SECTION.                                           UCAPIDAB
001300 SOURCE-COMPUTER.                                                 UCAPIDAB
001400 COPY  CPOTXUCS.                                                  UCAPIDAB
001500 DATA DIVISION.                                                   UCAPIDAB
001600 WORKING-STORAGE SECTION.                                         UCAPIDAB
001700                                                                  UCAPIDAB
001800 01  WS-MISC.                                                     UCAPIDAB
001900     05  WS-THIS-PGM-NAME          PIC X(08) VALUE 'UCAPIDAB'.    UCAPIDAB
002000     05  WS-ASN-PGM                PIC X(08) VALUE 'UCIIDASN'.    UCAPIDAB
002100     05  WS-PENDING                PIC X(01) VALUE 'P'.           UCAPIDAB
002200                                                                  UCAPIDAB
002300 01  WS-PMSG-MESSAGE.                                             UCAPIDAB
002400     05  WS-ID-MSG                 PIC X(45)                      UCAPIDAB
002500         VALUE 'UCAPIDAB - PENDING EMPLOYEE ID BACKOUT FAILED'.   UCAPIDAB
002600                                                                  UCAPIDAB
002700 01  UCCOMMON         EXTERNAL.                                   UCAPIDAB
002800     COPY UCCOMMON.                                               UCAPIDAB
002900                                                                  UCAPIDAB
003000 01  UCWSPMSG         EXTERNAL.                                   UCAPIDAB
003100     COPY UCWSPMSG.                                               UCAPIDAB
003200                                                                  UCAPIDAB
003300 01  UCIDWSAS         EXTERNAL.                                   UCAPIDAB
003400     COPY UCIDWSAS.                                               UCAPIDAB
003500                                                                  UCAPIDAB
003600 01  UCIDWSID         EXTERNAL.                                   UCAPIDAB
003700     COPY UCIDWSID.                                               UCAPIDAB
003800                                                                  UCAPIDAB
003900******************************************************************UCAPIDAB
004000*          SQL - WORKING STORAGE                                 *UCAPIDAB
004100******************************************************************UCAPIDAB
004200                                                                  UCAPIDAB
004300 LINKAGE SECTION.                                                 UCAPIDAB
004400                                                                  UCAPIDAB
004500     EJECT                                                        UCAPIDAB
004600                                                                  UCAPIDAB
004700 PROCEDURE DIVISION.                                              UCAPIDAB
004800                                                                  UCAPIDAB
004900 0000-MAINLINE   SECTION.                                         UCAPIDAB
005000**************************************************************    UCAPIDAB
005100*  MAIN LINE PROCESSING                                      *    UCAPIDAB
005200**************************************************************    UCAPIDAB
005300     INITIALIZE UCIDWSAS-EXT-DATA.                                UCAPIDAB
005400                                                                  UCAPIDAB
005500     IF UCIDWSID-IDB-INDIVIDUAL-ID NOT = SPACES      AND          UCAPIDAB
005600        UCIDWSID-IDB-INDIVIDUAL-ID NOT = LOW-VALUES  AND          UCAPIDAB
005700        UCIDWSID-IDB-STATUS            = WS-PENDING               UCAPIDAB
005800        SET UCIDWSAS-REQ-PENDING-DELETE TO TRUE                   UCAPIDAB
005900        MOVE UCIDWSID-IDB-INDIVIDUAL-ID TO UCIDWSAS-PASSED-IID    UCAPIDAB
006000        MOVE UCIDWSID-IDB-CHANGED-AT TO UCIDWSAS-PASSED-CHANGED-ATUCAPIDAB
006100        CALL WS-ASN-PGM                                           UCAPIDAB
006200        IF NOT UCIDWSAS-STAT-PEND-DELETED                         UCAPIDAB
006300           MOVE 'UCWRPMSG' TO UCWSPMSG-PGM-NAME                   UCAPIDAB
006400           MOVE SPACES     TO UCWSPMSG-DISPLAY-DATA               UCAPIDAB
006500           MOVE WS-ID-MSG  TO UCWSPMSG-DISPLAY-DATA               UCAPIDAB
006600           CALL UCWSPMSG-PGM-NAME USING DFHEIBLK DFHCOMMAREA      UCAPIDAB
006700        END-IF                                                    UCAPIDAB
006800     END-IF.                                                      UCAPIDAB
006900                                                                  UCAPIDAB
007000     GOBACK.                                                      UCAPIDAB
007100                                                                  UCAPIDAB
007200 0000-EXIT.                                                       UCAPIDAB
007300     EXIT.                                                        UCAPIDAB
007400                                                                  UCAPIDAB
007500***************    END OF SOURCE - UCAPIDAB    *******************UCAPIDAB
