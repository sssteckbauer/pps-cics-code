000000**************************************************************/   48611390
000001*  PROGRAM: PPGETMN                                          */   48611390
000002*  RELEASE: ___1360______ SERVICE REQUEST(S): ____14861____  */   48611390
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___01/28/02__  */   48611390
000004*  DESCRIPTION:                                              */   48611390
000005*  - ADDED REFERENCE TO THE DB2 BUF POINTER FOR GETMAIN AND  */   48611390
000006*    FREEMAIN PHASES OF CICS.                                */   48611390
000007**************************************************************/   48611390
000100**************************************************************/   36430815
000200*  PROGRAM: PPGETMN                                          */   36430815
000300*  RELEASE: ___0815______ SERVICE REQUEST(S): _____3643____  */   36430815
000400*  NAME:_______PXP_______ MODIFICATION DATE:  __09/17/93____ */   36430815
000500*  DESCRIPTION:                                              */   36430815
000600*  ADD SUPPORT FOR NEW GETMAINED AREAS                       */   36430815
000700**************************************************************/   36430815
000800**************************************************************/   36430795
000900*  PROGRAM: PPGETMN                                          */   36430795
001000*  RELEASE # ___0795___   SERVICE REQUEST NO(S) ___3643____  */   36430795
001100*  NAME ____PXP________   MODIFICATION DATE ____07/15/93_____*/   36430795
001200*  DESCRIPTION                                               */   36430795
001300*    THIS MODULE DOES GETMAINS AND SAVES ADDRESS IN CWA      */   36430795
001400**************************************************************/   36430795
001500 IDENTIFICATION DIVISION.                                         PPGETMN
001600 PROGRAM-ID. PPGETMN.                                             PPGETMN
001700 ENVIRONMENT DIVISION.                                            PPGETMN
001800 CONFIGURATION SECTION.                                           PPGETMN
001900 SOURCE-COMPUTER.                                                 PPGETMN
002000 COPY CPOTXUCS.                                                   PPGETMN
002100 OBJECT-COMPUTER.                                                 PPGETMN
002200 COPY CPOTXOBJ.                                                   PPGETMN
002300 DATA DIVISION.                                                   PPGETMN
002400     EJECT                                                        PPGETMN
002500                                                                  PPGETMN
002600 WORKING-STORAGE SECTION.                                         PPGETMN
002700     SKIP3                                                        PPGETMN
002800 01  FILLER    PIC X(27) VALUE 'WORKING STORAGE STARTS HERE'.     PPGETMN
002900                                                                  PPGETMN
003000 01  WS-LOW-VALUES        PIC X(01) VALUE LOW-VALUES.             PPGETMN
003100 01  WS-STATUS-FLAGS.                                             PPGETMN
003200     05  WS-RESP          PIC  S9(08) COMP.                       PPGETMN
003300     05  WS-CWA-LNG       PIC  S9(04) COMP.                       PPGETMN
003400     05  WS-PTR           POINTER.                                PPGETMN
003500     05  WS-PTR-9 REDEFINES                                       PPGETMN
003600         WS-PTR           PIC S9(09) COMP.                        PPGETMN
003700                                                                  PPGETMN
003800 01  CPWSGETM     EXTERNAL.                                       PPGETMN
003900     COPY CPWSGETM.                                               PPGETMN
004000 01  UCWSABND     EXTERNAL.                                       PPGETMN
004100     COPY UCWSABND.                                               PPGETMN
004200     EJECT                                                        PPGETMN
004300 LINKAGE SECTION.                                                 PPGETMN
004400 01  CPWSXCWA.                                                    PPGETMN
004500     COPY CPWSXCWA.                                               PPGETMN
004600 01  DUMMY-AREA.                                                  PPGETMN
004700     05  FILLER           PIC X(1024).                            PPGETMN
004800     EJECT                                                        PPGETMN
004900 PROCEDURE DIVISION.                                              PPGETMN
005000                                                                  PPGETMN
005100 0000-MAINLINE SECTION.                                           PPGETMN
005200                                                                  PPGETMN
005300     EVALUATE TRUE                                                PPGETMN
005400         WHEN CPWSGETM-VALID-RESOURCE                             PPGETMN
005500         WHEN CPWSGETM-CWAADDR-FUNCTION                           PPGETMN
005600             CONTINUE                                             PPGETMN
005700         WHEN  OTHER                                              PPGETMN
005800             MOVE 'RESOURCE IS UNKNOWN             '              PPGETMN
005900                                 TO UCWSABND-MSG-DATA             PPGETMN
006000             MOVE 'CPWSGETM-RESOURCE =                '           PPGETMN
006100                                 TO UCWSABND-ABEND-DATA (1)       PPGETMN
006200             MOVE CPWSGETM-RESOURCE                               PPGETMN
006300                                 TO UCWSABND-ABEND-DATA (2)       PPGETMN
006400             PERFORM 9999-ABEND                                   PPGETMN
006500     END-EVALUATE.                                                PPGETMN
006600                                                                  PPGETMN
006700     PERFORM 3000-GET-CWA-ADDR.                                   PPGETMN
006800                                                                  PPGETMN
006900     IF NOT CPWSXCWA-SET-ID-DONE                                  PPGETMN
007000        SET CPWSXCWA-SET-ID-DONE TO TRUE                          PPGETMN
007100        MOVE LOW-VALUE TO  CPWSXCWA-POINTERS                      PPGETMN
007200     END-IF.                                                      PPGETMN
007300     EVALUATE TRUE                                                PPGETMN
007400         WHEN CPWSGETM-GETMAIN-FUNCTION                           PPGETMN
007500             PERFORM 1000-GETMAIN                                 PPGETMN
007600         WHEN CPWSGETM-FREEMAIN-FUNCTION                          PPGETMN
007700             PERFORM 2000-FREEMAIN                                PPGETMN
007800         WHEN CPWSGETM-CWAADDR-FUNCTION                           PPGETMN
007900             CONTINUE                                             PPGETMN
008000         WHEN OTHER                                               PPGETMN
008100             MOVE 'FUNCTION IS UNKNOWN             '              PPGETMN
008200                                 TO UCWSABND-MSG-DATA             PPGETMN
008300             MOVE 'CPWSGETM-FUNCTION =                '           PPGETMN
008400                                 TO UCWSABND-ABEND-DATA (1)       PPGETMN
008500             MOVE CPWSGETM-FUNCTION                               PPGETMN
008600                                 TO UCWSABND-ABEND-DATA (2)       PPGETMN
008700             PERFORM 9999-ABEND                                   PPGETMN
008800     END-EVALUATE.                                                PPGETMN
008900                                                                  PPGETMN
009000     GOBACK.                                                      PPGETMN
009100                                                                  PPGETMN
009200 0000-EXIT.                                                       PPGETMN
009300     EXIT.                                                        PPGETMN
009400                                                                  PPGETMN
009500     EJECT                                                        PPGETMN
009600                                                                  PPGETMN
009700 1000-GETMAIN                SECTION.                             PPGETMN
009800                                                                  PPGETMN
009900     IF CPWSGETM-GETMAIN-FLENGTH NOT > ZERO                       PPGETMN
010000             MOVE 'LENGTH FOR GETMAIN BAD          '              PPGETMN
010100                                 TO UCWSABND-MSG-DATA             PPGETMN
010200             MOVE 'CPWSGETM-GETMAIN-FLENGTH =         '           PPGETMN
010300                                 TO UCWSABND-ABEND-DATA (1)       PPGETMN
010400             MOVE CPWSGETM-GETMAIN-FLENGTH                        PPGETMN
010500                                 TO UCWSABND-ABEND-DATA (2)       PPGETMN
010600        PERFORM 9999-ABEND                                        PPGETMN
010700     END-IF.                                                      PPGETMN
010800                                                                  PPGETMN
010900     EXEC CICS                                                    PPGETMN
011000          GETMAIN                                                 PPGETMN
011100          SET(WS-PTR-9)                                           PPGETMN
011200          FLENGTH(CPWSGETM-GETMAIN-FLENGTH)                       PPGETMN
011300          INITIMG(WS-LOW-VALUES)                                  PPGETMN
011400          SHARED                                                  PPGETMN
011500          NOSUSPEND                                               PPGETMN
011600          NOHANDLE                                                PPGETMN
011700          RESP   (WS-RESP)                                        PPGETMN
011800     END-EXEC.                                                    PPGETMN
011900                                                                  PPGETMN
012000     IF WS-RESP NOT = DFHRESP(NORMAL)                             PPGETMN
012100             MOVE 'EXEC CICS GETMAIN FAILED        '              PPGETMN
012200                                 TO UCWSABND-MSG-DATA             PPGETMN
012300             MOVE 'RESP =                             '           PPGETMN
012400                                 TO UCWSABND-ABEND-DATA (1)       PPGETMN
012500             MOVE WS-RESP                                         PPGETMN
012600                                 TO UCWSABND-ABEND-DATA (2)       PPGETMN
012700        PERFORM 9999-ABEND                                        PPGETMN
012800     END-IF.                                                      PPGETMN
012900                                                                  PPGETMN
013000     EVALUATE TRUE                                                PPGETMN
013100       WHEN CPWSGETM-PPEDBDET-CALL                                PPGETMN
013200           MOVE WS-PTR-9 TO CPWSXCWA-PPEDBDET-PTR-9               PPGETMN
013300       WHEN CPWSGETM-CTO4XX-CALL                                  PPGETMN
013400           MOVE WS-PTR-9 TO CPWSXCWA-CTO4XX-PTR-9                 PPGETMN
013500       WHEN CPWSGETM-DET-OLD-FORMAT                               PPGETMN
013600           MOVE WS-PTR-9 TO CPWSXCWA-DET-PTR-9                    PPGETMN
013700       WHEN CPWSGETM-DOS-DB2-FORMAT                               36430815
013800           MOVE WS-PTR-9 TO CPWSXCWA-DB2-DOS-PTR-9                36430815
013900       WHEN CPWSGETM-PRM-DB2-FORMAT                               36430815
014000           MOVE WS-PTR-9 TO CPWSXCWA-DB2-PRM-PTR-9                36430815
014100       WHEN CPWSGETM-CST-DB2-FORMAT                               36430815
014200           MOVE WS-PTR-9 TO CPWSXCWA-DB2-CST-PTR-9                36430815
014300       WHEN CPWSGETM-GTN-DB2-FORMAT                               PPGETMN
014400           MOVE WS-PTR-9 TO CPWSXCWA-DB2-GTN-PTR-9                PPGETMN
014500       WHEN CPWSGETM-MST-08-MESSAGES                              PPGETMN
014600           MOVE WS-PTR-9 TO CPWSXCWA-MST08-PTR-9                  PPGETMN
014700       WHEN CPWSGETM-MST-12-MESSAGES                              PPGETMN
014800           MOVE WS-PTR-9 TO CPWSXCWA-MST12-PTR-9                  PPGETMN
014900       WHEN CPWSGETM-RTD-TABLE                                    PPGETMN
015000           MOVE WS-PTR-9 TO CPWSXCWA-RTD-PTR-9                    PPGETMN
015100       WHEN CPWSGETM-PGT-08-TABLE                                 PPGETMN
015200           MOVE WS-PTR-9 TO CPWSXCWA-PGT08-PTR-9                  PPGETMN
015300       WHEN CPWSGETM-PGT-12-TABLE                                 PPGETMN
015400           MOVE WS-PTR-9 TO CPWSXCWA-PGT12-PTR-9                  PPGETMN
015500       WHEN CPWSGETM-XDRT-TABLE                                   PPGETMN
015600           MOVE WS-PTR-9 TO CPWSXCWA-XDRT-PTR-9                   PPGETMN
015610       WHEN CPWSGETM-BUF-DB2-FORMAT                               48611390
015620           MOVE WS-PTR-9 TO CPWSXCWA-DB2-BUF-PTR-9                48611390
015700     END-EVALUATE.                                                PPGETMN
015800                                                                  PPGETMN
015900 1000-EXIT.                                                       PPGETMN
016000     EXIT.                                                        PPGETMN
016100                                                                  PPGETMN
016200     EJECT                                                        PPGETMN
016300                                                                  PPGETMN
016400 2000-FREEMAIN               SECTION.                             PPGETMN
016500                                                                  PPGETMN
016600                                                                  PPGETMN
016700     EVALUATE TRUE                                                PPGETMN
016800       WHEN CPWSGETM-PPEDBDET-CALL                                PPGETMN
016900         MOVE CPWSXCWA-PPEDBDET-PTR-9 TO WS-PTR-9                 PPGETMN
017000       WHEN CPWSGETM-CTO4XX-CALL                                  PPGETMN
017100         MOVE CPWSXCWA-CTO4XX-PTR-9 TO WS-PTR-9                   PPGETMN
017200       WHEN CPWSGETM-DET-OLD-FORMAT                               PPGETMN
017300         MOVE CPWSXCWA-DET-PTR-9 TO WS-PTR-9                      PPGETMN
017400       WHEN CPWSGETM-DOS-DB2-FORMAT                               36430815
017500         MOVE CPWSXCWA-DB2-DOS-PTR-9 TO WS-PTR-9                  36430815
017600       WHEN CPWSGETM-PRM-DB2-FORMAT                               36430815
017700         MOVE CPWSXCWA-DB2-PRM-PTR-9 TO WS-PTR-9                  36430815
017800       WHEN CPWSGETM-CST-DB2-FORMAT                               36430815
017900         MOVE CPWSXCWA-DB2-CST-PTR-9 TO WS-PTR-9                  36430815
018000       WHEN CPWSGETM-GTN-DB2-FORMAT                               PPGETMN
018100         MOVE CPWSXCWA-DB2-GTN-PTR-9 TO WS-PTR-9                  PPGETMN
018200       WHEN CPWSGETM-MST-08-MESSAGES                              PPGETMN
018300         MOVE CPWSXCWA-MST08-PTR-9 TO WS-PTR-9                    PPGETMN
018400       WHEN CPWSGETM-MST-12-MESSAGES                              PPGETMN
018500         MOVE CPWSXCWA-MST12-PTR-9 TO WS-PTR-9                    PPGETMN
018600       WHEN CPWSGETM-RTD-TABLE                                    PPGETMN
018700         MOVE CPWSXCWA-RTD-PTR-9 TO WS-PTR-9                      PPGETMN
018800       WHEN CPWSGETM-PGT-08-TABLE                                 PPGETMN
018900         MOVE CPWSXCWA-PGT08-PTR-9 TO WS-PTR-9                    PPGETMN
019000       WHEN CPWSGETM-PGT-12-TABLE                                 PPGETMN
019100         MOVE CPWSXCWA-PGT12-PTR-9 TO WS-PTR-9                    PPGETMN
019200       WHEN CPWSGETM-XDRT-TABLE                                   PPGETMN
019300         MOVE CPWSXCWA-XDRT-PTR-9 TO WS-PTR-9                     PPGETMN
019310       WHEN CPWSGETM-BUF-DB2-FORMAT                               48611390
019320         MOVE CPWSXCWA-DB2-BUF-PTR-9 TO WS-PTR-9                  48611390
019400     END-EVALUATE.                                                PPGETMN
019500                                                                  PPGETMN
019600     SET ADDRESS OF DUMMY-AREA TO WS-PTR.                         PPGETMN
019700                                                                  PPGETMN
019800     EXEC CICS                                                    PPGETMN
019900          FREEMAIN                                                PPGETMN
020000          DATA(DUMMY-AREA)                                        PPGETMN
020100          NOHANDLE                                                PPGETMN
020200          RESP   (WS-RESP)                                        PPGETMN
020300     END-EXEC.                                                    PPGETMN
020400                                                                  PPGETMN
020500     EVALUATE TRUE                                                PPGETMN
020600       WHEN CPWSGETM-PPEDBDET-CALL                                PPGETMN
020700         MOVE ZEROES TO CPWSXCWA-PPEDBDET-PTR-9                   PPGETMN
020800       WHEN CPWSGETM-CTO4XX-CALL                                  PPGETMN
020900         MOVE ZEROES TO CPWSXCWA-CTO4XX-PTR-9                     PPGETMN
021000       WHEN CPWSGETM-DET-OLD-FORMAT                               PPGETMN
021100         MOVE ZEROES TO CPWSXCWA-DET-PTR-9                        PPGETMN
021200       WHEN CPWSGETM-DOS-DB2-FORMAT                               36430815
021300         MOVE ZEROES TO CPWSXCWA-DB2-DOS-PTR-9                    36430815
021400       WHEN CPWSGETM-PRM-DB2-FORMAT                               36430815
021500         MOVE ZEROES TO CPWSXCWA-DB2-PRM-PTR-9                    36430815
021600       WHEN CPWSGETM-CST-DB2-FORMAT                               36430815
021700         MOVE ZEROES TO CPWSXCWA-DB2-CST-PTR-9                    36430815
021800       WHEN CPWSGETM-GTN-DB2-FORMAT                               PPGETMN
021900         MOVE ZEROES TO CPWSXCWA-DB2-GTN-PTR-9                    PPGETMN
022000       WHEN CPWSGETM-MST-08-MESSAGES                              PPGETMN
022100         MOVE ZEROES TO CPWSXCWA-MST08-PTR-9                      PPGETMN
022200       WHEN CPWSGETM-MST-12-MESSAGES                              PPGETMN
022300         MOVE ZEROES TO CPWSXCWA-MST12-PTR-9                      PPGETMN
022400       WHEN CPWSGETM-RTD-TABLE                                    PPGETMN
022500         MOVE ZEROES TO CPWSXCWA-RTD-PTR-9                        PPGETMN
022600       WHEN CPWSGETM-PGT-08-TABLE                                 PPGETMN
022700         MOVE ZEROES TO CPWSXCWA-PGT08-PTR-9                      PPGETMN
022800       WHEN CPWSGETM-PGT-12-TABLE                                 PPGETMN
022900         MOVE ZEROES TO CPWSXCWA-PGT12-PTR-9                      PPGETMN
023000       WHEN CPWSGETM-XDRT-TABLE                                   PPGETMN
023100         MOVE ZEROES TO CPWSXCWA-XDRT-PTR-9                       PPGETMN
023110       WHEN CPWSGETM-BUF-DB2-FORMAT                               48611390
023120         MOVE ZEROES TO CPWSXCWA-DB2-BUF-PTR-9                    48611390
023200     END-EVALUATE.                                                PPGETMN
023300                                                                  PPGETMN
023400 2000-EXIT.                                                       PPGETMN
023500     EXIT.                                                        PPGETMN
023600                                                                  PPGETMN
023700     EJECT                                                        PPGETMN
023800                                                                  PPGETMN
023900 3000-GET-CWA-ADDR           SECTION.                             PPGETMN
024000                                                                  PPGETMN
024100                                                                  PPGETMN
024200     EXEC CICS                                                    PPGETMN
024300          ASSIGN                                                  PPGETMN
024400          CWALENG(WS-CWA-LNG)                                     PPGETMN
024500          NOHANDLE                                                PPGETMN
024600          RESP   (WS-RESP)                                        PPGETMN
024700     END-EXEC.                                                    PPGETMN
024800                                                                  PPGETMN
024900     IF WS-RESP NOT = DFHRESP(NORMAL)                             PPGETMN
025000             MOVE 'EXEC CICS ASSIGN FAILED         '              PPGETMN
025100                                 TO UCWSABND-MSG-DATA             PPGETMN
025200             MOVE 'RESP =                             '           PPGETMN
025300                                 TO UCWSABND-ABEND-DATA (1)       PPGETMN
025400             MOVE WS-RESP                                         PPGETMN
025500                                 TO UCWSABND-ABEND-DATA (2)       PPGETMN
025600        PERFORM 9999-ABEND                                        PPGETMN
025700     END-IF.                                                      PPGETMN
025800                                                                  PPGETMN
025900     IF WS-CWA-LNG < LENGTH OF CPWSXCWA                           PPGETMN
026000             MOVE 'CWA LENGTH TOO SHORT            '              PPGETMN
026100                                 TO UCWSABND-MSG-DATA             PPGETMN
026200             MOVE 'LENGTH =                           '           PPGETMN
026300                                 TO UCWSABND-ABEND-DATA (1)       PPGETMN
026400             MOVE WS-CWA-LNG                                      PPGETMN
026500                                 TO UCWSABND-ABEND-DATA (2)       PPGETMN
026600             PERFORM 9999-ABEND                                   PPGETMN
026700     END-IF.                                                      PPGETMN
026800                                                                  PPGETMN
026900     EXEC CICS                                                    PPGETMN
027000          ADDRESS                                                 PPGETMN
027100          CWA(CPWSGETM-CWA-PTR)                                   PPGETMN
027200          NOHANDLE                                                PPGETMN
027300          RESP   (WS-RESP)                                        PPGETMN
027400     END-EXEC.                                                    PPGETMN
027500                                                                  PPGETMN
027600     IF WS-RESP NOT = DFHRESP(NORMAL)                             PPGETMN
027700             MOVE 'EXEC CICS ADDRESS FAILED        '              PPGETMN
027800                                 TO UCWSABND-MSG-DATA             PPGETMN
027900             MOVE 'RESP =                             '           PPGETMN
028000                                 TO UCWSABND-ABEND-DATA (1)       PPGETMN
028100             MOVE WS-RESP                                         PPGETMN
028200                                 TO UCWSABND-ABEND-DATA (2)       PPGETMN
028300        PERFORM 9999-ABEND                                        PPGETMN
028400     END-IF.                                                      PPGETMN
028500     SET ADDRESS OF CPWSXCWA TO CPWSGETM-CWA-PTR.                 PPGETMN
028600                                                                  PPGETMN
028700 3000-EXIT.                                                       PPGETMN
028800     EXIT.                                                        PPGETMN
028900                                                                  PPGETMN
029000     EJECT.                                                       PPGETMN
029100                                                                  PPGETMN
029200******************************************************************PPGETMN
029300*********                                                  *******PPGETMN
029400 9999-ABEND    SECTION.                                           PPGETMN
029500                                                                  PPGETMN
029600     MOVE 'PPGETMN' TO UCWSABND-ABENDING-PGM.                     PPGETMN
029700                                                                  PPGETMN
029800     COPY UCPDABND.                                               PPGETMN
