000010**************************************************************/   EFIX1078
000020*  PROGRAM: PPAPORNF                                         */   EFIX1078
000030*  RELEASE: ___1078______ SERVICE REQUEST(S): _____EFIX____  */   EFIX1078
000040*  NAME:_______SRS_______ MODIFICATION DATE:  __07/17/96____ */   EFIX1078
000041*  DESCRIPTION: ERROR REPORT 1415:                           */   EFIX1078
000050*  - REPLACE ID-LENGTH AND LOCATION DB2 WITH CALL TO PPINITWK*/   EFIX1078
000051*    COMMENT OUT UNNECESSARY CODE                            */   EFIX1078
000060**************************************************************/   EFIX1078
000010**************************************************************/   36430943
000020*  PROGRAM: PPAPORNF                                         */   36430943
000030*  RELEASE: ___0943______ SERVICE REQUEST(S): _____3643____  */   36430943
000040*  NAME:_______SRS_______ MODIFICATION DATE:  __11/10/94____ */   36430943
000050*  INCLUSION OF KEY SWITCHING LOGIC                          */   36430943
000060**************************************************************/   36430943
000100**************************************************************/   36430911
000200*  PROGRAM: PPAPORNF                                         */   36430911
000300*  RELEASE # ___0911___   SERVICE REQUEST NO(S)____3643______*/   36430911
000400*  NAME _____SRS_______   MODIFICATION DATE ____06/30/94_____*/   36430911
000500*  DESCRIPTION                                               */   36430911
000600*    EDB ENTRY/UPDATE NEW FUNCTION SETUP APPLICATION         */   36430911
000700*    PROCESSOR.                                              */   36430911
000800*                                                            */   36430911
000900**************************************************************/   36430911
001000 IDENTIFICATION DIVISION.                                         PPAPORNF
001100 PROGRAM-ID. PPAPORNF.                                            PPAPORNF
001200 ENVIRONMENT DIVISION.                                            PPAPORNF
001300 CONFIGURATION SECTION.                                           PPAPORNF
001400 SOURCE-COMPUTER.                                                 PPAPORNF
001500 COPY CPOTXUCS.                                                   PPAPORNF
001600 DATA DIVISION.                                                   PPAPORNF
001700 WORKING-STORAGE SECTION.                                         PPAPORNF
001800 01  WS-COMN.                                                     PPAPORNF
002110     05  WS-PPINITWK                PIC X(08)  VALUE 'PPINITWK'.  EFIX1078
002200***05  WS-START                            PIC X(27) VALUE        EFIX1078
002300*****  'WORKING STORAGE STARTS HERE'.                             EFIX1078
002400***05  WS-RESP                             PIC S9(8) COMP.        EFIX1078
002500*01  WS-MESS-NOS.                                                 EFIX1078
002600*****05  MP0996                            PIC X(5) VALUE 'P0996'.EFIX1078
002400 01  PPDB2MSG-INTERFACE.                                          PPAPORNF
002500     COPY CPLNKDB2.                                               PPAPORNF
002600 01  UCWSABND    EXTERNAL.                                        PPAPORNF
002700     COPY UCWSABND.                                               PPAPORNF
002800 01  CPWSSPEC    EXTERNAL.                                        PPAPORNF
002900     COPY CPWSSPEC.                                               PPAPORNF
002910 01  CPWSKEYS    EXTERNAL.                                        36430943
002920     COPY CPWSKEYS.                                               36430943
003410 01  CPWSWORK    EXTERNAL.                                        EFIX1078
003420     COPY CPWSWORK.                                               EFIX1078
003000******************************************************************PPAPORNF
003100*          SQL - WORKING STORAGE                                 *PPAPORNF
003200******************************************************************PPAPORNF
003300 01  UCCOMMON   EXTERNAL.                                         PPAPORNF
003400     EXEC SQL                                                     PPAPORNF
003500      INCLUDE UCCOMMON                                            PPAPORNF
003600     END-EXEC.                                                    PPAPORNF
004200*01  PRM-ROW.                                                     EFIX1078
004300*****EXEC SQL                                                     EFIX1078
004400*****     INCLUDE PPPVZPRM                                        EFIX1078
004500*****END-EXEC.                                                    EFIX1078
004600*01  CCR-ROW.                                                     EFIX1078
004700*****EXEC SQL                                                     EFIX1078
004800*****     INCLUDE PPPVZCCR                                        EFIX1078
004900*****END-EXEC.                                                    EFIX1078
005000*****EXEC SQL                                                     EFIX1078
005100*****     INCLUDE SQLCA                                           EFIX1078
005200*****END-EXEC.                                                    EFIX1078
004800                                                                  PPAPORNF
004900 PROCEDURE DIVISION.                                              PPAPORNF
005000                                                                  PPAPORNF
005100 0000-MAIN-LINE   SECTION.                                        PPAPORNF
005200                                                                  PPAPORNF
005300     EXEC SQL                                                     PPAPORNF
005400          INCLUDE CPPDXE99                                        PPAPORNF
005500     END-EXEC.                                                    PPAPORNF
005600     MOVE 'PPAPORNF' TO DB2MSG-PGM-ID.                            PPAPORNF
005700     MOVE ZERO TO UCCOMMON-RETURN-CODE.                           PPAPORNF
005800     IF NOT SPEC-INITIALIZED                                      PPAPORNF
005900        PERFORM 1000-INIT-CPWSSPEC                                PPAPORNF
006000     END-IF.                                                      PPAPORNF
006503     IF    (CPWSKEYS-EMPLOYEE-ID = SPACES OR                      EFIX1078
006504            CPWSKEYS-EMPLOYEE-ID = LOW-VALUES)                    EFIX1078
006505        OR (CPWSKEYS-EMPLOYEE-ID NOT = SPEC-ENTERED-ID)           EFIX1078
006510        SET UCCOMMON-SWITCH-KEYS TO TRUE                          EFIX1078
006520     END-IF.                                                      EFIX1078
006100     GOBACK.                                                      PPAPORNF
006200                                                                  PPAPORNF
006300 0000-EXIT.                                                       PPAPORNF
006400     EXIT.                                                        PPAPORNF
006500                                                                  PPAPORNF
006600 1000-INIT-CPWSSPEC   SECTION.                                    PPAPORNF
006700                                                                  PPAPORNF
006800     INITIALIZE CPWSSPEC.                                         PPAPORNF
006900     SET SPEC-INITIALIZED TO TRUE.                                PPAPORNF
007000     MOVE 1 TO SPEC-CURRENT-PAGE                                  PPAPORNF
007100               SPEC-TOTAL-PAGES.                                  PPAPORNF
007103     IF   (CPWSKEYS-EMPLOYEE-ID NOT = SPACES)                     36430943
007104      AND (CPWSKEYS-EMPLOYEE-ID NOT = LOW-VALUES)                 36430943
007120        MOVE CPWSKEYS-EMPLOYEE-ID TO SPEC-ENTERED-ID              36430943
007140     END-IF.                                                      36430943
008100*****MOVE 57 TO PRM-NUMBER.                                       EFIX1078
008200*****PERFORM 9010-SELECT-PRM.                                     EFIX1078
008300*****MOVE PRM-DATA TO SPEC-ID-LENGTH.                             EFIX1078
008400*****PERFORM 9000-SELECT-CCR.                                     EFIX1078
008500*****                                                             EFIX1078
008600*****                                                             EFIX1078
008700*1100-EXIT.                                                       EFIX1078
008800*****EXIT.                                                        EFIX1078
008900*****                                                             EFIX1078
009000*9000-SELECT-CCR   SECTION.                                       EFIX1078
009100**************************************************************    EFIX1078
009200*  SELECT A ROW FROM CCR (CAMPUS CONTROL RECORD) TABLE       *    EFIX1078
009300**************************************************************    EFIX1078
009400*****                                                             EFIX1078
009500*****MOVE 'SELECT CCR' TO DB2MSG-TAG.                             EFIX1078
009600*****                                                             EFIX1078
009700*****EXEC SQL                                                     EFIX1078
009800*****     SELECT CCR_LOCATION                                     EFIX1078
009900*****     INTO  :CCR-LOCATION                                     EFIX1078
010000*****     FROM PPPVZCCR_CCR                                       EFIX1078
010100*****END-EXEC.                                                    EFIX1078
010200*****                                                             EFIX1078
010300*****IF SQLCODE = ZERO                                            EFIX1078
010400*****   MOVE CCR-LOCATION TO SPEC-LOCATION                        EFIX1078
010500*****ELSE                                                         EFIX1078
010600*****   PERFORM 999999-SQL-ERROR                                  EFIX1078
010700*****END-IF.                                                      EFIX1078
010800*****                                                             EFIX1078
010900*9000-EXIT.                                                       EFIX1078
011000*****EXIT.                                                        EFIX1078
011100*****                                                             EFIX1078
011200*9010-SELECT-PRM  SECTION.                                        EFIX1078
011300**************************************************************    EFIX1078
011400*  SELECT A ROW FROM PRM (PARAMETERS) TABLE                  *    EFIX1078
011500**************************************************************    EFIX1078
011600*****                                                             EFIX1078
011700*****MOVE 'SELECT PRM' TO DB2MSG-TAG.                             EFIX1078
011800*****EXEC SQL                                                     EFIX1078
011900*****     SELECT PRM_DATA                                         EFIX1078
012000*****     INTO  :PRM-DATA                                         EFIX1078
012100*****     FROM   PPPVZPRM_PRM                                     EFIX1078
012200*****     WHERE  PRM_NUMBER = :PRM-NUMBER                         EFIX1078
012300*****END-EXEC.                                                    EFIX1078
012400*****IF SQLCODE NOT = ZERO                                        EFIX1078
012500*****   PERFORM 999999-SQL-ERROR                                  EFIX1078
012600*****END-IF.                                                      EFIX1078
012700*****                                                             EFIX1078
012800*9010-EXIT.                                                       EFIX1078
012900*****EXIT.                                                        EFIX1078
012910                                                                  EFIX1078
012920     IF NOT CPWSWORK-INITIALIZED                                  EFIX1078
012930        CALL WS-PPINITWK USING DFHEIBLK                           EFIX1078
012940          ON EXCEPTION                                            EFIX1078
012950             STRING 'ERROR CALLING PROGRAM: ' DELIMITED BY SIZE   EFIX1078
012960                    WS-PPINITWK            DELIMITED BY SIZE      EFIX1078
012970                         INTO UCWSABND-ABEND-DATA (1)             EFIX1078
012980             END-STRING                                           EFIX1078
012990             PERFORM 9999-ABEND                                   EFIX1078
012991        END-CALL                                                  EFIX1078
012992     END-IF.                                                      EFIX1078
012993                                                                  EFIX1078
012996     MOVE CPWSWORK-ID-LENGTH TO SPEC-ID-LENGTH.                   EFIX1078
012997     MOVE CPWSWORK-LOCATION TO SPEC-LOCATION.                     EFIX1078
012998     MOVE CPWSWORK-CAMPUS-NAME TO SPEC-CAMPUS-NAME.               EFIX1078
007600                                                                  PPAPORNF
012200 9999-ABEND    SECTION.                                           PPAPORNF
012300**************************************************************    PPAPORNF
012400*  COPIED PROCEDURE DIVISION CODE                            *    PPAPORNF
012500**************************************************************    PPAPORNF
012600                                                                  PPAPORNF
012700     MOVE 'PPAPORNF' TO UCWSABND-ABENDING-PGM.                    PPAPORNF
012800     COPY UCPDABND.                                               PPAPORNF
012900                                                                  PPAPORNF
013000 9999-EXIT.                                                       PPAPORNF
013100     EXIT.                                                        PPAPORNF
013200                                                                  PPAPORNF
014200*999999-SQL-ERROR SECTION.                                        EFIX1078
014300**************************************************************    EFIX1078
014400*  COPIED PROCEDURE DIVISION CODE FOR SQL ERROR HANDLING     *    EFIX1078
014500**************************************************************    EFIX1078
014600*****                                                             EFIX1078
014700***** EXEC SQL                                                    EFIX1078
014800*****      INCLUDE CPPDXP99                                       EFIX1078
014900***** END-EXEC.                                                   EFIX1078
015000***** MOVE 'SQL ERROR - SEE PRECEDING DB2 MESSAGES'               EFIX1078
015100*****                                 TO UCWSABND-ABEND-DATA (1). EFIX1078
015200***** PERFORM 9999-ABEND.                                         EFIX1078
015300*****                                                             EFIX1078
015400*999999-EXIT.                                                     EFIX1078
015500*****  EXIT.                                                      EFIX1078
014700***************    END OF SOURCE - PPAPORNF    *******************PPAPORNF
