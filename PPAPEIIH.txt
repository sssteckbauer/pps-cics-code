000100**************************************************************/   36430911
000200*  PROGRAM: PPAPEIIH                                         */   36430911
000300*  RELEASE # ___0911___   SERVICE REQUEST NO(S)____3643______*/   36430911
000400*  NAME _____MLG_______   MODIFICATION DATE ____06/30/94_____*/   36430911
000500*  DESCRIPTION                                               */   36430911
000600*     THIS IS A COMPLETE REPLACEMENT OF PPAPEIIH             */   36430911
000700*               AS RELEASED IN 795                           */   36430911
000800*                                                            */   36430911
000900**************************************************************/   36430911
001000 IDENTIFICATION DIVISION.                                         PPAPEIIH
001100 PROGRAM-ID. PPAPEIIH.                                            PPAPEIIH
001200 ENVIRONMENT DIVISION.                                            PPAPEIIH
001210 CONFIGURATION SECTION.                                           PPAPEIIH
001220 SOURCE-COMPUTER.                                                 PPAPEIIH
001230 COPY  CPOTXUCS.                                                  PPAPEIIH
001300 DATA DIVISION.                                                   PPAPEIIH
001400 WORKING-STORAGE SECTION.                                         PPAPEIIH
001700                                                                  PPAPEIIH
001800 01  WS-COMN.                                                     PPAPEIIH
001903     05  WS-START                          PIC X(27) VALUE        PPAPEIIH
002100         'WORKING STORAGE STARTS HERE'.                           PPAPEIIH
002200     05  WS-KEY-COUNT                    PIC 9(02) VALUE ZEROS.   PPAPEIIH
002000                                                                  PPAPEIIH
002100 01  WS-MESS-NOS.                                                 PPAPEIIH
002200     05 MP0006                           PIC X(5)  VALUE 'P0006'. PPAPEIIH
002600     05 MU0030                           PIC X(5)  VALUE 'U0030'. PPAPEIIH
002300                                                                  PPAPEIIH
002800                                                                  PPAPEIIH
010900 01  UCFTRARE  EXTERNAL.                                          PPAPEIIH
010910     COPY UCFTRARE.                                               PPAPEIIH
003100 01  WS-PPEIFT0 REDEFINES UCFTRARE.                               PPAPEIIH
003200     COPY PPEIFT0.                                                PPAPEIIH
010913                                                                  PPAPEIIH
003400 01  UCCOMMON               EXTERNAL.                             PPAPEIIH
011200     COPY UCCOMMON.                                               PPAPEIIH
011300                                                                  PPAPEIIH
003700 01  UCPIROIH               EXTERNAL.                             PPAPEIIH
011410     COPY UCPIROIH.                                               PPAPEIIH
011420                                                                  PPAPEIIH
004000 01  CPWSWORK               EXTERNAL.                             PPAPEIIH
004100     COPY CPWSWORK.                                               PPAPEIIH
004200                                                                  PPAPEIIH
004300 01  CPWSFOOT               EXTERNAL.                             PPAPEIIH
011422     COPY CPWSFOOT.                                               PPAPEIIH
011423                                                                  PPAPEIIH
004600 01  UCWSABND               EXTERNAL.                             PPAPEIIH
011440     COPY UCWSABND.                                               PPAPEIIH
011450                                                                  PPAPEIIH
004900     COPY DFHAID.                                                 PPAPEIIH
012200                                                                  PPAPEIIH
012300                                                                  PPAPEIIH
012900 LINKAGE SECTION.                                                 PPAPEIIH
013000                                                                  PPAPEIIH
013100     EJECT                                                        PPAPEIIH
013300                                                                  PPAPEIIH
013400 PROCEDURE DIVISION.                                              PPAPEIIH
013600                                                                  PPAPEIIH
005800                                                                  PPAPEIIH
005900 0000-MAINLINE                        SECTION.                    PPAPEIIH
013710**************************************************************    PPAPEIIH
006100*  MAIN LINE PROCESSING                                      *    PPAPEIIH
013750**************************************************************    PPAPEIIH
013800                                                                  PPAPEIIH
015100     SET UCCOMMON-RETURN-NO-ERRORS TO TRUE.                       PPAPEIIH
006500     MOVE COMMANDI TO UCCOMMON-COMMAND-DATA.                      PPAPEIIH
006600     MOVE NEXT-FUNCI TO CPWSFOOT-ENTERED-FUNC.                    PPAPEIIH
006700     SET UCPIROIH-KEY-NOT-ENTERED TO TRUE.                        PPAPEIIH
017100                                                                  PPAPEIIH
006900     IF UCCOMMON-UPD-IN-PROGRESS                                  PPAPEIIH
007000      AND UCCOMMON-FUNC-TYPE-INQUIRY                              PPAPEIIH
007100        MOVE MU0030 TO UCCOMMON-HOT-MSG-NUMBER                    PPAPEIIH
007200     END-IF.                                                      PPAPEIIH
007300                                                                  PPAPEIIH
007400     IF EIBAID = DFHENTER                                         PPAPEIIH
007500        PERFORM 1000-CHECK-KEY-COUNT                              PPAPEIIH
007600        IF UCCOMMON-RETURN-NO-ERRORS                              PPAPEIIH
007700           PERFORM 2000-CHECK-DATA-ENTERED                        PPAPEIIH
007800        END-IF                                                    PPAPEIIH
007900     END-IF.                                                      PPAPEIIH
008000                                                                  PPAPEIIH
034530     GOBACK.                                                      PPAPEIIH
034550                                                                  PPAPEIIH
034551 0000-EXIT.                                                       PPAPEIIH
034552     EXIT.                                                        PPAPEIIH
034553                                                                  PPAPEIIH
034554     EJECT                                                        PPAPEIIH
034555                                                                  PPAPEIIH
034556 1000-CHECK-KEY-COUNT                 SECTION.                    PPAPEIIH
034557**************************************************************    PPAPEIIH
009000*  COUNT THE KEY FIELDS ENTERED                              *    PPAPEIIH
034561**************************************************************    PPAPEIIH
034562                                                                  PPAPEIIH
034563     MOVE ZERO TO WS-KEY-COUNT.                                   PPAPEIIH
034564                                                                  PPAPEIIH
009500     IF FTR-IDL > ZERO AND                                        PPAPEIIH
009600        FTR-IDI NOT = SPACES                                      PPAPEIIH
034567        ADD 1 TO WS-KEY-COUNT                                     PPAPEIIH
034568     END-IF.                                                      PPAPEIIH
034569                                                                  PPAPEIIH
010000     IF FTR-NAMEL > ZERO AND                                      PPAPEIIH
010100        FTR-NAMEI NOT = SPACES                                    PPAPEIIH
034572        ADD 1 TO WS-KEY-COUNT                                     PPAPEIIH
034573     END-IF.                                                      PPAPEIIH
034574                                                                  PPAPEIIH
010500     IF FTR-SSNL > ZERO AND                                       PPAPEIIH
010600        FTR-SSNI NOT = SPACES                                     PPAPEIIH
034577        ADD 1 TO WS-KEY-COUNT                                     PPAPEIIH
034578     END-IF.                                                      PPAPEIIH
034579                                                                  PPAPEIIH
034580     IF WS-KEY-COUNT > 1                                          PPAPEIIH
034581        SET UCCOMMON-RETURN-ERRORS-FOUND TO TRUE                  PPAPEIIH
034582        MOVE MP0006 TO  UCCOMMON-MSG-NUMBER                       PPAPEIIH
011300        SET UCCOMMON-REDISPLAY-FUNC TO TRUE                       PPAPEIIH
011400        SET CPWSWORK-REDISPLAY-EEID TO TRUE                       PPAPEIIH
011500        SET CPWSWORK-REDISPLAY-NAME TO TRUE                       PPAPEIIH
011600        SET CPWSWORK-REDISPLAY-SSN TO TRUE                        PPAPEIIH
034583     END-IF.                                                      PPAPEIIH
034584                                                                  PPAPEIIH
034585 1000-EXIT.                                                       PPAPEIIH
034586     EXIT.                                                        PPAPEIIH
034587                                                                  PPAPEIIH
034588     EJECT                                                        PPAPEIIH
034589                                                                  PPAPEIIH
034590 2000-CHECK-DATA-ENTERED              SECTION.                    PPAPEIIH
034591**************************************************************    PPAPEIIH
012600*  MOVE KEY DATA TO CPWSFOOT                                 *    PPAPEIIH
034595**************************************************************    PPAPEIIH
034596                                                                  PPAPEIIH
012900     IF (FTR-IDI NOT = SPACES)                                    PPAPEIIH
013000       AND (FTR-IDI NOT = LOW-VALUES)                             PPAPEIIH
013100         MOVE FTR-IDI TO CPWSFOOT-ENTERED-ID                      PPAPEIIH
034602         SET UCPIROIH-KEY-ENTERED TO TRUE                         PPAPEIIH
013300         MOVE UCCOMMON-ATR-ENTRY TO FTR-IDA                       PPAPEIIH
034604     END-IF.                                                      PPAPEIIH
034605                                                                  PPAPEIIH
013600     IF (FTR-NAMEI NOT = SPACES)                                  PPAPEIIH
013700       AND (FTR-NAMEI NOT = LOW-VALUES)                           PPAPEIIH
013800         MOVE FTR-NAMEI TO CPWSFOOT-ENTERED-NAME                  PPAPEIIH
034609         SET UCPIROIH-KEY-ENTERED TO TRUE                         PPAPEIIH
014000         MOVE UCCOMMON-ATR-ENTRY TO FTR-NAMEA                     PPAPEIIH
034611     END-IF.                                                      PPAPEIIH
034612                                                                  PPAPEIIH
014300     IF (FTR-SSNI NOT = SPACES)                                   PPAPEIIH
014400       AND (FTR-SSNI NOT = LOW-VALUES)                            PPAPEIIH
014500         MOVE FTR-SSNI TO CPWSFOOT-ENTERED-SSN                    PPAPEIIH
034616         SET UCPIROIH-KEY-ENTERED TO TRUE                         PPAPEIIH
014700         MOVE UCCOMMON-ATR-ENTRY TO FTR-SSNA                      PPAPEIIH
034618     END-IF.                                                      PPAPEIIH
034619                                                                  PPAPEIIH
034620 2000-EXIT.                                                       PPAPEIIH
034621     EXIT.                                                        PPAPEIIH
034622                                                                  PPAPEIIH
034623     EJECT                                                        PPAPEIIH
015400                                                                  PPAPEIIH
034624 9999-ABEND    SECTION.                                           PPAPEIIH
034625**************************************************************    PPAPEIIH
034626*  COPIED PROCEDURE DIVISION CODE FOR ABEND HANDLING         *    PPAPEIIH
034627**************************************************************    PPAPEIIH
034629     MOVE 'PPAPEIIH' TO UCWSABND-ABENDING-PGM.                    PPAPEIIH
034630                                                                  PPAPEIIH
034631     COPY UCPDABND.                                               PPAPEIIH
034640                                                                  PPAPEIIH
034700 9999-EXIT.                                                       PPAPEIIH
034800***************    END OF SOURCE - PPAPEIIH    *******************PPAPEIIH
