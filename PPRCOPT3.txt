000010**************************************************************/   32021138
000020*  PROGRAM: PPRCOPT3                                         */   32021138
000030*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000040*  NAME:______PAYJIG_____ MODIFICATION DATE:  ___08/04/97__  */   32021138
000050*  DESCRIPTION:                                              */   32021138
000060*  - MODIFIED FOR FULL ACCOUNTING UNIT                       */   32021138
000070**************************************************************/   32021138
000100**************************************************************/   36430911
000200*  PROGRAM: PPRCOPT3                                         */   36430911
000300*  RELEASE # ___0911___   SERVICE REQUEST NO(S)____3643______*/   36430911
000400*  NAME ____SRS________   CREATION DATE:________06/30/94_____*/   36430911
000500*  DESCRIPTION                                               */   36430911
000600*    THIS IS A REPLACEMENT MODULE FOR OLRC OPTION 3          */   36430911
000700**************************************************************/   36430911
000800 IDENTIFICATION DIVISION.                                         PPRCOPT3
000900 PROGRAM-ID. PPRCOPT3.                                            PPRCOPT3
001000 ENVIRONMENT DIVISION.                                            PPRCOPT3
001100 DATA DIVISION.                                                   PPRCOPT3
001200 WORKING-STORAGE SECTION.                                         PPRCOPT3
001300 01  WS-MISC.                                                     PPRCOPT3
001410     05  WS-PPRCAUDT               PIC X(08)   VALUE 'PPRCAUDT'.  PPRCOPT3
001420     05  WS-PPRCADVC               PIC X(08)   VALUE 'PPRCADVC'.  PPRCOPT3
001430     05  WS-PPRCVEND               PIC X(08)   VALUE 'PPRCVEND'.  PPRCOPT3
001440     05  WS-HANDDRAWN-CNT          PIC 999     VALUE ZERO.        PPRCOPT3
001500     05  IX                        PIC S9(4) COMP.                PPRCOPT3
001600     05  IX2                       PIC S9(4) COMP.                PPRCOPT3
001700     05  IX3                       PIC S9(4) COMP.                PPRCOPT3
001800     05  WS-ELEM-NO                PIC 9(4).                      PPRCOPT3
002100     05  NEW-NEXT-CHECK-NUMBER          PIC X(6).                 PPRCOPT3
002200     05  NEW-NEXT-CHECK-NUM9 REDEFINES                            PPRCOPT3
002300            NEW-NEXT-CHECK-NUMBER       PIC 9(6).                 PPRCOPT3
004600                                                                  PPRCOPT3
004700 01  EXPENSE-DISTRIBUTION-IC5.      COPY CPWSXIC5.                PPRCOPT3
004710 01  XC3T-RECORD-IMAGE.             COPY CPWSXC3T.                PPRCOPT3
004800 01  RC0T-TRANSACTION-CONTROL.      COPY CPWSRC0T.                PPRCOPT3
002900 01  PPDB2MSG-INTERFACE.            COPY CPLNKDB2.                PPRCOPT3
004900 01  RCPW-INTERFACE-DATA EXTERNAL.  COPY CPWSRCPW.                PPRCOPT3
003100 01  CPWSRCTS            EXTERNAL.  COPY CPWSRCTS.                PPRCOPT3
003200 01  UCCOMMON            EXTERNAL.  COPY UCCOMMON.                PPRCOPT3
003300 01  CPWSSPEC            EXTERNAL.  COPY CPWSSPEC.                PPRCOPT3
003400 01  UCWSABND            EXTERNAL.                                PPRCOPT3
003500     EXEC SQL                                                     PPRCOPT3
003600          INCLUDE UCWSABND                                        PPRCOPT3
003700     END-EXEC.                                                    PPRCOPT3
006300 01  WS-ABC.                                                      PPRCOPT3
006400     EXEC SQL                                                     PPRCOPT3
006500          INCLUDE PPPVZABC                                        PPRCOPT3
006600     END-EXEC.                                                    PPRCOPT3
006700 01  WS-ABD.                                                      PPRCOPT3
006800     EXEC SQL                                                     PPRCOPT3
006900          INCLUDE PPPVZABD                                        PPRCOPT3
007000     END-EXEC.                                                    PPRCOPT3
007200     EXEC SQL                                                     PPRCOPT3
007600          INCLUDE SQLCA                                           PPRCOPT3
007700     END-EXEC.                                                    PPRCOPT3
007800                                                                  PPRCOPT3
005000 PROCEDURE DIVISION.                                              PPRCOPT3
007900                                                                  PPRCOPT3
008400     EXEC SQL                                                     PPRCOPT3
005300          INCLUDE CPPDXE99                                        PPRCOPT3
008600     END-EXEC.                                                    PPRCOPT3
008700                                                                  PPRCOPT3
005600     PERFORM 3000-UPDATE-ABEYANCE THRU 3999-UPDATE-EXIT           PPRCOPT3
005700     IF SPEC-MSSG-NO (1) NOT = SPACES                             PPRCOPT3
010000        GO TO 0100-END                                            PPRCOPT3
010100     END-IF.                                                      PPRCOPT3
006000     CALL WS-PPRCAUDT USING DFHEIBLK.                             PPRCOPT3
006100     IF SPEC-MSSG-NO (1) NOT = SPACES                             PPRCOPT3
010420        GO TO 0100-END                                            PPRCOPT3
010500     END-IF.                                                      PPRCOPT3
006400     CALL WS-PPRCADVC USING DFHEIBLK.                             PPRCOPT3
006500     IF SPEC-MSSG-NO (1) NOT = SPACES                             PPRCOPT3
010520        GO TO 0100-END                                            PPRCOPT3
010530     END-IF.                                                      PPRCOPT3
006800     CALL WS-PPRCVEND USING DFHEIBLK.                             PPRCOPT3
010600                                                                  PPRCOPT3
010730 0100-END.                                                        PPRCOPT3
010800                                                                  PPRCOPT3
010900     GOBACK.                                                      PPRCOPT3
011000                                                                  PPRCOPT3
007400 3000-UPDATE-ABEYANCE.                                            PPRCOPT3
007500                                                                  PPRCOPT3
007600     PERFORM 9850-SELECT-ABEY-CHECK-NUMBER.                       PPRCOPT3
024500     INITIALIZE RCPW-INTERFACE-DATA.                              PPRCOPT3
024600     MOVE 'RC30' TO RCPW-TRANS-ID.                                PPRCOPT3
024700     MOVE RCTS-ID TO RCPW-ID-NO.                                  PPRCOPT3
024800     MOVE RCTS-NAME TO RCPW-NAME-26.                              PPRCOPT3
024900     MOVE RCTS-SSN TO RCPW-SS-NUMBER.                             PPRCOPT3
008200*****MOVE RCTS-LIAB-ACCT TO RCPW-LIABILITY-ACCT                   32021138
008210     MOVE RCTS-LIAB-FAU TO RCPW-LIABILITY-FAU                     32021138
025100     MOVE RCTS-CHECK-AMT-N TO RCPW-NET-PAY.                       PPRCOPT3
008400     MOVE UCCOMMON-DATE-X-YY TO RCPW-CHECK-YR                     PPRCOPT3
008500     MOVE UCCOMMON-DATE-X-MM TO RCPW-CHECK-MO                     PPRCOPT3
008600     MOVE UCCOMMON-DATE-X-DD TO RCPW-CHECK-DA                     PPRCOPT3
025500     MOVE RCTS-VENDOR-DATA-1 TO RCPW-VENDOR-DATA(1:45)            PPRCOPT3
025600     MOVE RCTS-VENDOR-DATA-2 TO RCPW-VENDOR-DATA(46:45)           PPRCOPT3
025700     MOVE RCTS-ADVICE TO RCPW-PAY-ADVICE-MSSG.                    PPRCOPT3
025800     PERFORM VARYING IX FROM 1 BY 1                               PPRCOPT3
025900       UNTIL IX > 8                                               PPRCOPT3
026000          OR RCTS-DS-GTN (IX) = SPACES                            PPRCOPT3
026100      ADD 1 TO RCPW-DEDX                                          PPRCOPT3
026200      MOVE RCTS-DS-GTN (IX) TO RCPW-DED-GTN (RCPW-DEDX)           PPRCOPT3
026300      MOVE RCTS-DS-AMT-N (IX) TO RCPW-CUR-AMT (RCPW-DEDX)         PPRCOPT3
026400      IF RCTS-DS-SIGN (IX) = 'R'                                  PPRCOPT3
026500         MOVE '3' TO RCPW-SPEC-TRANS (RCPW-DEDX)                  PPRCOPT3
026600      ELSE                                                        PPRCOPT3
026700         MOVE '1' TO RCPW-SPEC-TRANS (RCPW-DEDX)                  PPRCOPT3
026800      END-IF                                                      PPRCOPT3
026900     END-PERFORM.                                                 PPRCOPT3
027100     IF RCPW-DEDX NOT > ZERO                                      PPRCOPT3
027200        GO TO 3999-UPDATE-EXIT                                    PPRCOPT3
027300     END-IF.                                                      PPRCOPT3
027500     ADD 1 TO NEW-NEXT-CHECK-NUM9.                                PPRCOPT3
028600     MOVE SPACES TO XC3T-RECORD-IMAGE.                            PPRCOPT3
028700     MOVE ZERO TO XC3T-XFOOT9.                                    PPRCOPT3
028800     MOVE RCPW-ID-NO TO XC3T-ID.                                  PPRCOPT3
028900     MOVE 'DS' TO XC3T-TRAN-CD.                                   PPRCOPT3
029000     MOVE RCPW-CHECK-DATE TO XC3T-TRAN-DATE(2:6)                  PPRCOPT3
029100     MOVE ZERO TO IX2                                             PPRCOPT3
029200                  IX3.                                            PPRCOPT3
029300     PERFORM VARYING IX FROM 1 BY 1                               PPRCOPT3
029400       UNTIL IX > RCPW-DEDX                                       PPRCOPT3
029500      ADD 1 TO IX2                                                PPRCOPT3
029600      MOVE RCPW-CUR-DED-CODE (IX) TO XC3T-ELEM-NO-3 (IX2)         PPRCOPT3
029700      MOVE RCPW-CUR-AMT (IX) TO XC3T-AMT9 (IX2)                   PPRCOPT3
029800      ADD RCPW-CUR-AMT (IX) TO XC3T-XFOOT9                        PPRCOPT3
029900      IF RCPW-SPEC-TRANS (IX) = '3'                               PPRCOPT3
030000         MOVE 'R' TO XC3T-SIGN (IX2)                              PPRCOPT3
030100      ELSE                                                        PPRCOPT3
030200         MOVE '+' TO XC3T-SIGN (IX2)                              PPRCOPT3
030300      END-IF                                                      PPRCOPT3
030400      IF IX2 = 4                                                  PPRCOPT3
030500         PERFORM 4000-CREATE-DS-TRAN                              PPRCOPT3
030600      END-IF                                                      PPRCOPT3
030700     END-PERFORM                                                  PPRCOPT3
030800     IF IX2 > ZERO                                                PPRCOPT3
030900        PERFORM 4000-CREATE-DS-TRAN                               PPRCOPT3
031000     END-IF.                                                      PPRCOPT3
031110     INITIALIZE RC0T-TRANSACTION-IMAGE                            PPRCOPT3
031120     MOVE RCPW-ID-NO TO RC0T-EMPLOYEE-ID                          PPRCOPT3
031121                        RC0T-ORIG-EMPLOYEE-ID                     PPRCOPT3
031140     MOVE NEW-NEXT-CHECK-NUMBER TO RC0T-CHECK-NUMBER              PPRCOPT3
031141                                   RC0T-ORIG-CHECK-NUMBER         PPRCOPT3
031150     MOVE 'R0' TO RC0T-TRANSACTION-CODE                           PPRCOPT3
031160     MOVE '01' TO RC0T-ENTRY-SEQUENCE-NO                          PPRCOPT3
031170     MOVE RCPW-NAME-26 TO RC0T-EMPLOYEE-NAME                      PPRCOPT3
031171     MOVE RCPW-CHECK-YR TO RC0T-CHECK-YR                          PPRCOPT3
031172     MOVE RCPW-CHECK-MO TO RC0T-CHECK-MO                          PPRCOPT3
031173     MOVE RCPW-CHECK-DA TO RC0T-CHECK-DA                          PPRCOPT3
031174     MOVE RCPW-TOTAL-GROSS TO RC0T-TRANS-GROSS                    PPRCOPT3
031175     MOVE RCPW-NET-PAY TO RC0T-TRANS-NET                          PPRCOPT3
031176     MOVE 'C' TO RC0T-EDB-PROCESS-YEAR                            PPRCOPT3
031177     COMPUTE RC0T-RECD-CNT = WS-HANDDRAWN-CNT + 1                 PPRCOPT3
031180     MOVE RC0T-TRANSACTION-CONTROL TO WS-ABD                      PPRCOPT3
031191     PERFORM 9950-INSERT-HANDDRAWN.                               PPRCOPT3
031210     PERFORM 9900-UPDATE-ABEY-CHECK-NUMBER.                       PPRCOPT3
031300                                                                  PPRCOPT3
031400 3999-UPDATE-EXIT.                                                PPRCOPT3
031500     EXIT.                                                        PPRCOPT3
031600                                                                  PPRCOPT3
031700 4000-CREATE-DS-TRAN.                                             PPRCOPT3
031800                                                                  PPRCOPT3
031900     ADD 1 TO IX3                                                 PPRCOPT3
032000     MOVE RCPW-ID-NO TO EMPLOYEE-ID OF WS-ABD                     PPRCOPT3
032100     MOVE NEW-NEXT-CHECK-NUMBER TO CHECK-NUMBER                   PPRCOPT3
032200     MOVE 'R4' TO TRANSACTION-CODE                                PPRCOPT3
032300     MOVE IX3 TO WS-ELEM-NO                                       PPRCOPT3
032400     MOVE WS-ELEM-NO (3:2) TO ENTRY-SEQUENCE-NO                   PPRCOPT3
032500     MOVE XC3T-RECORD-IMAGE TO TRANSACTION-IMAGE                  PPRCOPT3
032600     MOVE SPACES TO XC3T-RECORD-IMAGE                             PPRCOPT3
032700     MOVE RCPW-ID-NO TO XC3T-ID.                                  PPRCOPT3
032800     MOVE 'DS' TO XC3T-TRAN-CD.                                   PPRCOPT3
032900     MOVE RCPW-CHECK-DATE TO XC3T-TRAN-DATE(2:6)                  PPRCOPT3
033000     MOVE ZERO TO IX2                                             PPRCOPT3
033100                  XC3T-XFOOT9.                                    PPRCOPT3
033200     PERFORM 9950-INSERT-HANDDRAWN.                               PPRCOPT3
033300                                                                  PPRCOPT3
036500 9850-SELECT-ABEY-CHECK-NUMBER.                                   PPRCOPT3
036600                                                                  PPRCOPT3
017200     MOVE '9850-SELECT-ABEY-CHECK' TO DB2MSG-TAG.                 PPRCOPT3
036700     EXEC SQL                                                     PPRCOPT3
036800       SELECT NEXT_CHECK_NUMBER                                   PPRCOPT3
036900       INTO :NEXT-CHECK-NUMBER                                    PPRCOPT3
037000       FROM PPPVZABC_ABC                                          PPRCOPT3
037100     END-EXEC.                                                    PPRCOPT3
037300     MOVE NEXT-CHECK-NUMBER TO NEW-NEXT-CHECK-NUMBER              PPRCOPT3
037400     IF NEW-NEXT-CHECK-NUM9 IS NOT NUMERIC                        PPRCOPT3
037500        MOVE ZERO TO NEW-NEXT-CHECK-NUMBER                        PPRCOPT3
037600     END-IF.                                                      PPRCOPT3
037700                                                                  PPRCOPT3
037800 9900-UPDATE-ABEY-CHECK-NUMBER.                                   PPRCOPT3
037900                                                                  PPRCOPT3
018500     MOVE '9900-UPDATE-ABEY-CHECK' TO DB2MSG-TAG.                 PPRCOPT3
038000     EXEC SQL                                                     PPRCOPT3
038100       UPDATE PPPVZABC_ABC                                        PPRCOPT3
038200       SET NEXT_CHECK_NUMBER = :NEW-NEXT-CHECK-NUMBER             PPRCOPT3
038300     END-EXEC.                                                    PPRCOPT3
038400                                                                  PPRCOPT3
038500 9950-INSERT-HANDDRAWN.                                           PPRCOPT3
038600                                                                  PPRCOPT3
019300     MOVE '9950-INSERT-HANDRAWN' TO DB2MSG-TAG.                   PPRCOPT3
038700     EXEC SQL                                                     PPRCOPT3
038800       INSERT INTO PPPVZABD_ABD                                   PPRCOPT3
038900           VALUES (:WS-ABD)                                       PPRCOPT3
039000     END-EXEC.                                                    PPRCOPT3
039010     ADD 1 TO WS-HANDDRAWN-CNT.                                   PPRCOPT3
039100                                                                  PPRCOPT3
039200 9999-ABEND.                                                      PPRCOPT3
020100**************************************************************    PPRCOPT3
020200*  COPIED PROCEDURE DIVISION CODE FOR ABEND HANDLING         *    PPRCOPT3
020300**************************************************************    PPRCOPT3
020400     MOVE 'PPRCOPT3' TO UCWSABND-ABENDING-PGM.                    PPRCOPT3
020500     COPY UCPDABND.                                               PPRCOPT3
039300                                                                  PPRCOPT3
020700*999999-SQL-ERROR.                                                PPRCOPT3
020800**************************************************************    PPRCOPT3
020900*  COPIED PROCEDURE DIVISION CODE FOR SQL ERROR HANDLING     *    PPRCOPT3
021000**************************************************************    PPRCOPT3
021100      EXEC SQL                                                    PPRCOPT3
021200           INCLUDE CPPDXP99                                       PPRCOPT3
021300      END-EXEC.                                                   PPRCOPT3
021400      MOVE 'SQL ERROR - SEE PRECEDING DB2 MESSAGES'               PPRCOPT3
021500                                      TO UCWSABND-ABEND-DATA (1). PPRCOPT3
021600      PERFORM 9999-ABEND.                                         PPRCOPT3
021700                                                                  PPRCOPT3
