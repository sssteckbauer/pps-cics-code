000000**************************************************************/   36430951
000001*  PROGRAM: PPAPHDFP                                         */   36430951
000002*  RELEASE: ___0951______ SERVICE REQUEST(S): _____3643____  */   36430951
000003*  NAME:_______MLG_______ MODIFICATION DATE:  __12/21/94____ */   36430951
000004*  DESCRIPTION:                                              */   36430951
000005*    REMOVE REFERENCES TO "OLD SYSTEM"                       */   36430951
000007**************************************************************/   36430951
000100**************************************************************/   36430944
000200*  PROGRAM: PPAPHDFP                                         */   36430944
000300*  RELEASE # ___0944___   SERVICE REQUEST NO(S)____3643______*/   36430944
000400*  NAME _____MLG_______   MODIFICATION DATE ____11/17/94_____*/   36430944
000500*  DESCRIPTION                                               */   36430944
000600*    HDB INQUIRY FUNCTION PRE-EDIT APPLICATION PROCESSOR     */   36430944
000700*                                                            */   36430944
000800**************************************************************/   36430944
000900 IDENTIFICATION DIVISION.                                         PPAPHDFP
001000 PROGRAM-ID. PPAPHDFP.                                            PPAPHDFP
001100 ENVIRONMENT DIVISION.                                            PPAPHDFP
001200 CONFIGURATION SECTION.                                           PPAPHDFP
001300 SOURCE-COMPUTER.                                                 PPAPHDFP
001400 COPY CPOTXUCS.                                                   PPAPHDFP
001500                                                                  PPAPHDFP
001600 DATA DIVISION.                                                   PPAPHDFP
001700 WORKING-STORAGE SECTION.                                         PPAPHDFP
001800                                                                  PPAPHDFP
001900 01  WS-COMN.                                                     PPAPHDFP
002000     05  WS-START                          PIC X(27) VALUE        PPAPHDFP
002100       'WORKING STORAGE STARTS HERE'.                             PPAPHDFP
002200     05  WS-RESP                           PIC S9(8) COMP.        PPAPHDFP
002300     05  WS-SUB                            PIC S9(04) COMP.       PPAPHDFP
002400     05  WS-EMPLOYEE-ID                    PIC X(09).             PPAPHDFP
002500     05  WS-COUNT                          PIC S9(8) COMP         PPAPHDFP
002600                                                     VALUE ZERO.  PPAPHDFP
002700                                                                  PPAPHDFP
002800 01  WS-MESS-NOS.                                                 PPAPHDFP
002900     05  MP0058                            PIC X(5) VALUE 'P0058'.PPAPHDFP
003000                                                                  PPAPHDFP
003100 01  PPDB2MSG-INTERFACE.                                          PPAPHDFP
003200     COPY CPLNKDB2.                                               PPAPHDFP
003300                                                                  PPAPHDFP
003400 01  UCWSABND    EXTERNAL.                                        PPAPHDFP
003500     COPY UCWSABND.                                               PPAPHDFP
003600                                                                  PPAPHDFP
003700 01  UCPISCRN    EXTERNAL.                                        PPAPHDFP
003800     COPY UCPISCRN.                                               PPAPHDFP
003900                                                                  PPAPHDFP
004000 01  CPWSKEYS    EXTERNAL.                                        PPAPHDFP
004100     COPY CPWSKEYS.                                               PPAPHDFP
004200                                                                  PPAPHDFP
004300 01  PAY-ROW       EXTERNAL.                                      PPAPHDFP
004400     COPY CPWSRPAY.                                               PPAPHDFP
004500                                                                  PPAPHDFP
004600 01  PER-ROW       EXTERNAL.                                      PPAPHDFP
004700     COPY CPWSRPER.                                               PPAPHDFP
004800                                                                  PPAPHDFP
004900 01  UCCOMMON   EXTERNAL.                                         PPAPHDFP
005000     EXEC SQL                                                     PPAPHDFP
005100      INCLUDE UCCOMMON                                            PPAPHDFP
005200     END-EXEC.                                                    PPAPHDFP
005300                                                                  PPAPHDFP
005400 01  CFN-ROW   EXTERNAL.                                          PPAPHDFP
005500     EXEC SQL                                                     PPAPHDFP
005600      INCLUDE UC0VZCFN                                            PPAPHDFP
005700     END-EXEC.                                                    PPAPHDFP
005800                                                                  PPAPHDFP
005900******************************************************************PPAPHDFP
006000*          SQL - WORKING STORAGE                                 *PPAPHDFP
006100******************************************************************PPAPHDFP
006200                                                                  PPAPHDFP
006300 01  IDX-ROW   EXTERNAL.                                          PPAPHDFP
006400     EXEC SQL                                                     PPAPHDFP
006500          INCLUDE PPPVZIDX                                        PPAPHDFP
006600     END-EXEC.                                                    PPAPHDFP
006700                                                                  PPAPHDFP
006800     EXEC SQL                                                     PPAPHDFP
006900          INCLUDE SQLCA                                           PPAPHDFP
007000     END-EXEC.                                                    PPAPHDFP
007100                                                                  PPAPHDFP
007200 LINKAGE SECTION.                                                 PPAPHDFP
007300                                                                  PPAPHDFP
007400                                                                  PPAPHDFP
007500 EJECT                                                            PPAPHDFP
007600                                                                  PPAPHDFP
007700 PROCEDURE DIVISION.                                              PPAPHDFP
007800                                                                  PPAPHDFP
007900 0000-MAIN-LINE   SECTION.                                        PPAPHDFP
008000**************************************************************    PPAPHDFP
008100*  MAIN LINE PROCESSING                                      *    PPAPHDFP
008200**************************************************************    PPAPHDFP
008300                                                                  PPAPHDFP
008400     EXEC SQL                                                     PPAPHDFP
008500          INCLUDE CPPDXE99                                        PPAPHDFP
008600     END-EXEC.                                                    PPAPHDFP
008700                                                                  PPAPHDFP
008800     MOVE 'PPAPHDFP' TO DB2MSG-PGM-ID.                            PPAPHDFP
008900                                                                  PPAPHDFP
009000     IF UCCOMMON-SUBSYSTEM-INIT                                   PPAPHDFP
009100      AND (CFN-FUNCTION-ID   NOT = 'ILOK')                        PPAPHDFP
009200      AND (CFN-FUNCTION-ID   NOT = 'YLOK')                        PPAPHDFP
009300      AND (CFN-FUNCTION-TYPE NOT = 'M')                           PPAPHDFP
009400******AND (CFN-OLD-SYSTEM-IND NOT = 'Y')                          36430951
009500      AND (CPWSKEYS-EMPLOYEE-ID NOT = SPACES)                     PPAPHDFP
009600      AND (CPWSKEYS-EMPLOYEE-ID NOT = LOW-VALUES)                 PPAPHDFP
009700        MOVE CPWSKEYS-EMPLOYEE-ID TO WS-EMPLOYEE-ID               PPAPHDFP
009800        PERFORM 1000-CHECK-ID                                     PPAPHDFP
009900     END-IF.                                                      PPAPHDFP
010000                                                                  PPAPHDFP
010100     GOBACK.                                                      PPAPHDFP
010200                                                                  PPAPHDFP
010300 0000-EXIT.                                                       PPAPHDFP
010400     EXIT.                                                        PPAPHDFP
010500                                                                  PPAPHDFP
010600                                                                  PPAPHDFP
010700 EJECT                                                            PPAPHDFP
010800                                                                  PPAPHDFP
010900 1000-CHECK-ID    SECTION.                                        PPAPHDFP
011000**************************************************************    PPAPHDFP
011100*  DETERMINE WHETHER THE EMPLOYEE ID EXISTS ON THE HDB, AND  *    PPAPHDFP
011200*  IF SO THE USER'S AUTHORIZATION FOR THAT ID                *    PPAPHDFP
011300**************************************************************    PPAPHDFP
011400                                                                  PPAPHDFP
011500     PERFORM 8000-SELECT-COUNT-IDX.                               PPAPHDFP
011600                                                                  PPAPHDFP
011700     IF WS-COUNT > ZERO                                           PPAPHDFP
011800        CONTINUE                                                  PPAPHDFP
011900     ELSE                                                         PPAPHDFP
012000        MOVE MP0058 TO UCCOMMON-MSG-NUMBER                        PPAPHDFP
012100        SET UCCOMMON-RETURN-ERRORS-FOUND TO TRUE                  PPAPHDFP
012200     END-IF.                                                      PPAPHDFP
012300                                                                  PPAPHDFP
012400 1000-EXIT.                                                       PPAPHDFP
012500     EXIT.                                                        PPAPHDFP
012600                                                                  PPAPHDFP
012700 EJECT                                                            PPAPHDFP
012800                                                                  PPAPHDFP
012900 8000-SELECT-COUNT-IDX SECTION.                                   PPAPHDFP
013000**************************************************************    PPAPHDFP
013100*  COUNT THE PER TABLE ROWS FOR THE GIVEN ID                 *    PPAPHDFP
013200**************************************************************    PPAPHDFP
013300                                                                  PPAPHDFP
013400     MOVE 'COUNT IDX' TO DB2MSG-TAG.                              PPAPHDFP
013500                                                                  PPAPHDFP
013600     EXEC SQL                                                     PPAPHDFP
013700        SELECT COUNT (*)                                          PPAPHDFP
013800        INTO :WS-COUNT                                            PPAPHDFP
013900        FROM PPPVZIDX_IDX                                         PPAPHDFP
014000        WHERE EMPLID = :WS-EMPLOYEE-ID                            PPAPHDFP
014100     END-EXEC.                                                    PPAPHDFP
014200                                                                  PPAPHDFP
014300 8000-EXIT.                                                       PPAPHDFP
014400     EXIT.                                                        PPAPHDFP
014500                                                                  PPAPHDFP
014600 EJECT                                                            PPAPHDFP
014700                                                                  PPAPHDFP
014800 9999-ABEND    SECTION.                                           PPAPHDFP
014900**************************************************************    PPAPHDFP
015000*  COPIED PROCEDURE DIVISION CODE                            *    PPAPHDFP
015100**************************************************************    PPAPHDFP
015200                                                                  PPAPHDFP
015300     MOVE 'PPAPHDFP' TO UCWSABND-ABENDING-PGM.                    PPAPHDFP
015400                                                                  PPAPHDFP
015500     COPY UCPDABND.                                               PPAPHDFP
015600                                                                  PPAPHDFP
015700 9999-EXIT.                                                       PPAPHDFP
015800     EXIT.                                                        PPAPHDFP
015900                                                                  PPAPHDFP
016000                                                                  PPAPHDFP
016100 EJECT                                                            PPAPHDFP
016200*999999-SQL-ERROR SECTION.                                        PPAPHDFP
016300**************************************************************    PPAPHDFP
016400*  COPIED PROCEDURE DIVISION CODE FOR SQL ERROR HANDLING     *    PPAPHDFP
016500**************************************************************    PPAPHDFP
016600                                                                  PPAPHDFP
016700      EXEC SQL                                                    PPAPHDFP
016800           INCLUDE CPPDXP99                                       PPAPHDFP
016900      END-EXEC.                                                   PPAPHDFP
017000                                                                  PPAPHDFP
017100      MOVE 'SQL ERROR - SEE PRECEDING DB2 MESSAGES'               PPAPHDFP
017200                                      TO UCWSABND-ABEND-DATA (1). PPAPHDFP
017300                                                                  PPAPHDFP
017400      PERFORM 9999-ABEND.                                         PPAPHDFP
017500                                                                  PPAPHDFP
017600 999999-EXIT.                                                     PPAPHDFP
017700       EXIT.                                                      PPAPHDFP
017800***************    END OF SOURCE - PPAPHDFP    *******************PPAPHDFP
