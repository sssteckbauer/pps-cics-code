000100**************************************************************/   36510832
000200*  PROGRAM: UCA4SASR                                         */   36510832
000300*  RELEASE: ___0832______ SERVICE REQUEST(S): _____3651____  */   36510832
000400*  NAME:_______DDM_______ MODIFICATION DATE:  __11/12/93____ */   36510832
000500*  DESCRIPTION: DSS - DISTRIBUTED SYSTEM SERVICES            */   36510832
000600*    BUNDLE AND BUNDLE HIGHLIGHTING DEFINITION.              */   36510832
000610*    STORE/RESTORE TS QUEUES FOR DSS PSEUDO-CONVERSATION     */   36510832
000700*                                                            */   36510832
000800**************************************************************/   36510832
000900 IDENTIFICATION DIVISION.                                         UCA4SASR
001000 PROGRAM-ID. UCA4SASR.                                            UCA4SASR
001100 ENVIRONMENT DIVISION.                                            UCA4SASR
001200 CONFIGURATION SECTION.                                           UCA4SASR
001300 SOURCE-COMPUTER.                                                 UCA4SASR
001400 COPY  CPOTXUCS.                                                  UCA4SASR
001500 DATA DIVISION.                                                   UCA4SASR
001600 WORKING-STORAGE SECTION.                                         UCA4SASR
001700                                                                  UCA4SASR
001800 01  WS-COMN.                                                     UCA4SASR
001900     05  WS-START                          PIC X(27) VALUE        UCA4SASR
002000         'WORKING STORAGE STARTS HERE'.                           UCA4SASR
002100     05  WS-RESP                           PIC S9(8) COMP.        UCA4SASR
002200     05  WS-LENGTH                         PIC S9(08) COMP.       UCA4SASR
002300     05  WS-START-POSITION                 PIC S9(08) COMP.       UCA4SASR
002400     05  TS-QUEUE-ID.                                             UCA4SASR
002500         10  TS-QUEUE-NAME-SS              PIC X(02).             UCA4SASR
002600         10  TS-QUEUE-NAME-N1              PIC 9(01).             UCA4SASR
002700         10  TS-QUEUE-NAME-N2              PIC 9(01).             UCA4SASR
002800         10  TS-QUEUE-TERMID               PIC X(04).             UCA4SASR
002900     05  TS-QUEUE-LENGTH                   PIC S9(04) COMP.       UCA4SASR
003000     05  TS-QUEUE-ITEM                     PIC S9(04) COMP.       UCA4SASR
003100     05  TS-AUDIT-QUEUE-ID.                                       UCA4SASR
003200         10  TS-AUDIT-Q-SS-NAME            PIC X(02).             UCA4SASR
003300         10  FILLER                        PIC X(02) VALUE 'AU'.  UCA4SASR
003400         10  TS-AUDIT-Q-TERM-ID            PIC X(04).             UCA4SASR
003500     05  TS-AUDIT-QUEUE-LENGTH             PIC S9(04) COMP        UCA4SASR
003600                                                      VALUE 8.    UCA4SASR
003700     05  TS-AUDIT-QUEUE-DATA.                                     UCA4SASR
003800         10  TS-AUDIT-Q-QUEUE-NAME         PIC X(08).             UCA4SASR
003900*                                                                 UCA4SASR
004000 01  WS-TS-AREA                            PIC X(24000).          UCA4SASR
004500*                                                                 UCA4SASR
004600 01  WS-MESS-NOS.                                                 UCA4SASR
004700     05  U0000                             PIC X(5) VALUE 'U0000'.UCA4SASR
004800     EJECT                                                        UCA4SASR
004900 01  PPDB2MSG-INTERFACE.                                          UCA4SASR
005000     COPY CPLNKDB2.                                               UCA4SASR
005100     EJECT                                                        UCA4SASR
005200 01  UCCOMMON   EXTERNAL.                                         UCA4SASR
005300     COPY UCCOMMON.                                               UCA4SASR
005400     EJECT                                                        UCA4SASR
005500 01  UCPIROSR   EXTERNAL.                                         UCA4SASR
005600     COPY UCPIROSR.                                               UCA4SASR
005700     EJECT                                                        UCA4SASR
005800 01  CPWSBUND   EXTERNAL.                                         UCA4SASR
005900     COPY CPWSBUND.                                               UCA4SASR
006300     EJECT                                                        UCA4SASR
006400 01  UCWSABND    EXTERNAL.                                        UCA4SASR
006500     COPY UCWSABND.                                               UCA4SASR
006600     EJECT                                                        UCA4SASR
007400 LINKAGE SECTION.                                                 UCA4SASR
007500     EJECT                                                        UCA4SASR
007600 PROCEDURE DIVISION.                                              UCA4SASR
007700****************************************************************  UCA4SASR
007800*            P R O C E D U R E  D I V I S I O N                   UCA4SASR
007900****************************************************************  UCA4SASR
008400 0000-MAINLINE SECTION.                                           UCA4SASR
008500**************************************************************    UCA4SASR
008600*  MAIN LINE PROCESSING SECTION                              *    UCA4SASR
008700*  PERFORM STORE OR RESTORE FUNCTIONS DEPENDING ON REQUEST   *    UCA4SASR
008800*  CODE.                                                     *    UCA4SASR
008900*  IF LOGICAL FUNCTION IS CANCEL, IT IS A REQUEST TO         *    UCA4SASR
009000*  RESTORE THE DATA FROM A HOLD AREA.                        *    UCA4SASR
009100*                                                            *    UCA4SASR
009200*  THE FOLLOWING CONVENTIONS ARE USED FOR TEMP STORAGE QUEUE *    UCA4SASR
009300*  NAMES: SSXYTTTT WHERE:                                    *    UCA4SASR
009400*    SS = SUBSYSTEM ID PASSED FROM UCROUTER                  *    UCA4SASR
009500*    X  = NESTING LEVEL PASSED FROM UCROUTER                 *    UCA4SASR
009600*    Y  = QUEUE IDENTIFIER                                   *    UCA4SASR
009700*    TTTT = TERMINAL ID                                      *    UCA4SASR
009800*                                                            *    UCA4SASR
009900*  THE VALUES FOR THE QUEUE IDENTIFIER ARE AS FOLLOWS:       *    UCA4SASR
010000*                                                            *    UCA4SASR
010100*    QUEUE ID=1 - BUND - BUNDLE MAINTENANCE DATA             *    UCA4SASR
010200**************************************************************    UCA4SASR
010800     MOVE ZERO TO UCCOMMON-RETURN-CODE                            UCA4SASR
010900*                                                                 UCA4SASR
011100     EVALUATE TRUE                                                UCA4SASR
011200       WHEN UCCOMMON-STORE-DATA                                   UCA4SASR
011300         PERFORM 1000-STORE-DATA                                  UCA4SASR
011400       WHEN OTHER                                                 UCA4SASR
011500         PERFORM 2000-RESTORE-DATA                                UCA4SASR
012000     END-EVALUATE.                                                UCA4SASR
012200*                                                                 UCA4SASR
012300*                                                                 UCA4SASR
012400     GOBACK.                                                      UCA4SASR
012500*                                                                 UCA4SASR
012600 0000-EXIT.                                                       UCA4SASR
012700     EXIT.                                                        UCA4SASR
012800     EJECT                                                        UCA4SASR
012900 1000-STORE-DATA   SECTION.                                       UCA4SASR
013000**************************************************************    UCA4SASR
013100*  STORE EXTERNAL AREAS IN TEMP STORAGE                      *    UCA4SASR
013200**************************************************************    UCA4SASR
013300*                                                                 UCA4SASR
013400     MOVE EIBTRMID TO TS-QUEUE-TERMID.                            UCA4SASR
013500*                                                                 UCA4SASR
013600     MOVE UCCOMMON-SUBSYSTEM-ID TO TS-QUEUE-NAME-SS.              UCA4SASR
013700     MOVE UCPIROSR-NESTED-FUNCTION-SW TO TS-QUEUE-NAME-N1.        UCA4SASR
013800*                                                                 UCA4SASR
013900**************************************************************    UCA4SASR
014000*  QUEUE 1 CONTAINS BUND - BUNDLE MAINTENANCE DATA                UCA4SASR
014100**************************************************************    UCA4SASR
014200     MOVE 1 TO TS-QUEUE-NAME-N2.                                  UCA4SASR
014300*                                                                 UCA4SASR
014400     PERFORM 1100-INITIALIZE-QUEUE.                               UCA4SASR
014500*                                                                 UCA4SASR
014600     MOVE 1 TO TS-QUEUE-ITEM.                                     UCA4SASR
014700     MOVE LENGTH OF CPWSBUND TO TS-QUEUE-LENGTH.                  UCA4SASR
014800*                                                                 UCA4SASR
014900     EXEC CICS                                                    UCA4SASR
015000          WRITEQ                                                  UCA4SASR
015100          QUEUE  (TS-QUEUE-ID)                                    UCA4SASR
015200          FROM   (CPWSBUND)                                       UCA4SASR
015300          ITEM   (TS-QUEUE-ITEM)                                  UCA4SASR
015400          LENGTH (TS-QUEUE-LENGTH)                                UCA4SASR
015500          NOHANDLE                                                UCA4SASR
015600          RESP   (WS-RESP)                                        UCA4SASR
015700     END-EXEC.                                                    UCA4SASR
015800*                                                                 UCA4SASR
015900     IF WS-RESP NOT = DFHRESP(NORMAL)                             UCA4SASR
016000        PERFORM 9999-ABEND                                        UCA4SASR
016100     END-IF.                                                      UCA4SASR
016200*                                                                 UCA4SASR
016300 1000-EXIT.                                                       UCA4SASR
016400     EXIT.                                                        UCA4SASR
016500     EJECT                                                        UCA4SASR
016600 1100-INITIALIZE-QUEUE   SECTION.                                 UCA4SASR
016700**************************************************************    UCA4SASR
016800*  DELETE THE TEMP STORAGE QUEUE, IF IT IS THERE. IF IT'S    *    UCA4SASR
016900*  THE FIRST TIME THROUGH, WRITE THE QUEUE NAME TO THE       *    UCA4SASR
017000*  TEMP STORAGE AUDIT QUEUE.                                 *    UCA4SASR
017100**************************************************************    UCA4SASR
017200     EXEC CICS                                                    UCA4SASR
017300          DELETEQ TS                                              UCA4SASR
017400          QUEUE (TS-QUEUE-ID)                                     UCA4SASR
017500          NOHANDLE                                                UCA4SASR
017600          RESP(WS-RESP)                                           UCA4SASR
017700     END-EXEC.                                                    UCA4SASR
017800*                                                                 UCA4SASR
017900     IF UCCOMMON-SUBSYSTEM-INIT                                   UCA4SASR
018000        PERFORM 9200-AUDIT-TS-QUEUE                               UCA4SASR
018100     END-IF.                                                      UCA4SASR
018200*                                                                 UCA4SASR
018300 1100-EXIT.                                                       UCA4SASR
018400     EXIT.                                                        UCA4SASR
018500     EJECT                                                        UCA4SASR
018600 2000-RESTORE-DATA   SECTION.                                     UCA4SASR
018700**************************************************************    UCA4SASR
018800*  READ TEMP STORAGE QUEUES AND POPULATE EXTERNAL AREAS      *    UCA4SASR
018900**************************************************************    UCA4SASR
019000*                                                                 UCA4SASR
019100     MOVE EIBTRMID TO TS-QUEUE-TERMID.                            UCA4SASR
019200*                                                                 UCA4SASR
019300     MOVE UCCOMMON-SUBSYSTEM-ID TO TS-QUEUE-NAME-SS.              UCA4SASR
019400     MOVE UCPIROSR-NESTED-FUNCTION-SW TO TS-QUEUE-NAME-N1.        UCA4SASR
019500*                                                                 UCA4SASR
019600**************************************************************    UCA4SASR
019700*  QUEUE 1 CONTAINS BUND - BUNDLE MAINTENANCE DATA                UCA4SASR
019800**************************************************************    UCA4SASR
019900     MOVE 1 TO TS-QUEUE-NAME-N2.                                  UCA4SASR
020000*                                                                 UCA4SASR
020100     MOVE 1 TO TS-QUEUE-ITEM.                                     UCA4SASR
020200*                                                                 UCA4SASR
020300     EXEC CICS                                                    UCA4SASR
020400          READQ                                                   UCA4SASR
020500          QUEUE  (TS-QUEUE-ID)                                    UCA4SASR
020600          INTO   (CPWSBUND)                                       UCA4SASR
020700          ITEM   (TS-QUEUE-ITEM)                                  UCA4SASR
020800          NOHANDLE                                                UCA4SASR
020900          RESP   (WS-RESP)                                        UCA4SASR
021000     END-EXEC.                                                    UCA4SASR
021100*                                                                 UCA4SASR
021200     IF WS-RESP = DFHRESP(NORMAL)                                 UCA4SASR
021300        CONTINUE                                                  UCA4SASR
021400     ELSE                                                         UCA4SASR
021500        PERFORM 9999-ABEND                                        UCA4SASR
021600     END-IF.                                                      UCA4SASR
021700*                                                                 UCA4SASR
021800 2000-EXIT.                                                       UCA4SASR
021900     EXIT.                                                        UCA4SASR
023400     EJECT                                                        UCA4SASR
023500 9200-AUDIT-TS-QUEUE   SECTION.                                   UCA4SASR
023600**************************************************************    UCA4SASR
023700*  WRITE THE TEMP STORAGE AUDIT QUEUE CONTAINING THE NAME    *    UCA4SASR
023800*  OF A TEMP STORAGE QUEUE BEING WRITTEN.                    *    UCA4SASR
023900**************************************************************    UCA4SASR
024000*                                                                 UCA4SASR
024100     MOVE UCCOMMON-SUBSYSTEM-ID TO TS-AUDIT-Q-SS-NAME.            UCA4SASR
024200     MOVE EIBTRMID TO TS-AUDIT-Q-TERM-ID.                         UCA4SASR
024300     MOVE TS-QUEUE-ID TO TS-AUDIT-Q-QUEUE-NAME.                   UCA4SASR
024400*                                                                 UCA4SASR
024500     EXEC CICS                                                    UCA4SASR
024600          WRITEQ TS                                               UCA4SASR
024700          QUEUE  (TS-AUDIT-QUEUE-ID)                              UCA4SASR
024800          FROM   (TS-AUDIT-QUEUE-DATA)                            UCA4SASR
024900          LENGTH (TS-AUDIT-QUEUE-LENGTH)                          UCA4SASR
025000          NOHANDLE                                                UCA4SASR
025100          RESP   (WS-RESP)                                        UCA4SASR
025200     END-EXEC.                                                    UCA4SASR
025300*                                                                 UCA4SASR
025400     IF WS-RESP NOT = DFHRESP(NORMAL)                             UCA4SASR
025500        PERFORM 9999-ABEND                                        UCA4SASR
025600     END-IF.                                                      UCA4SASR
025700*                                                                 UCA4SASR
025800 9200-EXIT.                                                       UCA4SASR
025900     EXIT.                                                        UCA4SASR
026000     EJECT                                                        UCA4SASR
026100 9999-ABEND    SECTION.                                           UCA4SASR
026200**************************************************************    UCA4SASR
026300*  COPIED PROCEDURE DIVISION CODE                            *    UCA4SASR
026400**************************************************************    UCA4SASR
026500*                                                                 UCA4SASR
026600     MOVE 'UCA4SASR' TO UCWSABND-ABENDING-PGM.                    UCA4SASR
026700*                                                                 UCA4SASR
026800     COPY UCPDABND.                                               UCA4SASR
026900*                                                                 UCA4SASR
027000 9999-EXIT.                                                       UCA4SASR
027100     EXIT.                                                        UCA4SASR
