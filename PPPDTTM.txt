000100**************************************************************/   36430795
000200*  PROGRAM: PPPDTTM                                          */   36430795
000300*  RELEASE # ___0795___   SERVICE REQUEST NO(S)____3643______*/   36430795
000400*  NAME __EMS     _____   MODIFICATION DATE ____07/15/93_____*/   36430795
000500*  DESCRIPTION                                               */   36430795
000600*    ENTRY/UPDATE PAYROLL HEADER/FOOTER FORMAT PROGRAM       */   36430795
000700*                                                            */   36430795
000800*                                                            */   36430795
000900**************************************************************/   36430795
001000 IDENTIFICATION DIVISION.                                         PPPDTTM
001100 PROGRAM-ID. PPPDTTM.                                             PPPDTTM
001200 ENVIRONMENT DIVISION.                                            PPPDTTM
001210 CONFIGURATION SECTION.                                           PPPDTTM
001220 SOURCE-COMPUTER.                                                 PPPDTTM
001230 COPY  CPOTXUCS.                                                  PPPDTTM
001300 DATA DIVISION.                                                   PPPDTTM
001400 WORKING-STORAGE SECTION.                                         PPPDTTM
001500                                                                  PPPDTTM
001800 01  WS-COMN.                                                     PPPDTTM
001900   05  WS-START                          PIC X(27) VALUE          PPPDTTM
002000       'WORKING STORAGE STARTS HERE'.                             PPPDTTM
003000                                                                  PPPDTTM
003010 01  PPDB2MSG-INTERFACE.                                          PPPDTTM
003020     COPY CPLNKDB2.                                               PPPDTTM
003030                                                                  PPPDTTM
003092 01 LE370-TIMESTAMP-CONV-AREA.                                    PPPDTTM
003093    05  WS-CEESECS                       PIC X(08)                PPPDTTM
003094                                             VALUE 'CEESECS'.     PPPDTTM
003095    05  WS-CEEDATM                       PIC X(08)                PPPDTTM
003096                                             VALUE 'CEEDATM'.     PPPDTTM
003097    05  LILLIAN-TIMESTAMP                  COMP-2.                PPPDTTM
003098    05  CHAR-TIMESTAMP.                                           PPPDTTM
003099        10  CHAR-TIMESTAMP-LENGTH          PIC S9(04) COMP.       PPPDTTM
003100        10  CHAR-TIMESTAMP-STRING          PIC X(100).            PPPDTTM
003101    05  PICTURE-OUTPUT-TIMESTAMP.                                 PPPDTTM
003102        10  FILLER                         PIC S9(04) COMP        PPPDTTM
003103                                               VALUE 17.          PPPDTTM
003104        10  FILLER                         PIC X(17)              PPPDTTM
003105                         VALUE 'MM/DD/YY HH:MI:SS'.               PPPDTTM
003106    05  PICTURE-INPUT-TIMESTAMP.                                  PPPDTTM
003107        10  FILLER                         PIC S9(04) COMP        PPPDTTM
003108                                               VALUE 19.          PPPDTTM
003109        10  FILLER                         PIC X(19)              PPPDTTM
003110                                    VALUE 'YYYY-MM-DD-HH.MI.SS'.  PPPDTTM
003111    05  TIMESTAMP-CONV-RETURN-CODE         PIC X(12).             PPPDTTM
003112                                                                  PPPDTTM
003113 01  UCWSABND     EXTERNAL.                                       PPPDTTM
003114     COPY UCWSABND.                                               PPPDTTM
003115                                                                  PPPDTTM
005000                                                                  PPPDTTM
005200******************************************************************PPPDTTM
005300*          SQL - WORKING STORAGE                                 *PPPDTTM
005400******************************************************************PPPDTTM
005500                                                                  PPPDTTM
005910                                                                  PPPDTTM
005920 01  PER-ROW.                                                     PPPDTTM
005930     EXEC SQL                                                     PPPDTTM
005940          INCLUDE PPPVZPER                                        PPPDTTM
005950     END-EXEC.                                                    PPPDTTM
006000                                                                  PPPDTTM
006200     EXEC SQL                                                     PPPDTTM
006300          INCLUDE SQLCA                                           PPPDTTM
006400     END-EXEC.                                                    PPPDTTM
006500                                                                  PPPDTTM
006600******************************************************************PPPDTTM
006700*                                                                *PPPDTTM
006800*                  SQL - SELECTS                                 *PPPDTTM
006900*                                                                *PPPDTTM
007000******************************************************************PPPDTTM
007010 LINKAGE SECTION.                                                 PPPDTTM
007020***************************************************************** PPPDTTM
007030*    COMMAREA                                                     PPPDTTM
007040***************************************************************** PPPDTTM
007050                                                                  PPPDTTM
007060 01  DFHCOMMAREA             PIC X(4080).                         PPPDTTM
007070 01  CA-COMMAREA REDEFINES   DFHCOMMAREA.                         PPPDTTM
007080     EXEC SQL                                                     PPPDTTM
007090          INCLUDE CPWSCOMA                                        PPPDTTM
007091     END-EXEC.                                                    PPPDTTM
007092                                                                  PPPDTTM
007093 PROCEDURE DIVISION.                                              PPPDTTM
007420                                                                  PPPDTTM
007500 0000-MAINLINE                        SECTION.                    PPPDTTM
007510                                                                  PPPDTTM
007512     EXEC SQL                                                     PPPDTTM
007513          INCLUDE CPPDXE99                                        PPPDTTM
007514     END-EXEC.                                                    PPPDTTM
007515                                                                  PPPDTTM
007520     IF CA-UNIQUE-EE-ID NOT = SPACES                              PPPDTTM
007530        PERFORM 1000-FORMAT-LAST-DATE-TIME                        PPPDTTM
007531     ELSE                                                         PPPDTTM
007534        MOVE SPACES TO CA-LAST-CHG-DATE                           PPPDTTM
007535                       CA-LAST-CHG-TIME                           PPPDTTM
007536     END-IF.                                                      PPPDTTM
007537                                                                  PPPDTTM
007538     GOBACK.                                                      PPPDTTM
007539                                                                  PPPDTTM
007540 0000-EXIT.                                                       PPPDTTM
007550     EXIT.                                                        PPPDTTM
007551                                                                  PPPDTTM
007552     EJECT                                                        PPPDTTM
007553                                                                  PPPDTTM
015700 1000-FORMAT-LAST-DATE-TIME   SECTION.                            PPPDTTM
015851**************************************************************    PPPDTTM
015852*  OBTAIN THE LAST UPDATE TIMESTAMP FROM THE PER ROW         *    PPPDTTM
015853*  CALL TWO LE370 TIMESTAMP CONVERSION ROUTINES:             *    PPPDTTM
015854*    CEESECS CONVERTS THE STANDARD TIMESTAMP FORMAT TO THE   *    PPPDTTM
015855*    LILLIAN TIMESTAMP FORMAT.                               *    PPPDTTM
015856*    CEEDATM CONVERTS THE LILLIAN TIMESTAMP FORMAT TO THE    *    PPPDTTM
015857*    FORMAT FOR SCREEN DISPLAY.                              *    PPPDTTM
015858**************************************************************    PPPDTTM
015859                                                                  PPPDTTM
015860      PERFORM 9000-SELECT-PER-TIMESTAMP.                          PPPDTTM
015861                                                                  PPPDTTM
015868      MOVE EMP-CHANGED-AT (1:19) TO CHAR-TIMESTAMP-STRING.        PPPDTTM
015869      MOVE 19 TO CHAR-TIMESTAMP-LENGTH.                           PPPDTTM
015870                                                                  PPPDTTM
015871      CALL WS-CEESECS USING CHAR-TIMESTAMP,                       PPPDTTM
015872         PICTURE-INPUT-TIMESTAMP, LILLIAN-TIMESTAMP,              PPPDTTM
015873         TIMESTAMP-CONV-RETURN-CODE.                              PPPDTTM
015874                                                                  PPPDTTM
015875      IF TIMESTAMP-CONV-RETURN-CODE NOT = LOW-VALUES              PPPDTTM
015876         MOVE 'BAD RETURN FROM CEESECS. RETURN-CODE = '           PPPDTTM
015877                                    TO UCWSABND-ABEND-DATA (1)    PPPDTTM
015878         MOVE TIMESTAMP-CONV-RETURN-CODE                          PPPDTTM
015879                                    TO UCWSABND-ABEND-DATA (2)    PPPDTTM
015880         PERFORM 9999-ABEND                                       PPPDTTM
015881      END-IF.                                                     PPPDTTM
015882                                                                  PPPDTTM
015883      MOVE SPACES TO CHAR-TIMESTAMP-STRING.                       PPPDTTM
015884                                                                  PPPDTTM
015885      CALL WS-CEEDATM USING LILLIAN-TIMESTAMP,                    PPPDTTM
015886         PICTURE-OUTPUT-TIMESTAMP, CHAR-TIMESTAMP,                PPPDTTM
015887         TIMESTAMP-CONV-RETURN-CODE.                              PPPDTTM
015888                                                                  PPPDTTM
015889      IF TIMESTAMP-CONV-RETURN-CODE NOT = LOW-VALUES              PPPDTTM
015890         MOVE 'BAD RETURN FROM CEEDATM. RETURN-CODE = '           PPPDTTM
015891                                    TO UCWSABND-ABEND-DATA (1)    PPPDTTM
015892         MOVE TIMESTAMP-CONV-RETURN-CODE                          PPPDTTM
015893                                    TO UCWSABND-ABEND-DATA (2)    PPPDTTM
015895         PERFORM 9999-ABEND                                       PPPDTTM
015896      END-IF.                                                     PPPDTTM
015897                                                                  PPPDTTM
015898      MOVE CHAR-TIMESTAMP (1:8) TO CA-LAST-CHG-DATE.              PPPDTTM
015899      MOVE CHAR-TIMESTAMP (10:8) TO CA-LAST-CHG-TIME.             PPPDTTM
015900                                                                  PPPDTTM
015905                                                                  PPPDTTM
015926     EJECT                                                        PPPDTTM
015927                                                                  PPPDTTM
015928 9000-SELECT-PER-TIMESTAMP   SECTION.                             PPPDTTM
015929**************************************************************    PPPDTTM
015930*  SELECT THE LAST CHANGE TIMESTAMP FROM THE PER TABLE.      *    PPPDTTM
015931**************************************************************    PPPDTTM
015932                                                                  PPPDTTM
015935     MOVE 'SELECT PER' TO DB2MSG-TAG.                             PPPDTTM
015936                                                                  PPPDTTM
015937     MOVE CA-UNIQUE-EE-ID TO EMPLOYEE-ID OF PER-ROW.              PPPDTTM
015941                                                                  PPPDTTM
015942     EXEC SQL                                                     PPPDTTM
015943          SELECT  EMP_CHANGED_AT                                  PPPDTTM
015945            INTO :PER-ROW.EMP-CHANGED-AT                          PPPDTTM
015947            FROM  PPPVZPER_PER                                    PPPDTTM
015948            WHERE EMPLOYEE_ID = :PER-ROW.EMPLOYEE-ID              PPPDTTM
015949     END-EXEC.                                                    PPPDTTM
015950                                                                  PPPDTTM
015951     IF SQLCODE NOT = ZERO                                        PPPDTTM
015952        PERFORM 999999-SQL-ERROR                                  PPPDTTM
015962     END-IF.                                                      PPPDTTM
015963                                                                  PPPDTTM
015964 9000-EXIT.                                                       PPPDTTM
015965     EXIT.                                                        PPPDTTM
015966                                                                  PPPDTTM
015967     EJECT                                                        PPPDTTM
015970                                                                  PPPDTTM
016000 9999-ABEND                           SECTION.                    PPPDTTM
016001**************************************************************    PPPDTTM
016002*  COPIED PROCEDURE DIVISION CODE FOR ABEND HANDLING         *    PPPDTTM
016003**************************************************************    PPPDTTM
016004                                                                  PPPDTTM
016100     MOVE 'PPPDTTM' TO UCWSABND-ABENDING-PGM.                     PPPDTTM
016200     COPY UCPDABND.                                               PPPDTTM
016210 9999-EXIT.                                                       PPPDTTM
016220      EJECT                                                       PPPDTTM
016221                                                                  PPPDTTM
016230*999999-SQL-ERROR SECTION.                                        PPPDTTM
016240**************************************************************    PPPDTTM
016250*  COPIED PROCEDURE DIVISION CODE FOR SQL ERROR HANDLING     *    PPPDTTM
016260**************************************************************    PPPDTTM
016270                                                                  PPPDTTM
016280      EXEC SQL                                                    PPPDTTM
016290           INCLUDE CPPDXP99                                       PPPDTTM
016300      END-EXEC.                                                   PPPDTTM
016400                                                                  PPPDTTM
016500      MOVE 'SQL ERROR - SEE PRECEDING DB2 MESSAGES'               PPPDTTM
016600                                      TO UCWSABND-ABEND-DATA (1). PPPDTTM
016700                                                                  PPPDTTM
016800      PERFORM 9999-ABEND.                                         PPPDTTM
016900                                                                  PPPDTTM
017000 999999-EXIT.                                                     PPPDTTM
017100       EXIT.                                                      PPPDTTM
017200***************    END OF SOURCE - PPPDTTM     *******************PPPDTTM
