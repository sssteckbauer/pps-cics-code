000010**************************************************************/   52131415
000020*  PROGRAM: UCPANMGC                                         */   52131415
000030*  RELEASE: ___1415______ SERVICE REQUEST(S): ____15213____  */   52131415
000040*  NAME:_______MLG_______ MODIFICATION DATE:  __06/10/02____ */   52131415
000050*  DESCRIPTION:                                              */   52131415
000060*     ADDED PROCESSING FOR HTML DATA.                        */   52131415
000080*                                                            */   52131415
000090**************************************************************/   52131415
000100**************************************************************/   36480863
000200*  PROGRAM: UCPANMGC                                         */   36480863
000300*  RELEASE # ___0863___   SERVICE REQUEST NO(S)____3648______*/   36480863
000400*  NAME _____AAC_______   MODIFICATION DATE ____01/27/94_____*/   36480863
000500*  DESCRIPTION                                               */   36480863
000600*    IO MODULE FOR PAN DRIVER - CICS VERSION                 */   36480863
000700*                                                            */   36480863
000800**************************************************************/   36480863
000900 IDENTIFICATION DIVISION.                                         UCPANMGC
001000 PROGRAM-ID. UCPANMGC.                                            UCPANMGC
001100 ENVIRONMENT DIVISION.                                            UCPANMGC
001200 CONFIGURATION SECTION.                                           UCPANMGC
001300 SOURCE-COMPUTER.                                                 UCPANMGC
001400 COPY  CPOTXUCS.                                                  UCPANMGC
001500 DATA DIVISION.                                                   UCPANMGC
001600 WORKING-STORAGE SECTION.                                         UCPANMGC
001700                                                                  UCPANMGC
001800 01  WS-COMN.                                                     UCPANMGC
001900   05  WS-START                          PIC X(27) VALUE          UCPANMGC
002000       'WORKING STORAGE STARTS HERE'.                             UCPANMGC
002100   05  WS-PGM-NAME                       PIC X(08)                UCPANMGC
002200                                         VALUE 'UCPANMGC'.        UCPANMGC
002300   05  WS-CTR                            PIC 9(02) VALUE ZERO.    UCPANMGC
002400   05  WS-CTR-2                          PIC 9(02) VALUE ZERO.    UCPANMGC
002500                                                                  UCPANMGC
002600 01  ONLINE-SIGNALS EXTERNAL.                                     UCPANMGC
002700     COPY CPWSONLI.                                               UCPANMGC
002710                                                                  UCPANMGC
002720 01  UCWSPANI  EXTERNAL.                                          UCPANMGC
002730     COPY UCWSPANI.                                               UCPANMGC
002800                                                                  UCPANMGC
002900 01  UCWSPANC  EXTERNAL.                                          UCPANMGC
003000     COPY UCWSPANC.                                               UCPANMGC
003100                                                                  UCPANMGC
003200 LINKAGE SECTION.                                                 UCPANMGC
003300                                                                  UCPANMGC
003400 PROCEDURE DIVISION.                                              UCPANMGC
003500                                                                  UCPANMGC
003600 0000-MAINLINE                        SECTION.                    UCPANMGC
003700     SET ADDRESS OF DFHEIBLK    TO CPWSONLI-EIB-PTR               UCPANMGC
003701     SET ADDRESS OF DFHCOMMAREA TO CPWSONLI-COMMAREA-PTR          UCPANMGC
003710     EVALUATE TRUE                                                UCPANMGC
003800      WHEN UCWSPANI-REQ-GETNEXT                                   52131415
003900        IF UCWSPANI-REQ-ADDR                                      52131415
004000          PERFORM 1000-GET-ADDR                                   52131415
004100        ELSE                                                      52131415
004200          IF UCWSPANI-REQ-TEXT                                    52131415
004300            PERFORM 2000-GET-TEXT                                 52131415
004400          ELSE                                                    52131415
004410            IF UCWSPANI-REQ-HTML                                  52131415
004420               PERFORM 3000-GET-HTML                              52131415
004430            ELSE                                                  52131415
004500               SET UCWSPANI-RET-ERROR TO TRUE                     52131415
004510            END-IF                                                52131415
004600          END-IF                                                  52131415
004700        END-IF                                                    52131415
004710***** WHEN UCWSPANI-REQ-GETNEXT                                   52131415
004720*****   IF UCWSPANI-REQ-ADDR                                      52131415
004730*****     PERFORM 1000-GET-ADDR                                   52131415
004740*****   ELSE                                                      52131415
004750*****     IF UCWSPANI-REQ-TEXT                                    52131415
004760*****       PERFORM 2000-GET-TEXT                                 52131415
004770*****     ELSE                                                    52131415
004780*****       SET UCWSPANI-RET-ERROR TO TRUE                        52131415
004790*****     END-IF                                                  52131415
004791*****   END-IF                                                    52131415
004800      WHEN OTHER                                                  UCPANMGC
004900        SET UCWSPANI-RET-ERROR TO TRUE                            UCPANMGC
005000     END-EVALUATE.                                                UCPANMGC
005100     GOBACK.                                                      UCPANMGC
005200 0000-EXIT.                                                       UCPANMGC
005300     EXIT.                                                        UCPANMGC
005400                                                                  UCPANMGC
005500                                                                  UCPANMGC
005600 EJECT                                                            UCPANMGC
005700 1000-GET-ADDR                        SECTION.                    UCPANMGC
005810     INITIALIZE  UCWSPANI-RET-DATA                                UCPANMGC
005830     MOVE LENGTH OF UCWSPANI-RET-DATA TO UCWSPANI-RET-DATA-LEN    UCPANMGC
005900     EXEC CICS                                                    UCPANMGC
006000        READQ TS                                                  UCPANMGC
006100        QUEUE  (UCWSPANC-REQ-ADDR-Q-NAME)                         UCPANMGC
006200        ITEM   (UCWSPANI-REQ-ITEM-NO)                             UCPANMGC
006210        INTO   (UCWSPANI-RET-DATA)                                UCPANMGC
006220        LENGTH (UCWSPANI-RET-DATA-LEN)                            UCPANMGC
006300        NOHANDLE                                                  UCPANMGC
006400     END-EXEC                                                     UCPANMGC
006500**** IF EIBRESP = DFHRESP(NORMAL)       PRE-TRANSLATED CODE       UCPANMGC
006501     IF EIBRESP = DFHRESP(NORMAL)                                 UCPANMGC
006510      SET UCWSPANI-RET-OK TO TRUE                                 UCPANMGC
006520     ELSE                                                         UCPANMGC
006521****   IF EIBRESP = DFHRESP(ITEMERR)    PRE-TRANSLATED CODE       UCPANMGC
006522       IF EIBRESP = DFHRESP(ITEMERR)                              UCPANMGC
006523         SET UCWSPANI-RET-NOMORE TO TRUE                          UCPANMGC
006524       ELSE                                                       UCPANMGC
006525         SET UCWSPANI-RET-ERROR  TO TRUE                          UCPANMGC
006526       END-IF                                                     UCPANMGC
006527     END-IF.                                                      UCPANMGC
006600 1000-EXIT.                                                       UCPANMGC
006700     EXIT.                                                        UCPANMGC
006800                                                                  UCPANMGC
006900                                                                  UCPANMGC
007000 EJECT                                                            UCPANMGC
007100 2000-GET-TEXT                        SECTION.                    UCPANMGC
007210     INITIALIZE  UCWSPANI-RET-DATA                                UCPANMGC
007220     MOVE LENGTH OF UCWSPANI-RET-DATA TO UCWSPANI-RET-DATA-LEN    UCPANMGC
007221     EXEC CICS                                                    UCPANMGC
007230        READQ TS                                                  UCPANMGC
007240        QUEUE  (UCWSPANC-REQ-TEXT-Q-NAME)                         UCPANMGC
007241        ITEM   (UCWSPANI-REQ-ITEM-NO)                             UCPANMGC
007250        INTO   (UCWSPANI-RET-DATA)                                UCPANMGC
007260        LENGTH (UCWSPANI-RET-DATA-LEN)                            UCPANMGC
007270        NOHANDLE                                                  UCPANMGC
007280     END-EXEC                                                     UCPANMGC
007290**** IF EIBRESP = DFHRESP(NORMAL)       PRE-TRANSLATED CODE       UCPANMGC
007291     IF EIBRESP = DFHRESP(NORMAL)                                 UCPANMGC
007293      SET UCWSPANI-RET-OK TO TRUE                                 UCPANMGC
007294     ELSE                                                         UCPANMGC
007295****   IF EIBRESP = DFHRESP(ITEMERR)    PRE-TRANSLATED CODE       UCPANMGC
007296       IF EIBRESP = DFHRESP(ITEMERR)                              UCPANMGC
007297         SET UCWSPANI-RET-NOMORE TO TRUE                          UCPANMGC
007298       ELSE                                                       UCPANMGC
007299         SET UCWSPANI-RET-ERROR  TO TRUE                          UCPANMGC
007300       END-IF                                                     UCPANMGC
007301     END-IF.                                                      UCPANMGC
007310 2000-EXIT.                                                       UCPANMGC
007400     EXIT.                                                        UCPANMGC
007401                                                                  52131415
007410 3000-GET-HTML                        SECTION.                    52131415
007420     INITIALIZE  UCWSPANI-RET-HTML.                               52131415
007430     MOVE LENGTH OF UCWSPANI-RET-HTML TO UCWSPANI-RET-HTML-LEN.   52131415
007440     EXEC CICS                                                    52131415
007450        READQ TS                                                  52131415
007460        QUEUE  (UCWSPANC-REQ-HTML-Q-NAME)                         52131415
007470        ITEM   (UCWSPANI-REQ-ITEM-NO)                             52131415
007480        INTO   (UCWSPANI-RET-HTML)                                52131415
007490        LENGTH (UCWSPANI-RET-HTML-LEN)                            52131415
007491        NOHANDLE                                                  52131415
007492     END-EXEC.                                                    52131415
007493**** IF EIBRESP = DFHRESP(NORMAL)       PRE-TRANSLATED CODE       52131415
007494     IF EIBRESP = DFHRESP(NORMAL)                                 52131415
007495      SET UCWSPANI-RET-OK TO TRUE                                 52131415
007496     ELSE                                                         52131415
007497****   IF EIBRESP = DFHRESP(ITEMERR)    PRE-TRANSLATED CODE       52131415
007498       IF EIBRESP = DFHRESP(ITEMERR)                              52131415
007499****   OR EIBRESP = DFHRESP(QIDERR)    PRE-TRANSLATED CODE        52131415
007500       OR EIBRESP = DFHRESP(QIDERR)                               52131415
007501         SET UCWSPANI-RET-NOMORE TO TRUE                          52131415
007502       ELSE                                                       52131415
007510         SET UCWSPANI-RET-ERROR  TO TRUE                          52131415
007520       END-IF                                                     52131415
007530     END-IF.                                                      52131415
007540 3000-EXIT.                                                       52131415
007550     EXIT.                                                        52131415
007500***************    END OF SOURCE - UCPANMGC    *******************UCPANMGC
