000010**************************************************************/   32021138
000020*  PROGRAM: UCPPPPAR                                         */   32021138
000030*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000040*  NAME:______PAYJIG_____ MODIFICATION DATE:  ___08/04/97__  */   32021138
000050*  DESCRIPTION:                                              */   32021138
000060*  - MODIFIED FOR FULL ACCOUNTING UNIT                       */   32021138
000070**************************************************************/   32021138
000100**************************************************************/   36270604
000200*  PROGRAM: UCPPPPAR                                         */   36270604
000300*  RELEASE # ___0604___   SERVICE REQUEST NO(S)____3627______*/   36270604
000400*  NAME __C RIDLON_____   MODIFICATION DATE ____10/02/91_____*/   36270604
000500*  DESCRIPTION                                               */   36270604
000600*                                                            */   36270604
000700*  NEW ARSM CONTROL MODULE FOR PAR INQUIRY.                  */   36270604
000800*                                                            */   36270604
000900**************************************************************/   36270604
001000 IDENTIFICATION DIVISION.                                         UCPPPPAR
001100 PROGRAM-ID. UCPPPPAR.                                            UCPPPPAR
001200 ENVIRONMENT DIVISION.                                            UCPPPPAR
001300 DATA DIVISION.                                                   UCPPPPAR
001400 WORKING-STORAGE SECTION.                                         UCPPPPAR
001500 COPY CPYRIGHT.                                                   UCPPPPAR
001600                                                                  UCPPPPAR
001700 01  WS-MISC.                                                     UCPPPPAR
001800     05  WS-EMPLOYEE-ID                PIC X(9).                  UCPPPPAR
001900     05  WS-AUTH-ID                    PIC X(8).                  UCPPPPAR
002000     05  WS-ID                         PIC X(8).                  UCPPPPAR
002100     05  WS-ID-RULE                    PIC X(8).                  UCPPPPAR
002200     05  WS-RESOURCE-TYPE              PIC X(8).                  UCPPPPAR
002300     05  WS-PRIVILEGE                  PIC X(6).                  UCPPPPAR
002400     05  WS-LEVEL                      PIC 99.                    UCPPPPAR
002500     05  WS-RULE-LEVEL                 PIC 99.                    UCPPPPAR
002600     05  WS-INTENT-LEVEL               PIC 99.                    UCPPPPAR
002700     05  WS-ACCESS-RULE                PIC X(8).                  UCPPPPAR
002800     05  WS-CONJ-REF                   PIC X(8).                  UCPPPPAR
002900     05  WS-HOME-DEPT                  PIC X(6).                  UCPPPPAR
003000     05  WS-ASSOC-DEPT                 PIC X(6).                  UCPPPPAR
003100     05  WS-RANGE-VALUES.                                         UCPPPPAR
003200         10  WS-RANGE-VALUE-1          PIC X(9).                  UCPPPPAR
003300         10  WS-RANGE-VALUE-2          PIC X(9).                  UCPPPPAR
003400     05  WS-RULE-FLAG                  PIC X VALUE 'Y'.           UCPPPPAR
003500         88  RULE-FND                        VALUE 'Y'.           UCPPPPAR
003600         88  RULE-NOT-FND                    VALUE 'N'.           UCPPPPAR
003700     05  WS-ASSOC-FLAG                 PIC X VALUE 'Y'.           UCPPPPAR
003800         88  ASSOC-FND                       VALUE 'Y'.           UCPPPPAR
003900         88  ASSOC-NOT-FND                   VALUE 'N'.           UCPPPPAR
004000     05  WS-CONJ-FLAG                  PIC X VALUE 'Y'.           UCPPPPAR
004100         88  CONJ-FND                        VALUE 'Y'.           UCPPPPAR
004200         88  CONJ-NOT-FND                    VALUE 'N'.           UCPPPPAR
004300                                                                  UCPPPPAR
004400 01  WS-ACCESS-TYPES.                                             UCPPPPAR
004500 COPY  UCWSXACC.                                                  UCPPPPAR
004600                                                                  UCPPPPAR
004700******************************************************************UCPPPPAR
004800*          SQL - WORKING STORAGE                                 *UCPPPPAR
004900******************************************************************UCPPPPAR
005000 01  WS-UC0VZGRP.                                                 UCPPPPAR
005100     EXEC SQL                                                     UCPPPPAR
005200          INCLUDE UC0VGRP1                                        UCPPPPAR
005300     END-EXEC.                                                    UCPPPPAR
005400                                                                  UCPPPPAR
005500 01  WS-UC0VACC1.                                                 UCPPPPAR
005600     EXEC SQL                                                     UCPPPPAR
005700          INCLUDE UC0VACC1                                        UCPPPPAR
005800     END-EXEC.                                                    UCPPPPAR
005900                                                                  UCPPPPAR
006000 01  WS-UC0VACC2.                                                 UCPPPPAR
006100     EXEC SQL                                                     UCPPPPAR
006200          INCLUDE UC0VACC2                                        UCPPPPAR
006300     END-EXEC.                                                    UCPPPPAR
006400                                                                  UCPPPPAR
006500     EXEC SQL DECLARE                                             UCPPPPAR
006600          ACC-CURS CURSOR FOR                                     UCPPPPAR
006700          SELECT                                                  UCPPPPAR
006800              RESOURCE_TYPE,                                      UCPPPPAR
006900              ACCESS_RULE,                                        UCPPPPAR
007000              IDENTIFIER,                                         UCPPPPAR
007100              ACCESS_PRIORITY,                                    UCPPPPAR
007200              ACCESS_PRIVILEGE,                                   UCPPPPAR
007300              CONJ_RULE,                                          UCPPPPAR
007400              CONJ_IND                                            UCPPPPAR
007500          FROM UC0VACC1_ACC                                       UCPPPPAR
007600          WHERE RESOURCE_TYPE    = :WS-RESOURCE-TYPE              UCPPPPAR
007700            AND (IDENTIFIER      = :WS-ID                         UCPPPPAR
007800                 OR IDENTIFIER   IN                               UCPPPPAR
007900                (SELECT GROUP_ID                                  UCPPPPAR
008000                 FROM   UC0VGRP1_GRP                              UCPPPPAR
008100                 WHERE  AUTH_ID  = :WS-AUTH-ID))                  UCPPPPAR
008200            AND CONJ_IND         <> 'Y'                           UCPPPPAR
008300          ORDER BY ACCESS_PRIORITY                                UCPPPPAR
008400     END-EXEC.                                                    UCPPPPAR
008500                                                                  UCPPPPAR
008600 01  WS-UC0VASC1.                                                 UCPPPPAR
008700     EXEC SQL                                                     UCPPPPAR
008800          INCLUDE UC0VASC1                                        UCPPPPAR
008900     END-EXEC.                                                    UCPPPPAR
009000                                                                  UCPPPPAR
009100     EXEC SQL DECLARE                                             UCPPPPAR
009200          ASSOC-CURS CURSOR FOR                                   UCPPPPAR
009300          SELECT                                                  UCPPPPAR
009400              IDENTIFIER,                                         UCPPPPAR
009500              ACCESS_RULE,                                        UCPPPPAR
009600              SEQ_NUMBER,                                         UCPPPPAR
009700              ASSOCIATION_DATA                                    UCPPPPAR
009800          FROM UC0VASC1_ASC                                       UCPPPPAR
009900          WHERE IDENTIFIER       = :WS-ID-RULE                    UCPPPPAR
010000            AND ACCESS_RULE      = :WS-ACCESS-RULE                UCPPPPAR
010100     END-EXEC.                                                    UCPPPPAR
010200*****                                                             32021138
010300*01  WS-PPPVZEUD.                                                 32021138
010400*****EXEC SQL                                                     32021138
010500*****     INCLUDE PPPVZEUD                                        32021138
010600*****END-EXEC.                                                    32021138
010200                                                                  UCPPPPAR
010400     EXEC SQL                                                     UCPPPPAR
010900          INCLUDE SQLCA                                           UCPPPPAR
011000     END-EXEC.                                                    UCPPPPAR
011100                                                                  UCPPPPAR
011200******************************************************************UCPPPPAR
011300*                                                                *UCPPPPAR
011400*                  SQL - SELECTS                                 *UCPPPPAR
011500*                                                                *UCPPPPAR
011600******************************************************************UCPPPPAR
011700*                                                                *UCPPPPAR
011800*        THE VIEW USED IN THIS PROGRAM ARE:                      *UCPPPPAR
011900*                                                                *UCPPPPAR
012000*    VIEW               VIEW DDL AND DCL INCLUDE                 *UCPPPPAR
012100*                        MEMBER NAMES                            *UCPPPPAR
012200*                                                                *UCPPPPAR
012300*  UC0VGRP1_GRP          UC0VGRP1                                *UCPPPPAR
012400*  UC0VACC1_ACC          UC0VACC1                                *UCPPPPAR
012500*  UC0VACC2_ACC          UC0VACC2                                *UCPPPPAR
012600*  UC0VASC1_ASC          UC0VASC1                                *UCPPPPAR
012800******************************************************************UCPPPPAR
012900                                                                  UCPPPPAR
013000 LINKAGE SECTION.                                                 UCPPPPAR
013100                                                                  UCPPPPAR
013200 01  ARSM-INTERFACE.                                              UCPPPPAR
013300 COPY UCLNARSM.                                                   UCPPPPAR
013400                                                                  UCPPPPAR
013500 PROCEDURE DIVISION USING ARSM-INTERFACE.                         UCPPPPAR
013600                                                                  UCPPPPAR
013700     MOVE ARSM-AUTH-ID       TO WS-AUTH-ID                        UCPPPPAR
013800                                WS-ID.                            UCPPPPAR
013900     MOVE ARSM-RESOURCE-TYPE TO WS-RESOURCE-TYPE.                 UCPPPPAR
014000                                                                  UCPPPPAR
014100     PERFORM 1000-FIND-RULE.                                      UCPPPPAR
014200                                                                  UCPPPPAR
014300     GOBACK.                                                      UCPPPPAR
014400                                                                  UCPPPPAR
014500 1000-FIND-RULE.                                                  UCPPPPAR
014600                                                                  UCPPPPAR
014700     MOVE 'Y' TO WS-RULE-FLAG.                                    UCPPPPAR
014800                                                                  UCPPPPAR
014900     PERFORM 8110-OPEN-ACC-CURS.                                  UCPPPPAR
015000     PERFORM 1100-GET-RULE UNTIL RULE-NOT-FND                     UCPPPPAR
015100                           OR ARSM-PERMISSION = 'Y'.              UCPPPPAR
015200     PERFORM 8130-CLOSE-ACC-CURS.                                 UCPPPPAR
015300                                                                  UCPPPPAR
015400 1100-GET-RULE.                                                   UCPPPPAR
015500                                                                  UCPPPPAR
015600     PERFORM 8120-FETCH-ACC-CURS.                                 UCPPPPAR
015700                                                                  UCPPPPAR
015800     IF RULE-FND                                                  UCPPPPAR
015900        PERFORM 1200-CHECK-INTENT                                 UCPPPPAR
016000                                                                  UCPPPPAR
016100        IF WS-INTENT-LEVEL < WS-RULE-LEVEL                        UCPPPPAR
016200           CONTINUE                                               UCPPPPAR
016300        ELSE                                                      UCPPPPAR
016400           PERFORM 1300-PROCESS-RULE                              UCPPPPAR
016500        END-IF                                                    UCPPPPAR
016600                                                                  UCPPPPAR
016700        PERFORM UNTIL ARSM-PERMISSION = 'N'                       UCPPPPAR
016800            OR                                                    UCPPPPAR
016900         (CONJ-RULE OF WS-UC0VACC1 = SPACES)                      UCPPPPAR
017000                                                                  UCPPPPAR
017100          MOVE 'Y' TO WS-CONJ-FLAG                                UCPPPPAR
017200          MOVE CONJ-RULE OF WS-UC0VACC1 TO WS-CONJ-REF            UCPPPPAR
017300          PERFORM 8450-SELECT-CONJ-RULE                           UCPPPPAR
017400          MOVE 'N' TO ARSM-PERMISSION                             UCPPPPAR
017500          IF CONJ-FND                                             UCPPPPAR
017600            MOVE WS-UC0VACC2 TO WS-UC0VACC1                       UCPPPPAR
017700            PERFORM 1300-PROCESS-RULE                             UCPPPPAR
017800          END-IF                                                  UCPPPPAR
017900        END-PERFORM                                               UCPPPPAR
018000     END-IF.                                                      UCPPPPAR
018100                                                                  UCPPPPAR
018200 1200-CHECK-INTENT.                                               UCPPPPAR
018300                                                                  UCPPPPAR
018400     MOVE  ACCESS-PRIVILEGE OF WS-UC0VACC1                        UCPPPPAR
018500                              TO WS-PRIVILEGE.                    UCPPPPAR
018600     MOVE  99                 TO WS-LEVEL.                        UCPPPPAR
018700     PERFORM 1210-CHECK-PRIVILEGE VARYING WS-SUB-ACC FROM 1       UCPPPPAR
018800             BY 1 UNTIL WS-SUB-ACC > WS-SUB-ACC-MAX.              UCPPPPAR
018900                                                                  UCPPPPAR
019000     MOVE WS-LEVEL            TO WS-RULE-LEVEL.                   UCPPPPAR
019100                                                                  UCPPPPAR
019200     MOVE  ARSM-ACCESS-INTENT TO WS-PRIVILEGE.                    UCPPPPAR
019300     MOVE  99                 TO WS-LEVEL.                        UCPPPPAR
019400     PERFORM 1210-CHECK-PRIVILEGE VARYING WS-SUB-ACC FROM 1       UCPPPPAR
019500             BY 1 UNTIL WS-SUB-ACC > WS-SUB-ACC-MAX.              UCPPPPAR
019600                                                                  UCPPPPAR
019700     MOVE WS-LEVEL            TO WS-INTENT-LEVEL.                 UCPPPPAR
019800                                                                  UCPPPPAR
019900 1210-CHECK-PRIVILEGE.                                            UCPPPPAR
020000                                                                  UCPPPPAR
020100     IF  WS-PRIVILEGE  = WS-ACCESS (WS-SUB-ACC)                   UCPPPPAR
020200         MOVE WS-ACCESS-LEVEL (WS-SUB-ACC) TO WS-LEVEL            UCPPPPAR
020300     END-IF.                                                      UCPPPPAR
020400                                                                  UCPPPPAR
020500 1300-PROCESS-RULE.                                               UCPPPPAR
020600                                                                  UCPPPPAR
020700     IF  ACCESS-RULE OF WS-UC0VACC1 = 'UNIVRSAL'                  UCPPPPAR
020800        MOVE 'Y' TO ARSM-PERMISSION                               UCPPPPAR
020900     ELSE                                                         UCPPPPAR
021000        PERFORM 2000-FIND-ASSOC-DATA                              UCPPPPAR
021100     END-IF.                                                      UCPPPPAR
021200                                                                  UCPPPPAR
021300 2000-FIND-ASSOC-DATA.                                            UCPPPPAR
021400                                                                  UCPPPPAR
021500     MOVE 'Y' TO WS-ASSOC-FLAG.                                   UCPPPPAR
021600                                                                  UCPPPPAR
021700     MOVE ACCESS-RULE OF WS-UC0VACC1  TO WS-ACCESS-RULE.          UCPPPPAR
021800     MOVE IDENTIFIER  OF WS-UC0VACC1  TO WS-ID-RULE.              UCPPPPAR
021900     PERFORM 8210-OPEN-ASSOC-CURS.                                UCPPPPAR
022000     PERFORM 2100-GET-ASSOC-DATA UNTIL ASSOC-NOT-FND              UCPPPPAR
022100                                 OR ARSM-PERMISSION = 'Y'.        UCPPPPAR
022200                                                                  UCPPPPAR
022300     PERFORM 8230-CLOSE-ASSOC-CURS.                               UCPPPPAR
022400                                                                  UCPPPPAR
022500 2100-GET-ASSOC-DATA.                                             UCPPPPAR
022600                                                                  UCPPPPAR
022700     PERFORM 8220-FETCH-ASSOC-CURS.                               UCPPPPAR
022800                                                                  UCPPPPAR
022900     IF ASSOC-FND                                                 UCPPPPAR
023000        MOVE ARSM-RESOURCE-NAME TO WS-EMPLOYEE-ID                 UCPPPPAR
023100                                                                  UCPPPPAR
023200        IF ACCESS-RULE OF WS-UC0VACC1 = 'HMEDPT'                  UCPPPPAR
023300           PERFORM 2110-CHECK-HMEDPT-DATA                         UCPPPPAR
023400        ELSE                                                      UCPPPPAR
023500        IF ACCESS-RULE OF WS-UC0VACC1 = 'EDBRNG'                  UCPPPPAR
023600           PERFORM 2130-CHECK-EDBRNG-DATA                         UCPPPPAR
023700        END-IF                                                    UCPPPPAR
023800        END-IF                                                    UCPPPPAR
023900     END-IF.                                                      UCPPPPAR
024000                                                                  UCPPPPAR
024100 2110-CHECK-HMEDPT-DATA.                                          UCPPPPAR
024200                                                                  UCPPPPAR
024300     MOVE ASSOCIATION-DATA TO WS-ASSOC-DEPT.                      UCPPPPAR
024400                                                                  UCPPPPAR
024500     MOVE ARSM-QUALIFIERS TO WS-HOME-DEPT.                        UCPPPPAR
024600                                                                  UCPPPPAR
024700     IF  WS-ASSOC-DEPT = WS-HOME-DEPT                             UCPPPPAR
024800         MOVE 'Y'   TO ARSM-PERMISSION                            UCPPPPAR
024900     END-IF.                                                      UCPPPPAR
025000                                                                  UCPPPPAR
025100 2130-CHECK-EDBRNG-DATA.                                          UCPPPPAR
025200                                                                  UCPPPPAR
025300     MOVE ASSOCIATION-DATA TO WS-RANGE-VALUES.                    UCPPPPAR
025400                                                                  UCPPPPAR
025500     IF (WS-RANGE-VALUE-1 <  WS-EMPLOYEE-ID                       UCPPPPAR
025600                      OR                                          UCPPPPAR
025700         WS-RANGE-VALUE-1 =  WS-EMPLOYEE-ID)                      UCPPPPAR
025800                      AND                                         UCPPPPAR
025900        (WS-RANGE-VALUE-2 >  WS-EMPLOYEE-ID                       UCPPPPAR
026000                      OR                                          UCPPPPAR
026100         WS-RANGE-VALUE-2 =  WS-EMPLOYEE-ID)                      UCPPPPAR
026200         MOVE 'Y' TO ARSM-PERMISSION                              UCPPPPAR
026300     END-IF.                                                      UCPPPPAR
026400                                                                  UCPPPPAR
026500 8110-OPEN-ACC-CURS.                                              UCPPPPAR
026600                                                                  UCPPPPAR
026700     EXEC SQL                                                     UCPPPPAR
026800          OPEN ACC-CURS                                           UCPPPPAR
026900     END-EXEC.                                                    UCPPPPAR
027000                                                                  UCPPPPAR
027100     IF SQLCODE = -818                                            UCPPPPAR
027200        MOVE 'N' TO WS-RULE-FLAG                                  UCPPPPAR
027300        MOVE 1   TO ARSM-RETURN-CODE                              UCPPPPAR
027400     ELSE                                                         UCPPPPAR
027500     IF SQLCODE < ZERO                                            UCPPPPAR
027600        MOVE 'N' TO WS-RULE-FLAG                                  UCPPPPAR
027700        MOVE 2   TO ARSM-RETURN-CODE                              UCPPPPAR
027800     END-IF                                                       UCPPPPAR
027900     END-IF.                                                      UCPPPPAR
028000                                                                  UCPPPPAR
028100 8120-FETCH-ACC-CURS.                                             UCPPPPAR
028200                                                                  UCPPPPAR
028300     EXEC SQL                                                     UCPPPPAR
028400         FETCH ACC-CURS                                           UCPPPPAR
028500         INTO :WS-UC0VACC1                                        UCPPPPAR
028600     END-EXEC.                                                    UCPPPPAR
028700                                                                  UCPPPPAR
028800     IF SQLCODE = 100                                             UCPPPPAR
028900        MOVE 'N' TO WS-RULE-FLAG                                  UCPPPPAR
029000     ELSE                                                         UCPPPPAR
029100     IF SQLCODE = -818                                            UCPPPPAR
029200        MOVE 'N' TO WS-RULE-FLAG                                  UCPPPPAR
029300        MOVE 1   TO ARSM-RETURN-CODE                              UCPPPPAR
029400     ELSE                                                         UCPPPPAR
029500     IF SQLCODE < ZERO                                            UCPPPPAR
029600        MOVE 'N' TO WS-RULE-FLAG                                  UCPPPPAR
029700        MOVE 2   TO ARSM-RETURN-CODE                              UCPPPPAR
029800     END-IF                                                       UCPPPPAR
029900     END-IF                                                       UCPPPPAR
030000     END-IF.                                                      UCPPPPAR
030100                                                                  UCPPPPAR
030200 8130-CLOSE-ACC-CURS.                                             UCPPPPAR
030300                                                                  UCPPPPAR
030400     EXEC SQL                                                     UCPPPPAR
030500         CLOSE ACC-CURS                                           UCPPPPAR
030600     END-EXEC.                                                    UCPPPPAR
030700                                                                  UCPPPPAR
030800     IF SQLCODE = -818                                            UCPPPPAR
030900        MOVE 'N' TO WS-RULE-FLAG                                  UCPPPPAR
031000        MOVE 1   TO ARSM-RETURN-CODE                              UCPPPPAR
031100     ELSE                                                         UCPPPPAR
031200     IF SQLCODE < ZERO                                            UCPPPPAR
031300        MOVE 'N' TO WS-RULE-FLAG                                  UCPPPPAR
031400        MOVE 2   TO ARSM-RETURN-CODE                              UCPPPPAR
031500     END-IF                                                       UCPPPPAR
031600     END-IF.                                                      UCPPPPAR
031700                                                                  UCPPPPAR
031800 8210-OPEN-ASSOC-CURS.                                            UCPPPPAR
031900                                                                  UCPPPPAR
032000     EXEC SQL                                                     UCPPPPAR
032100          OPEN ASSOC-CURS                                         UCPPPPAR
032200     END-EXEC.                                                    UCPPPPAR
032300                                                                  UCPPPPAR
032400     IF SQLCODE = -818                                            UCPPPPAR
032500        MOVE 'N' TO WS-ASSOC-FLAG                                 UCPPPPAR
032600        MOVE 1   TO ARSM-RETURN-CODE                              UCPPPPAR
032700     ELSE                                                         UCPPPPAR
032800     IF SQLCODE < ZERO                                            UCPPPPAR
032900        MOVE 'N' TO WS-ASSOC-FLAG                                 UCPPPPAR
033000        MOVE 2   TO ARSM-RETURN-CODE                              UCPPPPAR
033100     END-IF                                                       UCPPPPAR
033200     END-IF.                                                      UCPPPPAR
033300                                                                  UCPPPPAR
033400 8220-FETCH-ASSOC-CURS.                                           UCPPPPAR
033500                                                                  UCPPPPAR
033600     EXEC SQL                                                     UCPPPPAR
033700         FETCH ASSOC-CURS                                         UCPPPPAR
033800         INTO :WS-UC0VASC1                                        UCPPPPAR
033900     END-EXEC.                                                    UCPPPPAR
034000                                                                  UCPPPPAR
034100     IF SQLCODE = 100                                             UCPPPPAR
034200        MOVE 'N' TO WS-ASSOC-FLAG                                 UCPPPPAR
034300     ELSE                                                         UCPPPPAR
034400     IF SQLCODE = -818                                            UCPPPPAR
034500        MOVE 'N' TO WS-ASSOC-FLAG                                 UCPPPPAR
034600        MOVE 1   TO ARSM-RETURN-CODE                              UCPPPPAR
034700     ELSE                                                         UCPPPPAR
034800     IF SQLCODE < ZERO                                            UCPPPPAR
034900        MOVE 'N' TO WS-ASSOC-FLAG                                 UCPPPPAR
035000        MOVE 2   TO ARSM-RETURN-CODE                              UCPPPPAR
035100     END-IF                                                       UCPPPPAR
035200     END-IF                                                       UCPPPPAR
035300     END-IF.                                                      UCPPPPAR
035400                                                                  UCPPPPAR
035500 8230-CLOSE-ASSOC-CURS.                                           UCPPPPAR
035600                                                                  UCPPPPAR
035700     EXEC SQL                                                     UCPPPPAR
035800         CLOSE ASSOC-CURS                                         UCPPPPAR
035900     END-EXEC.                                                    UCPPPPAR
036000                                                                  UCPPPPAR
036100     IF SQLCODE = -818                                            UCPPPPAR
036200        MOVE 'N' TO WS-ASSOC-FLAG                                 UCPPPPAR
036300        MOVE 1   TO ARSM-RETURN-CODE                              UCPPPPAR
036400     ELSE                                                         UCPPPPAR
036500     IF SQLCODE < ZERO                                            UCPPPPAR
036600        MOVE 'N' TO WS-ASSOC-FLAG                                 UCPPPPAR
036700        MOVE 2   TO ARSM-RETURN-CODE                              UCPPPPAR
036800     END-IF                                                       UCPPPPAR
036900     END-IF.                                                      UCPPPPAR
037000                                                                  UCPPPPAR
037100 8450-SELECT-CONJ-RULE.                                           UCPPPPAR
037200                                                                  UCPPPPAR
037300     EXEC SQL                                                     UCPPPPAR
037400          SELECT                                                  UCPPPPAR
037500              RESOURCE_TYPE,                                      UCPPPPAR
037600              ACCESS_RULE,                                        UCPPPPAR
037700              IDENTIFIER,                                         UCPPPPAR
037800              ACCESS_PRIORITY,                                    UCPPPPAR
037900              ACCESS_PRIVILEGE,                                   UCPPPPAR
038000              CONJ_RULE,                                          UCPPPPAR
038100              CONJ_IND,                                           UCPPPPAR
038200              CHANGED_BY                                          UCPPPPAR
038300          INTO :WS-UC0VACC2                                       UCPPPPAR
038400          FROM UC0VACC2_ACC                                       UCPPPPAR
038500          WHERE RESOURCE_TYPE    = :WS-RESOURCE-TYPE              UCPPPPAR
038600            AND (IDENTIFIER      = :WS-ID                         UCPPPPAR
038700                 OR IDENTIFIER   IN                               UCPPPPAR
038800                (SELECT GROUP_ID                                  UCPPPPAR
038900                 FROM   UC0VGRP1_GRP                              UCPPPPAR
039000                 WHERE  AUTH_ID  = :WS-AUTH-ID))                  UCPPPPAR
039100            AND ACCESS_RULE      = :WS-CONJ-REF                   UCPPPPAR
039200     END-EXEC                                                     UCPPPPAR
039300                                                                  UCPPPPAR
039400     MOVE 'Y' TO WS-CONJ-FLAG                                     UCPPPPAR
039500     EVALUATE SQLCODE                                             UCPPPPAR
039600     WHEN ZERO                                                    UCPPPPAR
039700        CONTINUE                                                  UCPPPPAR
039800     WHEN +100                                                    UCPPPPAR
039900        MOVE 'N' TO WS-CONJ-FLAG                                  UCPPPPAR
040000        MOVE 3   TO ARSM-RETURN-CODE                              UCPPPPAR
040100     WHEN -811                                                    UCPPPPAR
040200        MOVE 'N' TO WS-CONJ-FLAG                                  UCPPPPAR
040300        MOVE 3   TO ARSM-RETURN-CODE                              UCPPPPAR
040400     WHEN -818                                                    UCPPPPAR
040500        MOVE 'N' TO WS-CONJ-FLAG                                  UCPPPPAR
040600        MOVE 1   TO ARSM-RETURN-CODE                              UCPPPPAR
040700     WHEN OTHER                                                   UCPPPPAR
040800        MOVE 'N' TO WS-CONJ-FLAG                                  UCPPPPAR
040900        MOVE 2   TO ARSM-RETURN-CODE                              UCPPPPAR
041000     END-EVALUATE.                                                UCPPPPAR
