000000**************************************************************/   48441334
000001*  PROGRAM: UCTAXMLC                                         */   48441334
000002*  RELEASE: ___1334______ SERVICE REQUEST(S): ____14844____  */   48441334
000003*  NAME:_______QUAN______ CREATION DATE:      ___01/11/01__  */   48441334
000004*  DESCRIPTION:                                              */   48441334
000005*    W-4/DE4 EMAIL TRIGGER DRIVER - CICS VERSION             */   48441334
000008**************************************************************/   48441334
000900 IDENTIFICATION DIVISION.                                         UCTAXMLC
001000 PROGRAM-ID. UCTAXMLC.                                            UCTAXMLC
001100 ENVIRONMENT DIVISION.                                            UCTAXMLC
001200 CONFIGURATION SECTION.                                           UCTAXMLC
001300 SOURCE-COMPUTER.                                                 UCTAXMLC
001400 COPY  CPOTXUCS.                                                  UCTAXMLC
001500 DATA DIVISION.                                                   UCTAXMLC
001600 WORKING-STORAGE SECTION.                                         UCTAXMLC
001700                                                                  UCTAXMLC
001800 01  WS-COMN.                                                     UCTAXMLC
001900   05  WS-START                          PIC X(27) VALUE          UCTAXMLC
002000       'WORKING STORAGE STARTS HERE'.                             UCTAXMLC
002100   05  WS-PGM-NAME                       PIC X(08)                UCTAXMLC
002200                                         VALUE 'UCTAXMLC'.        UCTAXMLC
002300   05  WS-EMAIL-Q                        PIC X(04) VALUE 'TAXQ'.  UCTAXMLC
002310   05  WS-RESP                           PIC S9(08) COMP.         UCTAXMLC
002400                                                                  UCTAXMLC
002500 01  ONLINE-SIGNALS EXTERNAL.                                     UCTAXMLC
002600     COPY CPWSONLI.                                               UCTAXMLC
002700                                                                  UCTAXMLC
002800 01  UCWSTAXM  EXTERNAL.                                          UCTAXMLC
002900     COPY UCWSTAXM.                                               UCTAXMLC
003000                                                                  UCTAXMLC
003100 01  UCWSABND  EXTERNAL.                                          UCTAXMLC
003200     COPY UCWSABND.                                               UCTAXMLC
003300                                                                  UCTAXMLC
003400 LINKAGE SECTION.                                                 UCTAXMLC
003500                                                                  UCTAXMLC
003600 PROCEDURE DIVISION.                                              UCTAXMLC
003700                                                                  UCTAXMLC
003800 0000-MAINLINE                        SECTION.                    UCTAXMLC
003820                                                                  UCTAXMLC
003900     SET ADDRESS OF DFHEIBLK    TO CPWSONLI-EIB-PTR               UCTAXMLC
004000     SET ADDRESS OF DFHCOMMAREA TO CPWSONLI-COMMAREA-PTR          UCTAXMLC
004010                                                                  UCTAXMLC
004100     EXEC CICS                                                    UCTAXMLC
004200       WRITEQ TD                                                  UCTAXMLC
004300       QUEUE(WS-EMAIL-Q)                                          UCTAXMLC
004400       FROM(UCWSTAXM-DATA)                                        UCTAXMLC
004500       LENGTH(LENGTH OF UCWSTAXM-DATA)                            UCTAXMLC
004510       RESP(WS-RESP)                                              UCTAXMLC
004600     END-EXEC.                                                    UCTAXMLC
004700                                                                  UCTAXMLC
004800**** IF WS-RESP NOT = DFHRESP(NORMAL)   PRE-TRANSLATED CODE       UCTAXMLC
004900     IF WS-RESP NOT = DFHRESP(NORMAL)                             UCTAXMLC
005000       MOVE 'NONZERO RESP FROM TAXQ WRITEQ'                       UCTAXMLC
005001                             TO UCWSABND-ABEND-DATA (1)           UCTAXMLC
005002       MOVE WS-RESP                                               UCTAXMLC
005003                             TO UCWSABND-ABEND-DATA (2)           UCTAXMLC
005010       PERFORM 9999-ABEND                                         UCTAXMLC
005100     END-IF.                                                      UCTAXMLC
005200                                                                  UCTAXMLC
005300     GOBACK.                                                      UCTAXMLC
005400 0000-EXIT.                                                       UCTAXMLC
005500     EXIT.                                                        UCTAXMLC
005600                                                                  UCTAXMLC
005700                                                                  UCTAXMLC
005800 EJECT                                                            UCTAXMLC
005900**************************************************************    UCTAXMLC
006000*  ABEND HANDLING SECTION                                    *    UCTAXMLC
006100**************************************************************    UCTAXMLC
006200 9999-ABEND    SECTION.                                           UCTAXMLC
006300     MOVE WS-PGM-NAME TO UCWSABND-ABENDING-PGM                    UCTAXMLC
006400     COPY UCPDABND.                                               UCTAXMLC
006500 9999-EXIT.                                                       UCTAXMLC
006600     EXIT.                                                        UCTAXMLC
006700***************    END OF SOURCE - UCTAXMLC    *******************UCTAXMLC
