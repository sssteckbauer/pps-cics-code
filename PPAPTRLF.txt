000810**************************************************************/   36580967
000820*  PROGRAM: PPAPTRLF                                         */   36580967
000830*  RELEASE # ___0967___   SERVICE REQUEST NO(S)____3658______*/   36580967
000840*  NAME _____MLG_______   MODIFICATION DATE ____03/10/95_____*/   36580967
000850*  DESCRIPTION:                                              */   36580967
000860*      OPTR LOGICAL FUNCTION APPLICATION PROCESSOR           */   36580967
000880**************************************************************/   36580967
000900 IDENTIFICATION DIVISION.                                         PPAPTRLF
001000 PROGRAM-ID. PPAPTRLF.                                            PPAPTRLF
001100 ENVIRONMENT DIVISION.                                            PPAPTRLF
001200 CONFIGURATION SECTION.                                           PPAPTRLF
001300 SOURCE-COMPUTER.                 COPY CPOTXUCS.                  PPAPTRLF
001400 OBJECT-COMPUTER.                 COPY CPOTXOBJ.                  PPAPTRLF
001500 DATA DIVISION.                                                   PPAPTRLF
001600                                                                  PPAPTRLF
001700 WORKING-STORAGE SECTION.                                         PPAPTRLF
001800 01  WS-COMN.                                                     PPAPTRLF
001900   05  WS-START             PIC X(27) VALUE                       PPAPTRLF
002000       'WORKING STORAGE STARTS HERE'.                             PPAPTRLF
002100   05  WS-REL-NO            PIC X(05) VALUE 'I0886'.              PPAPTRLF
002200   05  WS-PGM-NAME          PIC X(08) VALUE 'PPAPTRLF'.           PPAPTRLF
002300   05  WS-PPGETTIM-PGM      PIC X(08) VALUE 'PPGETTIM'.           PPAPTRLF
002500   05  WS-RESP              PIC S9(8) COMP.                       PPAPTRLF
002600   05  WS-RETURN-CODE       PIC 9(04).                            PPAPTRLF
005000                                                                  PPAPTRLF
005010 01  WS-MESSAGES.                                                 PPAPTRLF
005020     05  MP0165             PIC X(05) VALUE 'P0165'.              PPAPTRLF
005030                                                                  PPAPTRLF
005100 01  CPWSTRIF    EXTERNAL.                                        PPAPTRLF
005200     COPY CPWSTRIF.                                               PPAPTRLF
005300                                                                  PPAPTRLF
008200 01  UCCOMMON    EXTERNAL.                                        PPAPTRLF
008300     EXEC SQL                                                     PPAPTRLF
008400      INCLUDE UCCOMMON                                            PPAPTRLF
008500     END-EXEC.                                                    PPAPTRLF
008600                                                                  PPAPTRLF
008700 01  UCWSABND    EXTERNAL.                                        PPAPTRLF
008800     COPY UCWSABND.                                               PPAPTRLF
008900                                                                  PPAPTRLF
009000 LINKAGE SECTION.                                                 PPAPTRLF
009100                                                                  PPAPTRLF
009200     EJECT                                                        PPAPTRLF
009300                                                                  PPAPTRLF
009400 PROCEDURE DIVISION.                                              PPAPTRLF
009500                                                                  PPAPTRLF
009600 0000-MAINLINE                        SECTION.                    PPAPTRLF
009700******************************************************************PPAPTRLF
009800*  THIS IS THE MAIN LINE PROCESSING SECTION.                     *PPAPTRLF
009900******************************************************************PPAPTRLF
010000                                                                  PPAPTRLF
010100     SET UCCOMMON-DISPLAY-DETAIL TO TRUE.                         PPAPTRLF
010200                                                                  PPAPTRLF
010300     EVALUATE UCCOMMON-LOGICAL-FUNCTION                           PPAPTRLF
010400       WHEN 'PRT-CHECKLST'                                        PPAPTRLF
010500         PERFORM 1000-CALL-PPGETTIM                               PPAPTRLF
010800       WHEN OTHER                                                 PPAPTRLF
010900         STRING                                                   PPAPTRLF
011000           'INVALID LOGICAL FUNCTION = '  DELIMITED BY SIZE       PPAPTRLF
011100            UCCOMMON-LOGICAL-FUNCTION DELIMITED BY SIZE           PPAPTRLF
011200            INTO UCWSABND-ABEND-DATA(1)                           PPAPTRLF
011300         END-STRING                                               PPAPTRLF
011400         PERFORM 9999-ABEND                                       PPAPTRLF
011500     END-EVALUATE.                                                PPAPTRLF
011600                                                                  PPAPTRLF
011700     GOBACK.                                                      PPAPTRLF
011800                                                                  PPAPTRLF
011900 0000-EXIT.                                                       PPAPTRLF
012000     EXIT.                                                        PPAPTRLF
012100                                                                  PPAPTRLF
012200     EJECT                                                        PPAPTRLF
012300                                                                  PPAPTRLF
012400 1000-CALL-PPGETTIM   SECTION.                                    PPAPTRLF
012500******************************************************************PPAPTRLF
012600*  CALL PPGETTIM TO GENERATE THE PRINTED CHECKLIST               *PPAPTRLF
012710******************************************************************PPAPTRLF
012800                                                                  PPAPTRLF
012810     SET CPWSTRIF-PRINT-CHECKLIST TO TRUE.                        PPAPTRLF
012820                                                                  PPAPTRLF
012900     CALL WS-PPGETTIM-PGM USING DFHEIBLK                          PPAPTRLF
013000       ON EXCEPTION                                               PPAPTRLF
013100          STRING 'ERROR CALLING PROGRAM: ' DELIMITED BY SIZE      PPAPTRLF
013200                 WS-PPGETTIM-PGM            DELIMITED BY SIZE     PPAPTRLF
013300                      INTO UCWSABND-ABEND-DATA (1)                PPAPTRLF
013400          END-STRING                                              PPAPTRLF
013500          PERFORM 9999-ABEND                                      PPAPTRLF
013600     END-CALL.                                                    PPAPTRLF
013700                                                                  PPAPTRLF
013800     IF UCCOMMON-MSG-NUMBER-NOT-SET                               PPAPTRLF
013810        MOVE MP0165 TO UCCOMMON-MSG-NUMBER                        PPAPTRLF
013820     END-IF.                                                      PPAPTRLF
013830                                                                  PPAPTRLF
013900 1000-EXIT.                                                       PPAPTRLF
014000     EXIT.                                                        PPAPTRLF
014100                                                                  PPAPTRLF
014200     EJECT                                                        PPAPTRLF
014300                                                                  PPAPTRLF
043000 9999-ABEND    SECTION.                                           PPAPTRLF
043100**************************************************************    PPAPTRLF
043200*  COPIED PROCEDURE DIVISION CODE FOR ABEND HANDLING         *    PPAPTRLF
043300**************************************************************    PPAPTRLF
043400                                                                  PPAPTRLF
043500     MOVE 'PPAPTRLF' TO UCWSABND-ABENDING-PGM.                    PPAPTRLF
043600                                                                  PPAPTRLF
043700     COPY UCPDABND.                                               PPAPTRLF
043800                                                                  PPAPTRLF
043900 9999-EXIT.                                                       PPAPTRLF
044000     EXIT.                                                        PPAPTRLF
044100***************    END OF SOURCE - PPAPTRLF    *******************PPAPTRLF
