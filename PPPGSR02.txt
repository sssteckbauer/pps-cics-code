000100**************************************************************/   36560949
000200*  PROGRAM: PPPGSR02                                         */   36560949
000300*  RELEASE: ___0949______ SERVICE REQUEST(S): ____3656_____  */   36560949
000400*  NAME:_____JYL_________ MODIFICATION DATE:  ___12/14/94__  */   36560949
000500*  DESCRIPTION: NEW ONLINE MODULE USED TO REFORMAT AND STORE */   36560949
000600*   GRADUATE STUDENT REPORTING RECORDS ON THE PPPQUF TABLE.  */   36560949
000700**************************************************************/   36560949
000800*----------------------------------------------------------------*PPPGSR02
000900 IDENTIFICATION DIVISION.                                         PPPGSR02
001000 PROGRAM-ID. PPPGSR02.                                            PPPGSR02
001100 AUTHOR. UCOP.                                                    PPPGSR02
001200 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPPGSR02
001300*REMARKS.                                                         PPPGSR02
001400******************************************************************PPPGSR02
001500******************************************************************PPPGSR02
001600 DATE-WRITTEN.  DECEMBER 1994.                                    PPPGSR02
001700 DATE-COMPILED. XX/XX/XX.                                         PPPGSR02
001800 ENVIRONMENT DIVISION.                                            PPPGSR02
001900 CONFIGURATION SECTION.                                           PPPGSR02
002000 SOURCE-COMPUTER.                                  COPY CPOTXUCS. PPPGSR02
002100 OBJECT-COMPUTER.                                  COPY CPOTXOBJ. PPPGSR02
002200 INPUT-OUTPUT SECTION.                                            PPPGSR02
002300 FILE-CONTROL.                                                    PPPGSR02
002400     EJECT                                                        PPPGSR02
002500 DATA DIVISION.                                                   PPPGSR02
002600 FILE SECTION.                                                    PPPGSR02
002700*----------------------------------------------------------------*PPPGSR02
002800 WORKING-STORAGE SECTION.                                         PPPGSR02
002900*----------------------------------------------------------------*PPPGSR02
003000 01  A-STND-PROG-ID                 PIC X(8) VALUE 'PPPGSR02'.    PPPGSR02
003100*                                                                 PPPGSR02
003200 01  WS-MISC-DATA.                                                PPPGSR02
003300     05  WS-QUF-SEQ                 PIC S9(3)  VALUE ZERO.        PPPGSR02
003400     05  WS-USA-ZERO-DATE      PIC X(10) VALUE '01/01/0001'.      PPPGSR02
003500     05  ASUB                       PIC S9(3)  COMP VALUE +0.     PPPGSR02
003600     05  DSUB                       PIC S9(3)  COMP VALUE +0.     PPPGSR02
003700     05  LAST-DIST                  PIC S9(3)  COMP VALUE +0.     PPPGSR02
003800     05  DIST-MATCH-FLAG            PIC X(1).                     PPPGSR02
003900         88 DIST-PROCESSED                          VALUE 'Y'.    PPPGSR02
004000         88 NO-DIST-MATCH                           VALUE 'N'.    PPPGSR02
004100*                                                                 PPPGSR02
004200 01  CALLED-MODULES.                             COPY CPWSXRTN.   PPPGSR02
004300*                                                                 PPPGSR02
004400 01  PPDB2MSG-INTERFACE.                         COPY CPLNKDB2.   PPPGSR02
004500*                                                                 PPPGSR02
004600 01  GRADUATE-STUDENT-RECORD.                    COPY CPWSGSRR.   PPPGSR02
004700     EJECT                                                        PPPGSR02
004800******************************************************************PPPGSR02
004900**     E  X  T  E  R  N  A  L  S      S  E  C  T  I  O  N       **PPPGSR02
005000**--------------------------------------------------------------**PPPGSR02
005100*                                                                 PPPGSR02
005200 01  GRADUATE-STUDENT-RECORDS      EXTERNAL.     COPY CPWSGSRX.   PPPGSR02
005300*                                                                 PPPGSR02
005400 01  UCWSABND                      EXTERNAL.     COPY UCWSABND.   PPPGSR02
005500*                                                                 PPPGSR02
005600 01  CPWSGETM                      EXTERNAL.     COPY CPWSGETM.   PPPGSR02
005700*                                                                 PPPGSR02
005800 01  DOST-DESCR-OF-SERV-TABLE      EXTERNAL.     COPY CPWSDOST.   PPPGSR02
005900*                                                                 PPPGSR02
006000******************************************************************PPPGSR02
006100**  DATA BASE AREAS                                             **PPPGSR02
006200******************************************************************PPPGSR02
006300 01  QUF-ROW.                                                     PPPGSR02
006400     EXEC SQL INCLUDE PPPVZQUF END-EXEC.                          PPPGSR02
006500**                                                                PPPGSR02
006600     EXEC SQL                                                     PPPGSR02
006700         INCLUDE SQLCA                                            PPPGSR02
006800     END-EXEC.                                                    PPPGSR02
006900     EJECT                                                        PPPGSR02
007000****************************************************************  PPPGSR02
007100 LINKAGE SECTION.                                                 PPPGSR02
007200*                                                                 PPPGSR02
007300 01  CPWSXCWA.                              COPY CPWSXCWA.        PPPGSR02
007400*                                                                 PPPGSR02
007500 01  TABLE-AREA.                                                  PPPGSR02
007600     05 TABLE-DATA               PIC X(400000).                   PPPGSR02
007700     EJECT                                                        PPPGSR02
007800*----------------------------------------------------------------*PPPGSR02
007900 PROCEDURE DIVISION.                                              PPPGSR02
008000*----------------------------------------------------------------*PPPGSR02
008100 0000-MAINLINE  SECTION.                                          PPPGSR02
008200*----------------------------------------------------------------*PPPGSR02
008300     EXEC CICS ADDRESS                                            PPPGSR02
008400         EIB(ADDRESS OF DFHEIBLK)                                 PPPGSR02
008500         COMMAREA(ADDRESS OF DFHCOMMAREA)                         PPPGSR02
008600     END-EXEC.                                                    PPPGSR02
008700*----------------------------------------------------------------*PPPGSR02
008800     EXEC SQL                                                     PPPGSR02
008900          INCLUDE CPPDXE99                                        PPPGSR02
009000     END-EXEC.                                                    PPPGSR02
009100****************************************************************  PPPGSR02
009200     MOVE A-STND-PROG-ID  TO DB2MSG-PGM-ID                        PPPGSR02
009300*                                                                 PPPGSR02
009400     SET CPWSGETM-CWAADDR-FUNCTION TO TRUE.                       PPPGSR02
009500     SET CPWSGETM-PPGETMN-NAME TO TRUE.                           PPPGSR02
009600     CALL CPWSGETM-PPGETMN USING DFHEIBLK DFHCOMMAREA.            PPPGSR02
009700     SET ADDRESS OF CPWSXCWA TO CPWSGETM-CWA-PTR.                 PPPGSR02
009800     IF CPWSXCWA-DB2-DOS-PTR-9 = ZERO                             PPPGSR02
009900         MOVE 'DOS TABLE MISSING ' TO UCWSABND-ABEND-DATA (1)     PPPGSR02
010000         PERFORM 9999-ABEND                                       PPPGSR02
010100     ELSE                                                         PPPGSR02
010200         SET ADDRESS OF TABLE-AREA TO CPWSXCWA-DB2-DOS-PTR        PPPGSR02
010300         MOVE TABLE-DATA(1:4)                                     PPPGSR02
010400                        TO DOST-DESCR-OF-SERV-TABLE(1:4)          PPPGSR02
010500         MOVE TABLE-DATA (1:LENGTH OF DOST-DESCR-OF-SERV-TABLE)   PPPGSR02
010600                        TO DOST-DESCR-OF-SERV-TABLE               PPPGSR02
010700     END-IF.                                                      PPPGSR02
010800*                                                                 PPPGSR02
010900     CALL PGM-PPPGSR01.                                           PPPGSR02
011000     IF GSRX-RETURN-OK                                            PPPGSR02
011100         PERFORM 2000-FORMAT-GRAD-RECORDS                         PPPGSR02
011200     ELSE                                                         PPPGSR02
011300         MOVE 'PPPGSR01 SQL ERROR' TO UCWSABND-ABEND-DATA (1)     PPPGSR02
011400         PERFORM 9999-ABEND                                       PPPGSR02
011500     END-IF.                                                      PPPGSR02
011600*                                                                 PPPGSR02
011700     GOBACK.                                                      PPPGSR02
011800*                                                                 PPPGSR02
011900*----------------------------------------------------------------*PPPGSR02
012000 2000-FORMAT-GRAD-RECORDS  SECTION.                               PPPGSR02
012100*----------------------------------------------------------------*PPPGSR02
012200     INITIALIZE GSRR-STUDENT-REPORTING.                           PPPGSR02
012300     MOVE WS-USA-ZERO-DATE TO GSR1-APPT-BEGIN-DATE                PPPGSR02
012400                              GSR1-APPT-END-DATE                  PPPGSR02
012500                              GSR2-APPT-BEGIN-DATE                PPPGSR02
012600                              GSR2-APPT-END-DATE                  PPPGSR02
012700                              GSR1-PAY-BEGIN-DATE                 PPPGSR02
012800                              GSR1-PAY-END-DATE                   PPPGSR02
012900                              GSR2-PAY-BEGIN-DATE                 PPPGSR02
013000                              GSR2-PAY-END-DATE.                  PPPGSR02
013100     SET NO-DIST-MATCH TO TRUE.                                   PPPGSR02
013200     MOVE ZERO TO ASUB, DSUB, LAST-DIST, WS-QUF-SEQ.              PPPGSR02
013300     MOVE CPWSGSRX-EMP-DATA TO GSRR-EMP-DATA.                     PPPGSR02
013400*                                                                 PPPGSR02
013500     IF CPWSGSRX-APPT-COUNT > ZERO                                PPPGSR02
013600         PERFORM 3000-PROCESS-APPTS                               PPPGSR02
013700              VARYING ASUB FROM +1 BY +1 UNTIL                    PPPGSR02
013800              ASUB > CPWSGSRX-APPT-COUNT                          PPPGSR02
013900     ELSE                                                         PPPGSR02
014000         PERFORM 4000-FORMAT-QUF-RECORD                           PPPGSR02
014100     END-IF.                                                      PPPGSR02
014200*                                                                 PPPGSR02
014300*----------------------------------------------------------------*PPPGSR02
014400 3000-PROCESS-APPTS   SECTION.                                    PPPGSR02
014500*----------------------------------------------------------------*PPPGSR02
014600     MOVE CPWSGSRX-APPT-DATA (ASUB) TO GSRR-APPT-DATA.            PPPGSR02
014700     INITIALIZE GSRR-DIST-DATA.                                   PPPGSR02
014800     MOVE WS-USA-ZERO-DATE TO GSR1-PAY-BEGIN-DATE                 PPPGSR02
014900                              GSR1-PAY-END-DATE                   PPPGSR02
015000                              GSR2-PAY-BEGIN-DATE                 PPPGSR02
015100                              GSR2-PAY-END-DATE.                  PPPGSR02
015200     IF CPWSGSRX-DIST-COUNT > ZERO                                PPPGSR02
015300         SET NO-DIST-MATCH TO TRUE                                PPPGSR02
015400         MOVE LAST-DIST TO DSUB                                   PPPGSR02
015500         PERFORM 3500-PROCESS-DIST                                PPPGSR02
015600         IF NO-DIST-MATCH                                         PPPGSR02
015700             PERFORM 4000-FORMAT-QUF-RECORD                       PPPGSR02
015800         END-IF                                                   PPPGSR02
015900     ELSE                                                         PPPGSR02
016000         PERFORM 4000-FORMAT-QUF-RECORD                           PPPGSR02
016100     END-IF.                                                      PPPGSR02
016200*                                                                 PPPGSR02
016300*----------------------------------------------------------------*PPPGSR02
016400 3500-PROCESS-DIST  SECTION.                                      PPPGSR02
016500*----------------------------------------------------------------*PPPGSR02
016600 3510-LOOP.                                                       PPPGSR02
016700     ADD +1 TO DSUB.                                              PPPGSR02
016800     IF DSUB > CPWSGSRX-DIST-COUNT                                PPPGSR02
016900         NEXT SENTENCE                                            PPPGSR02
017000     ELSE                                                         PPPGSR02
017100         MOVE CPWSGSRX-DIST-DATA (DSUB) TO GSRR-DIST-DATA         PPPGSR02
017200         IF GSR1-DIST-NUM1 = GSR1-APPT-NUM1                       PPPGSR02
017300             PERFORM 4000-FORMAT-QUF-RECORD                       PPPGSR02
017400             INITIALIZE GSRR-DIST-DATA                            PPPGSR02
017500             MOVE WS-USA-ZERO-DATE TO GSR1-PAY-BEGIN-DATE         PPPGSR02
017600                                      GSR1-PAY-END-DATE           PPPGSR02
017700                                      GSR2-PAY-BEGIN-DATE         PPPGSR02
017800                                      GSR2-PAY-END-DATE           PPPGSR02
017900             SET DIST-PROCESSED TO TRUE                           PPPGSR02
018000             MOVE DSUB TO LAST-DIST                               PPPGSR02
018100             GO TO 3510-LOOP                                      PPPGSR02
018200         ELSE                                                     PPPGSR02
018300             IF GSR1-DIST-NUM1 > GSR1-APPT-NUM1                   PPPGSR02
018400                 INITIALIZE GSRR-DIST-DATA                        PPPGSR02
018500                 MOVE WS-USA-ZERO-DATE TO GSR1-PAY-BEGIN-DATE     PPPGSR02
018600                                          GSR1-PAY-END-DATE       PPPGSR02
018700                                          GSR2-PAY-BEGIN-DATE     PPPGSR02
018800                                          GSR2-PAY-END-DATE       PPPGSR02
018900             ELSE                                                 PPPGSR02
019000                 INITIALIZE GSRR-DIST-DATA                        PPPGSR02
019100                 MOVE WS-USA-ZERO-DATE TO GSR1-PAY-BEGIN-DATE     PPPGSR02
019200                                          GSR1-PAY-END-DATE       PPPGSR02
019300                                          GSR2-PAY-BEGIN-DATE     PPPGSR02
019400                                          GSR2-PAY-END-DATE       PPPGSR02
019500                 GO TO 3510-LOOP                                  PPPGSR02
019600             END-IF                                               PPPGSR02
019700         END-IF                                                   PPPGSR02
019800     END-IF.                                                      PPPGSR02
019900*                                                                 PPPGSR02
020000*----------------------------------------------------------------*PPPGSR02
020100 4000-FORMAT-QUF-RECORD    SECTION.                               PPPGSR02
020200*----------------------------------------------------------------*PPPGSR02
020300     MOVE 'GRADDATA'             TO QUF-DD-NAME.                  PPPGSR02
020400     ADD +1 TO WS-QUF-SEQ.                                        PPPGSR02
020500     MOVE WS-QUF-SEQ             TO QUF-PROCESS-SEQ.              PPPGSR02
020600     MOVE GSR1-EMPLOYEE-ID       TO QUF-EMPLOYEE-ID.              PPPGSR02
020700     MOVE GSRR-STUDENT-REPORTING TO QUF-RECORD-TEXT.              PPPGSR02
020800     MOVE GSRR-QUF-REC-LENGTH    TO QUF-RECORD-LEN.               PPPGSR02
020900     MOVE 'QUF-INSERT     '      TO DB2MSG-TAG.                   PPPGSR02
021000*                                                                 PPPGSR02
021100     EXEC SQL INSERT                                              PPPGSR02
021200       INTO PPPVZQUF_QUF                                          PPPGSR02
021300          (QUF_DD_NAME,                                           PPPGSR02
021400           QUF_EMPLOYEE_ID,                                       PPPGSR02
021500           QUF_PROCESS_SEQ,                                       PPPGSR02
021600           QUF_REC_TIME,                                          PPPGSR02
021700           QUF_RECORD)                                            PPPGSR02
021800       VALUES                                                     PPPGSR02
021900          (:QUF-DD-NAME,                                          PPPGSR02
022000           :QUF-EMPLOYEE-ID,                                      PPPGSR02
022100           :QUF-PROCESS-SEQ,                                      PPPGSR02
022200           CURRENT TIMESTAMP,                                     PPPGSR02
022300           :QUF-RECORD)                                           PPPGSR02
022400     END-EXEC.                                                    PPPGSR02
022500*                                                                 PPPGSR02
022600*----------------------------------------------------------------*PPPGSR02
022700 9999-ABEND  SECTION.                                             PPPGSR02
022800*----------------------------------------------------------------*PPPGSR02
022900     MOVE 'PPPGSR02' TO UCWSABND-ABENDING-PGM.                    PPPGSR02
023000     COPY UCPDABND.                                               PPPGSR02
023100*                                                                 PPPGSR02
023200******************************************************************PPPGSR02
023300*999999-SQL-ERROR.                                               *PPPGSR02
023400******************************************************************PPPGSR02
023500*    THIS CODE IS EXECUTED IF A NEGATIVE SQLCODE IS RETURNED     *PPPGSR02
023600******************************************************************PPPGSR02
023700*                                                                 PPPGSR02
023800     EXEC SQL                                                     PPPGSR02
023900          INCLUDE CPPDXP99                                        PPPGSR02
024000     END-EXEC.                                                    PPPGSR02
024100     MOVE 'SQL ERROR - SEE PRECEDING DB2 MESSAGES'                PPPGSR02
024200         TO UCWSABND-ABEND-DATA (1).                              PPPGSR02
024300     PERFORM 9999-ABEND.                                          PPPGSR02
024400******************************************************************PPPGSR02
024500**   E N D  S O U R C E   ----  PPPGSR02 ----                   **PPPGSR02
024600******************************************************************PPPGSR02
