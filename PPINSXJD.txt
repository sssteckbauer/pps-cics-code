000001**************************************************************/   36400897
000002*  PROGRAM: PPINSXJD                                         */   36400897
000003*  RELEASE: ___0897______ SERVICE REQUEST(S): _____3640____  */   36400897
000004*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___05/02/94__  */   36400897
000005*  DESCRIPTION:                                              */   36400897
000006*  - ERROR REPORT 1150:                                      */   36400897
000007*    MODIFY CODE TO REFLECT NEW DATA STRUCTURE IN CPWSEFDT   */   36400897
000008*    AND CPPDEFDT.                                           */   36400897
000009**************************************************************/   36400897
000001**************************************************************/   12570722
000002*  PROGRAM: PPINSXJD                                         */   12570722
000003*  RELEASE: ___0722______ SERVICE REQUEST(S): ____11257____  */   12570722
000004*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___12/08/92__  */   12570722
000005*  DESCRIPTION:                                              */   12570722
000006*   -ONLY U AND UUU LEGAL PLANS VALID ON BRT TABLE; PERFORM  */   12570722
000007*    LOOKUP 2 TIMES. RETURN ABORT RC WHEN INVALID CODE.      */   12570722
000008**************************************************************/   12570722
000010**************************************************************/   05390718
000020*  PROGRAM: PPINSXJD                                         */   05390718
000030*  RELEASE: ___0718______ SERVICE REQUEST(S): ____10539____  */   05390718
000040*  REFERENCE RELEASE: ___651___                              */   05390718
000050*  NAME:__H. TRUONG _____ MODIFICATION DATE:  ___11/23/92__  */   05390718
000060*  DESCRIPTION:                                              */   05390718
000070*  - MODIFIED CODE TO ACCOMMODATE THE NEW RULE WHEN TO TAKE  */   05390718
000080*    INSURANCE DEDUCTIONS.                                   */   05390718
000090**************************************************************/   05390718
000100**************************************************************/   36220651
000200*  PROGRAM: PPINSXJD                                         */   36220651
000300*  RELEASE # __0651____   SERVICE REQUEST NO(S)____3622______*/   36220651
000400*  NAME __C RIDLON_____   MODIFICATION DATE ____04/13/92_____*/   36220651
000500*  DESCRIPTION                                               */   36220651
000600*                                                            */   36220651
000700*        CHANGED PROGRAM TO USE COVERAGE EFFECTIVE DATES     */   36220651
000800*                                                            */   36220651
000900*                                                            */   36220651
001000**************************************************************/   36220651
001010**************************************************************/   36190594
001020*  PROGRAM: PPINSXJD                                         */   36190594
001030*  RELEASE # ___0594___   SERVICE REQUEST NO(S)____3619______*/   36190594
001040*  NAME __C RIDLON_____   MODIFICATION DATE ____08/09/91_____*/   36190594
001050*  DESCRIPTION                                               */   36190594
001060*                                                            */   36190594
001070*        THIS IS A COMPLETE REPLACEMENT OF PPINSXJD          */   36190594
001080*                AS ISSUED IN RELEASE 0541                   */   36190594
001090*                                                            */   36190594
001091**************************************************************/   36190594
001100 IDENTIFICATION DIVISION.                                         PPINSXJD
001200 PROGRAM-ID. PPINSXJD.                                            PPINSXJD
001300 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPINSXJD
001400 AUTHOR.                                                          PPINSXJD
001500 DATE-WRITTEN.                                                    PPINSXJD
001600 DATE-COMPILED.                                                   PPINSXJD
001700*REMARKS.                                                         PPINSXJD
001800******************************************************************PPINSXJD
001900*                                                                *PPINSXJD
002000*  THIS MODULE PERFORMS GROUP LEGAL PLAN PREMIUM CALCULATIONS.   *PPINSXJD
002100* IN ADDITION IT RETRIEVES GROUP LEGAL PLAN RATES FROM THE       *PPINSXJD
002200* BENEFITS RATES TABLE BY CALLING PROGRAMS.                      *PPINSXJD
002300*                                                                *PPINSXJD
002400*                                                                *PPINSXJD
002500*                                                                *PPINSXJD
002600******************************************************************PPINSXJD
002700 ENVIRONMENT DIVISION.                                            PPINSXJD
002800     SKIP3                                                        PPINSXJD
002900******************************************************************PPINSXJD
003000*                                                                *PPINSXJD
003100*                         DATA DIVISION                          *PPINSXJD
003200*                                                                *PPINSXJD
003300******************************************************************PPINSXJD
003400 DATA DIVISION.                                                   PPINSXJD
003500     SKIP1                                                        PPINSXJD
003600     EJECT                                                        PPINSXJD
003700******************************************************************PPINSXJD
003800*                                                                *PPINSXJD
003900*                    WORKING-STORAGE SECTION                     *PPINSXJD
004000*                                                                *PPINSXJD
004100******************************************************************PPINSXJD
004200 WORKING-STORAGE SECTION.                                         PPINSXJD
004300     SKIP1                                                        PPINSXJD
004400 77  WS-ID                        PIC X(27)                       PPINSXJD
004500              VALUE 'WORKING-STORAGE BEGINS HERE'.                PPINSXJD
004600     SKIP3                                                        PPINSXJD
004620 01  DED-EFF-DATE-COMMAREA EXTERNAL. COPY CPWSEFDT.               36220651
004700 01  WS-COMN.                                                     PPINSXJD
004800 COPY CPWSCOMN.                                                   PPINSXJD
004900     SKIP3                                                        PPINSXJD
005000 01  COVERAGE-SUBSCRIPT              PIC 9(01).                   PPINSXJD
005100*    *******************************************                  PPINSXJD
005200*    * USED TO GET PREMIUM AMOUNT BY PLAN CODE                    PPINSXJD
005300*    *******************************************                  PPINSXJD
005400 01  FLAGS-AND-SWITCHES.                                          PPINSXJD
005500     05  PROGRAM-STATUS-FLAG         PIC X(01)   VALUE '0'.       PPINSXJD
005600         88  PROGRAM-STATUS-NORMAL               VALUE '0'.       PPINSXJD
005700         88  PROGRAM-STATUS-EXITING              VALUE '1'.       PPINSXJD
005800     05  FOUND-RATE-SW               PIC X(01)   VALUE 'N'.       PPINSXJD
005900         88  FOUND-RATE                          VALUE 'Y'.       PPINSXJD
006000                                                                  PPINSXJD
006100 01  CONSTANT-VALUES.                                             PPINSXJD
006200*    *************************************************************PPINSXJD
006300*    * THE FOLLOWING SHOULD BE THE SAME VALUE AS FOR THE OCCURS  *PPINSXJD
006400*    *************************************************************PPINSXJD
006500     05  CONSTANT-BENEFIT-TYPE       PIC X(01)   VALUE 'J'.       PPINSXJD
006600     05  CONSTANT-NORMAL             PIC X(01)   VALUE '0'.       PPINSXJD
006700     05  CONSTANT-ABORT              PIC X(01)   VALUE '1'.       PPINSXJD
006800     05  CONSTANT-EXIT-PROGRAM       PIC X(01)   VALUE '1'.       PPINSXJD
006900******************************************************************PPINSXJD
007000*                USED FOR DATE CALCS                             *PPINSXJD
007100******************************************************************PPINSXJD
007200     05  WS-PPEND-DATE-HOLD       PIC 9(6).                       PPINSXJD
007300     05  WS-PPEND-AREA  REDEFINES                                 PPINSXJD
007400         WS-PPEND-DATE-HOLD.                                      PPINSXJD
007500         10  WS-PPEND-YYMM        PIC 9(4).                       PPINSXJD
007600         10  FILLER               PIC XX.                         PPINSXJD
007700*                                                                *PPINSXJD
007800     05  WS-EFFECT-DATE-HOLD      PIC 9(6).                       PPINSXJD
007900     05  WS-EFFECT-AREA REDEFINES                                 PPINSXJD
008000         WS-EFFECT-DATE-HOLD.                                     PPINSXJD
008100         10  WS-EFFECT-YYMM       PIC 9(4).                       PPINSXJD
008200         10  FILLER               PIC XX.                         PPINSXJD
008300*                                                                *PPINSXJD
008400     EJECT                                                        PPINSXJD
008500 01  XUTF-UTILITY-FUNCTIONS.                                      PPINSXJD
008600                                  COPY 'CPWSXUTF'.                PPINSXJD
008700 01  WS-BRT-KEY.                                                  PPINSXJD
008800         10  WS-BRT-KEY-CONST    PIC X(04) VALUE ' BRT'.          PPINSXJD
008900         10  WS-BRT-KEY-VALUES.                                   PPINSXJD
009000             15  WS-BRT-BRSC      PIC X(05).                      PPINSXJD
009100             15  WS-BRT-BEN-TYPE  PIC X.                          PPINSXJD
009200             15  WS-BRT-BEN-PLAN  PIC X(02).                      PPINSXJD
009300             15  WS-BRT-SEQ-NO REDEFINES WS-BRT-BEN-PLAN PIC 99.  PPINSXJD
009400 01  WS-DEFAULT-BRSC          PIC X(05) VALUE '00   '.            PPINSXJD
009500 01  WS-DEFAULT-BRT           PIC X(05) VALUE '<<   '.            PPINSXJD
009600     EJECT                                                        PPINSXJD
009700 01  XBRT-BENEFITS-RATES-RECORD.                                  PPINSXJD
009800                                  COPY 'CPWSXBRT'.                PPINSXJD
009900                                                                  PPINSXJD
010000     EJECT                                                        PPINSXJD
010100 01  CTL-INTERFACE.                                               PPINSXJD
010200                                  COPY 'CPWSXCIF'.                PPINSXJD
010300     SKIP3                                                        PPINSXJD
010400 01  CTL-SEGMENT-TABLE.                                           PPINSXJD
010500                                  COPY 'CPWSXCST'.                PPINSXJD
010600     EJECT                                                        PPINSXJD
010700******************************************************************PPINSXJD
010800*                                                                *PPINSXJD
010900*                        LINKAGE SECTION                         *PPINSXJD
011000*                                                                *PPINSXJD
011100******************************************************************PPINSXJD
011200 LINKAGE SECTION.                                                 PPINSXJD
011300                                                                  PPINSXJD
011400 01  DFHCOMMAREA        PIC X(4080).                              PPINSXJD
011500 01  CA-COMMAREA   REDEFINES DFHCOMMAREA.                         PPINSXJD
011600     EXEC SQL                                                     PPINSXJD
011700          INCLUDE CPWSCOMA                                        PPINSXJD
011800     END-EXEC.                                                    PPINSXJD
011900 01  PPBENXJD-INTERFACE.          COPY 'CPLNKXJD'.                PPINSXJD
012000     EJECT                                                        PPINSXJD
012100******************************************************************PPINSXJD
012200*                                                                *PPINSXJD
012300*                       PROCEDURE DIVISION                       *PPINSXJD
012400*                                                                *PPINSXJD
012500******************************************************************PPINSXJD
012600     SKIP1                                                        PPINSXJD
012700 PROCEDURE DIVISION USING PPBENXJD-INTERFACE.                     PPINSXJD
012800     SKIP1                                                        PPINSXJD
012900 A000-MAINLINE SECTION.                                           PPINSXJD
013000     SKIP2                                                        PPINSXJD
013100     MOVE CONSTANT-NORMAL TO PROGRAM-STATUS-FLAG.                 PPINSXJD
013200     SKIP1                                                        PPINSXJD
013300     PERFORM B010-INITIALIZE THRU B019-INITIALIZE-EXIT.           PPINSXJD
013400     SKIP1                                                        PPINSXJD
013500     IF PROGRAM-STATUS-NORMAL                                     PPINSXJD
013600        IF KXJD-ACTION-GETTING-RATE OR KXJD-ACTION-CALCULATING    PPINSXJD
013700            PERFORM C010-GET-RATE THRU C019-GET-RATE-EXIT         PPINSXJD
013800            IF PROGRAM-STATUS-NORMAL                              PPINSXJD
013900                IF KXJD-ACTION-CALCULATING AND FOUND-RATE AND     PPINSXJD
014000                   KXJD-ENROLLED-IN-JD                            PPINSXJD
014100                    PERFORM D010-CALCULATE                        PPINSXJD
014200                        THRU D019-CALCULATE-EXIT.                 PPINSXJD
014300     SKIP3                                                        PPINSXJD
014400 A009-GOBACK.                                                     PPINSXJD
014500     GOBACK.                                                      PPINSXJD
014600     EJECT                                                        PPINSXJD
014700 B000-INITIALIZATION SECTION.                                     PPINSXJD
014800     SKIP2                                                        PPINSXJD
014900 B010-INITIALIZE.                                                 PPINSXJD
015000     SKIP1                                                        PPINSXJD
015100     MOVE CONSTANT-NORMAL TO KXJD-RETURN-STATUS-FLAG.             PPINSXJD
015200*                                                                 PPINSXJD
015300******************************************************************PPINSXJD
015400*      SET NUMERIC RETURN FIELDS TO ZERO                         *PPINSXJD
015500******************************************************************PPINSXJD
015600*                                                                 PPINSXJD
015700     MOVE ZERO TO KXJD-MAX-CONTRIBUTION                           PPINSXJD
015800                  KXJD-PREMIUM-AMT                                PPINSXJD
015900                  KXJD-EMPLOYEE-DEDUCTION                         PPINSXJD
016000                  KXJD-EMPLOYER-CONTRIBUTION.                     PPINSXJD
016100                                                                  PPINSXJD
016200     IF KXJD-ACTION-GETTING-RATE OR KXJD-ACTION-CALCULATING       PPINSXJD
016300         PERFORM B030-VALIDATE-LOOKUP-ARGS                        PPINSXJD
016400             THRU B039-VALIDATE-LOOKUP-ARGS-EXIT.                 PPINSXJD
016500                                                                  PPINSXJD
016600     IF KXJD-ACTION-CALCULATING                                   PPINSXJD
016700         PERFORM B050-VALIDATE-ENROLLMENT                         PPINSXJD
016800             THRU B059-VALIDATE-ENROLLMENT-EXIT.                  PPINSXJD
016900     SKIP3                                                        PPINSXJD
017000 B019-INITIALIZE-EXIT.                                            PPINSXJD
017100     EXIT.                                                        PPINSXJD
017200     SKIP3                                                        PPINSXJD
017300                                                                  PPINSXJD
017400 B030-VALIDATE-LOOKUP-ARGS.                                       PPINSXJD
017500     SKIP1                                                        PPINSXJD
017600     MOVE '0' TO KXJD-INVALID-LOOKUP-ARG-FLAG.                    PPINSXJD
017700                                                                  PPINSXJD
017800     IF KXJD-PLAN-CODE = SPACE                                    PPINSXJD
017900         MOVE '1' TO KXJD-INVALID-LOOKUP-ARG-FLAG                 PPINSXJD
018000     ELSE IF NOT KXJD-VALID-COVERAGE-CODE                         PPINSXJD
018010         MOVE CONSTANT-ABORT TO KXJD-RETURN-STATUS-FLAG           12570722
018100         MOVE '1' TO KXJD-INVALID-LOOKUP-ARG-FLAG.                PPINSXJD
018200     SKIP1                                                        PPINSXJD
018300     IF KXJD-INVALID-LOOKUP-ARG                                   PPINSXJD
018400         MOVE '1' TO PROGRAM-STATUS-FLAG.                         PPINSXJD
018500     SKIP1                                                        PPINSXJD
018600     MOVE '1' TO KXJD-NOT-ENROLLED-IN-JD-FLAG.                    PPINSXJD
018700     SKIP3                                                        PPINSXJD
018800 B039-VALIDATE-LOOKUP-ARGS-EXIT.                                  PPINSXJD
018900     EXIT.                                                        PPINSXJD
019000     SKIP3                                                        PPINSXJD
019100                                                                  PPINSXJD
019200 B050-VALIDATE-ENROLLMENT.                                        PPINSXJD
019300     SKIP1                                                        PPINSXJD
019400     MOVE '0' TO KXJD-NOT-ENROLLED-IN-JD-FLAG.                    PPINSXJD
019500*****                                                          CD 36400897
019700     IF KXJD-INVALID-LOOKUP-ARG                                   PPINSXJD
019800         MOVE '1' TO KXJD-NOT-ENROLLED-IN-JD-FLAG                 PPINSXJD
019810     END-IF.                                                      36220651
020502     SKIP2                                                        36220651
020560******************************************************************36220651
020570*    IF THE USER HAS FAILED TO ASSIGN AN INDICATOR IN THE GTN    *36220651
020580*    TABLE, USE 'E' AS A DEFAULT.                                *36220651
020590******************************************************************36220651
020591     IF EFDT-INPUT-XGTN-ICED-IND = 'A' OR 'E' OR 'V'              36220651
020592        CONTINUE                                                  36220651
020593     ELSE                                                         36220651
020594        MOVE 'E' TO EFDT-INPUT-XGTN-ICED-IND                      36220651
020595     END-IF.                                                      36220651
020596******************************************************************36220651
020597*    THIS CODE CAN BYPASS NORMAL DEDUCTIONS BASED ON DEDUCTION   *36220651
020598*    EFFECTIVE DATE CONSIDERATIONS                               *36220651
020599******************************************************************36220651
020600*****                                                          CD 36400897
020601     MOVE KXJD-COVERAGE-EFFECT-DATE TO EFDT-DATE-YYMMDD.          36400897
020602*****MOVE KXJD-COVERAGE-EFFECT-DATE(1:2) TO EFDT-INPUT-EFF-YY.    36400897
020603*****MOVE KXJD-COVERAGE-EFFECT-DATE(3:2) TO EFDT-INPUT-EFF-MM.    36400897
020604*****MOVE KXJD-COVERAGE-EFFECT-DATE(5:2) TO EFDT-INPUT-EFF-DD.    36400897
020607     MOVE KXJD-PAY-CYCLE-END-DATE(1:2) TO EFDT-XPCR-YY.           05390718
020608     MOVE KXJD-PAY-CYCLE-END-DATE(3:2) TO EFDT-XPCR-MM.           05390718
020609     MOVE KXJD-PAY-CYCLE-END-DATE(5:2) TO EFDT-XPCR-DD.           05390718
020610     PERFORM Z01-PROCESS-EFF-DATE.                                36220651
020611     IF EFDT-DONT-TAKE-DEDUCTION                                  36220651
020612        MOVE '1'  TO KXJD-NOT-ENROLLED-IN-JD-FLAG                 36220651
020613     END-IF.                                                      36220651
020614                                                                  36220651
020620     SKIP1                                                        PPINSXJD
020700     IF KXJD-NOT-ENROLLED-IN-JD                                   PPINSXJD
020800         MOVE '1' TO PROGRAM-STATUS-FLAG.                         PPINSXJD
020900     SKIP3                                                        PPINSXJD
021000 B059-VALIDATE-ENROLLMENT-EXIT.                                   PPINSXJD
021100     EXIT.                                                        PPINSXJD
021200     EJECT                                                        PPINSXJD
021300 C000-RATE-RETRIEVAL SECTION.                                     PPINSXJD
021400     SKIP2                                                        PPINSXJD
021500 C010-GET-RATE.                                                   PPINSXJD
021600     SKIP1                                                        PPINSXJD
021700     IF KXJD-BRSC  = WS-DEFAULT-BRSC                              PPINSXJD
021800        MOVE WS-DEFAULT-BRT TO WS-BRT-BRSC                        PPINSXJD
021900     ELSE                                                         PPINSXJD
022000        MOVE KXJD-BRSC  TO WS-BRT-BRSC.                           PPINSXJD
022100     MOVE CONSTANT-BENEFIT-TYPE TO WS-BRT-BEN-TYPE.               PPINSXJD
022200     MOVE KXJD-PLAN-CODE        TO WS-BRT-BEN-PLAN.               PPINSXJD
022300     SKIP1                                                        PPINSXJD
022400     MOVE 'N' TO FOUND-RATE-SW.                                   PPINSXJD
022500     SKIP1                                                        PPINSXJD
022600     PERFORM P010-BRT-LOOKUP-RATE-LOOP                            PPINSXJD
022700        THRU P019-BRT-LOOKUP-RATE-LOOP-EXIT                       PPINSXJD
022800         UNTIL FOUND-RATE OR                                      PPINSXJD
022900******************************************************************PPINSXJD
023000*                BLANK KEY INDICATES NOT ELIGIBLE FOR BENEFITS   *PPINSXJD
023100******************************************************************PPINSXJD
023200               NOT PROGRAM-STATUS-NORMAL.                         PPINSXJD
023300                                                                  PPINSXJD
023400     IF FOUND-RATE                                                PPINSXJD
023500         PERFORM C030-GET-PREM-AND-CONTRIB THRU                   PPINSXJD
023600                 C039-GET-PREM-AND-CONTRIB-EXIT.                  PPINSXJD
023700     SKIP3                                                        PPINSXJD
023800 C019-GET-RATE-EXIT.                                              PPINSXJD
023900     EXIT.                                                        PPINSXJD
024000     SKIP3                                                        PPINSXJD
024100 C030-GET-PREM-AND-CONTRIB.                                       PPINSXJD
024200     SKIP1                                                        PPINSXJD
024300     PERFORM C050-GET-PREMIUM-LOOP THRU                           PPINSXJD
024400             C059-GET-PREMIUM-LOOP-EXIT                           PPINSXJD
024500         VARYING COVERAGE-SUBSCRIPT FROM 1 BY 1                   PPINSXJD
024600*****                                                          CD 36400897
024620             UNTIL COVERAGE-SUBSCRIPT > 2.                        12570722
024700                                                                  PPINSXJD
024800     PERFORM C070-GET-CONTRIBUTION THRU                           PPINSXJD
024900             C079-GET-CONTRIBUTION-EXIT.                          PPINSXJD
025000     SKIP3                                                        PPINSXJD
025100 C039-GET-PREM-AND-CONTRIB-EXIT.                                  PPINSXJD
025200     EXIT.                                                        PPINSXJD
025300     SKIP3                                                        PPINSXJD
025400 C050-GET-PREMIUM-LOOP.                                           PPINSXJD
025500     SKIP1                                                        PPINSXJD
025600     IF XBRT-JD-CODE (COVERAGE-SUBSCRIPT) = KXJD-COVERAGE-CODE    PPINSXJD
025700         MOVE XBRT-JD-RATE (COVERAGE-SUBSCRIPT) TO                PPINSXJD
025800              KXJD-PREMIUM-AMT.                                   PPINSXJD
025900     SKIP3                                                        PPINSXJD
026000 C059-GET-PREMIUM-LOOP-EXIT.                                      PPINSXJD
026100     EXIT.                                                        PPINSXJD
026200     SKIP3                                                        PPINSXJD
026300 C070-GET-CONTRIBUTION.                                           PPINSXJD
026400     SKIP1                                                        PPINSXJD
026500     IF KXJD-COVERAGE-CODE = 'U'                                  PPINSXJD
026600         MOVE XBRT-JD-UC-SINGLE TO KXJD-MAX-CONTRIBUTION          PPINSXJD
026700*****                                                          CD 36400897
026900     ELSE                                                         PPINSXJD
027000         MOVE XBRT-JD-UC-FAMILY TO KXJD-MAX-CONTRIBUTION.         PPINSXJD
027100     SKIP3                                                        PPINSXJD
027200 C079-GET-CONTRIBUTION-EXIT.                                      PPINSXJD
027300     EXIT.                                                        PPINSXJD
027400     EJECT                                                        PPINSXJD
027500 D000-LEGAL-PLAN-CALC SECTION.                                    PPINSXJD
027600     SKIP2                                                        PPINSXJD
027700 D010-CALCULATE.                                                  PPINSXJD
027800     SKIP1                                                        PPINSXJD
027900     IF KXJD-PREMIUM-AMT < KXJD-MAX-CONTRIBUTION                  PPINSXJD
028000         COMPUTE KXJD-EMPLOYER-CONTRIBUTION =                     PPINSXJD
028100                 KXJD-PREMIUM-AMT                                 PPINSXJD
028200         COMPUTE KXJD-EMPLOYEE-DEDUCTION = ZERO                   PPINSXJD
028300     ELSE                                                         PPINSXJD
028400         COMPUTE KXJD-EMPLOYER-CONTRIBUTION =                     PPINSXJD
028500                 KXJD-MAX-CONTRIBUTION                            PPINSXJD
028600         COMPUTE KXJD-EMPLOYEE-DEDUCTION =                        PPINSXJD
028700                 KXJD-PREMIUM-AMT - KXJD-EMPLOYER-CONTRIBUTION.   PPINSXJD
028800     SKIP3                                                        PPINSXJD
028900 D019-CALCULATE-EXIT.                                             PPINSXJD
029000     EXIT.                                                        PPINSXJD
029100     EJECT                                                        PPINSXJD
029200 P000-SET-UP-BRT-KEY SECTION.                                     PPINSXJD
029300     SKIP2                                                        PPINSXJD
029400 P010-BRT-LOOKUP-RATE-LOOP.                                       PPINSXJD
029500     SKIP1                                                        PPINSXJD
029600     MOVE 'N' TO FOUND-RATE-SW.                                   PPINSXJD
029700                                                                  PPINSXJD
030200                                                                  PPINSXJD
030103*****                                                          CD 36400897
030600*        ************************************                     PPINSXJD
030700*        * RETRIEVE RATE FROM TABLE USING KEY                     PPINSXJD
030800*        ************************************                     PPINSXJD
030900         PERFORM P050-ACCESS-CTL THRU P059-ACCESS-CTL-EXIT.       PPINSXJD
030910     IF XBRT-KEY = WS-BRT-KEY                                     36220651
030920         MOVE 'Y' TO FOUND-RATE-SW                                36220651
030930     END-IF.                                                      36220651
031000     SKIP3                                                        PPINSXJD
031100 P019-BRT-LOOKUP-RATE-LOOP-EXIT.                                  PPINSXJD
031200     EXIT.                                                        PPINSXJD
031300     SKIP3                                                        PPINSXJD
031400 P050-ACCESS-CTL.                                                 PPINSXJD
031500     SKIP1                                                        PPINSXJD
031600     MOVE WS-BRT-KEY       TO CTL-SEG-TAB-KEY.                    PPINSXJD
031700     MOVE WS-BRT-KEY       TO IO-CTL-NOM-KEY.                     PPINSXJD
031800     PERFORM R020-CALL-PPIOCTL THRU R029-CALL-PPIOCTL-EXIT.       PPINSXJD
031900                                                                  PPINSXJD
032000     IF IO-CTL-ERROR-CODE NOT = '00' AND                          PPINSXJD
032100                          NOT = '05' AND                          PPINSXJD
032200                          NOT = '06' AND                          PPINSXJD
032300                          NOT = '13' AND                          PPINSXJD
032400                          NOT = '21'                              PPINSXJD
032500*        ******************************************               PPINSXJD
032600*        * ABORT PROGRAM DUE TO ERROR READING TABLE               PPINSXJD
032700*        ******************************************               PPINSXJD
032800         MOVE CONSTANT-ABORT TO KXJD-RETURN-STATUS-FLAG           PPINSXJD
032900         MOVE CONSTANT-EXIT-PROGRAM TO PROGRAM-STATUS-FLAG        PPINSXJD
033000     ELSE IF IO-CTL-ERROR-CODE = '00' AND                         PPINSXJD
033100         CTL-SEG-TAB-DEL NOT = HIGH-VALUE                         PPINSXJD
033200*        ***************************************************      PPINSXJD
033300*        * HIGH-VALUE WOULD INDICATE THAT RECORD IS DELETED,      PPINSXJD
033400*        * WHICH WOULD NOT BE A TRUE "FIND"                       PPINSXJD
033500*        ***************************************************      PPINSXJD
033600         MOVE 'Y' TO FOUND-RATE-SW                                PPINSXJD
033700     ELSE IF IO-CTL-ERROR-CODE  = '05' OR '06' OR '13' OR '21'    PPINSXJD
033800*        *******************************************************  PPINSXJD
033900*        * RATE NOT FOUND, SO GET KEY FOR NEXT ITERATION OF LOOP  PPINSXJD
034000*        *******************************************************  PPINSXJD
034100                IF WS-BRT-BRSC NOT = WS-DEFAULT-BRT               PPINSXJD
034200                   MOVE '2'  TO KXJD-RETURN-STATUS-FLAG           PPINSXJD
034300                   MOVE WS-DEFAULT-BRT  TO WS-BRT-BRSC            PPINSXJD
034400                   GO TO P050-ACCESS-CTL                          PPINSXJD
034500                ELSE                                              PPINSXJD
034600                   MOVE CONSTANT-ABORT TO KXJD-RETURN-STATUS-FLAG PPINSXJD
034700                   MOVE CONSTANT-EXIT-PROGRAM TO                  PPINSXJD
034800                                       PROGRAM-STATUS-FLAG.       PPINSXJD
034900                                                                  PPINSXJD
035000*    **********************************************************   PPINSXJD
035100*    * SAVE PREVIOUS, TO AVOID UNECESSARY DISK ACCESS IN FUTURE   PPINSXJD
035200*    **********************************************************   PPINSXJD
035300                                                                  PPINSXJD
035400     IF FOUND-RATE                                                PPINSXJD
035500         MOVE CTL-SEGMENT-TABLE TO XBRT-BENEFITS-RATES-RECORD.    PPINSXJD
035600     SKIP3                                                        PPINSXJD
035700 P059-ACCESS-CTL-EXIT.                                            PPINSXJD
035800     EXIT.                                                        PPINSXJD
035900     EJECT                                                        PPINSXJD
036000 R000-SUB-PROGRAM-CALLS SECTION.                                  PPINSXJD
036100     SKIP2                                                        PPINSXJD
036200 R020-CALL-PPIOCTL.                                               PPINSXJD
036300     SKIP1                                                        PPINSXJD
036400     MOVE IO-CTL-NOM-KEY   TO CTL-SEG-TAB-KEY.                    PPINSXJD
036500                                                                  PPINSXJD
036600     EXEC CICS                                                    PPINSXJD
036700             READ DATASET(CA-CTL-DD)                              PPINSXJD
036800             INTO(CTL-SEGMENT-TABLE)                              PPINSXJD
036900             RIDFLD(CTL-SEG-TAB-KEY)                              PPINSXJD
037000             NOHANDLE                                             PPINSXJD
037100             RESP(WS-RESP)                                        PPINSXJD
037200     END-EXEC.                                                    PPINSXJD
037300                                                                  PPINSXJD
037400     IF WS-RESP  = DFHRESP(NORMAL)                                PPINSXJD
037500        IF CTL-SEG-TAB-DEL = HIGH-VALUES                          PPINSXJD
037600           MOVE 21          TO IO-CTL-ERROR-CODE                  PPINSXJD
037700        ELSE                                                      PPINSXJD
037800           MOVE ZERO        TO IO-CTL-ERROR-CODE                  PPINSXJD
037900        END-IF                                                    PPINSXJD
038000     ELSE                                                         PPINSXJD
038100     IF WS-RESP  = DFHRESP(NOTFND)                                PPINSXJD
038200        MOVE 13          TO IO-CTL-ERROR-CODE                     PPINSXJD
038300     ELSE                                                         PPINSXJD
038400        MOVE 24 TO IO-CTL-ERROR-CODE                              PPINSXJD
038500     END-IF                                                       PPINSXJD
038600     END-IF.                                                      PPINSXJD
038700                                                                  PPINSXJD
038800     SKIP3                                                        PPINSXJD
038900 R029-CALL-PPIOCTL-EXIT.                                          PPINSXJD
039000     EXIT.                                                        PPINSXJD
039100 Z01-PROCESS-EFF-DATE SECTION.                                    36220651
039200     COPY CPPDEFDT.                                               36220651
