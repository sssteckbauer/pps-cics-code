000000**************************************************************/   36430875
000001*  PROGRAM: UCWIDDS                                          */   36430875
000002*  RELEASE: ___0875______ SERVICE REQUEST(S): _____3643____  */   36430875
000003*  NAME:_______AAC_______ MODIFICATION DATE:  __02/24/94____ */   36430875
000004*  DESCRIPTION:                                              */   36430875
000005*    CORRECT SCREEN ATTRIBUTES FOR ID NUMBER                 */   36430875
000006*                                                            */   36430875
000007**************************************************************/   36430875
000100**************************************************************/   36430821
000200*  PROGRAM: UCWIDDS                                          */   36430821
000300*  RELEASE: ___0821______ SERVICE REQUEST(S): _____3643____  */   36430821
000400*  NAME:_______STC_______ MODIFICATION DATE:  __10/05/93____ */   36430821
000500*  DESCRIPTION:                                              */   36430821
000600*    ID DISPLAY SCREEN PROCESSOR                             */   36430821
000700*                                                            */   36430821
000800**************************************************************/   36430821
000900 IDENTIFICATION DIVISION.                                         UCWIDDS
001000 PROGRAM-ID. UCWIDDS.                                             UCWIDDS
001100 ENVIRONMENT DIVISION.                                            UCWIDDS
001200 DATA DIVISION.                                                   UCWIDDS
001300 WORKING-STORAGE SECTION.                                         UCWIDDS
001400 01 WS-MISC.                                                      UCWIDDS
001500    05  BIRTH-MO-SUB           PIC 9(4)  VALUE ZERO COMP.         UCWIDDS
001600    05  WS-ASSIGN-PGM          PIC X(08) VALUE 'UCIIDASN'.        UCWIDDS
001700    05  WS-UPDATE-PGM          PIC X(08) VALUE 'UCIIDMNT'.        UCWIDDS
001800    05  WS-MAINT-PGM           PIC X(08) VALUE 'UCIIDMNT'.        UCWIDDS
001900    05  WS-IDB-UTILITY         PIC X(08) VALUE 'UCIID100'.        UCWIDDS
002000    05  WS-THIS-PGM-NAME       PIC X(08) VALUE 'UCWIDDS '.        UCWIDDS
002100****05  WS-RELEASE-NUMBER      PIC X(05) VALUE 'E0821'.           36430875
002110    05  WS-RELEASE-NUMBER      PIC X(05) VALUE 'E0875'.           36430875
002200    05  EDIT-SUB               PIC S9(04) COMP VALUE ZERO.        UCWIDDS
002300    05  ERASE-EOF              PIC X(01) VALUE X'80'.             UCWIDDS
002400    05  WS-FIELD-IN-ERROR-FLAG PIC X     VALUE 'N'.               UCWIDDS
002500        88  NO-ERRORS-FLAGGED            VALUE 'N'.               UCWIDDS
002600        88  ERRORS-FLAGGED               VALUE 'Y'.               UCWIDDS
002700    05  USER-ID.                                                  UCWIDDS
002800        10  WS-TERM-ID         PIC X(4).                          UCWIDDS
002900        10  WS-TRAN-ID         PIC X(4).                          UCWIDDS
003000    05  WS-BDATE-ERROR-FLAG    PIC X     VALUE 'N'.               UCWIDDS
003100        88  BDATE-ERROR                  VALUE 'Y'.               UCWIDDS
003200    05  WS-EDIT-ERRORS-FLAG    PIC X     VALUE 'N'.               UCWIDDS
003300        88  ERRORS-IN-EDIT               VALUE 'Y'.               UCWIDDS
003400        88  NO-ERRORS-IN-EDIT            VALUE 'N'.               UCWIDDS
003500    05  WS-BIRTHDATE-REFORMAT.                                    UCWIDDS
003600        10  BIRTH-MO           PIC 9(2).                          UCWIDDS
003700        10  BIRTH-DAY          PIC 9(2).                          UCWIDDS
003800        10  BIRTH-YEAR         PIC 9(2).                          UCWIDDS
003900                                                                  UCWIDDS
004000    05  WS-DB2-BIRTHDATE.                                         UCWIDDS
004100        10  BIRTH-MO-DB2       PIC X(2).                          UCWIDDS
004200        10  FILLER             PIC X.                             UCWIDDS
004300        10  BIRTH-DAY-DB2      PIC X(2).                          UCWIDDS
004400        10  FILLER             PIC X.                             UCWIDDS
004500        10  BIRTH-YR-DB2-1     PIC X(2).                          UCWIDDS
004600        10  BIRTH-YEAR-DB2     PIC X(2).                          UCWIDDS
004700                                                                  UCWIDDS
004800    05  WS-DATE-O.                                                UCWIDDS
004900        10  WS-DATE-MM-O       PIC XX.                            UCWIDDS
005000        10  FILLER-DATE1       PIC X.                             UCWIDDS
005100        10  WS-DATE-DD-O       PIC XX.                            UCWIDDS
005200        10  FILLER-DATE2       PIC X.                             UCWIDDS
005300        10  WS-DATE-YY-O       PIC XX.                            UCWIDDS
005400                                                                  UCWIDDS
005500    05  WS-DB2-BIRTHDATE-OUT.                                     UCWIDDS
005600        10  BIRTH-MO-OUT       PIC X(2).                          UCWIDDS
005700        10  FILLER             PIC X.                             UCWIDDS
005800        10  BIRTH-DD-OUT       PIC X(2).                          UCWIDDS
005900        10  FILLER             PIC X.                             UCWIDDS
006000        10  BIRTH-YR-OUT1      PIC X(2).                          UCWIDDS
006100        10  BIRTH-YR-OUT2      PIC X(2).                          UCWIDDS
006200                                                                  UCWIDDS
006300    05  WS-MON-TAB.                                               UCWIDDS
006400        10  FILLER             PIC X(2)  VALUE '31'.              UCWIDDS
006500        10  FILLER             PIC X(2)  VALUE '29'.              UCWIDDS
006600        10  FILLER             PIC X(2)  VALUE '31'.              UCWIDDS
006700        10  FILLER             PIC X(2)  VALUE '30'.              UCWIDDS
006800        10  FILLER             PIC X(2)  VALUE '31'.              UCWIDDS
006900        10  FILLER             PIC X(2)  VALUE '30'.              UCWIDDS
007000        10  FILLER             PIC X(2)  VALUE '31'.              UCWIDDS
007100        10  FILLER             PIC X(2)  VALUE '31'.              UCWIDDS
007200        10  FILLER             PIC X(2)  VALUE '30'.              UCWIDDS
007300        10  FILLER             PIC X(2)  VALUE '31'.              UCWIDDS
007400        10  FILLER             PIC X(2)  VALUE '30'.              UCWIDDS
007500        10  FILLER             PIC X(2)  VALUE '31'.              UCWIDDS
007600    05  WS-MON-TABLE REDEFINES WS-MON-TAB.                        UCWIDDS
007700        10  MAX-DAYS-IN-MO OCCURS 12     PIC X(2).                UCWIDDS
007800     EJECT                                                        UCWIDDS
007900 01 WS-CALLED-PROGRAMS.                                           UCWIDDS
008000    05 WS-PGM-IIDIDN           PIC X(08) VALUE 'UCIIDASN'.        UCWIDDS
008100    05 WS-PGM-IIDMNT           PIC X(08) VALUE 'UCIIDMNT'.        UCWIDDS
008200     EJECT                                                        UCWIDDS
008300 01 UCCOMMON                  EXTERNAL.                           UCWIDDS
008400    COPY UCCOMMON.                                                UCWIDDS
008500     EJECT                                                        UCWIDDS
008600 01 UCPISCRN                  EXTERNAL.                           UCWIDDS
008700    COPY UCPISCRN.                                                UCWIDDS
008800     EJECT                                                        UCWIDDS
008900 01 UCIDWSWK                  EXTERNAL.                           UCWIDDS
009000    COPY UCIDWSWK.                                                UCWIDDS
009100     EJECT                                                        UCWIDDS
009200 01 UCIDWS10                  EXTERNAL.                           UCWIDDS
009300    COPY UCIDWS10.                                                UCWIDDS
009400     EJECT                                                        UCWIDDS
009500 01 UCIDWSID                  EXTERNAL.                           UCWIDDS
009600    COPY UCIDWSID.                                                UCWIDDS
009700     EJECT                                                        UCWIDDS
009800 01 UCIDWSIX                  EXTERNAL.                           UCWIDDS
009900    COPY UCIDWSIX.                                                UCWIDDS
010000     EJECT                                                        UCWIDDS
010100 01 UCIDWSAS                  EXTERNAL.                           UCWIDDS
010200    COPY UCIDWSAS.                                                UCWIDDS
010300     EJECT                                                        UCWIDDS
010400 01 UCIDWSMT                  EXTERNAL.                           UCWIDDS
010500    COPY UCIDWSMT.                                                UCWIDDS
010600     EJECT                                                        UCWIDDS
010700 01 UCWSABND                  EXTERNAL.                           UCWIDDS
010800    COPY UCWSABND.                                                UCWIDDS
010900     EJECT                                                        UCWIDDS
011000 01 UCDTLARE                  EXTERNAL.                           UCWIDDS
011100    COPY UCDTLARE.                                                UCWIDDS
011200 01 IDDS-SCRN REDEFINES UCDTLARE.                                 UCWIDDS
011300    COPY UCIDDS0.                                                 UCWIDDS
011400     EJECT                                                        UCWIDDS
011500 LINKAGE SECTION.                                                 UCWIDDS
011600     EJECT                                                        UCWIDDS
011700                                                                  UCWIDDS
011800 PROCEDURE DIVISION.                                              UCWIDDS
011900                                                                  UCWIDDS
012000 0000-MAINLINE   SECTION.                                         UCWIDDS
012100                                                                  UCWIDDS
012200     MOVE WS-RELEASE-NUMBER TO UCPISCRN-DTL-REL-NO.               UCWIDDS
012300                                                                  UCWIDDS
012400     IF UCIDWSWK-CURSOR-IN-KEY-ID OR                              UCWIDDS
012500        UCIDWSWK-CURSOR-IN-KEY-SSN OR                             UCWIDDS
012600        UCIDWSWK-CURSOR-IN-KEY-DOB OR                             UCWIDDS
012700        UCIDWSWK-CURSOR-IN-KEY-LNAME                              UCWIDDS
012800       SET UCPISCRN-CURSOR-IN-FOOTER                              UCWIDDS
012900                            TO TRUE                               UCWIDDS
013000       GO TO 0000-EXIT-A                                          UCWIDDS
013100     ELSE                                                         UCWIDDS
013200       SET UCPISCRN-CURSOR-IN-DETAIL                              UCWIDDS
013300                            TO TRUE                               UCWIDDS
013400     END-IF.                                                      UCWIDDS
013500                                                                  UCWIDDS
013600     EVALUATE TRUE                                                UCWIDDS
013700        WHEN UCCOMMON-DISPLAY-REQUEST                             UCWIDDS
013800             MOVE LOW-VALUES TO UCIDDS0O                          UCWIDDS
013900             PERFORM 0100-INITIALIZE-FIELDS                       UCWIDDS
014000             PERFORM 0200-INITIALIZE-LABELS                       UCWIDDS
014100             PERFORM 1500-FORMAT-SCREEN                           UCWIDDS
014200        WHEN OTHER                                                UCWIDDS
014300             PERFORM 9000-INVALID-REQUEST-CODE                    UCWIDDS
014400     END-EVALUATE.                                                UCWIDDS
014500                                                                  UCWIDDS
014600     MOVE LENGTH OF UCIDDS0O TO UCPISCRN-DETAIL-MAP-LENGTH.       UCWIDDS
014700                                                                  UCWIDDS
014800 0000-EXIT-A.                                                     UCWIDDS
014900                                                                  UCWIDDS
015000     GOBACK.                                                      UCWIDDS
015100                                                                  UCWIDDS
015200 0000-EXIT.                                                       UCWIDDS
015300     EXIT.                                                        UCWIDDS
015400     EJECT                                                        UCWIDDS
015500 0100-INITIALIZE-FIELDS SECTION.                                  UCWIDDS
015600                                                                  UCWIDDS
015700                                                                  UCWIDDS
015800     MOVE UCCOMMON-CLR-ENTRY TO                                   UCWIDDS
015900                                NAME-LASTC                        UCWIDDS
016000                                NAME-FIRSTC                       UCWIDDS
016100                                NAME-MIDDLEC                      UCWIDDS
016200                                NAME-SUFFIXC                      UCWIDDS
016300                                SSNC                              UCWIDDS
016400                                BIRTHDATEC                        UCWIDDS
016500                                REFERRED-IDC                      UCWIDDS
016600                                STATUSC                           UCWIDDS
016700                                CHANGED-BYC                       UCWIDDS
016800                                ASSIGNED-ATC                      UCWIDDS
016900                                CHANGED-ATC                       UCWIDDS
017000                                                                  UCWIDDS
017100     MOVE UCCOMMON-ATR-ENTRY TO                                   UCWIDDS
017200****                            INDIVIDUAL-IDA                    36430875
017300                                NAME-LASTA                        UCWIDDS
017400                                NAME-FIRSTA                       UCWIDDS
017500                                NAME-MIDDLEA                      UCWIDDS
017600                                NAME-SUFFIXA                      UCWIDDS
017700                                SSNA                              UCWIDDS
017800                                BIRTHDATEA                        UCWIDDS
017900                                REFERRED-IDA                      UCWIDDS
018000                                STATUSA                           UCWIDDS
018100                                CHANGED-BYA                       UCWIDDS
018200                                ASSIGNED-ATA                      UCWIDDS
018300                                CHANGED-ATA                       UCWIDDS
018400                                                                  UCWIDDS
018500     MOVE UCCOMMON-HLT-ENTRY TO                                   UCWIDDS
018600****                            INDIVIDUAL-IDH                    36430875
018700                                NAME-LASTH                        UCWIDDS
018800                                NAME-FIRSTH                       UCWIDDS
018900                                NAME-MIDDLEH                      UCWIDDS
019000                                NAME-SUFFIXH                      UCWIDDS
019100                                SSNH                              UCWIDDS
019200                                BIRTHDATEH                        UCWIDDS
019300                                REFERRED-IDH                      UCWIDDS
019400                                STATUSH                           UCWIDDS
019500                                CHANGED-BYH                       UCWIDDS
019600                                ASSIGNED-ATH                      UCWIDDS
019700                                CHANGED-ATH                       UCWIDDS
019800                                                                  UCWIDDS
019900     MOVE UCCOMMON-ATR-ENTRY-PRO TO                               UCWIDDS
020000****                            INDIVIDUAL-IDA                    36430875
020100                                NAME-LASTA                        UCWIDDS
020200                                NAME-FIRSTA                       UCWIDDS
020300                                NAME-MIDDLEA                      UCWIDDS
020400                                NAME-SUFFIXA                      UCWIDDS
020500                                SSNA                              UCWIDDS
020600                                BIRTHDATEA                        UCWIDDS
020700                                REFERRED-IDA                      UCWIDDS
020800                                STATUSA                           UCWIDDS
020900                                CHANGED-BYA                       UCWIDDS
021000                                ASSIGNED-ATA                      UCWIDDS
021100                                CHANGED-ATA.                      UCWIDDS
021200                                                                  UCWIDDS
021210     MOVE UCCOMMON-ATR-NORM-DISPL TO INDIVIDUAL-IDA.              36430875
021220     MOVE UCCOMMON-CLR-NORM-DISPL TO INDIVIDUAL-IDC.              36430875
021230     MOVE UCCOMMON-HLT-NORM-DISPL TO INDIVIDUAL-IDH.              36430875
021300 0100-EXIT.                                                       UCWIDDS
021400     EXIT.                                                        UCWIDDS
021500     EJECT                                                        UCWIDDS
021600 0200-INITIALIZE-LABELS SECTION.                                  UCWIDDS
021700     MOVE UCCOMMON-CLR-FLD-LABEL TO                               UCWIDDS
021800                              L-INDIVIDUAL-IDC                    UCWIDDS
021900                              L-NAME-LASTC                        UCWIDDS
022000                              L-NAME-FIRSTC                       UCWIDDS
022100                              L-NAME-MIDDLEC                      UCWIDDS
022200                              L-NAME-SUFFIXC                      UCWIDDS
022300                              L-SSNC                              UCWIDDS
022400                              L-BIRTHDATEC                        UCWIDDS
022500                              L-REFERRED-IDC                      UCWIDDS
022600                              L-STATUSC                           UCWIDDS
022700                              L-CHANGED-BYC                       UCWIDDS
022800                              L-ASSIGNED-ATC                      UCWIDDS
022900                              L-CHANGED-ATC                       UCWIDDS
023000                                                                  UCWIDDS
023100     MOVE UCCOMMON-ATR-FLD-LABEL TO                               UCWIDDS
023200                              L-INDIVIDUAL-IDA                    UCWIDDS
023300                              L-NAME-LASTA                        UCWIDDS
023400                              L-NAME-FIRSTA                       UCWIDDS
023500                              L-NAME-MIDDLEA                      UCWIDDS
023600                              L-NAME-SUFFIXA                      UCWIDDS
023700                              L-SSNA                              UCWIDDS
023800                              L-BIRTHDATEA                        UCWIDDS
023900                              L-REFERRED-IDA                      UCWIDDS
024000                              L-STATUSA                           UCWIDDS
024100                              L-CHANGED-BYA                       UCWIDDS
024200                              L-ASSIGNED-ATA                      UCWIDDS
024300                              L-CHANGED-ATA                       UCWIDDS
024400                                                                  UCWIDDS
024500     MOVE UCCOMMON-HLT-FLD-LABEL TO                               UCWIDDS
024600                              L-INDIVIDUAL-IDH                    UCWIDDS
024700                              L-NAME-LASTH                        UCWIDDS
024800                              L-NAME-FIRSTH                       UCWIDDS
024900                              L-NAME-MIDDLEH                      UCWIDDS
025000                              L-NAME-SUFFIXH                      UCWIDDS
025100                              L-SSNH                              UCWIDDS
025200                              L-BIRTHDATEH                        UCWIDDS
025300                              L-REFERRED-IDH                      UCWIDDS
025400                              L-STATUSH                           UCWIDDS
025500                              L-CHANGED-BYH                       UCWIDDS
025600                              L-ASSIGNED-ATH                      UCWIDDS
025700                              L-CHANGED-ATH.                      UCWIDDS
025800                                                                  UCWIDDS
025900 0200-EXIT.                                                       UCWIDDS
026000     EXIT.                                                        UCWIDDS
026100     EJECT                                                        UCWIDDS
026200                                                                  UCWIDDS
026300                                                                  UCWIDDS
026400 1500-FORMAT-SCREEN SECTION.                                      UCWIDDS
026500                                                                  UCWIDDS
026600     IF UCIDWSWK-IID-KEY-ID NOT = SPACES                          UCWIDDS
026700        MOVE UCIDWSWK-IID-KEY-ID                                  UCWIDDS
026800                                 TO UCIDWS10-PASSED-IID           UCWIDDS
026900        SET UCIDWS10-REQ-VERIFY                                   UCWIDDS
027000                                 TO TRUE                          UCWIDDS
027100        GO TO 1500-FORMAT-CONT                                    UCWIDDS
027200     END-IF.                                                      UCWIDDS
027300     IF UCIDWSWK-IID-KEY-SSN NOT = SPACES                         UCWIDDS
027400        MOVE UCIDWSWK-IID-KEY-SSN                                 UCWIDDS
027500                                 TO UCIDWS10-PASSED-SSN           UCWIDDS
027600        SET UCIDWS10-REQ-FIND                                     UCWIDDS
027700                                 TO TRUE                          UCWIDDS
027800     END-IF.                                                      UCWIDDS
027900     IF UCIDWSWK-IID-KEY-DOB NOT = SPACES                         UCWIDDS
028000        MOVE UCIDWSWK-IID-KEY-DOB                                 UCWIDDS
028100                                 TO UCIDWS10-PASSED-BIRTHDATE     UCWIDDS
028200        SET UCIDWS10-REQ-FIND                                     UCWIDDS
028300                                 TO TRUE                          UCWIDDS
028400     END-IF.                                                      UCWIDDS
028500                                                                  UCWIDDS
028600 1500-FORMAT-CONT.                                                UCWIDDS
028700     IF NOT UCIDWS10-REQ-FIND AND NOT UCIDWS10-REQ-VERIFY         UCWIDDS
028800        GO TO 1500-EXIT                                           UCWIDDS
028900     END-IF.                                                      UCWIDDS
029000                                                                  UCWIDDS
029100     CALL WS-IDB-UTILITY.                                         UCWIDDS
029200                                                                  UCWIDDS
029300     IF (UCIDWS10-STAT-VRFY-FND OR UCIDWS10-STAT-VRFY-REF OR      UCWIDDS
029400        UCIDWS10-STAT-FIND-FND OR UCIDWS10-STAT-FIND-LIST) AND    UCWIDDS
029500        (UCIDWS10-RECORD-RET-COUNT = 1)                           UCWIDDS
029600        MOVE UCIDWS10-RET-IID (1)                                 UCWIDDS
029700                                 TO INDIVIDUAL-IDO                UCWIDDS
029800                                    UCIDWSID-IDB-INDIVIDUAL-ID    UCWIDDS
029900        MOVE UCIDWS10-RET-SSN (1)                                 UCWIDDS
030000                                 TO SSNO                          UCWIDDS
030100                                    UCIDWSID-IDB-SSN              UCWIDDS
030200        MOVE UCIDWS10-RET-BIRTHDATE (1)                           UCWIDDS
030300                                 TO UCIDWSID-IDB-BIRTHDATE        UCWIDDS
030400                                    WS-DB2-BIRTHDATE              UCWIDDS
030500        MOVE BIRTH-MO-DB2        TO WS-DATE-MM-O                  UCWIDDS
030600        MOVE BIRTH-DAY-DB2       TO WS-DATE-DD-O                  UCWIDDS
030700        MOVE BIRTH-YEAR-DB2      TO WS-DATE-YY-O                  UCWIDDS
030800        MOVE '/'                 TO FILLER-DATE1                  UCWIDDS
030900                                    FILLER-DATE2                  UCWIDDS
031000        MOVE WS-DATE-O           TO BIRTHDATEO                    UCWIDDS
031100        MOVE UCIDWS10-RET-NAME-LAST (1)                           UCWIDDS
031200                                 TO NAME-LASTO                    UCWIDDS
031300                                    UCIDWSID-IDB-LAST-NAME-TEXT   UCWIDDS
031400        MOVE UCIDWS10-RET-NAME-FIRST (1)                          UCWIDDS
031500                                 TO NAME-FIRSTO                   UCWIDDS
031600                                    UCIDWSID-IDB-FIRST-NAME-TEXT  UCWIDDS
031700        MOVE UCIDWS10-RET-NAME-MIDDLE (1)                         UCWIDDS
031800                                 TO NAME-MIDDLEO                  UCWIDDS
031900                                    UCIDWSID-IDB-MIDDLE-NAME-TEXT UCWIDDS
032000        MOVE UCIDWS10-RET-NAME-SUFFIX (1)                         UCWIDDS
032100                                 TO NAME-SUFFIXO                  UCWIDDS
032200                                    UCIDWSID-IDB-SUFFIX           UCWIDDS
032300        MOVE UCIDWS10-RET-REF-IID (1)                             UCWIDDS
032400                                 TO REFERRED-IDO                  UCWIDDS
032500                                    UCIDWSID-IDB-REFERRED-IID     UCWIDDS
032600        MOVE UCIDWS10-RET-STATUS (1)                              UCWIDDS
032700                                 TO STATUSO                       UCWIDDS
032800                                    UCIDWSID-IDB-STATUS           UCWIDDS
032900        MOVE UCIDWS10-RET-ASSIGNED-AT (1)                         UCWIDDS
033000                                 TO ASSIGNED-ATO                  UCWIDDS
033100                                    UCIDWSID-IDB-ASSIGNED-AT      UCWIDDS
033200        MOVE UCIDWS10-RET-CHANGED-BY (1)                          UCWIDDS
033300                                 TO CHANGED-BYO                   UCWIDDS
033400                                    UCIDWSID-IDB-CHANGED-BY       UCWIDDS
033500        MOVE UCIDWS10-RET-CHANGED-AT (1)                          UCWIDDS
033600                                 TO CHANGED-ATO                   UCWIDDS
033700                                    UCIDWSID-IDB-CHANGED-AT       UCWIDDS
033800     END-IF.                                                      UCWIDDS
033900                                                                  UCWIDDS
034000 1500-EXIT.                                                       UCWIDDS
034100     EXIT.                                                        UCWIDDS
034200                                                                  UCWIDDS
034300                                                                  UCWIDDS
034400 9000-INVALID-REQUEST-CODE SECTION.                               UCWIDDS
034500                                                                  UCWIDDS
034600     MOVE 'INVALID UCCOMMON-REQUEST-CODE'                         UCWIDDS
034700       TO UCWSABND-MSG-DATA.                                      UCWIDDS
034800                                                                  UCWIDDS
034900     MOVE WS-THIS-PGM-NAME TO UCWSABND-ABENDING-PGM.              UCWIDDS
035000                                                                  UCWIDDS
035100     MOVE SPACES          TO UCWSABND-ABEND-DATA(1)               UCWIDDS
035200                             UCWSABND-ABEND-DATA(2).              UCWIDDS
035300                                                                  UCWIDDS
035400     STRING                                                       UCWIDDS
035500           'INVALID REQUEST CODE = '  DELIMITED BY SIZE           UCWIDDS
035600            UCCOMMON-REQUEST-CODE     DELIMITED BY SIZE           UCWIDDS
035700       INTO UCWSABND-ABEND-DATA(1)                                UCWIDDS
035800     END-STRING.                                                  UCWIDDS
035900                                                                  UCWIDDS
036000     PERFORM 9999-ABEND.                                          UCWIDDS
036100                                                                  UCWIDDS
036200 9000-EXIT.                                                       UCWIDDS
036300     EXIT.                                                        UCWIDDS
036400     EJECT                                                        UCWIDDS
036500                                                                  UCWIDDS
036600 9999-ABEND                SECTION.                               UCWIDDS
036700 COPY UCPDABND.                                                   UCWIDDS
036800                                                                  UCWIDDS
036900 9999-EXIT.                                                       UCWIDDS
037000      EXIT.                                                       UCWIDDS
037100     EJECT                                                        UCWIDDS
037200                                                                  UCWIDDS
037300 999999-SQL-ERROR SECTION.                                        UCWIDDS
037400                                                                  UCWIDDS
037500     STRING                                                       UCWIDDS
037600           'SQL ERROR FROM '            DELIMITED BY SIZE         UCWIDDS
037700            UCIDWSMT-ERROR-PROG         DELIMITED BY SIZE         UCWIDDS
037800            ', IN PARA '                DELIMITED BY SIZE         UCWIDDS
037900            UCIDWSMT-ERROR-PARA         DELIMITED BY SIZE         UCWIDDS
038000            ' = '                       DELIMITED BY SIZE         UCWIDDS
038100            UCIDWSMT-ERROR-CODE         DELIMITED BY SIZE         UCWIDDS
038200       INTO UCWSABND-ABEND-DATA(1)                                UCWIDDS
038300                                                                  UCWIDDS
038400      PERFORM 9999-ABEND.                                         UCWIDDS
038500                                                                  UCWIDDS
038600 999999-EXIT.                                                     UCWIDDS
038700       EXIT.                                                      UCWIDDS
