000800*==========================================================%      UCSD
000200*=    PROGRAM: UCARSM                                     =%      UCSD
000300*=    CHANGE # 0114         PROJ. REQUEST: REL1184        =%      UCSD0114
000400*=    NAME:                 MODIFICATION DATE: 04/01/99   =%      UCSD0114
000500*=                                                        =%      UCSD0114
000600*=    DESCRIPTION                                         =%      UCSD0114
000700*=    SWICTHED DEPT USER ACCESS SECURITY FROM IDPR SUB    =%      UCSD0114
000800*=    SYSTEM TO EDB SUB SYSTEM                            =%      UCSD0114
008000*===========================================================%     UCSD0114
000100**************************************************************/   EFIX1022
000200*  DSNTYPE: UCARSM                                           */   EFIX1022
000300*  RELEASE: ___1022______ SERVICE REQUEST(S): _____EFIX____  */   EFIX1022
000300*  REF REL: ___0967______                                    */   EFIX1022
000400*  NAME:___PHIL THOMPSON_ MODIFICATION DATE:  __10/04/95____ */   EFIX1022
000500*  DESCRIPTION:                                              */   EFIX1022
000600*  -  ERROR REPORT 1334:                                     */   EFIX1022
000600*     ADD THF AS VALID RESOURCE TYPE: CALL UCPPPEDB.         */   EFIX1022
000100**************************************************************/   EFIX1022
000100**************************************************************/   36430795
000200*  DSNTYPE: UCARSM                                           */   36430795
000300*  RELEASE: ___0795______ SERVICE REQUEST(S): _____3643____  */   36430795
000400*  NAME:___PXP___________ MODIFICATION DATE:  __07/15/93____ */   36430795
000500*  DESCRIPTION:                                              */   36430795
000600*     ALTER SYNCPOINT CALL.                                  */   36430795
000700**************************************************************/   36430795
000100**************************************************************/   36330704
000200*  PROGRAM: UCARSM                                           */   36330704
000300*  RELEASE # ___0704___   SERVICE REQUEST NO(S)____3633______*/   36330704
000400*  NAME __B.I.T._______   MODIFICATION DATE ____10/02/92_____*/   36330704
000500*  DESCRIPTION                                               */   36330704
000600*                                                            */   36330704
000700*        MODIFIED FOR IMPLEMENTATION OF EMPLOYEE             */   36330704
000800*                      RECORD HISTORY                        */   36330704
000900*                                                            */   36330704
001000**************************************************************/   36330704
000100**************************************************************/   36270604
000200*  PROGRAM: UCARSM                                           */   36270604
000300*  RELEASE # ___0604___   SERVICE REQUEST NO(S)____3627______*/   36270604
000400*  NAME __C RIDLON_____   MODIFICATION DATE ____10/02/91_____*/   36270604
000500*  DESCRIPTION                                               */   36270604
000600*                                                            */   36270604
000700*        THIS IS A COMPLETE REPLACEMENT OF UCARSM            */   36270604
000800*                AS ISSUED IN RELEASE 0594                   */   36270604
000900*                                                            */   36270604
001000**************************************************************/   36270604
001100 IDENTIFICATION DIVISION.                                         UCARSM
001200 PROGRAM-ID. UCARSM.                                              UCARSM
001300 ENVIRONMENT DIVISION.                                            UCARSM
001400 DATA DIVISION.                                                   UCARSM
001500 WORKING-STORAGE SECTION.                                         UCARSM
001600 COPY CPYRIGHT.                                                   UCARSM
001700                                                                  UCARSM
001800 01  WS-MISC.                                                     UCARSM
001900     05  WS-MODULE                     PIC X(8) VALUE SPACES.     UCARSM
002000     05  WS-RESOURCE-FLAG              PIC X VALUE 'N'.           UCARSM
002100         88  RESOURCE-FOUND                  VALUE 'Y'.           UCARSM
002400                                                                  UCARSM
002500 LINKAGE SECTION.                                                 UCARSM
002600                                                                  UCARSM
002700 01  ARSM-INTERFACE.                                              UCARSM
002800 COPY UCLNARSM.                                                   UCARSM
002900                                                                  UCARSM
003000 PROCEDURE DIVISION USING ARSM-INTERFACE.                         UCARSM
003100 0000-MAINLINE.                                                   UCARSM
003200                                                                  UCARSM
003300     MOVE 'N' TO ARSM-PERMISSION.                                 UCARSM
003400     MOVE ZERO TO ARSM-RETURN-CODE.                               UCARSM
003500                                                                  UCARSM
003400     EVALUATE ARSM-RESOURCE-TYPE                                  UCARSM
003500       WHEN 'EDB     '                                            UCARSM
003600            MOVE 'Y' TO WS-RESOURCE-FLAG                          UCARSM
003700            MOVE 'UCPPPEDB' TO WS-MODULE                          UCARSM
003800       WHEN 'PAR     '                                            UCARSM
003900            MOVE 'Y' TO WS-RESOURCE-FLAG                          UCARSM
005300            MOVE 'EDB' TO ARSM-RESOURCE-TYPE                      UCSD0114
004000            MOVE 'UCPPPEDB' TO WS-MODULE                          UCSD0114
004000*****       MOVE 'UCPPPPAR' TO WS-MODULE                          UCSD0114
005100       WHEN 'HDB     '                                            36330704
005200            MOVE 'Y' TO WS-RESOURCE-FLAG                          36330704
005300            MOVE 'UCPPPERH' TO WS-MODULE                          36330704
005100       WHEN 'THF     '                                            EFIX1022
005200            MOVE 'Y' TO WS-RESOURCE-FLAG                          EFIX1022
005300            MOVE 'EDB' TO ARSM-RESOURCE-TYPE                      EFIX1022
005300            MOVE 'UCPPPEDB' TO WS-MODULE                          EFIX1022
004100     END-EVALUATE.                                                UCARSM
003900                                                                  UCARSM
004600     IF RESOURCE-FOUND                                            UCARSM
006400********EXEC CICS SYNCPOINT END-EXEC                              36430795
004500        CALL WS-MODULE USING DFHEIBLK DFHCOMMAREA ARSM-INTERFACE  UCARSM
006600********EXEC CICS SYNCPOINT END-EXEC                              36430795
004700     ELSE                                                         UCARSM
004800        EXEC CICS ABEND ABCODE('ARSM') CANCEL END-EXEC            UCARSM
004900     END-IF.                                                      UCARSM
004700                                                                  UCARSM
006100     GOBACK.                                                      UCARSM
006200                                                                  UCARSM
