      ****************************************************************
      *  PLWEBCHM - CICS WEB SERVER APPLICATION PROGRAM (FOR PPS)    *
      ****************************************************************
      *  PLWEBCHM: CREATED 9/01/97 BY BOB ORTIZ (ACT - PPS TEAM)     *
      *                                                              *
      *  FUNCTION: THIS PROGRAM IS "HTML" AWARE AND RECEIVES HTTP    *
      *            ENCODED DATA FROM THE CICS WEB SERVER, RETRIEVES  *
      *            AND RETURNS INQUIRY HELP DATA TO THE WEB SERVER   *
      *            IN AN HTTP/HTML FORMAT. IT FUNCTIONS AS A DRIVER  *
      *            PROGRAM AND INVOKES THE ROUTINES REQUIRED TO      *
      *            OBTAIN PPS (ONLINE SYSTEM) DATA ELEMENTS DESCRIP- *
      *            TION DATA FROM A KSDS VSAM FILE.                  *
      *                                                              *
      *  HOW INVOKED: CALLED BY THE CICS WEB INTERFACE (I.E.,BY THE  *
      *               ALIAS TRANSACTION 'CWBA' - WHICH IN TURN IS    *
      *               INVOKED BY THE WEB SERVER CONTROLLER)          *
      *               FOR ADDITIONAL INFORMATION REFER TO: CICS WEB  *
      *               INTERFACE GUIDE FOR MVS/ESA 4.1 (SC33-1892-00) *
      *                                                              *
      *  INPUT PARAMETERS: COMMAREA - WHICH CONTAINS THE HTTP DATA   *
      *                    (I.E.,HTTP HEADERS,URL AND QUERY STRING)  *
      *                    THIS AREA IS NOT USED HOWEVER BECAUSE THE *
      *                    REQUIRED INPUT INFORMATION IS OBTAINED    *
      *                    VIA THE ENVIRONMENT PROGRAM 'DFHWBENV'    *
      *                                                              *
      *  OUTPUT PARAMETERS: COMMAREA                                 *
      *                                                              *
      *  PROGRAMS CALLED: DFHWBENV, DFHWBPA, PLWEBCHS                *
      *                                                              *
      ****************************************************************
       IDENTIFICATION DIVISION.                                         UCROUTER
       PROGRAM-ID. PLWEBCHM.                                            UCROUTER
       ENVIRONMENT DIVISION.                                            UCROUTER
       CONFIGURATION SECTION.                                           UCROUTER
       SOURCE-COMPUTER.

       DATA DIVISION.                                                   UCROUTER

       WORKING-STORAGE SECTION.                                         UCROUTER

      * MISC DATA/CONSTANTS
       01  COMMAREA-LENGTH        PIC S9(4) COMP VALUE 1024.
       01  WS2-COMM-LENGTH        PIC S9(4) COMP VALUE 9216.
       01  WS3-COMM-LENGTH        PIC S9(4) COMP VALUE 1024.
       01  WS4-COMM-LENGTH        PIC S9(4) COMP VALUE 4047.
       01  PGMHTML                PIC X(8)  VALUE 'PLWEBCHS'.
       01  PGMID                  PIC X(8)  VALUE SPACES.

      *  UPPER CASE CONVERSION CONSTANTS
       01   LOWER       PIC X(26)   VALUE 'abcdefghijklmnopqrstuvwxyz'.
       01   FILLER REDEFINES LOWER.
            05  LOWERC OCCURS 26 INDEXED BY LOWERC-IDX  PIC X.
       01   UPPER       PIC X(26)   VALUE 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.
       01   FILLER REDEFINES UPPER.
            05  UPPERC OCCURS 26    PIC X.
       01   CHAR-COUNT              PIC 99 COMP.

       01  RESPONSE-CODE            PIC S9(8) COMP.


      * HTTP/HTML O/P AREA (4096K) USED BY CICS/WEB ENVIRONMENT PROGRAM
       01  WS1-COMMAREA.                                                00272000
           05 OUTLEN         PIC 9(8)      COMP VALUE 9216.             45100000
           05 OUTRESP        PIC X(15)  VALUE 'HTTP/1.0 200 OK'.        45500000

      *    CARRIAGE RETURN + LINEFEED = X'0D25'= 3365 = BYTES 13,37
           05 CR-LF-1        PIC 9(04)  VALUE 3365 COMP.                00171000
           05 OUTHDR1        PIC X(24)  VALUE 'CONTENT-TYPE: TEXT/HTML'.46900000
           05 CR-LF-2        PIC 9(04)  VALUE 3365 COMP.                00171000
           05 CR-LF-3        PIC 9(04)  VALUE 3365 COMP.                00171000

           05 HTMLTITLE      PIC X(34)                                  96800000
                VALUE '<HTML><TITLE>EMPLOYEE HELP</TITLE>'.             96800000
           05 OUTMSG1                   PIC X(9133) VALUE SPACES.       96800000
           05 OUTMSG6  REDEFINES OUTMSG1.                               96800000
              10  ERROR-RC              PIC XX.
              10  ERROR-MSG.
                  15 ERRMSG1            PIC X(20).
                  15 ERRMSG2            PIC X(9111).


      * O/P AREA (1024K) USED BY CICS/WEB ENVIRONMENT PROGRAM
       01  WS2-COMMAREA.                                                00272000
           05 DATALEN        PIC 9(8)        COMP VALUE ZERO.           45100000
           05 ENVDATA        PIC X(1010)     VALUE SPACES.              45100000
           05 ENVDATA2       PIC X(10)       VALUE SPACES.              45100000


      * O/P AREA (1024K) USED BY CICS/WEB PARSING PROGRAM
       01  WS3-COMMAREA.                                                00272000
           05 PARSEPARM      PIC X(30)  VALUE 'QUERY_STRING&&'.         45100000
           05 PARSRESULT REDEFINES PARSEPARM.
              10 PARSEQS     PIC X(8).
              10 QSEXTRA     PIC X(22).
           05 PARSEDATA      PIC X(994) VALUE SPACES.                   45100000


      * I/O DFHCOMMAREA (9216K) USED BY PLWEBCHS SUBROUTINE
       01   WS4-COMMAREA.                                               00272000
            05  WS4-DFHCOM-INDEX               PIC X(8) VALUE SPACES.   45100000
            05  WS4-DFHOM-RET-STATUS           PIC XX.
            05  WS4-DFHCOM-RETURN-REC.                                  45100000
                10  WS4-COM-RET-HTML-TTL       PIC X(10).
                10  WS4-DFHCOM-RET-TITLE       PIC X(30).
                10  WS4-DFHCOM-RET-HTML-TTLND  PIC X(5).
                10  WS4-DFHCOM-RET-PRE         PIC X(5).
                10  WS4-DFHCOM-RET-DATA        PIC X(9073).


       LINKAGE SECTION.                                                 00331000
       01  DFHCOMMAREA PIC X(6061).                                     00333000


      *---------------------------------------------------------------
       PROCEDURE DIVISION.
       0000-INITIAL.
           IF EIBCALEN > 0                                              00470000
              GO TO 0100-MAINLINE
            ELSE
              GO TO 9999-EXIT
           END-IF.
       0000-INITIAL-EXIT.


      * --------------------------------------------------------------
      * 1) USE ENVIRONMENT PROGRAM TO OBTAIN THE ENVIRONEMT VARIABLE *
      *    DATA WHICH INCLUDES THE 'QUERY_STRING' VARIABLE DATA      *
      * --------------------------------------------------------------
       0100-MAINLINE.
           MOVE 'DFHWBENV' TO PGMID.
           EXEC CICS LINK PROGRAM(PGMID)                                00471000
                          COMMAREA(WS2-COMMAREA)
                          LENGTH(WS2-COMM-LENGTH)
                          RESP(RESPONSE-CODE)
           END-EXEC.                                                    00471000
           PERFORM 0500-RESPONSE-CHECK.
      *    MOVE ENVIRONMENT DATA TO PARSE COMM AREA FOR PARSING ROUTINE
           MOVE ENVDATA TO PARSEDATA.                                   00510000
           GO TO 0200-OBTAIN-QRSTRING.

      * --------------------------------------------------------------
      * 2) USE PARSING PROGRAM TO OBTAIN ONLY THE QUERY_STRING DATA  *
      *    VALUE                                                     *
      * --------------------------------------------------------------
       0200-OBTAIN-QRSTRING.
           INSPECT WS3-COMMAREA CONVERTING X'0E' TO '.'.
           MOVE 'DFHWBPA' TO PGMID.
           EXEC CICS LINK PROGRAM(PGMID)                                00471000
                          COMMAREA(WS3-COMMAREA)
                          LENGTH(WS3-COMM-LENGTH)
                          RESP(RESPONSE-CODE)
           END-EXEC.                                                    00471000
           PERFORM 0500-RESPONSE-CHECK.
           IF PARSEQS = LOW-VALUES
      *          IDENTIFY PGM THAT WILL PROVIDE MAIN HELP MENU HTML TEXT
                 MOVE 'PLWEBCHS' TO PGMID
                 MOVE 'DEFAULT' TO WS4-DFHCOM-INDEX                     00510000
             ELSE
                 MOVE 'PLWEBCHS' TO PGMID
                 PERFORM 0600-CONVERT-TO-UPPERCASE
                 INSPECT PARSEQS REPLACING ALL LOW-VALUES BY SPACES
                 MOVE PARSEQS TO WS4-DFHCOM-INDEX                       00510000
           END-IF.
           GO TO 0300-OBTAIN-HTMLTEXT.

      * --------------------------------------------------------------
      * 3) INVOKE SUBROUTINE TO OBTAIN HTML TEXT BASED ON THE QUERY  *
      *    STRING VALUE (WHICH IS USED AS A VSAM KEY TO OBAIN TEXT)  *
      * --------------------------------------------------------------
       0300-OBTAIN-HTMLTEXT.
           EXEC CICS LINK PROGRAM(PGMID)                                00471000
                          COMMAREA(WS4-COMMAREA)
                          LENGTH(WS4-COMM-LENGTH)
                          RESP(RESPONSE-CODE)
           END-EXEC.                                                    00471000
           PERFORM 0500-RESPONSE-CHECK.
           STRING WS4-DFHCOM-RETURN-REC
                  DELIMITED BY 'END OF DESC' INTO OUTMSG1.
           GO TO 9999-EXIT.

      *-----------------------------------------
      *  SUPPORT FUNCTION ROUTINES             *
      *-----------------------------------------
       0500-RESPONSE-CHECK.
      *    IF BAD LINK TERMINATE DRIVER PROGRAM WITH HTML ERROR TEXT
           IF RESPONSE-CODE NOT = DFHRESP(NORMAL)
                 MOVE RESPONSE-CODE TO ERROR-RC
                 MOVE 'BAD RC FROM LINK TO ' TO ERRMSG1
                 MOVE PGMID TO ERRMSG2
                 GO TO 9999-EXIT
           END-IF.                                                      00620000
       0500-RESPONSE-CHECK-EXIT.

       0600-CONVERT-TO-UPPERCASE.
           PERFORM WITH TEST AFTER VARYING CHAR-COUNT FROM 1 BY +1
               UNTIL CHAR-COUNT = 26
               INSPECT PARSEQS REPLACING ALL LOWERC(CHAR-COUNT) BY
                       UPPERC(CHAR-COUNT)
           END-PERFORM.
       0600-CONVERT-TO-UPPERCASE-EXIT.


      * --------------------------------------------------------------
      * 4) MOVE HTTP/HTML TEXT TO COMM AREA & RETURN TO CICS WEB     *  00510000
      *    SERVER (WHICH WILL RETURN THE COMM AREA TEXT TO BROWSER)  *  00510000
      * --------------------------------------------------------------
       9999-EXIT.
           MOVE WS1-COMMAREA TO DFHCOMMAREA.                            00510000
           EXEC CICS RETURN END-EXEC.                                   00621000
           STOP RUN.

