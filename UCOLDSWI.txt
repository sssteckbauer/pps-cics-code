000000**************************************************************/   36430875
000001*  PROGRAM: UCOLDSWI                                         */   36430875
000002*  RELEASE: ___0875______ SERVICE REQUEST(S): _____3643____  */   36430875
000003*  NAME:_______AAC_______ MODIFICATION DATE:  __02/24/94____ */   36430875
000004*  DESCRIPTION:                                              */   36430875
000005*   ADJUST CODE ADDED IN R0815 TO CHECK FOR NO AUTHORIZATION */   36430875
000006*   TO OLD SYSTEM FUNCTION                                   */   36430875
000007**************************************************************/   36430875
000100**************************************************************/   36430833
000200*  PROGRAM: UCOLDSWI                                         */   36430833
000300*  RELEASE: ___0833______ SERVICE REQUEST(S): _____3643____  */   36430833
000400*  NAME:_______PXP_______ MODIFICATION DATE:  __11/18/93____ */   36430833
000500*  DESCRIPTION:                                              */   36430833
000600*  CORRECT NAVIGATION WHEN BUNDLE IS ENTERED AND WHO IS      */   36430833
000700*  INVOKED                                                   */   36430833
000800**************************************************************/   36430833
000100**************************************************************/   36430815
000200*  PROGRAM: UCOLDSWI                                         */   36430815
000300*  RELEASE: ___0815______ SERVICE REQUEST(S): _____3643____  */   36430815
000400*  NAME:_______PXP_______ MODIFICATION DATE:  __09/17/93____ */   36430815
000500*  DESCRIPTION:                                              */   36430815
000600*  ADJUST INTERFACE  TO OLD NAVIGATION TO TRIGGER WHO        */   36430815
000700**************************************************************/   36430815
000100**************************************************************/   36430795
000200*  PROGRAM: UCOLDSWI                                         */   36430795
000300*  RELEASE # ___0795___   SERVICE REQUEST NO(S)____3643______*/   36430795
000400*  NAME __MLG__________   MODIFICATION DATE ____07/15/93_____*/   36430795
000500*  DESCRIPTION                                               */   36430795
000600*                                                            */   36430795
000700*     SWITCH TO "OLD SYSTEM" FUNCTION PROCESSOR              */   36430795
000800*                                                            */   36430795
000900**************************************************************/   36430795
001000 IDENTIFICATION DIVISION.                                         UCOLDSWI
001100 PROGRAM-ID. UCOLDSWI.                                            UCOLDSWI
001200 ENVIRONMENT DIVISION.                                            UCOLDSWI
001300 CONFIGURATION SECTION.                                           UCOLDSWI
001400 SOURCE-COMPUTER.                                                 UCOLDSWI
001500 COPY  CPOTXUCS.                                                  UCOLDSWI
001600 DATA DIVISION.                                                   UCOLDSWI
001700 WORKING-STORAGE SECTION.                                         UCOLDSWI
001800                                                                  UCOLDSWI
001900 01  WS-COMN.                                                     UCOLDSWI
002000     05  WS-START                          PIC X(27) VALUE        UCOLDSWI
002100       'WORKING STORAGE STARTS HERE'.                             UCOLDSWI
002200     05 WS-PPFCVALD-PGM-NAME               PIC X(08)              UCOLDSWI
002300                                               VALUE 'PPFCVALD'.  UCOLDSWI
002310     05 WS-ORCA-SWITCH                     PIC X     VALUE SPACE. UCOLDSWI
002320        88  ORCA-SWITCH                    VALUE 'Y'.             UCOLDSWI
002400                                                                  UCOLDSWI
002500     COPY CPWSCOMN.                                               UCOLDSWI
002600                                                                  UCOLDSWI
002700 01  WS-MESS-NOS.                                                 UCOLDSWI
002800     05  MU0030                     PIC X(5) VALUE 'U0030'.       UCOLDSWI
004510     05  MP0002                     PIC X(5) VALUE 'P0002'.       36430875
002900                                                                  UCOLDSWI
003000 01  PPDB2MSG-INTERFACE.                                          UCOLDSWI
003100     COPY CPLNKDB2.                                               UCOLDSWI
003200                                                                  UCOLDSWI
003300 01  UCCOMMON   EXTERNAL.                                         UCOLDSWI
003400     COPY UCCOMMON.                                               UCOLDSWI
003500                                                                  UCOLDSWI
003600 01  CPWSWORK   EXTERNAL.                                         UCOLDSWI
003700     COPY CPWSWORK.                                               UCOLDSWI
003800                                                                  UCOLDSWI
003900 01  UCOLDINT   EXTERNAL.                                         UCOLDSWI
004000     COPY UCOLDINT.                                               UCOLDSWI
004100                                                                  UCOLDSWI
004200 01  UCWSABND    EXTERNAL.                                        UCOLDSWI
004300     COPY UCWSABND.                                               UCOLDSWI
004400                                                                  UCOLDSWI
004500 01  CPWSFOOT    EXTERNAL.                                        UCOLDSWI
004600     COPY CPWSFOOT.                                               UCOLDSWI
004700                                                                  UCOLDSWI
004800 01  CA-COMMAREA EXTERNAL.                                        UCOLDSWI
004900     COPY CPWSCOMA.                                               UCOLDSWI
005000                                                                  UCOLDSWI
005100******************************************************************UCOLDSWI
005200*          SQL - WORKING STORAGE                                 *UCOLDSWI
005300******************************************************************UCOLDSWI
005400                                                                  UCOLDSWI
005500 01  CFN-ROW.                                                     UCOLDSWI
005600     EXEC SQL                                                     UCOLDSWI
005700          INCLUDE UC0VZCFN                                        UCOLDSWI
005800     END-EXEC.                                                    UCOLDSWI
005900                                                                  UCOLDSWI
005910 01  MSG-ROW.                                                     UCOLDSWI
005920     EXEC SQL                                                     UCOLDSWI
005930          INCLUDE UC0VZMSG                                        UCOLDSWI
005940     END-EXEC.                                                    UCOLDSWI
005950                                                                  UCOLDSWI
006000     EXEC SQL                                                     UCOLDSWI
006100          INCLUDE SQLCA                                           UCOLDSWI
006200     END-EXEC.                                                    UCOLDSWI
006300                                                                  UCOLDSWI
006400******************************************************************UCOLDSWI
006500*                                                                *UCOLDSWI
006600*                  SQL - SELECTS                                 *UCOLDSWI
006700*                                                                *UCOLDSWI
006800******************************************************************UCOLDSWI
006900 LINKAGE SECTION.                                                 UCOLDSWI
007000                                                                  UCOLDSWI
007100     EJECT                                                        UCOLDSWI
007200                                                                  UCOLDSWI
007300 PROCEDURE DIVISION.                                              UCOLDSWI
007400                                                                  UCOLDSWI
007500 0000-MAIN-LINE   SECTION.                                        UCOLDSWI
007600**************************************************************    UCOLDSWI
007700*  MAIN LINE PROCESSING SECTION                              *    UCOLDSWI
007800**************************************************************    UCOLDSWI
007900                                                                  UCOLDSWI
008000     EXEC SQL                                                     UCOLDSWI
008100          INCLUDE CPPDXE99                                        UCOLDSWI
008200     END-EXEC.                                                    UCOLDSWI
008300                                                                  UCOLDSWI
008400     PERFORM 0100-INITIALIZATIONS.                                UCOLDSWI
008500                                                                  UCOLDSWI
008600     PERFORM 1000-CALL-PPFCVALD.                                  UCOLDSWI
008700                                                                  UCOLDSWI
009000     IF UCCOMMON-RETURN-NO-ERRORS                                 UCOLDSWI
009100        IF (CPWSWORK-UNIQUE-EE-ID NOT = SPACES)                   UCOLDSWI
009200         AND (CPWSWORK-UNIQUE-EE-ID NOT = LOW-VALUES)             UCOLDSWI
009210          AND (NOT ORCA-SWITCH)                                   UCOLDSWI
009300           PERFORM 2000-CALL-UNIQUE                               UCOLDSWI
009400        ELSE                                                      UCOLDSWI
009500           IF  NOT CA-MENU-FUNCTION                               UCOLDSWI
009600            OR ORCA-SWITCH                                        UCOLDSWI
009700              MOVE UCOLDINT-NEXT-FUNCTION TO                      UCOLDSWI
009800                          CA-BROWSE-RETURN                        UCOLDSWI
009900              SET UCCOMMON-SWITCH-FUNCTIONS TO TRUE               UCOLDSWI
010000              MOVE CA-BROWSE-FUNCTION TO                          UCOLDSWI
010100                            UCCOMMON-FUNCTION-REQUESTED           UCOLDSWI
010200           END-IF                                                 UCOLDSWI
010300        END-IF                                                    UCOLDSWI
010400     END-IF.                                                      UCOLDSWI
011800                                                                  36430815
011900     IF UCCOMMON-RETURN-NO-ERRORS                                 36430815
012000         CONTINUE                                                 36430815
012100     ELSE                                                         36430815
013012       IF UCCOMMON-MSG-NUMBER NOT = MP0002                        36430875
013013*      PPFCVALD HAS DETERMINED THAT WE HAVE ACCESS TO             36430875
013014*      THE REQUESTED FUNCTION CODE.                               36430875
012200         MOVE UCOLDINT-NEXT-FUNCTION TO CA-BROWSE-RETURN          36430815
012300         SET UCCOMMON-SWITCH-FUNCTIONS TO TRUE                    36430815
012400         MOVE CA-BROWSE-FUNCTION TO UCCOMMON-FUNCTION-REQUESTED   36430815
012500         SET UCCOMMON-RETURN-NO-ERRORS TO TRUE                    36430815
012600         GO TO 0010-GOBACK                                        36430815
013410       END-IF                                                     36430875
012700     END-IF.                                                      36430815
010500                                                                  UCOLDSWI
010600     IF UCCOMMON-RETURN-NO-ERRORS                                 UCOLDSWI
010700        IF UCOLDINT-NEXT-FUNC-IS-WHO                              UCOLDSWI
010800           IF CPWSFOOT-ENTERED-FUNC NOT = 'WHO'                   UCOLDSWI
010900              PERFORM 3000-WHO-SETUP                              UCOLDSWI
011000           ELSE                                                   UCOLDSWI
011100              MOVE 'WHO' TO CA-BROWSE-RETURN                      UCOLDSWI
011200           END-IF                                                 UCOLDSWI
011300        END-IF                                                    UCOLDSWI
011400     END-IF.                                                      UCOLDSWI
011500                                                                  UCOLDSWI
013900 0010-GOBACK.                                                     36430815
011600     GOBACK.                                                      UCOLDSWI
011700                                                                  UCOLDSWI
011800 0000-EXIT.                                                       UCOLDSWI
011900     EXIT.                                                        UCOLDSWI
012000                                                                  UCOLDSWI
012100     EJECT                                                        UCOLDSWI
012200                                                                  UCOLDSWI
012300 0100-INITIALIZATIONS   SECTION.                                  UCOLDSWI
012400**************************************************************    UCOLDSWI
012500*  MISC INITIALIZATIONS                                      *    UCOLDSWI
012600**************************************************************    UCOLDSWI
012700                                                                  UCOLDSWI
012800     MOVE ZERO TO UCCOMMON-RETURN-CODE.                           UCOLDSWI
012900     SET UCOLDINT-RECORD-KEY-ENTRY-NO TO TRUE.                    UCOLDSWI
013000     MOVE UCOLDINT-NEXT-FUNCTION TO CA-NEXT-FUNCTION.             UCOLDSWI
013100     MOVE UCCOMMON-FUNCTION      TO CA-LAST-FUNCTION.             UCOLDSWI
013200     MOVE 'MENU'                 TO CA-LAST-MENU.                 UCOLDSWI
013300     MOVE 'A' TO CA-EDB-VERSION.                                  UCOLDSWI
013310     MOVE SPACES TO CA-UNIQUE-EE-ID.                              UCOLDSWI
013400                                                                  UCOLDSWI
013500     IF UCCOMMON-NESTED-FUNCTION                                  UCOLDSWI
013600        MOVE MU0030 TO MSG-NUMBER                                 UCOLDSWI
013610        PERFORM 9100-SELECT-MSG                                   UCOLDSWI
013620        IF SQLCODE = ZERO                                         UCOLDSWI
013630           MOVE MSG-TEXT TO CA-CRITICAL-MSG                       UCOLDSWI
013640        ELSE                                                      UCOLDSWI
013650           PERFORM 999999-SQL-ERROR                               UCOLDSWI
013660        END-IF                                                    UCOLDSWI
013700     ELSE                                                         UCOLDSWI
013800        MOVE SPACES TO CA-CRITICAL-MSG                            UCOLDSWI
013900     END-IF.                                                      UCOLDSWI
017100                                                                  36430815
017200     EVALUATE TRUE                                                36430815
017300       WHEN (CPWSFOOT-ENTERED-ID NOT = SPACES)                    36430815
017400        AND (CPWSFOOT-ENTERED-ID NOT = LOW-VALUES)                36430815
017500         MOVE CPWSFOOT-ENTERED-ID TO CA-ENTERED-ID                36430815
017600       WHEN (CPWSFOOT-ENTERED-NAME NOT = SPACES)                  36430815
017700        AND (CPWSFOOT-ENTERED-NAME NOT = LOW-VALUES)              36430815
017800         MOVE CPWSFOOT-ENTERED-NAME TO CA-ENTERED-NAME            36430815
017900       WHEN (CPWSFOOT-ENTERED-SSN NOT = SPACES)                   36430815
018000        AND (CPWSFOOT-ENTERED-SSN NOT = LOW-VALUES)               36430815
018100         MOVE CPWSFOOT-ENTERED-SSN TO CA-ENTERED-SSN              36430815
018200     END-EVALUATE.                                                36430815
014000                                                                  UCOLDSWI
014100 0100-EXIT.                                                       UCOLDSWI
014200     EXIT.                                                        UCOLDSWI
014300                                                                  UCOLDSWI
014400     EJECT                                                        UCOLDSWI
014500                                                                  UCOLDSWI
014600 1000-CALL-PPFCVALD   SECTION.                                    UCOLDSWI
014700**************************************************************    UCOLDSWI
014800*  CALL PPFCVALD TO VALIDATE FUNCTION                        *    UCOLDSWI
014900**************************************************************    UCOLDSWI
015000                                                                  UCOLDSWI
015100     CALL WS-PPFCVALD-PGM-NAME USING DFHEIBLK CA-COMMAREA.        UCOLDSWI
015200                                                                  UCOLDSWI
015300     EVALUATE CA-PPFCVALD-RETURN-CODE                             UCOLDSWI
015400       WHEN 0                                                     UCOLDSWI
015500         CONTINUE                                                 UCOLDSWI
015600       WHEN 1                                                     UCOLDSWI
015700         MOVE CA-MESSAGE-NO  TO UCCOMMON-MSG-NUMBER               UCOLDSWI
015800         MOVE SPACES TO CA-NEXT-FUNCTION                          UCOLDSWI
015900                        CA-COMMAND                                UCOLDSWI
016000         SET UCCOMMON-RETURN-ERRORS-FOUND TO TRUE                 UCOLDSWI
016010       WHEN 9                                                     UCOLDSWI
016020         SET ORCA-SWITCH TO TRUE                                  UCOLDSWI
016100     END-EVALUATE.                                                UCOLDSWI
016200                                                                  UCOLDSWI
016300 1000-EXIT.                                                       UCOLDSWI
016400     EXIT.                                                        UCOLDSWI
016500                                                                  UCOLDSWI
016600     EJECT                                                        UCOLDSWI
016700                                                                  UCOLDSWI
016800 2000-CALL-UNIQUE   SECTION.                                      UCOLDSWI
016900**************************************************************    UCOLDSWI
017000*  CALL PPUNIXXX TO VALIDATE ID                              *    UCOLDSWI
017100**************************************************************    UCOLDSWI
017200                                                                  UCOLDSWI
021800     IF CA-ENTERED-ID = LOW-VALUE OR SPACE                        36430815
021900         IF UCCOMMON-BUN-APPL-DATA = 'H' OR 'R'                   36430815
022000             CONTINUE                                             36430815
022100         ELSE                                                     36430815
022200             MOVE CPWSWORK-UNIQUE-EE-ID TO CA-ENTERED-ID          36430815
022300         END-IF                                                   36430815
022400     END-IF.                                                      36430815
022500*****MOVE CPWSWORK-UNIQUE-EE-ID TO CA-ENTERED-ID.                 36430815
022600*****MOVE SPACES TO CA-ENTERED-NAME.                              36430815
022700*****MOVE SPACES TO CA-ENTERED-SSN.                               36430815
017600                                                                  UCOLDSWI
017700     CALL CA-UNIQUE-PROGRAM USING DFHEIBLK CA-COMMAREA.           UCOLDSWI
017800                                                                  UCOLDSWI
017900     IF CA-UNIQUE-RETURN-CODE = ZERO OR 1                         UCOLDSWI
018000        NEXT SENTENCE                                             UCOLDSWI
018100     ELSE                                                         UCOLDSWI
023400*****   MOVE CA-MESSAGE-NO TO UCCOMMON-MSG-NUMBER                 36430815
018300        SET UCCOMMON-RETURN-ERRORS-FOUND TO TRUE                  UCOLDSWI
018400     END-IF.                                                      UCOLDSWI
018500                                                                  UCOLDSWI
018600 2000-EXIT.                                                       UCOLDSWI
018700     EXIT.                                                        UCOLDSWI
018800                                                                  UCOLDSWI
018900     EJECT                                                        UCOLDSWI
019000                                                                  UCOLDSWI
019100 3000-WHO-SETUP   SECTION.                                        UCOLDSWI
019200**************************************************************    UCOLDSWI
019300*  SET COMMAREA FIELDS FOR WHO FUNCTION                      *    UCOLDSWI
019400**************************************************************    UCOLDSWI
019500                                                                  UCOLDSWI
019600     IF (CPWSFOOT-ENTERED-FUNC NOT = SPACES)                      UCOLDSWI
019700      AND (CPWSFOOT-ENTERED-FUNC NOT = LOW-VALUES)                UCOLDSWI
025800      AND (UCCOMMON-BUNDLE-SEQ NOT = +1)                          36430833
019800        MOVE CPWSFOOT-ENTERED-FUNC TO CA-BROWSE-RETURN            UCOLDSWI
019900     ELSE                                                         UCOLDSWI
020000        MOVE UCCOMMON-FUNCTION TO CA-BROWSE-RETURN                UCOLDSWI
020100     END-IF.                                                      UCOLDSWI
020200                                                                  UCOLDSWI
020300**************************************************************    UCOLDSWI
020400*  GET THE CFN ROW FOR THE FUNCTION ENTERED. IF IT IS AN     *    UCOLDSWI
020500*  OLD SYSTEM FUNCTION, THEN PPWWHO WILL HANDLE              *    UCOLDSWI
020600*  SWITCHING FUNCTIONS IF THE USER CURSOR SELECTS A FUNCTION.*    UCOLDSWI
020700*  OTHERWISE, UCROUTER WILL HANDLE IT SO CA-RETURN-TO-ROUTER *    UCOLDSWI
020800*  IS SET.                                                   *    UCOLDSWI
020900**************************************************************    UCOLDSWI
021000     IF (CPWSFOOT-ENTERED-FUNC NOT = SPACES)                      UCOLDSWI
021100       AND (CPWSFOOT-ENTERED-FUNC NOT = LOW-VALUES)               UCOLDSWI
021200         PERFORM 9000-SELECT-CFN                                  UCOLDSWI
021300         IF (SQLCODE NOT = ZERO)                                  UCOLDSWI
021310           OR (CFN-OLD-SYSTEM-IND NOT = 'Y')                      UCOLDSWI
021400             SET CA-RETURN-TO-ROUTER TO TRUE                      UCOLDSWI
021500         END-IF                                                   UCOLDSWI
021600     END-IF.                                                      UCOLDSWI
021700                                                                  UCOLDSWI
021800     MOVE SPACES TO CA-UNIQUE-EE-ID                               UCOLDSWI
021900                    CA-ENTERED-ID                                 UCOLDSWI
022000                    CA-ENTERED-NAME                               UCOLDSWI
022100                    CA-ENTERED-SSN.                               UCOLDSWI
022200                                                                  UCOLDSWI
022300     EVALUATE TRUE                                                UCOLDSWI
022400       WHEN (CPWSFOOT-ENTERED-ID NOT = SPACES)                    UCOLDSWI
022500        AND (CPWSFOOT-ENTERED-ID NOT = LOW-VALUES)                UCOLDSWI
022600         MOVE 'I' TO CA-WHO-SEQ                                   UCOLDSWI
022700         MOVE CPWSFOOT-ENTERED-ID TO CA-ENTERED-ID                UCOLDSWI
022800       WHEN (CPWSFOOT-ENTERED-NAME NOT = SPACES)                  UCOLDSWI
022900        AND (CPWSFOOT-ENTERED-NAME NOT = LOW-VALUES)              UCOLDSWI
023000         MOVE 'N' TO CA-WHO-SEQ                                   UCOLDSWI
023100         MOVE CPWSFOOT-ENTERED-NAME TO CA-ENTERED-NAME            UCOLDSWI
023200       WHEN (CPWSFOOT-ENTERED-SSN NOT = SPACES)                   UCOLDSWI
023300        AND (CPWSFOOT-ENTERED-SSN NOT = LOW-VALUES)               UCOLDSWI
023400         MOVE 'S' TO CA-WHO-SEQ                                   UCOLDSWI
023500         MOVE CPWSFOOT-ENTERED-SSN TO CA-ENTERED-SSN              UCOLDSWI
023600     END-EVALUATE.                                                UCOLDSWI
023700                                                                  UCOLDSWI
023800 3000-EXIT.                                                       UCOLDSWI
023900     EXIT.                                                        UCOLDSWI
024000                                                                  UCOLDSWI
024100     EJECT                                                        UCOLDSWI
024200                                                                  UCOLDSWI
024300 9000-SELECT-CFN   SECTION.                                       UCOLDSWI
024400**************************************************************    UCOLDSWI
024500*  SELECT FIELDS FROM THE CFN TABLE                          *    UCOLDSWI
024600**************************************************************    UCOLDSWI
024700                                                                  UCOLDSWI
024800     MOVE CPWSFOOT-ENTERED-FUNC TO CFN-FUNCTION-ID.               UCOLDSWI
024900                                                                  UCOLDSWI
024910     MOVE 'SELECT CFN' TO DB2MSG-TAG.                             UCOLDSWI
024920                                                                  UCOLDSWI
025000     EXEC SQL                                                     UCOLDSWI
025100          SELECT CFN_OLD_SYSTEM_IND                               UCOLDSWI
025200          INTO  :CFN-OLD-SYSTEM-IND                               UCOLDSWI
025300          FROM UC0VZCFN_CFN                                       UCOLDSWI
025400          WHERE CFN_FUNCTION_ID = :CFN-FUNCTION-ID                UCOLDSWI
025500     END-EXEC.                                                    UCOLDSWI
025600                                                                  UCOLDSWI
026100 9000-EXIT.                                                       UCOLDSWI
026200     EXIT.                                                        UCOLDSWI
026300                                                                  UCOLDSWI
026400     EJECT                                                        UCOLDSWI
026500                                                                  UCOLDSWI
026602 9100-SELECT-MSG      SECTION.                                    UCOLDSWI
026603**************************************************************    UCOLDSWI
026604*  SELECT A ROW FROM THE MSG (MESSAGE) TABLE                 *    UCOLDSWI
026605**************************************************************    UCOLDSWI
026608                                                                  UCOLDSWI
026609     MOVE 'SELECT MSG' TO DB2MSG-TAG.                             UCOLDSWI
026610                                                                  UCOLDSWI
026611     EXEC SQL                                                     UCOLDSWI
026612          SELECT  MSG_TEXT                                        UCOLDSWI
026614            INTO :MSG-TEXT                                        UCOLDSWI
026616            FROM  UC0VZMSG_MSG                                    UCOLDSWI
026617            WHERE MSG_NUMBER = :MSG-NUMBER                        UCOLDSWI
026618     END-EXEC.                                                    UCOLDSWI
026619                                                                  UCOLDSWI
026633 9100-EXIT.                                                       UCOLDSWI
026634     EXIT.                                                        UCOLDSWI
026635                                                                  UCOLDSWI
026636     EJECT                                                        UCOLDSWI
026637                                                                  UCOLDSWI
026640 9999-ABEND    SECTION.                                           UCOLDSWI
026700**************************************************************    UCOLDSWI
026800*  COPIED PROCEDURE DIVISION CODE                            *    UCOLDSWI
026900**************************************************************    UCOLDSWI
027000                                                                  UCOLDSWI
027100     MOVE 'UCOLDSWI' TO UCWSABND-ABENDING-PGM.                    UCOLDSWI
027200     COPY UCPDABND.                                               UCOLDSWI
027300                                                                  UCOLDSWI
027400 9999-EXIT.                                                       UCOLDSWI
027500     EXIT.                                                        UCOLDSWI
027600                                                                  UCOLDSWI
027700     EJECT                                                        UCOLDSWI
027800                                                                  UCOLDSWI
027900*999999-SQL-ERROR SECTION.                                        UCOLDSWI
028000**************************************************************    UCOLDSWI
028100*  COPIED PROCEDURE DIVISION CODE FOR SQL ERROR HANDLING     *    UCOLDSWI
028200**************************************************************    UCOLDSWI
028300                                                                  UCOLDSWI
028400      EXEC SQL                                                    UCOLDSWI
028500           INCLUDE CPPDXP99                                       UCOLDSWI
028600      END-EXEC.                                                   UCOLDSWI
028700                                                                  UCOLDSWI
028800      MOVE 'SQL ERROR - SEE PRECEDING DB2 MESSAGES'               UCOLDSWI
028900                                      TO UCWSABND-ABEND-DATA (1). UCOLDSWI
029000                                                                  UCOLDSWI
029100      PERFORM 9999-ABEND.                                         UCOLDSWI
029200                                                                  UCOLDSWI
029300 999999-EXIT.                                                     UCOLDSWI
029400       EXIT.                                                      UCOLDSWI
029500***************    END OF SOURCE - UCOLDSWI    *******************UCOLDSWI
