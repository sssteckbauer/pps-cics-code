000100**************************************************************/   36510832
000200*  PROGRAM: UCA2SACA                                         */   36510832
000300*  RELEASE: ___0832______ SERVICE REQUEST(S): _____3651____  */   36510832
000400*  NAME:_______DDM_______ MODIFICATION DATE:  __11/12/93____ */   36510832
000500*  DESCRIPTION: DSS - DISTRIBUTED SYSTEM SERVICES            */   36510832
000600*    REFRESH EXTERNALS AS NECESSARY FOR PRINT FUNCTIONS      */   36510832
000700*    TO RESET  AN ACTIVE UPDATE PROCESS.                     */   36510832
000800**************************************************************/   36510832
000900 IDENTIFICATION DIVISION.                                         UCA2SACA
001000 PROGRAM-ID. UCA2SACA.                                            UCA2SACA
001100 ENVIRONMENT DIVISION.                                            UCA2SACA
001200 CONFIGURATION SECTION.                                           UCA2SACA
001300 SOURCE-COMPUTER.                                                 UCA2SACA
001400 COPY  CPOTXUCS.                                                  UCA2SACA
001500     EJECT                                                        UCA2SACA
001600 DATA DIVISION.                                                   UCA2SACA
001700 WORKING-STORAGE SECTION.                                         UCA2SACA
001800 COPY CPYRIGHT.                                                   UCA2SACA
001900     EJECT                                                        UCA2SACA
002000 01 LIMITS-AND-CONSTANTS.                                         UCA2SACA
002100    05  PGM-NAME                           PIC X(08)              UCA2SACA
002200                                               VALUE 'UCA2SACA'.  UCA2SACA
002300    05  PGM-RELEASE-NBR                    PIC X(05)              UCA2SACA
002400                                               VALUE 'E0832'.     UCA2SACA
002500*                                                                 UCA2SACA
002600 01  WS-MESS-NOS.                                                 UCA2SACA
002700     05  U0000             PIC X(5) VALUE 'U0000'.                UCA2SACA
002800     EJECT                                                        UCA2SACA
002900 01  CPWSPRNT EXTERNAL.                                           UCA2SACA
003000     COPY CPWSPRNT.                                               UCA2SACA
003300     EJECT                                                        UCA2SACA
003400 01  UCCOMMON                EXTERNAL.                            UCA2SACA
003410     COPY UCCOMMON.                                               UCA2SACA
003800     EJECT                                                        UCA2SACA
003900 01  UCWSABND                EXTERNAL.                            UCA2SACA
003910     COPY UCWSABND.                                               UCA2SACA
005000     EJECT                                                        UCA2SACA
005100 PROCEDURE DIVISION.                                              UCA2SACA
005200****************************************************************  UCA2SACA
005300*            P R O C E D U R E  D I V I S I O N                   UCA2SACA
005400****************************************************************  UCA2SACA
005900 0000-MAINLINE SECTION.                                           UCA2SACA
006000**************************************************************    UCA2SACA
006100**************************************************************    UCA2SACA
006200     EVALUATE UCCOMMON-FUNCTION                                   UCA2SACA
006410     WHEN 'PRNT'                                                  UCA2SACA
006420       MOVE SPACE TO PRNT-UPD-AREA                                UCA2SACA
006430       MOVE UCCOMMON-PRINT-COPIES TO PRNT-COPIES-GRP              UCA2SACA
006500     WHEN 'EPRT'                                                  UCA2SACA
006600       MOVE SPACE TO EPRT-UPD-AREA                                UCA2SACA
006700     END-EVALUATE                                                 UCA2SACA
006800     GOBACK.                                                      UCA2SACA
006900 EJECT                                                            UCA2SACA
007000 9999-ABEND    SECTION.                                           UCA2SACA
007100**************************************************************    UCA2SACA
007200*  COPIED PROCEDURE DIVISION CODE FOR ABEND HANDLING         *    UCA2SACA
007300**************************************************************    UCA2SACA
007400*                                                                 UCA2SACA
007500     MOVE PGM-NAME   TO UCWSABND-ABENDING-PGM.                    UCA2SACA
007600*                                                                 UCA2SACA
007700     COPY UCPDABND.                                               UCA2SACA
007800*                                                                 UCA2SACA
007900 9999-EXIT.                                                       UCA2SACA
008000     EXIT.                                                        UCA2SACA
