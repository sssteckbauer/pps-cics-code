000010*==========================================================%      UCSD0061
000020*= UCSD LOCAL CHANGES HAVE BEEN IMPLEMENTED.              =%      UCSD0061
000030*= COLUMN 73 THRU 80 DENOTES CHANGE NUMBER - UCSD0061     =%      UCSD0061
000040*=                                                        =%      UCSD0061
000050*==========================================================%      UCSD0061
000060*==========================================================%      UCSD0061
000070*=    PROGRAM: PPAPORKS                                   =%      UCSD0061
000080*=    CHANGE # UCSD0061     PROJ. REQUEST: UCSD0061       =%      UCSD0061
000090*=    NAME: RON BLOCK       MODIFICATION DATE: 10/05/94   =%      UCSD0061
000091*=                                                        =%      UCSD0061
000092*=    DESCRIPTION:                                        =%      UCSD0061
000095*=     ADDED TEST FOR CPWSWORK-ID-LENGTH NUMERIC PRIOR    =%      UCSD0061
000096*=     TO CHECKING ITS VALUE.  IF ID WAS NOT ENTERED,     =%      UCSD0061
000097*=     PROGRAM WAS ABENDING.                              =%      UCSD0061
000098*==========================================================%      UCSD0061
000100**************************************************************/   EFIX1078
000200*  PROGRAM: PPAPORKS                                         */   EFIX1078
000300*  RELEASE: ___1078______ SERVICE REQUEST(S): _____EFIX____  */   EFIX1078
000400*  NAME:_______SRS_______ MODIFICATION DATE:  ___07/17/96___ */   EFIX1078
000401*  DESCRIPTION: ERROR REPORT 1415:                           */   EFIX1078
000500*  - FIX FOR ID FIELD ABEND.                                 */   EFIX1078
000510*  - ADD PPINITWK CALL FOR UCROUTER CALL FROM MAIN MENU      */   EFIX1078
000600**************************************************************/   EFIX1078
000100**************************************************************/   36430943
000200*  PROGRAM: PPAPORKS                                         */   36430943
000300*  RELEASE: ___0943______ SERVICE REQUEST(S): _____3643____  */   36430943
000400*  NAME:_______SRS_______ MODIFICATION DATE:  __11/10/94____ */   36430943
000500*  INCLUSION OF KEY SWITCHING LOGIC                          */   36430943
000510*  MODIFY CPWSRCPW EXTERNAL NAME REFERENCE FOR CONSISTENCY   */   36430943
000600**************************************************************/   36430943
000100**************************************************************/   36430911
000200*  PROGRAM: PPAPORKS                                         */   36430911
000300*  RELEASE: ___0911______ SERVICE REQUEST(S): _____3643____  */   36430911
000400*  NAME:_______SRS_______ MODIFICATION DATE:  __06/30/94____ */   36430911
000500*  DESCRIPTION: SPECIAL PROCESSES KEY SWITCH PROGRAM         */   36430911
000600**************************************************************/   36430911
000700 IDENTIFICATION DIVISION.                                         PPAPORKS
000800 PROGRAM-ID. PPAPORKS.                                            PPAPORKS
000900 ENVIRONMENT DIVISION.                                            PPAPORKS
001000 CONFIGURATION SECTION.                                           PPAPORKS
001100 SOURCE-COMPUTER.                                                 PPAPORKS
001200 COPY CPOTXUCS.                                                   PPAPORKS
001300 DATA DIVISION.                                                   PPAPORKS
001400 WORKING-STORAGE SECTION.                                         PPAPORKS
001500 01  WS-MISC.                                                     PPAPORKS
002810     05  MP0001                     PIC X(5) VALUE 'P0001'.       EFIX1078
001600     05  MP0403                     PIC X(5) VALUE 'P0403'.       PPAPORKS
003000     05  MP0404                     PIC X(5) VALUE 'P0404'.       EFIX1078
001700     05  MP0406                     PIC X(5) VALUE 'P0406'.       PPAPORKS
001800     05  MP0407                     PIC X(5) VALUE 'P0407'.       PPAPORKS
001900     05  MP0415                     PIC X(5) VALUE 'P0415'.       PPAPORKS
002000     05  MP0504                     PIC X(5) VALUE 'P0504'.       PPAPORKS
002100     05  MP0505                     PIC X(5) VALUE 'P0505'.       PPAPORKS
002200     05  MP0506                     PIC X(5) VALUE 'P0506'.       PPAPORKS
002300     05  MP0507                     PIC X(5) VALUE 'P0507'.       PPAPORKS
002400     05  MP0508                     PIC X(5) VALUE 'P0508'.       PPAPORKS
002500     05  WS-COUNT                   PIC S9(8) COMP VALUE ZERO.    PPAPORKS
003910     05  WS-PPINITWK                PIC X(08)  VALUE 'PPINITWK'.  EFIX1078
002600 01  BLID-BUILD-ID-AREA.                                          PPAPORKS
002700     COPY CPWSBLID.                                               PPAPORKS
002800 01  PPDB2MSG-INTERFACE.                                          PPAPORKS
002900     COPY CPLNKDB2.                                               PPAPORKS
003000 01  ONLINE-SIGNALS EXTERNAL.                      COPY CPWSONLI. PPAPORKS
003100 01  UCCOMMON                EXTERNAL.                            PPAPORKS
003200     EXEC SQL                                                     PPAPORKS
003300          INCLUDE UCCOMMON                                        PPAPORKS
003400     END-EXEC.                                                    PPAPORKS
003500 01  UCPIROKS                EXTERNAL.                            PPAPORKS
003600     EXEC SQL                                                     PPAPORKS
003700          INCLUDE UCPIROKS                                        PPAPORKS
003800     END-EXEC.                                                    PPAPORKS
003900 01  CPWSSPEC                EXTERNAL.                            PPAPORKS
004000     EXEC SQL                                                     PPAPORKS
004100          INCLUDE CPWSSPEC                                        PPAPORKS
004200     END-EXEC.                                                    PPAPORKS
004300 01  CPWSRCTS                EXTERNAL.                            PPAPORKS
004400     EXEC SQL                                                     PPAPORKS
004500          INCLUDE CPWSRCTS                                        PPAPORKS
004600     END-EXEC.                                                    PPAPORKS
004700*01  CPWSRCPW                EXTERNAL.                            36430943
004710 01  RCPW-INTERFACE-DATA     EXTERNAL.                            36430943
004800     EXEC SQL                                                     PPAPORKS
004900          INCLUDE CPWSRCPW                                        PPAPORKS
005000     END-EXEC.                                                    PPAPORKS
005100 01  CPWSORTS                EXTERNAL.                            PPAPORKS
005200     EXEC SQL                                                     PPAPORKS
005300          INCLUDE CPWSORTS                                        PPAPORKS
005400     END-EXEC.                                                    PPAPORKS
005500 01  CPWSORCA                EXTERNAL.                            PPAPORKS
005600     EXEC SQL                                                     PPAPORKS
005700          INCLUDE CPWSORCA                                        PPAPORKS
005800     END-EXEC.                                                    PPAPORKS
005900 01  CPWSWORK                EXTERNAL.                            PPAPORKS
006000     EXEC SQL                                                     PPAPORKS
006100          INCLUDE CPWSWORK                                        PPAPORKS
006200     END-EXEC.                                                    PPAPORKS
006210 01  CPWSKEYS                EXTERNAL.                            36430943
006220     EXEC SQL                                                     36430943
006230          INCLUDE CPWSKEYS                                        36430943
006240     END-EXEC.                                                    36430943
006300 01  UCWSABND                EXTERNAL.                            PPAPORKS
006400     EXEC SQL                                                     PPAPORKS
006500          INCLUDE UCWSABND                                        PPAPORKS
006600     END-EXEC.                                                    PPAPORKS
006700 01  WS-PER.                                                      PPAPORKS
006800     EXEC SQL                                                     PPAPORKS
006900           INCLUDE PPPVZPER                                       PPAPORKS
007000     END-EXEC.                                                    PPAPORKS
007100 01  WS-ABD.                                                      PPAPORKS
007200     EXEC SQL                                                     PPAPORKS
007300           INCLUDE PPPVZABD                                       PPAPORKS
007400     END-EXEC.                                                    PPAPORKS
007500 01  WS-ABE.                                                      PPAPORKS
007600     EXEC SQL                                                     PPAPORKS
007700           INCLUDE PPPVZABE                                       PPAPORKS
007800     END-EXEC.                                                    PPAPORKS
007900 01  WS-EUD.                                                      PPAPORKS
008000     EXEC SQL                                                     PPAPORKS
008100           INCLUDE PPPVZEUD                                       PPAPORKS
008200     END-EXEC.                                                    PPAPORKS
008300     EXEC SQL                                                     PPAPORKS
008400           INCLUDE SQLCA                                          PPAPORKS
008500     END-EXEC.                                                    PPAPORKS
008600                                                                  PPAPORKS
008700 PROCEDURE DIVISION.                                              PPAPORKS
008800                                                                  PPAPORKS
008900 0000-MAINLINE SECTION.                                           PPAPORKS
009000                                                                  PPAPORKS
009100     EXEC SQL                                                     PPAPORKS
009200          INCLUDE CPPDXE99                                        PPAPORKS
009300     END-EXEC.                                                    PPAPORKS
009400     PERFORM 1000-CHECK-KEYS.                                     PPAPORKS
009500     GOBACK.                                                      PPAPORKS
009600                                                                  PPAPORKS
009700 1000-CHECK-KEYS SECTION.                                         PPAPORKS
009800                                                                  PPAPORKS
009900     MOVE 'PPAPORKS' TO DB2MSG-PGM-ID.                            PPAPORKS
011810                                                                  EFIX1078
011820     IF NOT CPWSWORK-INITIALIZED                                  EFIX1078
011830        CALL WS-PPINITWK USING DFHEIBLK                           EFIX1078
011840          ON EXCEPTION                                            EFIX1078
011850             STRING 'ERROR CALLING PROGRAM: ' DELIMITED BY SIZE   EFIX1078
011860                    WS-PPINITWK            DELIMITED BY SIZE      EFIX1078
011870                         INTO UCWSABND-ABEND-DATA (1)             EFIX1078
011880             END-STRING                                           EFIX1078
011890             PERFORM 9999-ABEND                                   EFIX1078
011891        END-CALL                                                  EFIX1078
011892     END-IF.                                                      EFIX1078
011893                                                                  EFIX1078
009910     IF NOT CPWSKEYS-INITIALIZED                                  36430943
009920        INITIALIZE CPWSKEYS-DATA                                  36430943
009930        SET CPWSKEYS-INITIALIZED TO TRUE                          36430943
009940     END-IF.                                                      36430943
010000     IF SPEC-ENTERED-ID NOT = SPACES AND LOW-VALUES               PPAPORKS
010100        MOVE SPEC-ENTERED-ID TO BLID-ID-DISPLAY-FORMAT            PPAPORKS
012700********MOVE SPEC-ID-LENGTH TO CPWSWORK-ID-LENGTH                 EFIX1078
012800********MOVE SPEC-LOCATION TO CPWSWORK-LOCATION                   EFIX1078
010400        PERFORM BUILD-ID                                          PPAPORKS
010500        MOVE BLID-ID-EDB-FORMAT TO SPEC-ENTERED-ID                PPAPORKS
010510                                   UCPIROKS-RECORD-KEY            36430943
010520                                   CPWSKEYS-EMPLOYEE-ID           36430943
010600     END-IF.                                                      PPAPORKS
010700     IF SPEC-ORCA-FUNC                                            PPAPORKS
010800        GO TO 1300-CHECK-ORCA-KEYS                                PPAPORKS
010900     ELSE                                                         PPAPORKS
011000        GO TO 1700-CHECK-OLRC-KEYS                                PPAPORKS
011100     END-IF.                                                      PPAPORKS
011200                                                                  PPAPORKS
011300 1300-CHECK-ORCA-KEYS.                                            PPAPORKS
011400                                                                  PPAPORKS
011500     INITIALIZE CPWSORTS                                          PPAPORKS
011600                CPWSORCA.                                         PPAPORKS
011700     IF SPEC-ENTERED-ID = SPACES OR LOW-VALUES                    PPAPORKS
011800     OR SPEC-ENTERED-PAR = SPACES OR LOW-VALUES                   PPAPORKS
011900        MOVE MP0504 TO UCCOMMON-MSG-NUMBER                        PPAPORKS
012000        SET UCCOMMON-RETURN-ERRORS-FOUND TO TRUE                  PPAPORKS
012100        GO TO 1999-KEYS-EXIT                                      PPAPORKS
012200     END-IF.                                                      PPAPORKS
012300     PERFORM 8000-SELECT-ID-PER.                                  PPAPORKS
012400     IF  WS-COUNT = ZERO                                          PPAPORKS
012500     AND SPEC-ENTERED-FUNC NOT = 'DLTE'                           PPAPORKS
012600         MOVE MP0505 TO UCCOMMON-MSG-NUMBER                       PPAPORKS
012700         SET UCCOMMON-RETURN-ERRORS-FOUND TO TRUE                 PPAPORKS
012800         GO TO 1999-KEYS-EXIT                                     PPAPORKS
012900     END-IF.                                                      PPAPORKS
013000     IF NOT SPEC-ORCA-ABEY                                        PPAPORKS
013100        PERFORM 8600-SELECT-EUD                                   PPAPORKS
013200        IF SQLCODE NOT = ZERO                                     PPAPORKS
013300           MOVE MP0506 TO UCCOMMON-MSG-NUMBER                     PPAPORKS
013400           SET UCCOMMON-RETURN-ERRORS-FOUND TO TRUE               PPAPORKS
013500           GO TO 1999-KEYS-EXIT                                   PPAPORKS
013600        END-IF                                                    PPAPORKS
013700     END-IF.                                                      PPAPORKS
013800     PERFORM 8700-SELECT-ABE                                      PPAPORKS
013900     IF WS-COUNT = ZERO                                           PPAPORKS
014000       IF SPEC-ORCA-ABEY                                          PPAPORKS
014100          MOVE MP0507 TO UCCOMMON-MSG-NUMBER                      PPAPORKS
014200          SET UCCOMMON-RETURN-ERRORS-FOUND TO TRUE                PPAPORKS
014300       END-IF                                                     PPAPORKS
014400     ELSE                                                         PPAPORKS
014500       IF NOT SPEC-ORCA-ABEY                                      PPAPORKS
014600          MOVE MP0508 TO UCCOMMON-MSG-NUMBER                      PPAPORKS
014700          SET UCCOMMON-RETURN-ERRORS-FOUND TO TRUE                PPAPORKS
014800       END-IF                                                     PPAPORKS
014900     END-IF.                                                      PPAPORKS
015000     GO TO 1999-KEYS-EXIT.                                        PPAPORKS
015100                                                                  PPAPORKS
015200 1700-CHECK-OLRC-KEYS.                                            PPAPORKS
015300                                                                  PPAPORKS
015400     INITIALIZE CPWSRCTS                                          PPAPORKS
015500                RCPW-INTERFACE-DATA.                              36430943
018210*****                                                          CD EFIX1078
015600     MOVE SPEC-ENTERED-FUNC TO RCTS-TRANS-ID.                     PPAPORKS
015700     IF SPEC-RUSH-FUNC                                            PPAPORKS
018600        IF SPEC-ENTERED-ID = SPACES OR LOW-VALUES                 EFIX1078
018700           MOVE MP0404 TO UCCOMMON-MSG-NUMBER                     EFIX1078
018800           SET UCCOMMON-RETURN-ERRORS-FOUND TO TRUE               EFIX1078
018900           GO TO 1999-KEYS-EXIT                                   EFIX1078
019000        END-IF                                                    EFIX1078
015800        MOVE SPEC-ENTERED-ID TO RCTS-ID                           PPAPORKS
015900        PERFORM 8000-SELECT-ID-PER                                PPAPORKS
016000        MOVE SPEC-ENTERED-ID TO BLID-ID-EDB-FORMAT                PPAPORKS
019310*****                                                          CD EFIX1078
016110        IF CPWSWORK-ID-LENGTH NOT NUMERIC                         UCSD0061
016120           MOVE ZERO TO CPWSWORK-ID-LENGTH                        UCSD0061
016130        END-IF                                                    UCSD0061
016200        PERFORM FORMAT-DISPLAY-ID                                 PPAPORKS
016300        MOVE BLID-ID-DISPLAY-FORMAT TO SPEC-DISPLAY-ID            PPAPORKS
016400        IF WS-COUNT = 0                                           PPAPORKS
016500           MOVE MP0406 TO UCCOMMON-MSG-NUMBER                     PPAPORKS
016600        END-IF                                                    PPAPORKS
016700        GO TO 1999-KEYS-EXIT                                      PPAPORKS
016800     END-IF.                                                      PPAPORKS
016900     IF SPEC-ENTERED-FUNC = 'RCAD'                                PPAPORKS
017000        PERFORM 8100-SELECT-CHK-ABY                               PPAPORKS
017100        IF WS-COUNT NOT > ZERO                                    PPAPORKS
017200           MOVE MP0407 TO UCCOMMON-MSG-NUMBER                     PPAPORKS
017300           SET UCCOMMON-RETURN-ERRORS-FOUND TO TRUE               PPAPORKS
017400           SET UCCOMMON-REDISPLAY-FUNC TO TRUE                    PPAPORKS
017500           SET SPEC-REDISPLAY-ID TO TRUE                          PPAPORKS
017600           MOVE SPACES TO SPEC-ENTERED-ID                         PPAPORKS
017700        END-IF                                                    PPAPORKS
017800        GO TO 1999-KEYS-EXIT                                      PPAPORKS
017900     END-IF.                                                      PPAPORKS
018000     IF SPEC-ENTERED-FUNC = 'RCAI'                                PPAPORKS
018100       IF SPEC-ENTERED-CHECK NOT = SPACES AND LOW-VALUES          PPAPORKS
018200          PERFORM 8100-SELECT-CHK-ABY                             PPAPORKS
018300          IF WS-COUNT NOT > ZERO                                  PPAPORKS
018400             MOVE MP0407 TO UCCOMMON-MSG-NUMBER                   PPAPORKS
018500             SET UCCOMMON-RETURN-ERRORS-FOUND TO TRUE             PPAPORKS
018600             SET UCCOMMON-REDISPLAY-FUNC TO TRUE                  PPAPORKS
018700             SET SPEC-REDISPLAY-CHECK TO TRUE                     PPAPORKS
018800             MOVE SPACES TO SPEC-ENTERED-CHECK                    PPAPORKS
018900          END-IF                                                  PPAPORKS
019000       ELSE                                                       PPAPORKS
019100         MOVE SPEC-ENTERED-ID TO RCTS-ID                          PPAPORKS
019200         PERFORM 8200-SELECT-ID-ABY                               PPAPORKS
019300         IF WS-COUNT NOT > ZERO                                   PPAPORKS
019400            MOVE MP0415 TO UCCOMMON-MSG-NUMBER                    PPAPORKS
019500            SET UCCOMMON-RETURN-ERRORS-FOUND TO TRUE              PPAPORKS
019600            SET UCCOMMON-REDISPLAY-FUNC TO TRUE                   PPAPORKS
019700            SET SPEC-REDISPLAY-ID TO TRUE                         PPAPORKS
019800            MOVE SPACES TO SPEC-ENTERED-ID                        PPAPORKS
019900         END-IF                                                   PPAPORKS
020000       END-IF                                                     PPAPORKS
020100     END-IF.                                                      PPAPORKS
020200                                                                  PPAPORKS
020300 1999-KEYS-EXIT.                                                  PPAPORKS
020400      EXIT.                                                       PPAPORKS
020500                                                                  PPAPORKS
020600                                                                  PPAPORKS
020700 8000-SELECT-ID-PER SECTION.                                      PPAPORKS
020800                                                                  PPAPORKS
020900     MOVE 'SELECT ID PER' TO DB2MSG-TAG.                          PPAPORKS
021000     EXEC SQL                                                     PPAPORKS
021100         SELECT                                                   PPAPORKS
021200         COUNT (*)                                                PPAPORKS
021300         INTO :WS-COUNT                                           PPAPORKS
021400         FROM PPPVZPER_PER                                        PPAPORKS
021500         WHERE EMPLOYEE_ID =  :SPEC-ENTERED-ID                    PPAPORKS
021600     END-EXEC.                                                    PPAPORKS
021700                                                                  PPAPORKS
021800 8100-SELECT-CHK-ABY SECTION.                                     PPAPORKS
021900                                                                  PPAPORKS
022000     MOVE 'SELECT CHK ABY' TO DB2MSG-TAG.                         PPAPORKS
022100     EXEC SQL                                                     PPAPORKS
022200         SELECT                                                   PPAPORKS
022300         COUNT (*)                                                PPAPORKS
022400         INTO :WS-COUNT                                           PPAPORKS
022500         FROM PPPVZABD_ABD                                        PPAPORKS
022600         WHERE CHECK_NUMBER = :SPEC-ENTERED-CHECK                 PPAPORKS
022700     END-EXEC.                                                    PPAPORKS
022800                                                                  PPAPORKS
022900 8200-SELECT-ID-ABY SECTION.                                      PPAPORKS
023000                                                                  PPAPORKS
023100     MOVE 'SELECT ID ABY' TO DB2MSG-TAG.                          PPAPORKS
023200     EXEC SQL                                                     PPAPORKS
023300         SELECT                                                   PPAPORKS
023400         COUNT (*)                                                PPAPORKS
023500         INTO :WS-COUNT                                           PPAPORKS
023600         FROM PPPVZABD_ABD                                        PPAPORKS
023700         WHERE EMPLOYEE_ID =  :SPEC-ENTERED-ID                    PPAPORKS
023800     END-EXEC.                                                    PPAPORKS
023900                                                                  PPAPORKS
024000 8600-SELECT-EUD SECTION.                                         PPAPORKS
024100                                                                  PPAPORKS
024200     MOVE '8600-SELECT-EUD' TO DB2MSG-TAG.                        PPAPORKS
024300     EXEC SQL                                                     PPAPORKS
024400      SELECT                                                      PPAPORKS
024500        EMPLOYEE_ID ,                                             PPAPORKS
024600        PRI_GROSS_CTL ,                                           PPAPORKS
024700        SOC_SEC_NO ,                                              PPAPORKS
024800        EMP_NAME                                                  PPAPORKS
024900      INTO                                                        PPAPORKS
025000        :WS-EUD.EMPLOYEE-ID ,                                     PPAPORKS
025100        :WS-EUD.PRI-GROSS-CTL ,                                   PPAPORKS
025200        :WS-EUD.SOC-SEC-NO ,                                      PPAPORKS
025300        :WS-EUD.EMP-NAME                                          PPAPORKS
025400      FROM PPPVZEUD_EUD                                           PPAPORKS
025500      WHERE EMPLOYEE_ID  = :SPEC-ENTERED-ID                       PPAPORKS
025600        AND PRI_GROSS_CTL  = :SPEC-ENTERED-PAR                    PPAPORKS
025700     END-EXEC.                                                    PPAPORKS
025800     IF  SQLCODE  = +100                                          PPAPORKS
025900         MOVE MP0506 TO UCCOMMON-MSG-NUMBER                       PPAPORKS
026000     ELSE                                                         PPAPORKS
026100     IF SQLCODE = +0                                              PPAPORKS
026200        MOVE EMP-NAME OF WS-EUD   TO SPEC-NAME                    PPAPORKS
026300        MOVE SOC-SEC-NO OF WS-EUD TO SPEC-SSN                     PPAPORKS
026400     END-IF.                                                      PPAPORKS
026500                                                                  PPAPORKS
026600                                                                  PPAPORKS
026700 8700-SELECT-ABE SECTION.                                         PPAPORKS
026800                                                                  PPAPORKS
026900     EXEC SQL                                                     PPAPORKS
027000         SELECT                                                   PPAPORKS
027100         COUNT (*)                                                PPAPORKS
027200         INTO :WS-COUNT                                           PPAPORKS
027300         FROM PPPVZABE_ABE                                        PPAPORKS
027400         WHERE EMPLOYEE_ID =  :SPEC-ENTERED-ID                    PPAPORKS
027500           AND PAR_CONTROL_NUMBER = :SPEC-ENTERED-PAR             PPAPORKS
027600     END-EXEC.                                                    PPAPORKS
027700                                                                  PPAPORKS
027800*BUILD-ID   SECTION.                                              PPAPORKS
027900*FORMAT-DISPLAY-ID   SECTION.                                     PPAPORKS
028000**************************************************************    PPAPORKS
028100*  COPIED PROCEDURE DIVISION CODE FOR BUILDING AND           *    PPAPORKS
028200*  FORMATTING EMPLOYEE ID BASED ON LENGTH.                   *    PPAPORKS
028300**************************************************************    PPAPORKS
028400     COPY CPPDBLID.                                               PPAPORKS
028500                                                                  PPAPORKS
028600 9999-ABEND    SECTION.                                           PPAPORKS
028700**************************************************************    PPAPORKS
028800*  COPIED PROCEDURE DIVISION CODE                            *    PPAPORKS
028900**************************************************************    PPAPORKS
029000     MOVE 'PPAPORKS' TO UCWSABND-ABENDING-PGM.                    PPAPORKS
029100     COPY UCPDABND.                                               PPAPORKS
029200                                                                  PPAPORKS
029300 9999-EXIT.                                                       PPAPORKS
029400     EXIT.                                                        PPAPORKS
029500                                                                  PPAPORKS
029600*999999-SQL-ERROR SECTION.                                        PPAPORKS
029700**************************************************************    PPAPORKS
029800*  COPIED PROCEDURE DIVISION CODE FOR SQL ERROR HANDLING     *    PPAPORKS
029900**************************************************************    PPAPORKS
030000      EXEC SQL                                                    PPAPORKS
030100           INCLUDE CPPDXP99                                       PPAPORKS
030200      END-EXEC.                                                   PPAPORKS
030300      MOVE 'SQL ERROR - SEE PRECEDING DB2 MESSAGES'               PPAPORKS
030400                                      TO UCWSABND-ABEND-DATA (1). PPAPORKS
030500      PERFORM 9999-ABEND.                                         PPAPORKS
030600                                                                  PPAPORKS
030700 999999-EXIT.                                                     PPAPORKS
030800       EXIT.                                                      PPAPORKS
030900***************** END OF PPAPORKS ********************************PPAPORKS
