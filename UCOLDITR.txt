000101**************************************************************/   36430815
000102*  PROGRAM: UCOLDITR                                         */   36430815
000103*  RELEASE: ___0815______ SERVICE REQUEST(S): _____3643____  */   36430815
000104*  NAME:_______MLG_______ MODIFICATION DATE:  __09/17/93____ */   36430815
000105*  DESCRIPTION:                                              */   36430815
000106*    REMOVE SECTION 1000-GET-PROGRAM-ID. CODE IS BEING       */   36430815
000107*    MOVED TO PROGRAM UCOLDFUN.                                   36430815
000108*                                                            */   36430815
000109**************************************************************/   36430815
000112**************************************************************/   36400805
001100*  REL805 SKIPPED TEMPORARY                                  */   36400805
000113*  PROGRAM: UCOLDITR                                         */   36400805
000114*  RELEASE # ___0805___   SERVICE REQUEST NO(S)____3640______*/   36400805
000115*  NAME __SRS__________   MODIFICATION DATE ____08/18/93_____*/   36400805
000116*  DESCRIPTION                                               */   36400805
000117*  MODIFIED FOR SPECIAL PROCESSES JUMP KEY LOGIC             */   36400805
000118**************************************************************/   36400805
000100**************************************************************/   36430795
000200*  PROGRAM: UCOLDITR                                         */   36430795
000300*  RELEASE # ___0795___   SERVICE REQUEST NO(S)____3643______*/   36430795
000400*  NAME __MLG__________   MODIFICATION DATE ____07/15/93_____*/   36430795
000500*  DESCRIPTION                                               */   36430795
000600*                                                            */   36430795
000700*     "OLD SYSTEM" ITERATION PROCESSOR                       */   36430795
000800*                                                            */   36430795
000900**************************************************************/   36430795
001000 IDENTIFICATION DIVISION.                                         UCOLDITR
001100 PROGRAM-ID. UCOLDITR.                                            UCOLDITR
001200 ENVIRONMENT DIVISION.                                            UCOLDITR
001210 CONFIGURATION SECTION.                                           UCOLDITR
001220 SOURCE-COMPUTER.                                                 UCOLDITR
001230 COPY  CPOTXUCS.                                                  UCOLDITR
001300 DATA DIVISION.                                                   UCOLDITR
001400 WORKING-STORAGE SECTION.                                         UCOLDITR
001700                                                                  UCOLDITR
001800 01  WS-COMN.                                                     UCOLDITR
001903     05  WS-START                          PIC X(27) VALUE        UCOLDITR
001904         'WORKING STORAGE STARTS HERE'.                           UCOLDITR
001905     05  WS-UCPCKUTL                       PIC X(08)              UCOLDITR
001906                                               VALUE 'UCPCKUTL'.  UCOLDITR
002100     COPY CPWSCOMN.                                               UCOLDITR
010800                                                                  UCOLDITR
010900 01  WS-MESS-NOS.                                                 UCOLDITR
010910     05  MU0015                            PIC X(5) VALUE 'U0015'.UCOLDITR
010920     05  MP0503                            PIC X(5) VALUE 'P0503'.UCOLDITR
010921                                                                  UCOLDITR
010922 01  PPDB2MSG-INTERFACE.                                          UCOLDITR
010923     COPY CPLNKDB2.                                               UCOLDITR
010930                                                                  UCOLDITR
010950 01  UCCOMMON   EXTERNAL.                                         UCOLDITR
010960     COPY UCCOMMON.                                               UCOLDITR
011000                                                                  UCOLDITR
011100 01  UCOLDINT   EXTERNAL.                                         UCOLDITR
011200     COPY UCOLDINT.                                               UCOLDITR
011300                                                                  UCOLDITR
011310 01  CPWSWORK   EXTERNAL.                                         UCOLDITR
011320     COPY CPWSWORK.                                               UCOLDITR
011330                                                                  UCOLDITR
011340 01  CPWSFOOT   EXTERNAL.                                         UCOLDITR
011350     COPY CPWSFOOT.                                               UCOLDITR
011360                                                                  UCOLDITR
011400 01  WS-TRANSACTION-IDS.                                          UCOLDITR
011401     COPY CPWSFUNC.                                               UCOLDITR
011410                                                                  UCOLDITR
011430 01  UCWSABND    EXTERNAL.                                        UCOLDITR
011440     COPY UCWSABND.                                               UCOLDITR
011450                                                                  UCOLDITR
011460 01  CA-COMMAREA  EXTERNAL.                                       UCOLDITR
011470     COPY CPWSCOMA.                                               UCOLDITR
011471 01  PRNT-COMMAREA REDEFINES CA-COMMAREA.                         UCOLDITR
011472     COPY UCWSCOMP.                                               UCOLDITR
011480                                                                  UCOLDITR
011900******************************************************************UCOLDITR
012000*          SQL - WORKING STORAGE                                 *UCOLDITR
012100******************************************************************UCOLDITR
012200                                                                  UCOLDITR
012210 01  PCK-ROW   EXTERNAL.                                          UCOLDITR
012251     EXEC SQL                                                     UCOLDITR
012252          INCLUDE UC0VZPCK                                        UCOLDITR
012253     END-EXEC.                                                    UCOLDITR
012254                                                                  UCOLDITR
012260     EXEC SQL                                                     UCOLDITR
012270          INCLUDE SQLCA                                           UCOLDITR
012280     END-EXEC.                                                    UCOLDITR
012300                                                                  UCOLDITR
012400******************************************************************UCOLDITR
012500*                                                                *UCOLDITR
012600*                  SQL - SELECTS                                 *UCOLDITR
012700*                                                                *UCOLDITR
012800******************************************************************UCOLDITR
012900 LINKAGE SECTION.                                                 UCOLDITR
013000                                                                  UCOLDITR
013100     EJECT                                                        UCOLDITR
013200                                                                  UCOLDITR
013400 PROCEDURE DIVISION.                                              UCOLDITR
013500                                                                  UCOLDITR
013510 0000-MAIN-LINE   SECTION.                                        UCOLDITR
013520**************************************************************    UCOLDITR
013530*  MAIN LINE PROCESSING ROUTINE                              *    UCOLDITR
013560**************************************************************    UCOLDITR
013600                                                                  UCOLDITR
013700     PERFORM 0100-INITIALIZATIONS.                                UCOLDITR
015200                                                                  UCOLDITR
015210*****PERFORM 1000-GET-PROGRAM-ID.                                 36430815
015220                                                                  UCOLDITR
015245     IF EIBCALEN = +3000                                          UCOLDITR
015246        MOVE UCOLDINT-SCREEN-AREA-LENGTH                          UCOLDITR
015247                                     TO CA-PRNT-SCRN-AREA-LENGTH  UCOLDITR
015248     ELSE                                                         UCOLDITR
015249        PERFORM 2000-PROCESS-LOGICAL-FUNCTIONS                    UCOLDITR
015251     END-IF.                                                      UCOLDITR
017238                                                                  UCOLDITR
017294     GOBACK.                                                      UCOLDITR
017295                                                                  UCOLDITR
017300 0000-EXIT.                                                       UCOLDITR
017400     EXIT.                                                        UCOLDITR
017500                                                                  UCOLDITR
017600     EJECT                                                        UCOLDITR
034580                                                                  UCOLDITR
034581 0100-INITIALIZATIONS   SECTION.                                  UCOLDITR
034582**************************************************************    UCOLDITR
034583*  MISCELLANEOUS INITIALIZATIONS                             *    UCOLDITR
034586**************************************************************    UCOLDITR
034587                                                                  UCOLDITR
034588     MOVE ZERO TO UCCOMMON-RETURN-CODE.                           UCOLDITR
034589     MOVE 'UCOLDITR' TO DB2MSG-PGM-ID.                            UCOLDITR
034590     MOVE UCOLDINT-SCREEN-AREA-LENGTH TO CA-SCREEN-AREA-LENGTH.   UCOLDITR
034592                                                                  UCOLDITR
034598 0100-EXIT.                                                       UCOLDITR
034599     EXIT.                                                        UCOLDITR
034600                                                                  UCOLDITR
034601     EJECT                                                        UCOLDITR
034602                                                                  UCOLDITR
034604*1000-GET-PROGRAM-ID   SECTION.                                   36430815
034605**************************************************************    36430815
034606***SEARCH THE FUNCTION TABLE FOR THE TRANSACTION ID          *    36430815
034607***IN EIBTRNID. IF FOUND, SET THE UCOLDINT AND UCCOMMON      *    36430815
034608***FIELDS.                                                   *    36430815
034609**************************************************************    36430815
034610*****                                                             36430815
034611*****SEARCH WS-FUNC-TBL                                           36430815
034612*****  AT END                                                     36430815
034613*****    MOVE 'OLD SYSTEM TRANS ID NOT FOUND'                     36430815
034614*****                                 TO UCWSABND-ABEND-DATA (1)  36430815
034615*****    PERFORM 9999-ABEND                                       36430815
034616*****  WHEN WS-FUNC-TRANS (WS-FUNC-INDEX) = EIBTRNID              36430815
034617*****    MOVE WS-FUNC-PROGRAM (WS-FUNC-INDEX)                     36430815
034618*****                                   TO UCOLDINT-DTL-SCREEN-PGM36430815
034619*****    MOVE WS-FUNC-ID (WS-FUNC-INDEX) TO UCCOMMON-FUNCTION     36430815
034620*****END-SEARCH.                                                  36430815
034621*****                                                             36430815
034622*1000-EXIT.                                                       36430815
034623*****EXIT.                                                        36430815
034624*****                                                             36430815
034625*****EJECT                                                        36430815
034626*****                                                             36430815
034626 2000-PROCESS-LOGICAL-FUNCTIONS SECTION.                          UCOLDITR
034627**************************************************************    UCOLDITR
034628*  PROCESS FUNCTIONS ASSOCIATED WITH PFK 3 (PREV MENU),      *    UCOLDITR
034629*  9 (MAIN MANEU) OR @ (12 - RETURN)                         *    UCOLDITR
034630**************************************************************    UCOLDITR
034636                                                                  UCOLDITR
034637     IF UCCOMMON-FUNCTION = 'WHO'                                 UCOLDITR
034639         SET UCOLDINT-SWITCH-FUNC-NOT-OK TO TRUE                  UCOLDITR
034640     ELSE                                                         UCOLDITR
034641         SET UCOLDINT-SWITCH-FUNC-OK TO TRUE                      UCOLDITR
034642     END-IF.                                                      UCOLDITR
034643                                                                  UCOLDITR
034645     IF (CPWSFOOT-ENTERED-ID = SPACES OR LOW-VALUES)              UCOLDITR
034646       AND (CPWSFOOT-ENTERED-NAME = SPACES OR LOW-VALUES)         UCOLDITR
034647       AND (CPWSFOOT-ENTERED-SSN = SPACES OR LOW-VALUES)          UCOLDITR
034648       AND (NOT UCCOMMON-NESTED-FUNCTION)                         UCOLDITR
034652         IF CA-ENTERED-ID NOT = SPACES                            UCOLDITR
034653            MOVE CA-ENTERED-ID TO CPWSFOOT-ENTERED-ID             UCOLDITR
034654         ELSE                                                     UCOLDITR
034655            IF CA-EE-ID-9                                         UCOLDITR
034656               MOVE CA-UNIQUE-EE-ID TO CPWSFOOT-ENTERED-ID        UCOLDITR
034657            ELSE                                                  UCOLDITR
034658               MOVE CA-UNIQUE-EE-ID (3:CA-EE-ID-LENGTH)           UCOLDITR
034659                                        TO CPWSFOOT-ENTERED-ID    UCOLDITR
034660            END-IF                                                UCOLDITR
034661         END-IF                                                   UCOLDITR
034662         IF (CPWSFOOT-ENTERED-ID NOT = SPACES)                    UCOLDITR
034663          AND (CPWSFOOT-ENTERED-ID NOT = LOW-VALUES)              UCOLDITR
034664            SET UCOLDINT-RECORD-KEY-ENTRY TO TRUE                 UCOLDITR
034665         END-IF                                                   UCOLDITR
034669     END-IF.                                                      UCOLDITR
034670                                                                  UCOLDITR
034671     EVALUATE TRUE                                                UCOLDITR
034672       WHEN UCOLDINT-FUNCTION-KEY-IS-3                            UCOLDITR
034673         IF UCCOMMON-NESTED-FUNCTION                              UCOLDITR
034674            MOVE MU0015 TO UCCOMMON-MSG-NUMBER                    UCOLDITR
034675            SET UCCOMMON-RETURN-ERRORS-FOUND TO TRUE              UCOLDITR
034676         ELSE                                                     UCOLDITR
034677            IF CA-LAST-MENU = UCCOMMON-FUNCTION                   UCOLDITR
034678              OR CA-LAST-MENU = 'MENU'                            UCOLDITR
034679                SET UCCOMMON-LOGIC-FUNC-PREVMENU TO TRUE          UCOLDITR
034680            END-IF                                                UCOLDITR
034681         END-IF                                                   UCOLDITR
034682       WHEN UCOLDINT-FUNCTION-KEY-IS-9                            UCOLDITR
034683         IF UCCOMMON-NESTED-FUNCTION                              UCOLDITR
034684            MOVE MU0015 TO UCCOMMON-MSG-NUMBER                    UCOLDITR
034685            SET UCCOMMON-RETURN-ERRORS-FOUND TO TRUE              UCOLDITR
034686         ELSE                                                     UCOLDITR
020500            SET UCCOMMON-LOGIC-FUNC-MAINMENU TO TRUE              UCOLDITR
034688         END-IF                                                   UCOLDITR
034689       WHEN UCOLDINT-FUNCTION-KEY-IS-12                           UCOLDITR
034690         IF CA-UPDATE-PENDING                                     UCOLDITR
034691            SET UCCOMMON-RETURN-ERRORS-FOUND TO TRUE              UCOLDITR
034692            MOVE MP0503 TO UCCOMMON-MSG-NUMBER                    UCOLDITR
034693         ELSE                                                     UCOLDITR
034694            IF UCCOMMON-NESTED-FUNCTION                           UCOLDITR
034695               SET UCCOMMON-LOGIC-FUNC-RET-NEST TO TRUE           UCOLDITR
034696            ELSE                                                  UCOLDITR
034697               SET UCCOMMON-LOGIC-FUNC-EXIT TO TRUE               UCOLDITR
034698            END-IF                                                UCOLDITR
034699         END-IF                                                   UCOLDITR
034700     END-EVALUATE.                                                UCOLDITR
034701                                                                  UCOLDITR
034702     MOVE 'EI'  TO PCK-SUBSYSTEM-ID.                              UCOLDITR
034703     MOVE 'EDB' TO PCK-DATABASE.                                  UCOLDITR
034704     MOVE CA-EDB-VERSION TO PCK-VERSION.                          UCOLDITR
034705                                                                  UCOLDITR
034707     CALL WS-UCPCKUTL USING DFHEIBLK                              UCOLDITR
034708       ON EXCEPTION                                               UCOLDITR
034709          STRING 'ERROR CALLING PROGRAM: ' DELIMITED BY SIZE      UCOLDITR
034710                 WS-UCPCKUTL               DELIMITED BY SIZE      UCOLDITR
034711                      INTO UCWSABND-ABEND-DATA (1)                UCOLDITR
034712          END-STRING                                              UCOLDITR
034713          PERFORM 9999-ABEND                                      UCOLDITR
034714     END-CALL.                                                    UCOLDITR
034715                                                                  UCOLDITR
034716     MOVE PCK-COLLECTION TO UCOLDINT-COLLECTION.                  UCOLDITR
034717                                                                  UCOLDITR
034718 2000-EXIT.                                                       UCOLDITR
034719     EXIT.                                                        UCOLDITR
034720                                                                  UCOLDITR
034748     EJECT                                                        UCOLDITR
034749                                                                  UCOLDITR
034750 9999-ABEND    SECTION.                                           UCOLDITR
034751**************************************************************    UCOLDITR
034752*  COPIED PROCEDURE DIVISION CODE FOR HANDLING ABENDS        *    UCOLDITR
034753**************************************************************    UCOLDITR
034754                                                                  UCOLDITR
034755     MOVE 'UCOLDITR' TO UCWSABND-ABENDING-PGM.                    UCOLDITR
034756                                                                  UCOLDITR
034757     COPY UCPDABND.                                               UCOLDITR
034758                                                                  UCOLDITR
034760 9999-EXIT.                                                       UCOLDITR
034800     EXIT.                                                        UCOLDITR
034801                                                                  UCOLDITR
034802     EJECT                                                        UCOLDITR
034803                                                                  UCOLDITR
034900***************    END OF SOURCE - UCOLDITR    *******************UCOLDITR
