000010**************************************************************/   36430795
000020*  PROGRAM: PPBUTRPC                                         */   36430795
000030*  RELEASE # ___0795___   SERVICE REQUEST NO(S) ___3643____  */   36430795
000040*  NAME ____ET_________   MODIFICATION DATE ____07/15/93_____*/   36430795
000050*  DESCRIPTION                                               */   36430795
000060*              EDB ENTRY UPDATE SYSTEM                       */   36430795
000070*  CICS ONLINE I/O MODULE FOR THE DELETED DUES BENEFIT       */   36430795
000071*  REPORT  PGM PPBUTRPT.                                     */   36430795
000080*  THIS MODULE WRITES MESSAGES TO THE BUTRPT  QUEUE          */   36430795
000090*  AND WRITES THE NAME OF THE BUTRPT QUEUE ITSELF TO         */   36430795
000091*  THE AUDIT QUEUE FOR LATER CLEANUP                         */   36430795
000092**************************************************************/   36430795
000093 IDENTIFICATION DIVISION.                                         PPBUTRPC
000094 PROGRAM-ID. PPBUTRPC.                                            PPBUTRPC
000095 ENVIRONMENT DIVISION.                                            PPBUTRPC
000096 CONFIGURATION SECTION.                                           PPBUTRPC
000097 SOURCE-COMPUTER.                                                 PPBUTRPC
000098 COPY CPOTXUCS.                                                   PPBUTRPC
000099 OBJECT-COMPUTER.                                                 PPBUTRPC
000100 COPY CPOTXOBJ.                                                   PPBUTRPC
000101 DATA DIVISION.                                                   PPBUTRPC
000102     EJECT                                                        PPBUTRPC
000103******************************************************************PPBUTRPC
000104*********                                                  *******PPBUTRPC
000105*********                  W O R K I N G                   *******PPBUTRPC
000106*********                  S T O R A G E                   *******PPBUTRPC
000107*********                                                  *******PPBUTRPC
000108******************************************************************PPBUTRPC
000109                                                                  PPBUTRPC
000110 WORKING-STORAGE SECTION.                                         PPBUTRPC
000111     SKIP3                                                        PPBUTRPC
000112 01  FILLER    PIC X(27) VALUE 'WORKING STORAGE STARTS HERE'.     PPBUTRPC
000113                                                                  PPBUTRPC
000114 01  WS-STATUS-FLAGS.                                             PPBUTRPC
000115     05  WS-RESP          PIC  S9(08) COMP.                       PPBUTRPC
000116     05  WS-FIRST-TIME-SW PIC X     VALUE 'F'.                    PPBUTRPC
000117         88  WS-FIRST-TIME          VALUE 'F'.                    PPBUTRPC
000118         88  WS-NOT-FIRST-TIME      VALUE 'X'.                    PPBUTRPC
000119                                                                  PPBUTRPC
000120 01  TS-QUEUE-WS.                                                 PPBUTRPC
000121     05  TS-QUEUE-ID.                                             PPBUTRPC
000122         10  TS-QUEUE-NAME-SS              PIC X(02).             PPBUTRPC
000123         10  TS-QUEUE-NAME-NN              PIC X(02).             PPBUTRPC
000124         10  TS-QUEUE-TERMID               PIC X(04).             PPBUTRPC
000125     05  TS-QUEUE-LENGTH                   PIC S9(04) COMP.       PPBUTRPC
000126     05  TS-QUEUE-ITEM                     PIC S9(04) COMP.       PPBUTRPC
000127     05  TS-AUDIT-QUEUE-ID.                                       PPBUTRPC
000128         10  TS-AUDIT-Q-SS-NAME            PIC X(02).             PPBUTRPC
000129         10  FILLER                        PIC X(02) VALUE 'AU'.  PPBUTRPC
000130         10  TS-AUDIT-Q-TERM-ID            PIC X(04).             PPBUTRPC
000131     05  TS-AUDIT-QUEUE-LENGTH             PIC S9(04) COMP        PPBUTRPC
000132                                                      VALUE 8.    PPBUTRPC
000133     05  TS-AUDIT-QUEUE-DATA.                                     PPBUTRPC
000134         10  TS-AUDIT-Q-QUEUE-NAME         PIC X(08).             PPBUTRPC
000135     SKIP3                                                        PPBUTRPC
000136 01  CPWSBUTX EXTERNAL.                                           PPBUTRPC
000137     COPY CPWSBUTX.                                               PPBUTRPC
000138     SKIP3                                                        PPBUTRPC
000139 01  UCCOMMON   EXTERNAL.                                         PPBUTRPC
000140     COPY UCCOMMON.                                               PPBUTRPC
000141     SKIP3                                                        PPBUTRPC
000142 01  UCWSABND     EXTERNAL.                                       PPBUTRPC
000143     COPY UCWSABND.                                               PPBUTRPC
000144     EJECT                                                        PPBUTRPC
000145 LINKAGE SECTION.                                                 PPBUTRPC
000146     EJECT                                                        PPBUTRPC
000147 PROCEDURE DIVISION.                                              PPBUTRPC
000148******************************************************************PPBUTRPC
000149*********                                                  *******PPBUTRPC
000150*********                E X E C U T I V E                 *******PPBUTRPC
000151*********                  R O U T I N E                   *******PPBUTRPC
000152*********                                                  *******PPBUTRPC
000153******************************************************************PPBUTRPC
000154                                                                  PPBUTRPC
000155 0-MAINLINE    SECTION.                                           PPBUTRPC
000156                                                                  PPBUTRPC
000157     IF  NOT CPWSBUTX-WRITE-BUTRPT                                PPBUTRPC
000158         GO TO 0-EXIT.                                            PPBUTRPC
000159                                                                  PPBUTRPC
000160     PERFORM  1-WRITE-TO-THE-BUTRPT-QUEUE.                        PPBUTRPC
000161                                                                  PPBUTRPC
000162     IF WS-FIRST-TIME                                             PPBUTRPC
000163         SET WS-NOT-FIRST-TIME TO TRUE                            PPBUTRPC
000164                                                                  PPBUTRPC
000165         PERFORM  2-POST-TO-THE-AUDIT-QUEUE.                      PPBUTRPC
000166                                                                  PPBUTRPC
000167 0-EXIT.                                                          PPBUTRPC
000168     GOBACK.                                                      PPBUTRPC
000169     EJECT                                                        PPBUTRPC
000170******************************************************************PPBUTRPC
000171*********                                                  *******PPBUTRPC
000172*********             L O G I C     P A T H                *******PPBUTRPC
000173*********                        1                         *******PPBUTRPC
000174*********                                                  *******PPBUTRPC
000175******************************************************************PPBUTRPC
000185                                                                  PPBUTRPC
000186 1-WRITE-TO-THE-BUTRPT-QUEUE    SECTION.                          PPBUTRPC
000187                                                                  PPBUTRPC
000188     MOVE CPWSBUTX-PPBUTRPT-RECORD-LEN                            PPBUTRPC
000189                                  TO TS-QUEUE-LENGTH.             PPBUTRPC
000190     MOVE UCCOMMON-SUBSYSTEM-ID   TO TS-QUEUE-NAME-SS.            PPBUTRPC
000191     MOVE 'BU'                    TO TS-QUEUE-NAME-NN.            PPBUTRPC
000192     MOVE EIBTRMID                TO TS-QUEUE-TERMID.             PPBUTRPC
000193                                                                  PPBUTRPC
000194     EXEC CICS                                                    PPBUTRPC
000195          WRITEQ                                                  PPBUTRPC
000196          QUEUE  (TS-QUEUE-ID)                                    PPBUTRPC
000197          FROM   (CPWSBUTX-PPBUTRPT-RECORD)                       PPBUTRPC
000198          ITEM   (TS-QUEUE-ITEM)                                  PPBUTRPC
000199          LENGTH (TS-QUEUE-LENGTH)                                PPBUTRPC
000200          NOHANDLE                                                PPBUTRPC
000201          RESP   (WS-RESP)                                        PPBUTRPC
000202     END-EXEC.                                                    PPBUTRPC
000203                                                                  PPBUTRPC
000204     IF WS-RESP NOT = DFHRESP(NORMAL)                             PPBUTRPC
000205        PERFORM 9999-ABEND.                                       PPBUTRPC
000206 1-EXIT.                                                          PPBUTRPC
000207     EXIT.                                                        PPBUTRPC
000208     EJECT                                                        PPBUTRPC
000209******************************************************************PPBUTRPC
000210*********                                                  *******PPBUTRPC
000211*********             L O G I C     P A T H                *******PPBUTRPC
000212*********                        2                         *******PPBUTRPC
000213*********                                                  *******PPBUTRPC
000214******************************************************************PPBUTRPC
000215                                                                  PPBUTRPC
000216 2-POST-TO-THE-AUDIT-QUEUE  SECTION.                              PPBUTRPC
000217                                                                  PPBUTRPC
000218*****************************************                         PPBUTRPC
000219*         READ THE AUDIT QUEUE          *                         PPBUTRPC
000220*****************************************                         PPBUTRPC
000221                                                                  PPBUTRPC
000222     MOVE UCCOMMON-SUBSYSTEM-ID TO TS-AUDIT-Q-SS-NAME.            PPBUTRPC
000223     MOVE EIBTRMID              TO TS-AUDIT-Q-TERM-ID.            PPBUTRPC
000224                                                                  PPBUTRPC
000225     PERFORM                                                      PPBUTRPC
000226         VARYING TS-QUEUE-ITEM FROM +1 BY +1                      PPBUTRPC
000227         UNTIL TS-QUEUE-ID = TS-AUDIT-Q-QUEUE-NAME                PPBUTRPC
000228                          OR                                      PPBUTRPC
000229               WS-RESP NOT = DFHRESP(NORMAL)                      PPBUTRPC
000230                                                                  PPBUTRPC
000231             EXEC CICS                                            PPBUTRPC
000232                 READQ TS                                         PPBUTRPC
000233                 QUEUE  (TS-AUDIT-QUEUE-ID)                       PPBUTRPC
000234                 INTO   (TS-AUDIT-QUEUE-DATA)                     PPBUTRPC
000235                 ITEM   (TS-QUEUE-ITEM)                           PPBUTRPC
000236                 LENGTH (TS-AUDIT-QUEUE-LENGTH)                   PPBUTRPC
000237                 NOHANDLE                                         PPBUTRPC
000238                 RESP   (WS-RESP)                                 PPBUTRPC
000239             END-EXEC                                             PPBUTRPC
000240                                                                  PPBUTRPC
000241     END-PERFORM.                                                 PPBUTRPC
000242                                                                  PPBUTRPC
000243*****************************************                         PPBUTRPC
000244*    POST THE BUTRPT QUEUE NAME TO THE  *                         PPBUTRPC
000245*    AUDIT QUEUE IF MATCHING ITEM DOES  *                         PPBUTRPC
000246*      NOT EXIST OR THE AUDIT QUEUE     *                         PPBUTRPC
000247*        ITSELF DOESN'T EXIST YET       *                         PPBUTRPC
000248*****************************************                         PPBUTRPC
000249                                                                  PPBUTRPC
000250     IF  WS-RESP = DFHRESP(NORMAL)  AND                           PPBUTRPC
000251         TS-QUEUE-ID = TS-AUDIT-Q-QUEUE-NAME                      PPBUTRPC
000252                                                                  PPBUTRPC
000253         GO TO 2-EXIT                                             PPBUTRPC
000254                                                                  PPBUTRPC
000255     ELSE                                                         PPBUTRPC
000256     IF WS-RESP = DFHRESP(ITEMERR)                                PPBUTRPC
000257                        OR                                        PPBUTRPC
000258                  DFHRESP(QIDERR)                                 PPBUTRPC
000259                                                                  PPBUTRPC
000260         MOVE TS-QUEUE-ID   TO  TS-AUDIT-Q-QUEUE-NAME             PPBUTRPC
000261                                                                  PPBUTRPC
000262         EXEC CICS                                                PPBUTRPC
000263              WRITEQ TS                                           PPBUTRPC
000264              QUEUE  (TS-AUDIT-QUEUE-ID)                          PPBUTRPC
000265              FROM   (TS-AUDIT-QUEUE-DATA)                        PPBUTRPC
000266              LENGTH (TS-AUDIT-QUEUE-LENGTH)                      PPBUTRPC
000267              NOHANDLE                                            PPBUTRPC
000268              RESP   (WS-RESP)                                    PPBUTRPC
000269         END-EXEC                                                 PPBUTRPC
000270                                                                  PPBUTRPC
000271     ELSE                                                         PPBUTRPC
000272     IF WS-RESP NOT = DFHRESP(NORMAL)                             PPBUTRPC
000273        PERFORM 9999-ABEND                                        PPBUTRPC
000274     END-IF.                                                      PPBUTRPC
000275 2-EXIT.                                                          PPBUTRPC
000276     EXIT.                                                        PPBUTRPC
000277     EJECT.                                                       PPBUTRPC
000278******************************************************************PPBUTRPC
000279*********                                                  *******PPBUTRPC
000280*********                                                  *******PPBUTRPC
000281*********               S U B R O U T I N E S              *******PPBUTRPC
000282*********                                                  *******PPBUTRPC
000283*********                                                  *******PPBUTRPC
000284******************************************************************PPBUTRPC
000285                                                                  PPBUTRPC
000286 9999-ABEND    SECTION.                                           PPBUTRPC
000287                                                                  PPBUTRPC
000288     MOVE 'PPBUTRPC' TO UCWSABND-ABENDING-PGM.                    PPBUTRPC
000289                                                                  PPBUTRPC
000290     COPY UCPDABND.                                               PPBUTRPC
000291                                                                  PPBUTRPC
000292 9999-EXIT.                                                       PPBUTRPC
000300     EXIT.                                                        PPBUTRPC
