000000**************************************************************/   36430889
000001*  PROGRAM: UCAPIDUP                                         */   36430889
000002*  RELEASE: ___0889______ SERVICE REQUEST(S): _____3643____  */   36430889
000003*  NAME:_______STC_______ MODIFICATION DATE:  __04/08/94____ */   36430889
000004*  DESCRIPTION:                                              */   36430889
000005*    ADD CODE TO RESET THE BROWSE SELECT FLAG AFTER          */   36430889
000006*    SUCCESSFUL UPDATE                                       */   36430889
000007**************************************************************/   36430889
000008**************************************************************/   36430875
000009*  PROGRAM: UCAPIDUP                                         */   36430875
000010*  RELEASE: ___0875______ SERVICE REQUEST(S): _____3643____  */   36430875
000011*  NAME:_______AAC_______ MODIFICATION DATE:  __02/24/94____ */   36430875
000012*  DESCRIPTION:                                              */   36430875
000013*    ADD CODE TO RESET THE EDIT-ERROR FLAG AFTER SUCCESSFUL  */   36430875
000014*    UPDATE                                                  */   36430875
000015**************************************************************/   36430875
000100**************************************************************/   36430821
000200*  PROGRAM: UCAPIDUP                                         */   36430821
000300*  RELEASE: ___0821______ SERVICE REQUEST(S): _____3643____  */   36430821
000400*  NAME:_______STC_______ MODIFICATION DATE:  __10/05/93____ */   36430821
000500*  DESCRIPTION:                                              */   36430821
000600*    REQUEST FOR UPDATE                                      */   36430821
000700*    MAKE SECOND CALL TO THE IID SUBROUTINES                 */   36430821
000800**************************************************************/   36430821
000900 IDENTIFICATION DIVISION.                                         UCAPIDUP
001000 PROGRAM-ID. UCAPIDUP.                                            UCAPIDUP
001100 ENVIRONMENT DIVISION.                                            UCAPIDUP
001200 DATA DIVISION.                                                   UCAPIDUP
001300 WORKING-STORAGE SECTION.                                         UCAPIDUP
001400 COPY CPYRIGHT.                                                   UCAPIDUP
001500                                                                  UCAPIDUP
001600*01  WS-MESS-NOS.                                                 36430875
001700*    05  MP0000            PIC X(5) VALUE 'P0000'.                36430875
001800                                                                  UCAPIDUP
001900 01  WS-MISC-FIELDS.                                              UCAPIDUP
002000     05 WS-ID-SPACES-FLAG  PIC X    VALUE 'N'.                    UCAPIDUP
002100        88 ID-SPACES                VALUE 'Y'.                    UCAPIDUP
002200                                                                  UCAPIDUP
002300 01  PGM-UCIIDASN          PIC X(08) VALUE 'UCIIDASN'.            UCAPIDUP
002400 01  PGM-UCIIDMNT          PIC X(08) VALUE 'UCIIDMNT'.            UCAPIDUP
002500                                                                  UCAPIDUP
002600                                                                  UCAPIDUP
002700 01  UCIDWSMT                EXTERNAL.                            UCAPIDUP
002800     COPY UCIDWSMT.                                               UCAPIDUP
002900                                                                  UCAPIDUP
003000 01  UCIDWSBS                EXTERNAL.                            UCAPIDUP
003100     COPY UCIDWSBS.                                               UCAPIDUP
003200                                                                  UCAPIDUP
003300 01  UCIDWSWK                EXTERNAL.                            UCAPIDUP
003400     COPY UCIDWSWK.                                               UCAPIDUP
003500                                                                  UCAPIDUP
003600 01  UCIDWSAS                EXTERNAL.                            UCAPIDUP
003700     COPY UCIDWSAS.                                               UCAPIDUP
003800                                                                  UCAPIDUP
003900 01  UCIDWSIX                EXTERNAL.                            UCAPIDUP
004000     COPY UCIDWSIX.                                               UCAPIDUP
004100                                                                  UCAPIDUP
004200 01  UCIDWSID                EXTERNAL.                            UCAPIDUP
004300     COPY UCIDWSID.                                               UCAPIDUP
004400                                                                  UCAPIDUP
004500 01  UCCOMMON                EXTERNAL.                            UCAPIDUP
004600     EXEC SQL                                                     UCAPIDUP
004700          INCLUDE UCCOMMON                                        UCAPIDUP
004800     END-EXEC.                                                    UCAPIDUP
004900                                                                  UCAPIDUP
005000 01  UCWSABND                EXTERNAL.                            UCAPIDUP
005100     EXEC SQL                                                     UCAPIDUP
005200          INCLUDE UCWSABND                                        UCAPIDUP
005300     END-EXEC.                                                    UCAPIDUP
005400                                                                  UCAPIDUP
005500     EJECT                                                        UCAPIDUP
005600                                                                  UCAPIDUP
005700 PROCEDURE DIVISION.                                              UCAPIDUP
005800                                                                  UCAPIDUP
005900 0000-MAINLINE     SECTION.                                       UCAPIDUP
006000                                                                  UCAPIDUP
006100     IF UCIDWSWK-IDAS-SET                                         UCAPIDUP
006200        PERFORM 1000-UPDATE-WITH-ASN                              UCAPIDUP
006300     ELSE                                                         UCAPIDUP
006400        IF UCIDWSWK-IDUP-SET                                      UCAPIDUP
006500           PERFORM 2000-UPDATE-WITH-MNT                           UCAPIDUP
006600        END-IF                                                    UCAPIDUP
006700     END-IF.                                                      UCAPIDUP
006800                                                                  UCAPIDUP
006900     IF ID-SPACES                                                 UCAPIDUP
007000        SET UCIDWSBS-EDIT-REQ-SET TO TRUE                         UCAPIDUP
007100        GO TO 0000-RETURN                                         UCAPIDUP
007200     ELSE                                                         UCAPIDUP
007300        MOVE SPACES TO UCIDWSBS-SET-EDIT-REQUEST                  UCAPIDUP
007400     END-IF.                                                      UCAPIDUP
007500                                                                  UCAPIDUP
007600     INITIALIZE UCIDWSWK-IDAS-OR-IDUP.                            UCAPIDUP
007700                                                                  UCAPIDUP
007800     SET UCCOMMON-RETURN-NO-ERRORS TO TRUE.                       UCAPIDUP
007900                                                                  UCAPIDUP
008000     INITIALIZE UCIDWSID.                                         UCAPIDUP
008100     INITIALIZE UCIDWSIX.                                         UCAPIDUP
008200     INITIALIZE UCIDWSMT-EXT-DATA.                                UCAPIDUP
008300     MOVE SPACES                 TO UCIDWSBS-BROWSE-SELECT-SW     UCAPIDUP
008400                                    UCIDWSBS-IID-KEY-ID           UCAPIDUP
008500                                    UCIDWSBS-IID-KEY-SSN          UCAPIDUP
008600                                    UCIDWSBS-IID-KEY-DOB          UCAPIDUP
008700                                    UCIDWSWK-IDAS-CHG-LNAME       UCAPIDUP
008800                                    UCIDWSWK-IDAS-CHG-FNAME       UCAPIDUP
008900                                    UCIDWSWK-IDAS-CHG-MNAME       UCAPIDUP
009000                                    UCIDWSWK-IDAS-CHG-SUFFIX      UCAPIDUP
009100                                    UCIDWSWK-IDAS-CHG-BDATE       UCAPIDUP
009200*****                               UCIDWSWK-IDAS-RESOL-REQ.      36430889
009201                                    UCIDWSWK-IDAS-RESOL-REQ       36430889
009202                                    UCIDWSWK-IID-PGM-ID.          36430889
009300     SET UCCOMMON-NO-EDIT-ERRORS TO TRUE.                         36430875
009310     SET UCCOMMON-MSG-NUMBER-NOT-SET TO TRUE.                     36430875
009300                                                                  UCAPIDUP
009500 0000-RETURN.                                                     UCAPIDUP
009600                                                                  UCAPIDUP
009700     GOBACK.                                                      UCAPIDUP
009800                                                                  UCAPIDUP
009900 1000-UPDATE-WITH-ASN SECTION.                                    UCAPIDUP
010000                                                                  UCAPIDUP
010100**************************************************************    UCAPIDUP
010200*    EITHER DO PENDING COMMIT DO CHANGE STATUS OF            *    UCAPIDUP
010300*    NEWLY ASSIGNED ID OR                                    *    UCAPIDUP
010400*    UPDATE THE RESOLVED ID FROM THE ASSIGN SCREEN           *    UCAPIDUP
010500**************************************************************    UCAPIDUP
010600                                                                  UCAPIDUP
010700     MOVE UCIDWSID-IDB-INDIVIDUAL-ID                              UCAPIDUP
010800                                 TO UCIDWSAS-PASSED-IID.          UCAPIDUP
010900     MOVE UCIDWSID-IDB-SSN                                        UCAPIDUP
011000                                 TO UCIDWSAS-PASSED-SSN.          UCAPIDUP
011100     MOVE UCIDWSID-IDB-BIRTHDATE                                  UCAPIDUP
011200                                 TO UCIDWSAS-PASSED-BIRTHDATE.    UCAPIDUP
011300     MOVE UCIDWSID-IDB-LAST-NAME-TEXT                             UCAPIDUP
011400                                 TO UCIDWSAS-PASSED-NAME-LAST.    UCAPIDUP
011500     MOVE UCIDWSID-IDB-FIRST-NAME-TEXT                            UCAPIDUP
011600                                 TO UCIDWSAS-PASSED-NAME-FIRST.   UCAPIDUP
011700     MOVE UCIDWSID-IDB-MIDDLE-NAME-TEXT                           UCAPIDUP
011800                                 TO UCIDWSAS-PASSED-NAME-MIDDLE.  UCAPIDUP
011900     MOVE UCIDWSID-IDB-SUFFIX                                     UCAPIDUP
012000                                 TO UCIDWSAS-PASSED-NAME-SUFFIX.  UCAPIDUP
012100     MOVE UCIDWSID-IDB-CHANGED-BY                                 UCAPIDUP
012200                                 TO UCIDWSAS-PASSED-CHANGED-BY.   UCAPIDUP
012300     MOVE UCIDWSID-IDB-STATUS                                     UCAPIDUP
012400                                 TO UCIDWSAS-PASSED-STAT.         UCAPIDUP
012500     MOVE UCIDWSID-IDB-REFERRED-IID                               UCAPIDUP
012600                                 TO UCIDWSAS-PASSED-REF-IID.      UCAPIDUP
012700     MOVE UCIDWSID-IDB-CHANGED-AT                                 UCAPIDUP
012800                                 TO UCIDWSAS-PASSED-CHANGED-AT.   UCAPIDUP
012900                                                                  UCAPIDUP
013000     IF UCIDWSID-IDB-INDIVIDUAL-ID = SPACES OR LOW-VALUES         UCAPIDUP
013100        SET ID-SPACES            TO TRUE                          UCAPIDUP
013200        GO TO 1000-EXIT                                           UCAPIDUP
013300     END-IF.                                                      UCAPIDUP
013400                                                                  UCAPIDUP
013500     IF UCIDWSAS-PASSED-STAT = 'P'                                UCAPIDUP
013600        SET UCIDWSAS-REQ-PENDING-COMMIT                           UCAPIDUP
013700                                 TO TRUE                          UCAPIDUP
013800        CALL PGM-UCIIDASN                                         UCAPIDUP
013900        IF UCIDWSAS-NO-ERRORS                                     UCAPIDUP
014000           IF UCIDWSAS-STAT-PEND-COMMITTED OR                     UCAPIDUP
014100              UCIDWSAS-STAT-NOT-FOUND                             UCAPIDUP
014200                CONTINUE                                          UCAPIDUP
014300           END-IF                                                 UCAPIDUP
014400        ELSE                                                      UCAPIDUP
014500           STRING                                                 UCAPIDUP
014600              'SQL ERROR FROM '            DELIMITED BY SIZE      UCAPIDUP
014700               UCIDWSAS-ERROR-PROG         DELIMITED BY SIZE      UCAPIDUP
014800               ', IN PARA '                DELIMITED BY SIZE      UCAPIDUP
014900               UCIDWSAS-ERROR-PARA         DELIMITED BY SIZE      UCAPIDUP
015000               ' = '                       DELIMITED BY SIZE      UCAPIDUP
015100               UCIDWSAS-ERROR-CODE         DELIMITED BY SIZE      UCAPIDUP
015200               INTO UCWSABND-ABEND-DATA(1)                        UCAPIDUP
015300               PERFORM 9999-ABEND                                 UCAPIDUP
015400        END-IF                                                    UCAPIDUP
015500     ELSE                                                         UCAPIDUP
015600        SET UCIDWSAS-REQ-CONFIRMED-UPDT                           UCAPIDUP
015700                                 TO TRUE                          UCAPIDUP
015800        CALL PGM-UCIIDASN                                         UCAPIDUP
015900        IF UCIDWSAS-NO-ERRORS                                     UCAPIDUP
016000           IF UCIDWSAS-STAT-UPDATED        OR                     UCAPIDUP
016100              UCIDWSAS-STAT-NOT-FOUND                             UCAPIDUP
016200                CONTINUE                                          UCAPIDUP
016300           END-IF                                                 UCAPIDUP
016400        ELSE                                                      UCAPIDUP
016500           STRING                                                 UCAPIDUP
016600              'SQL ERROR FROM '            DELIMITED BY SIZE      UCAPIDUP
016700               UCIDWSAS-ERROR-PROG         DELIMITED BY SIZE      UCAPIDUP
016800               ', IN PARA '                DELIMITED BY SIZE      UCAPIDUP
016900               UCIDWSAS-ERROR-PARA         DELIMITED BY SIZE      UCAPIDUP
017000               ' = '                       DELIMITED BY SIZE      UCAPIDUP
017100               UCIDWSAS-ERROR-CODE         DELIMITED BY SIZE      UCAPIDUP
017200               INTO UCWSABND-ABEND-DATA(1)                        UCAPIDUP
017300               PERFORM 9999-ABEND                                 UCAPIDUP
017400        END-IF                                                    UCAPIDUP
017500     END-IF.                                                      UCAPIDUP
017600                                                                  UCAPIDUP
017700 1000-EXIT.                                                       UCAPIDUP
017800     EXIT.                                                        UCAPIDUP
017900 EJECT                                                            UCAPIDUP
018000                                                                  UCAPIDUP
018100 2000-UPDATE-WITH-MNT SECTION.                                    UCAPIDUP
018200                                                                  UCAPIDUP
018300**************************************************************    UCAPIDUP
018400*    IF WE ARE COMING FROM THE UPDATE SCREEN, THEN           *    UCAPIDUP
018500*    DO SECOND CALLS FOR COMMIT USING UCIIDMNT INSTEAD       *    UCAPIDUP
018600*    OF UCIIDASN                                             *    UCAPIDUP
018700**************************************************************    UCAPIDUP
018800                                                                  UCAPIDUP
018900     INITIALIZE UCIDWSMT-RETURN-STATUS.                           UCAPIDUP
019000     INITIALIZE UCIDWSMT-ERROR-CODE.                              UCAPIDUP
019100     INITIALIZE UCIDWSMT-ERROR-PARA.                              UCAPIDUP
019200     INITIALIZE UCIDWSMT-ERROR-PROG.                              UCAPIDUP
019300     INITIALIZE UCIDWSMT-RET-STAT-UCIDWS10.                       UCAPIDUP
019400                                                                  UCAPIDUP
019500     IF NOT UCIDWSMT-STAT-CHG-AUTH                                UCAPIDUP
019600        GO TO 2000-EXIT                                           UCAPIDUP
019700     END-IF.                                                      UCAPIDUP
019800                                                                  UCAPIDUP
019900     SET UCIDWSMT-REQ-COMMIT     TO TRUE.                         UCAPIDUP
020000                                                                  UCAPIDUP
020100     CALL PGM-UCIIDMNT.                                           UCAPIDUP
020200                                                                  UCAPIDUP
020300     IF NOT UCIDWSMT-FATAL-ERROR                                  UCAPIDUP
020400        IF UCIDWSMT-STAT-COMMITED                                 UCAPIDUP
020500           CONTINUE                                               UCAPIDUP
020600        ELSE                                                      UCAPIDUP
020700           IF UCIDWSMT-STAT-UNCOMMIT-NOID                         UCAPIDUP
020800****          MOVE MP0000        TO UCCOMMON-MSG-NUMBER           36430875
020810              MOVE 'ERROR RETURN FROM MNT/COMMIT' TO              36430875
020820                                      UCWSABND-ABEND-DATA(1)      36430875
020830              MOVE UCIDWSMT-RETURN-STAT           TO              36430875
020840                                      UCWSABND-ABEND-DATA(2)      36430875
020850              PERFORM 9999-ABEND                                  36430875
020900           END-IF                                                 UCAPIDUP
021000        END-IF                                                    UCAPIDUP
021100     ELSE                                                         UCAPIDUP
021200        STRING                                                    UCAPIDUP
021300           'SQL ERROR FROM '            DELIMITED BY SIZE         UCAPIDUP
021400            UCIDWSMT-ERROR-PROG         DELIMITED BY SIZE         UCAPIDUP
021500            ', IN PARA '                DELIMITED BY SIZE         UCAPIDUP
021600            UCIDWSMT-ERROR-PARA         DELIMITED BY SIZE         UCAPIDUP
021700            ' = '                       DELIMITED BY SIZE         UCAPIDUP
021800            UCIDWSMT-ERROR-CODE         DELIMITED BY SIZE         UCAPIDUP
021900            INTO UCWSABND-ABEND-DATA(1)                           UCAPIDUP
022000            PERFORM 9999-ABEND                                    UCAPIDUP
022100     END-IF.                                                      UCAPIDUP
022200                                                                  UCAPIDUP
022300 2000-EXIT.                                                       UCAPIDUP
022400     EXIT.                                                        UCAPIDUP
022500                                                                  UCAPIDUP
022600 9999-ABEND    SECTION.                                           UCAPIDUP
022700**************************************************************    UCAPIDUP
022800*  COPIED PROCEDURE DIVISION CODE FOR ABEND HANDLING         *    UCAPIDUP
022900**************************************************************    UCAPIDUP
023000                                                                  UCAPIDUP
023100     MOVE 'UCAPIDUP' TO UCWSABND-ABENDING-PGM.                    UCAPIDUP
023200                                                                  UCAPIDUP
023300     COPY UCPDABND.                                               UCAPIDUP
023400                                                                  UCAPIDUP
023500 9999-EXIT.                                                       UCAPIDUP
023600     EXIT.                                                        UCAPIDUP
023700                                                                  UCAPIDUP
023800     EJECT                                                        UCAPIDUP
023900                                                                  UCAPIDUP
024000***************    END OF SOURCE - UCAPIDUP    ****************   UCAPIDUP
