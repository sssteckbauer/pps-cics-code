000100**************************************************************/   EFIX1064
000200*  PROGRAM: PPAPTRNF                                         */   EFIX1064
000300*  RELEASE: ___1064______ SERVICE REQUEST(S): _____EFIX____  */   EFIX1064
000400*  NAME:_______JLT_______ MODIFICATION DATE:  __04/19/96____ */   EFIX1064
000500*  DESCRIPTION:                                              */   EFIX1064
000600*    - INITIALIZE 4 POSITION PAGE FIELDS (E.R.1396).         */   EFIX1064
000700**************************************************************/   EFIX1064
000100**************************************************************/   36580967
000200*  PROGRAM: PPAPTRNF                                         */   36580967
000300*  RELEASE: ___0967______ SERVICE REQUEST(S): _____3658____  */   36580967
000400*  NAME:_______MLG_______ MODIFICATION DATE:  __03/10/95____ */   36580967
000500*  DESCRIPTION:                                              */   36580967
000600*    OPTR NEW FUNCTION SETUP APPLICATION PROCESSOR           */   36580967
000700*                                                            */   36580967
000800**************************************************************/   36580967
000900 IDENTIFICATION DIVISION.                                         PPAPTRNF
001000 PROGRAM-ID. PPAPTRNF.                                            PPAPTRNF
001100 ENVIRONMENT DIVISION.                                            PPAPTRNF
001200 CONFIGURATION SECTION.                                           PPAPTRNF
001300 SOURCE-COMPUTER.                                                 PPAPTRNF
001400 COPY CPOTXUCS.                                                   PPAPTRNF
001500                                                                  PPAPTRNF
001600 DATA DIVISION.                                                   PPAPTRNF
001700 WORKING-STORAGE SECTION.                                         PPAPTRNF
001800                                                                  PPAPTRNF
001900 01  WS-COMN.                                                     PPAPTRNF
002000   05  WS-START                            PIC X(27) VALUE        PPAPTRNF
002100       'WORKING STORAGE STARTS HERE'.                             PPAPTRNF
002200   05  WS-RESP                             PIC S9(8) COMP.        PPAPTRNF
003000                                                                  PPAPTRNF
003400   05  PGM-PPINITWK                        PIC X(08)              PPAPTRNF
003500                                               VALUE 'PPINITWK'.  PPAPTRNF
003700 01  WS-MESS-NOS.                                                 PPAPTRNF
003800     05  MU0030                            PIC X(5) VALUE 'U0030'.PPAPTRNF
003900     05  MP0634                            PIC X(5) VALUE 'P0634'.PPAPTRNF
004000                                                                  PPAPTRNF
004700 01  UCWSABND    EXTERNAL.                                        PPAPTRNF
004800     COPY UCWSABND.                                               PPAPTRNF
004900                                                                  PPAPTRNF
005000 01  CPWSWORK    EXTERNAL.                                        PPAPTRNF
005100     COPY CPWSWORK.                                               PPAPTRNF
005200                                                                  PPAPTRNF
005210 01  CPWSPTRW    EXTERNAL.                                        PPAPTRNF
005220     COPY CPWSPTRW.                                               PPAPTRNF
005230                                                                  PPAPTRNF
005300 01  CPWSFOOT    EXTERNAL.                                        PPAPTRNF
005400     COPY CPWSFOOT.                                               PPAPTRNF
005500                                                                  PPAPTRNF
005600 01  CPWSKEYS    EXTERNAL.                                        PPAPTRNF
005700     COPY CPWSKEYS.                                               PPAPTRNF
005800                                                                  PPAPTRNF
005900 01  UCPISCRN    EXTERNAL.                                        PPAPTRNF
006000     COPY UCPISCRN.                                               PPAPTRNF
006100                                                                  PPAPTRNF
006200 01  UCCOMMON   EXTERNAL.                                         PPAPTRNF
006300     EXEC SQL                                                     PPAPTRNF
006400      INCLUDE UCCOMMON                                            PPAPTRNF
006410     END-EXEC.                                                    PPAPTRNF
006420                                                                  PPAPTRNF
012100 LINKAGE SECTION.                                                 PPAPTRNF
012300                                                                  PPAPTRNF
012400 EJECT                                                            PPAPTRNF
012500 PROCEDURE DIVISION.                                              PPAPTRNF
012600                                                                  PPAPTRNF
012700 0000-MAIN-LINE   SECTION.                                        PPAPTRNF
012800**************************************************************    PPAPTRNF
012900*  MAIN LINE PROCESSING                                      *    PPAPTRNF
013000**************************************************************    PPAPTRNF
013500                                                                  PPAPTRNF
013700     MOVE ZERO TO UCCOMMON-RETURN-CODE.                           PPAPTRNF
013900                                                                  PPAPTRNF
014000     IF CPWSWORK-EMPLOYEE-NOT-FOUND                               PPAPTRNF
014100        SET CPWSWORK-EMPLOYEE-FOUND TO TRUE                       PPAPTRNF
014200        MOVE SPACES TO CPWSWORK-UNIQUE-EE-ID                      PPAPTRNF
014300                       CPWSKEYS-EMPLOYEE-ID                       PPAPTRNF
014400     END-IF.                                                      PPAPTRNF
014500                                                                  PPAPTRNF
014600     IF UCCOMMON-UPD-IN-PROGRESS                                  PPAPTRNF
014800       IF UCCOMMON-FUNC-TYPE-INQUIRY                              PPAPTRNF
014900       OR UCCOMMON-FUNC-TYPE-SELE                                 PPAPTRNF
015000        MOVE MU0030 TO UCCOMMON-HOT-MSG-NUMBER                    PPAPTRNF
015100       END-IF                                                     PPAPTRNF
015200     END-IF.                                                      PPAPTRNF
015300                                                                  PPAPTRNF
015400     IF CPWSFOOT-ENTERED-FUNC = SPACES OR LOW-VALUES              PPAPTRNF
015500        IF (UCPISCRN-MENU-FUNCTION-SELECT NOT = SPACES)           PPAPTRNF
015600         AND (UCPISCRN-MENU-FUNCTION-SELECT NOT = LOW-VALUES)     PPAPTRNF
015700           MOVE UCPISCRN-MENU-FUNCTION-SELECT                     PPAPTRNF
015800                                     TO CPWSFOOT-ENTERED-FUNC     PPAPTRNF
015900        END-IF                                                    PPAPTRNF
016000     END-IF.                                                      PPAPTRNF
016100                                                                  PPAPTRNF
016500     IF UCCOMMON-RECORD-AUTH-VIEW                                 PPAPTRNF
016600      AND UCCOMMON-FUNC-TYPE-UPDATE                               PPAPTRNF
017300        MOVE MP0634 TO UCCOMMON-HOT-MSG-NUMBER                    PPAPTRNF
017400     ELSE                                                         PPAPTRNF
017500       IF UCCOMMON-HOT-MSG-NUMBER = MP0634                        PPAPTRNF
017600         SET UCCOMMON-HOT-MSG-NUM-NOT-SET TO TRUE                 PPAPTRNF
017700         SET UCCOMMON-HOT-MSG-NOT-SET TO TRUE                     PPAPTRNF
017800       END-IF                                                     PPAPTRNF
017900     END-IF.                                                      PPAPTRNF
018000                                                                  PPAPTRNF
019000                                                                  PPAPTRNF
021600     IF UCCOMMON-SUBSYSTEM-INIT                                   PPAPTRNF
021700        PERFORM 1000-INITIALIZE-SUBSYSTEM                         PPAPTRNF
022300     END-IF.                                                      PPAPTRNF
022400                                                                  PPAPTRNF
022410     MOVE ZEROES TO CPWSWORK-CURRENT-PAGE                         PPAPTRNF
022420                    CPWSWORK-TOTAL-PAGES.                         PPAPTRNF
011000     MOVE ZEROES TO CPWSWORK-CURRENT-PAGE-4                       EFIX1064
011100                    CPWSWORK-TOTAL-PAGES-4.                       EFIX1064
022430                                                                  PPAPTRNF
022440     MOVE SPACES TO CPWSWORK-PROGRAM-WORK-AREA.                   PPAPTRNF
022450                                                                  PPAPTRNF
022500     GOBACK.                                                      PPAPTRNF
022600                                                                  PPAPTRNF
022700 0000-EXIT.                                                       PPAPTRNF
022800     EXIT.                                                        PPAPTRNF
022900                                                                  PPAPTRNF
023000                                                                  PPAPTRNF
023100 EJECT                                                            PPAPTRNF
023200                                                                  PPAPTRNF
023300 1000-INITIALIZE-SUBSYSTEM   SECTION.                             PPAPTRNF
023310******************************************************************PPAPTRNF
023320*  INITIALIZE WORK EXTERNALS WHEN ENTERING SUBSYSTEMS.           *PPAPTRNF
023340******************************************************************PPAPTRNF
023400                                                                  PPAPTRNF
023500     IF NOT CPWSWORK-INITIALIZED                                  PPAPTRNF
023600        PERFORM 1100-INIT-CPWSWORK                                PPAPTRNF
023700     END-IF.                                                      PPAPTRNF
023800                                                                  PPAPTRNF
023900     IF NOT CPWSPTRW-INITIALIZED                                  PPAPTRNF
024000        INITIALIZE CPWSPTRW-DATA                                  PPAPTRNF
024100        SET CPWSPTRW-INITIALIZED TO TRUE                          PPAPTRNF
024300     END-IF.                                                      PPAPTRNF
024310                                                                  PPAPTRNF
024400 1000-EXIT.                                                       PPAPTRNF
024500     EXIT.                                                        PPAPTRNF
024600                                                                  PPAPTRNF
024700 EJECT                                                            PPAPTRNF
024800                                                                  PPAPTRNF
024900 1100-INIT-CPWSWORK   SECTION.                                    PPAPTRNF
025000**************************************************************    PPAPTRNF
025100*  CALL PPINITWK TO INITIALIZE CPWSWORK                      *    PPAPTRNF
025200**************************************************************    PPAPTRNF
025300                                                                  PPAPTRNF
025400     CALL PGM-PPINITWK USING DFHEIBLK                             PPAPTRNF
025500       ON EXCEPTION                                               PPAPTRNF
025600          STRING 'ERROR CALLING PROGRAM: ' DELIMITED BY SIZE      PPAPTRNF
025700                 PGM-PPINITWK              DELIMITED BY SIZE      PPAPTRNF
025800                      INTO UCWSABND-ABEND-DATA (1)                PPAPTRNF
025900          END-STRING                                              PPAPTRNF
026000          PERFORM 9999-ABEND                                      PPAPTRNF
026100     END-CALL.                                                    PPAPTRNF
028400                                                                  PPAPTRNF
028500 1100-EXIT.                                                       PPAPTRNF
028600     EXIT.                                                        PPAPTRNF
028700                                                                  PPAPTRNF
048400 EJECT                                                            PPAPTRNF
048500 9999-ABEND    SECTION.                                           PPAPTRNF
048600**************************************************************    PPAPTRNF
048700*  COPIED PROCEDURE DIVISION CODE                            *    PPAPTRNF
048800**************************************************************    PPAPTRNF
048900                                                                  PPAPTRNF
049000     MOVE 'PPAPTRNF' TO UCWSABND-ABENDING-PGM.                    PPAPTRNF
049100                                                                  PPAPTRNF
049200     COPY UCPDABND.                                               PPAPTRNF
049300                                                                  PPAPTRNF
049400 9999-EXIT.                                                       PPAPTRNF
049500     EXIT.                                                        PPAPTRNF
049600                                                                  PPAPTRNF
049700                                                                  PPAPTRNF
049800 EJECT                                                            PPAPTRNF
051500***************    END OF SOURCE - PPAPTRNF    *******************PPAPTRNF
