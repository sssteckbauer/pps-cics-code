000100**************************************************************/   36430889
000200*  PROGRAM: UCWABND                                          */   36430889
000300*  RELEASE: ___0889______ SERVICE REQUEST(S): _____3643____  */   36430889
000400*  NAME:_______AAC_______ MODIFICATION DATE:  __04/08/94____ */   36430889
000500*  DESCRIPTION:                                              */   36430889
000600* - CHNGD TO INSERT OAA "END" ROW INSTEAD OF UPDATING THE    */   36430889
000700*   "START" ROW.                                             */   36430889
000800* - GET ADDRESS OF EIB IN CASE THIS PROGRAM IS CALLED        */   36430889
000900*   WITHOUT IT.                                              */   36430889
001000**************************************************************/   36430889
000000**************************************************************/   36430833
000001*  PROGRAM: UCWABND                                          */   36430833
000002*  RELEASE: ___0833______ SERVICE REQUEST(S): _____3643____  */   36430833
000003*  NAME:_______MLG_______ MODIFICATION DATE:  __11/18/93____ */   36430833
000004*  DESCRIPTION:                                              */   36430833
000005*   1. MOVE THE OAA TABLE UPDATE TO AFTER THE DUMP TO AVOID  */   36430833
000006*      LOCKOUTS ON THE TABLE.                                */   36430833
000007*   2. ADD CODE TO EXECUTE A NEW APPLICATION PROCESSOR       */   36430833
000008*      "ABEND" TO PERFORM APPLICATION-SPECIFIC ABEND         */   36430833
000009*      PROCESSING.                                           */   36430833
000010*                                                            */   36430833
000011**************************************************************/   36430833
000000**************************************************************/   36430815
000001*  PROGRAM: UCWABND                                          */   36430815
000002*  RELEASE: ___0815______ SERVICE REQUEST(S): _____3643____  */   36430815
000003*  NAME:_______MLG_______ MODIFICATION DATE:  __09/17/93____ */   36430815
000004*  DESCRIPTION:                                              */   36430815
000005*  - UN-COMMENT THE COMMENTED-OUT DUMP STATEMENT.            */   36430815
000006*    ONLY SEND THE MAP IF THE TASK IS ATTACHED TO A TERMINAL */   36430815
000009*  - ADD CODE TO DELETE TEMP STORAGE QUEUES                  */   36430815
000012*                                                            */   36430815
000020**************************************************************/   36430815
000100**************************************************************/   36430795
000200*  PROGRAM: UCWABND                                          */   36430795
000300*  RELEASE # ___0795___   SERVICE REQUEST NO(S)____3643______*/   36430795
000400*  NAME __MLG__________   MODIFICATION DATE ____07/15/93_____*/   36430795
000500*  DESCRIPTION                                               */   36430795
000600*                                                            */   36430795
000700*     ABEND HANDLING PROGRAM FOR THE ONLINE APPLICATIONS     */   36430795
000800*     SYSTEM.                                                */   36430795
000810*                                                            */   36430795
000900**************************************************************/   36430795
001000 IDENTIFICATION DIVISION.                                         UCWABND
001100 PROGRAM-ID. UCWABND.                                             UCWABND
001200 ENVIRONMENT DIVISION.                                            UCWABND
001210 CONFIGURATION SECTION.                                           UCWABND
001220 SOURCE-COMPUTER.                      COPY CPOTXUCS.             UCWABND
001230 OBJECT-COMPUTER.                      COPY CPOTXOBJ.             UCWABND
001300 DATA DIVISION.                                                   UCWABND
001400 WORKING-STORAGE SECTION.                                         UCWABND
001500                                                                  UCWABND
001800 01  WS-COMN.                                                     UCWABND
001903     05  WS-START                          PIC X(27) VALUE        UCWABND
001904                                    'WORKING STORAGE STARTS HERE'.UCWABND
001924     05  WS-RESP                           PIC S9(8) COMP.        UCWABND
005600     05  WS-COUNT                          PIC S9(8) COMP.        36430889
001925     05  WS-ABEND-MSG-BEGIN                PIC X(50) VALUE        UCWABND
001926         '************ ABEND MESSAGES BEGIN HERE ***********'.    UCWABND
001927     05  WS-ABEND-MSG-END                  PIC X(48) VALUE        UCWABND
001928         '************ ABEND MESSAGES END HERE ***********'.      UCWABND
001929     05  WS-PROGRAM-MSG.                                          UCWABND
001930         10  FILLER                        PIC X(21) VALUE        UCWABND
001931                                    'ABENDING FUNCTION IS '.      UCWABND
001932         10  WS-ABENDING-FUNCTION          PIC X(04).             UCWABND
001933         10  FILLER                        PIC X(25) VALUE        UCWABND
001940                               '.    ABENDING PROGRAM IS '.       UCWABND
001950         10  WS-ABENDING-PGM               PIC X(08).             UCWABND
001960     05  WS-DATE-TIME-MSG.                                        UCWABND
001970         10  FILLER                        PIC X(12) VALUE        UCWABND
001980                                    'ABEND DATE: '.               UCWABND
001990         10  WS-ABEND-DATE                 PIC X(08).             UCWABND
002000         10  FILLER                        PIC X(15) VALUE        UCWABND
002100                                    '   ABEND TIME: '.            UCWABND
002200         10  WS-ABEND-TIME                 PIC X(08).             UCWABND
002300         10  FILLER                        PIC X(09) VALUE        UCWABND
002400                                    '   USER: '.                  UCWABND
002500         10  WS-ABEND-USER                 PIC X(08).             UCWABND
010800                                                                  UCWABND
010900     05  TS-QUEUE-ID.                                             36430815
010910         10  TS-QUEUE-NAME.                                       36430815
010940             15  TS-QUEUE-NAME-SS          PIC X(02).             36430815
010942             15  TS-QUEUE-NAME-N1          PIC 9(01).             36430815
010943             15  TS-QUEUE-NAME-N2          PIC 9(01).             36430815
010944         10  TS-QUEUE-TERM-ID              PIC X(04).             36430815
010945     05  TS-QUEUE-ITEM                     PIC S9(04) COMP.       36430815
010946     05  TS-QUEUE-LENGTH                   PIC S9(04) COMP.       36430815
010947     05  TS-AUDIT-QUEUE-ID.                                       36430815
010948         10  TS-AUDIT-Q-SS-NAME            PIC X(02).             36430815
010949         10  TS-AUDIT-Q-VARIABLE           PIC X(02).             36430815
010950             88  TS-AUDIT-Q-VAR-REGULAR              VALUE 'AU'.  36430815
010951             88  TS-AUDIT-Q-VAR-NESTED               VALUE 'AN'.  36430815
010952         10  TS-AUDIT-Q-TERM-ID            PIC X(04).             36430815
010953     05  TS-AUDIT-QUEUE-LENGTH             PIC S9(04) COMP        36430815
010954                                                      VALUE 8.    36430815
010955     05  TS-AUDIT-QUEUE-DATA.                                     36430815
010956         10  TS-AUDIT-Q-QUEUE-NAME         PIC X(08).             36430815
010958                                                                  36430815
010959     05  NULL1                             PIC S9(04) COMP.       36430833
010960     05  NULL2                             PIC S9(04) COMP.       36430833
010950 01  UCCOMMON   EXTERNAL.                                         UCWABND
010960     COPY UCCOMMON.                                               UCWABND
011000                                                                  UCWABND
011400 01  UCWSABND   EXTERNAL.                                         UCWABND
011410     COPY UCWSABND.                                               UCWABND
011411                                                                  UCWABND
011412 01  UCWSPMSG EXTERNAL.                                           UCWABND
011413     COPY UCWSPMSG.                                               UCWABND
011414                                                                  UCWABND
011415 01  UCABND0-DATA.                                                UCWABND
011416     COPY UCABND0.                                                UCWABND
011420                                                                  UCWABND
011430                                                                  UCWABND
011440******************************************************************UCWABND
011450*          SQL - WORKING STORAGE                                 *UCWABND
011460******************************************************************UCWABND
012915                                                                  UCWABND
012916 01  OAA-ROW.                                                     UCWABND
012917     EXEC SQL                                                     UCWABND
012918          INCLUDE UC0VZOAA                                        UCWABND
012919     END-EXEC.                                                    UCWABND
012920                                                                  UCWABND
012921 01  PGM-ROW.                                                     36430833
012922     EXEC SQL                                                     36430833
012923          INCLUDE UC0VZPGM                                        36430833
012924     END-EXEC.                                                    36430833
012925                                                                  36430833
012921     EXEC SQL                                                     UCWABND
012922          INCLUDE SQLCA                                           UCWABND
012923     END-EXEC.                                                    UCWABND
012924                                                                  UCWABND
012930 LINKAGE SECTION.                                                 UCWABND
013000                                                                  UCWABND
013100     EJECT                                                        UCWABND
013300                                                                  UCWABND
013400 PROCEDURE DIVISION.                                              UCWABND
013600                                                                  UCWABND
013601 0000-MAIN-LINE SECTION.                                          UCWABND
013602**************************************************************    36430833
013603*  THIS *IS* THE ABEND HANDLER, NO LOOPS PLEASE.             *    36430833
013604**************************************************************    36430833
013602**************************************************************    UCWABND
013603*  ROLL BACK ALL UPDATES.                                    *    UCWABND
013604*  WRITE THE END AUDIT RECORD FOR THE FUNCTION.              *    UCWABND
013605*  SEND THE ABEND MAP WITH DATA FROM THE INTERFACE           *    UCWABND
013606*  RETURN TO CICS                                            *    UCWABND
013607**************************************************************    UCWABND
014700                                                                  36430889
014800     EXEC CICS ADDRESS                                            36430889
014900          EIB(ADDRESS OF DFHEIBLK)                                36430889
015000     END-EXEC.                                                    36430889
013608                                                                  UCWABND
013609     EXEC CICS                                                    UCWABND
013610          SYNCPOINT ROLLBACK                                      UCWABND
013611     END-EXEC.                                                    UCWABND
013612                                                                  UCWABND
013652*****PERFORM 9000-UPDATE-OAA.                                     36430833
013637                                                                  UCWABND
013638     MOVE 'UCWRPMSG' TO UCWSPMSG-PGM-NAME.                        UCWABND
013639                                                                  UCWABND
013640     MOVE SPACES TO UCWSPMSG-DISPLAY-DATA.                        UCWABND
013641     CALL UCWSPMSG-PGM-NAME USING DFHEIBLK DFHCOMMAREA.           UCWABND
013642                                                                  UCWABND
013643     MOVE WS-ABEND-MSG-BEGIN TO UCWSPMSG-DISPLAY-DATA.            UCWABND
013644     CALL UCWSPMSG-PGM-NAME USING DFHEIBLK DFHCOMMAREA.           UCWABND
013645                                                                  UCWABND
013646     MOVE UCCOMMON-DATE-X TO WS-ABEND-DATE.                       UCWABND
013647     MOVE UCCOMMON-TIME-X TO WS-ABEND-TIME.                       UCWABND
013648     MOVE UCCOMMON-USERID TO WS-ABEND-USER.                       UCWABND
013649     MOVE WS-DATE-TIME-MSG TO UCWSPMSG-DISPLAY-DATA.              UCWABND
013650     CALL UCWSPMSG-PGM-NAME USING DFHEIBLK DFHCOMMAREA.           UCWABND
013651                                                                  UCWABND
013652     MOVE UCCOMMON-FUNCTION TO WS-ABENDING-FUNCTION.              UCWABND
013653     MOVE UCWSABND-ABENDING-PGM TO WS-ABENDING-PGM.               UCWABND
013654     MOVE WS-PROGRAM-MSG TO UCWSPMSG-DISPLAY-DATA.                UCWABND
013655     CALL UCWSPMSG-PGM-NAME USING DFHEIBLK DFHCOMMAREA.           UCWABND
013656                                                                  UCWABND
013657     IF UCWSABND-MSG-DATA NOT = SPACES                            UCWABND
013658        MOVE UCWSABND-MSG-DATA TO UCWSPMSG-DISPLAY-DATA           UCWABND
013659        CALL UCWSPMSG-PGM-NAME USING DFHEIBLK DFHCOMMAREA         UCWABND
013660     END-IF.                                                      UCWABND
013661                                                                  UCWABND
013662     IF UCWSABND-ABEND-DATA (1) NOT = SPACES                      UCWABND
013663        MOVE UCWSABND-ABEND-DATA (1) TO UCWSPMSG-DISPLAY-DATA     UCWABND
013664        CALL UCWSPMSG-PGM-NAME USING DFHEIBLK DFHCOMMAREA         UCWABND
013665     END-IF.                                                      UCWABND
013666                                                                  UCWABND
013667     IF UCWSABND-ABEND-DATA (2) NOT = SPACES                      UCWABND
013668        MOVE UCWSABND-ABEND-DATA (2) TO UCWSPMSG-DISPLAY-DATA     UCWABND
013669        CALL UCWSPMSG-PGM-NAME USING DFHEIBLK DFHCOMMAREA         UCWABND
013670     END-IF.                                                      UCWABND
013671                                                                  UCWABND
013672     MOVE WS-ABEND-MSG-END TO UCWSPMSG-DISPLAY-DATA.              UCWABND
013673     CALL UCWSPMSG-PGM-NAME USING DFHEIBLK DFHCOMMAREA.           UCWABND
013674                                                                  UCWABND
013675     IF EIBTRMID NOT = LOW-VALUES                                 36430815
013676        PERFORM 1000-CLEAN-UP-TS                                  36430815
013677     END-IF.                                                      36430815
013678                                                                  36430815
013709     PERFORM 2000-APPLICATION-PROCESSING.                         36430833
013710                                                                  36430833
013686     EXEC CICS                                                    36430815
013687          DUMP                                                    36430815
013688          DUMPCODE (UCCOMMON-FUNCTION)                            36430815
013689     END-EXEC.                                                    36430815
013690                                                                  36430815
013720*                                                               CD36430833
013800     PERFORM 9000-UPDATE-OAA.                                     36430833
013680                                                                  UCWABND
014100**************************************************************    36430815
014110*  IF EIBTRMID IS LOW-VALUES, THE TASK IS NOT ATTACHED TO    *    36430815
014120*  A TERMINAL SO NO MAP IS SENT.                             *    36430815
014140**************************************************************    36430815
014200     IF EIBTRMID NOT = LOW-VALUES                                 36430815
015100        EXEC CICS                                                 36430815
015200             SEND                                                 36430815
015210             MAP ('UCABND0')                                      36430815
015220             MAPSET ('UCABND0')                                   36430815
015300             FROM (UCABND0-DATA)                                  36430815
015400             ERASE                                                36430815
015410             FREEKB                                               36430815
015500             NOHANDLE                                             36430815
015510             RESP(WS-RESP)                                        36430815
015600        END-EXEC                                                  36430815
015610     END-IF.                                                      36430815
015620                                                                  36430815
015700*****EXEC CICS                                                    36430815
015800*****     SEND                                                    36430815
015900*****     MAP    ('UCABND0')                                      36430815
016000*****     MAPSET ('UCABND0')                                      36430815
016100*****     FROM (UCABND0-DATA)                                     36430815
016200*****     ERASE                                                   36430815
016300*****     FREEKB                                                  36430815
016400*****     NOHANDLE                                                36430815
016500*****     RESP(WS-RESP)                                           36430815
016600*****END-EXEC.                                                    36430815
017100                                                                  UCWABND
015100     EXEC CICS                                                    UCWABND
034531          RETURN                                                  UCWABND
015600     END-EXEC.                                                    UCWABND
017100                                                                  UCWABND
034534     GOBACK.                                                      UCWABND
034540                                                                  UCWABND
034550 0000-EXIT.                                                       UCWABND
034560     EXIT.                                                        UCWABND
034600                                                                  UCWABND
034601 1000-CLEAN-UP-TS   SECTION.                                      36430815
034602**************************************************************    36430815
034603*  DELETE TEMP STORAGE QUEUES FOR THE UC SUBSYSTEM AS WELL   *    36430815
034604*  AS THE CURRENT APPLICATION SUBSYSTEM.                     *    36430815
034605**************************************************************    36430815
034606                                                                  36430815
034607     MOVE EIBTRMID TO TS-AUDIT-Q-TERM-ID.                         36430815
034608                                                                  36430815
034610     MOVE 'UC' TO TS-AUDIT-Q-SS-NAME.                             36430815
034611     SET TS-AUDIT-Q-VAR-REGULAR TO TRUE.                          36430815
034612                                                                  36430815
034613     PERFORM 1100-DELETE-TS-QUEUES.                               36430815
034620                                                                  36430815
034621     IF UCCOMMON-SUBSYSTEM-ID NOT = 'UC'                          36430815
034622        MOVE UCCOMMON-SUBSYSTEM-ID TO TS-AUDIT-Q-SS-NAME          36430815
034623        PERFORM 1100-DELETE-TS-QUEUES                             36430815
034624        SET TS-AUDIT-Q-VAR-NESTED TO TRUE                         36430815
034625        PERFORM 1100-DELETE-TS-QUEUES                             36430815
034626     END-IF.                                                      36430815
034627                                                                  36430815
034628     IF UCCOMMON-NESTED-SUBSYSTEM NOT = SPACES                    36430815
034629        MOVE UCCOMMON-NESTED-SUBSYSTEM TO TS-AUDIT-Q-SS-NAME      36430815
034630        SET TS-AUDIT-Q-VAR-REGULAR TO TRUE                        36430815
034631        PERFORM 1100-DELETE-TS-QUEUES                             36430815
034632        SET TS-AUDIT-Q-VAR-NESTED TO TRUE                         36430815
034633        PERFORM 1100-DELETE-TS-QUEUES                             36430815
034635     END-IF.                                                      36430815
034636                                                                  36430815
034639 1000-EXIT.                                                       36430815
034640     EXIT.                                                        36430815
034641                                                                  36430815
034642     EJECT                                                        36430815
034643                                                                  36430815
034644 1100-DELETE-TS-QUEUES   SECTION.                                 36430815
034645**************************************************************    36430815
034646*  DELETE EACH TEMP STORAGE QUEUE NAMED IN THE AUDIT QUEUE   *    36430815
034647*  AND THEN DELETE THE AUDIT QUEUE.                          *    36430815
034649**************************************************************    36430815
034650                                                                  36430815
034655     MOVE DFHRESP(NORMAL) TO WS-RESP.                             36430815
034656                                                                  36430815
034657     PERFORM 1110-DELETE-ONE-QUEUE                                36430815
034658       VARYING TS-QUEUE-ITEM FROM 1 BY 1                          36430815
034659       UNTIL WS-RESP NOT = DFHRESP(NORMAL).                       36430815
034660                                                                  36430815
034661     EXEC CICS                                                    36430815
034662          DELETEQ TS                                              36430815
034663          QUEUE (TS-AUDIT-QUEUE-ID)                               36430815
034664          NOHANDLE                                                36430815
034665     END-EXEC.                                                    36430815
034667                                                                  36430815
034668 1100-EXIT.                                                       36430815
034669     EXIT.                                                        36430815
034670                                                                  36430815
034671     EJECT                                                        36430815
034672                                                                  36430815
034673 1110-DELETE-ONE-QUEUE   SECTION.                                 36430815
034674**************************************************************    36430815
034675*  READ THE TEMP STORAGE AUDIT QUEUE TO OBTAIN THE NAMES     *    36430815
034676*  OF TEMP STORAGE QUEUES WHICH HAVE BEEN PREVIOUSLY         *    36430815
034677*  WRITTEN. THEN DELETE THE QUEUES NAMED IN THE AUDIT QUEUE. *    36430815
034678**************************************************************    36430815
034679                                                                  36430815
034680     MOVE EIBTRMID TO TS-AUDIT-Q-TERM-ID.                         36430815
034681                                                                  36430815
034682     EXEC CICS                                                    36430815
034683          READQ TS                                                36430815
034684          QUEUE  (TS-AUDIT-QUEUE-ID)                              36430815
034685          INTO   (TS-AUDIT-QUEUE-DATA)                            36430815
034686          LENGTH (TS-AUDIT-QUEUE-LENGTH)                          36430815
034687          ITEM   (TS-QUEUE-ITEM)                                  36430815
034688          NOHANDLE                                                36430815
034689          RESP(WS-RESP)                                           36430815
034690     END-EXEC.                                                    36430815
034691                                                                  36430815
034692*****IF WS-RESP = DFHRESP(QIDERR) - PRE TRANSLATED CODE           36430815
034693     IF WS-RESP = DFHRESP(QIDERR)                                 36430815
034694        OR WS-RESP = DFHRESP(ITEMERR)                             36430815
034695           GO TO 1110-EXIT                                        36430815
034696     END-IF.                                                      36430815
034697                                                                  36430815
034698     EXEC CICS                                                    36430815
034699          DELETEQ TS                                              36430815
034700          QUEUE (TS-AUDIT-Q-QUEUE-NAME)                           36430815
034701          NOHANDLE                                                36430815
034702     END-EXEC.                                                    36430815
034703                                                                  36430815
034704     MOVE DFHRESP(NORMAL) TO WS-RESP.                             36430815
034705                                                                  36430815
034706 1110-EXIT.                                                       36430815
034707     EXIT.                                                        36430815
034708                                                                  36430815
034709     EJECT                                                        36430815
034710                                                                  36430815
034711 2000-APPLICATION-PROCESSING   SECTION.                           36430833
034712**************************************************************    36430833
034713*  THE ABEND APPLICATION PROCESSOR NAME IS OBTAINED FROM THE *    36430833
034714*  PGM TABLE. IF IT IS FOUND, THE PROCESSOR IS CALLED.       *    36430833
034716**************************************************************    36430833
034717                                                                  36430833
034718     MOVE 'ABEND' TO PGM-LOGICAL-PGM-ID.                          36430833
034719                                                                  36430833
034730     PERFORM 9100-SELECT-PGM.                                     36430833
034731                                                                  36430833
034732     IF SQLCODE = ZERO                                            36430833
034733        SET UCCOMMON-NO-ACTION TO TRUE                            36430833
034734        SET UCCOMMON-RETURN-NO-ERRORS TO TRUE                     36430833
034735        CALL PGM-CALL-NAME USING DFHEIBLK                         36430833
034743     END-IF.                                                      36430833
034744                                                                  36430833
034750 2000-EXIT.                                                       36430833
034751     EXIT.                                                        36430833
034752                                                                  36430833
034753     EJECT                                                        36430833
034760                                                                  36430833
034610 9000-UPDATE-OAA   SECTION.                                       UCWABND
034620**************************************************************    UCWABND
034630*  UPDATE THE OAA-ACTIVITY-TYPE IN THE OAA ROW.              *    UCWABND
034640**************************************************************    UCWABND
034650                                                                  UCWABND
036600     INITIALIZE OAA-ROW.                                          36430889
034660     MOVE 'A' TO OAA-ACTIVITY-TYPE.                               UCWABND
034670                                                                  UCWABND
034691     MOVE EIBTRMID TO OAA-TERMINAL-ID.                            UCWABND
034692     MOVE UCCOMMON-AUDIT-TIMESTAMP-START TO OAA-TIMESTAMP-STRT.   UCWABND
037100                                                                  36430889
037200     EXEC SQL                                                     36430889
037300          SELECT COUNT (*),                                       36430889
037400                 CHAR(CURRENT TIMESTAMP)                          36430889
037500          INTO :WS-COUNT,                                         36430889
037600               :OAA-TIMESTAMP-END                                 36430889
037700          FROM UC0VZPGM_PGM                                       36430889
037800     END-EXEC.                                                    36430889
037900                                                                  36430889
038000     EXEC SQL                                                     36430889
038100          INSERT INTO UC0VZOAA_OAA                                36430889
038200             (OAA_TERMINAL_ID                                     36430889
038300             ,OAA_TIMESTAMP_STRT                                  36430889
038400             ,OAA_TIMESTAMP_END                                   36430889
038500             ,OAA_APPLICATION_ID                                  36430889
038600             ,OAA_SUBSYSTEM_ID                                    36430889
038700             ,OAA_USER_ID                                         36430889
038800             ,OAA_FUNCTION_ID                                     36430889
038900             ,OAA_RECORD_KEY                                      36430889
039000             ,OAA_ACTIVITY_TYPE)                                  36430889
039100          VALUES                                                  36430889
039200             (:OAA-TERMINAL-ID                                    36430889
039300             ,:OAA-TIMESTAMP-STRT                                 36430889
039400             ,:OAA-TIMESTAMP-END                                  36430889
039500             ,:OAA-APPLICATION-ID                                 36430889
039600             ,:OAA-SUBSYSTEM-ID                                   36430889
039700             ,:OAA-USER-ID                                        36430889
039800             ,:OAA-FUNCTION-ID                                    36430889
039900             ,:OAA-RECORD-KEY                                     36430889
040000             ,:OAA-ACTIVITY-TYPE)                                 36430889
040100     END-EXEC.                                                    36430889
034693                                                                  UCWABND
040300*    EXEC SQL                                                     36430889
040400*         UPDATE UC0VZOAA_OAA                                     36430889
040500*           SET OAA_ACTIVITY_TYPE                                 36430889
040600*            = :OAA-ACTIVITY-TYPE,                                36430889
040700*               OAA_TIMESTAMP_END                                 36430889
040800*            =  CURRENT TIMESTAMP                                 36430889
040900*         WHERE OAA_TERMINAL_ID = :OAA-TERMINAL-ID                36430889
041000*           AND OAA_TIMESTAMP_STRT = :OAA-TIMESTAMP-STRT          36430889
041100*    END-EXEC.                                                    36430889
034730                                                                  UCWABND
034740 9000-EXIT.                                                       UCWABND
034750     EXIT.                                                        UCWABND
034760                                                                  UCWABND
034770     EJECT                                                        UCWABND
034821                                                                  36430833
034830 9100-SELECT-PGM   SECTION.                                       36430833
034840**************************************************************    36430833
034850*  THIS CODE IS COPIED FROM PROGRAM UCROUTER AND SHOULD      *    36430833
034851*  BE MAINTAINED IN SYNC WITH THAT PROGRAM.                  *    36430833
034852*  SELECT A ROW FROM THE PGM (PROGRAM) TABLE                 *    36430833
034860*                                                            *    36430833
034870*  1. THE FIRST SELECT WILL DETERMINE WHETHER THERE ARE ROWS *    36430833
034880*     FOR THE SPECIFIC SUBSYSTEM AND/OR DETAIL FUNCTION. IF  *    36430833
034890*     THERE ARE, THE SUBSYSTEM AND/OR DETAIL FUNCTION WILL BE*    36430833
034891*     RETURNED AS THE MAXIMUM VALUES. OTHERWISE, THE DEFAULT *    36430833
034892*     VALUE OF SPACES WILL BE RETURNED.                      *    36430833
034893*                                                            *    36430833
034894*  2. THE SECOND SELECT, SELECTS THE ACTUAL DATA USING THE   *    36430833
034895*     VALUES OBTAINED IN THE FIRST SELECT.                   *    36430833
034896**************************************************************    36430833
034897                                                                  36430833
034898     MOVE UCCOMMON-SUBSYSTEM-ID TO PGM-SUBSYSTEM-ID.              36430833
034899     MOVE UCCOMMON-FUNCTION TO PGM-DETAIL-FUNC.                   36430833
034900                                                                  36430833
034930     EXEC SQL                                                     36430833
034940          SELECT MAX(PGM_SUBSYSTEM_ID),                           36430833
034950                 MAX(PGM_DETAIL_FUNC)                             36430833
034960          INTO  :PGM-SUBSYSTEM-ID :NULL1,                         36430833
034970                :PGM-DETAIL-FUNC :NULL2                           36430833
034980          FROM UC0VZPGM_PGM                                       36430833
034990          WHERE                                                   36430833
034991            (PGM_SUBSYSTEM_ID = :PGM-SUBSYSTEM-ID                 36430833
034992             OR PGM_SUBSYSTEM_ID = ' ')                           36430833
034993          AND                                                     36430833
034994            (PGM_DETAIL_FUNC  = :PGM-DETAIL-FUNC                  36430833
034995             OR PGM_DETAIL_FUNC = ' ')                            36430833
034996          AND PGM_LOGICAL_PGM_ID =  :PGM-LOGICAL-PGM-ID           36430833
034997     END-EXEC.                                                    36430833
034998                                                                  36430833
034999**************************************************************    36430833
035000*  WHEN EITHER NULL FIELD IS LESS THAN ZERO, THERE WERE      *    36430833
035010*  NO ROWS TO PRODUCE A MAX VALUE. THE SQLCODE IS            *    36430833
035020*  ARTIFICIALLY SET TO 100 HERE SO THAT "NOT FOUND"          *    36430833
035030*  PROCESSING WILL BE PERFORMED.                             *    36430833
035040**************************************************************    36430833
035050                                                                  36430833
035080     IF SQLCODE = ZERO                                            36430833
035090        IF NULL1 < ZERO OR NULL2 < ZERO                           36430833
035091           MOVE 100 TO SQLCODE                                    36430833
035092        ELSE                                                      36430833
035093           EXEC SQL                                               36430833
035094                SELECT PGM_CALL_NAME                              36430833
035095                INTO  :PGM-CALL-NAME                              36430833
035096                FROM UC0VZPGM_PGM                                 36430833
035097                WHERE PGM_SUBSYSTEM_ID =  :PGM-SUBSYSTEM-ID       36430833
035098                AND PGM_DETAIL_FUNC    =  :PGM-DETAIL-FUNC        36430833
035099                AND PGM_LOGICAL_PGM_ID =  :PGM-LOGICAL-PGM-ID     36430833
035100           END-EXEC                                               36430833
035110        END-IF                                                    36430833
035120     END-IF.                                                      36430833
035130                                                                  36430833
035191 9100-EXIT.                                                       36430833
035192     EXIT.                                                        36430833
035193                                                                  36430833
035194     EJECT                                                        36430833
035195                                                                  36430833
034800***************    END OF SOURCE - UCWABND    ******************* UCWABND
