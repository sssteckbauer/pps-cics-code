000100**************************************************************/   36430944
000200*  PROGRAM: PPAPHDSR                                         */   36430944
000300*  RELEASE # ___0944___   SERVICE REQUEST NO(S)____3643______*/   36430944
000400*  NAME __MLG__________   MODIFICATION DATE ____11/17/94_____*/   36430944
000500*  DESCRIPTION                                               */   36430944
000700*     HDB INQUIRY STORE/RESTORE APPLICATION PROCESSOR        */   36430944
000900**************************************************************/   36430944
001000 IDENTIFICATION DIVISION.                                         PPAPHDSR
001100 PROGRAM-ID. PPAPHDSR.                                            PPAPHDSR
001200 ENVIRONMENT DIVISION.                                            PPAPHDSR
001300 CONFIGURATION SECTION.                                           PPAPHDSR
001400 SOURCE-COMPUTER.                                                 PPAPHDSR
001500 COPY  CPOTXUCS.                                                  PPAPHDSR
001600 DATA DIVISION.                                                   PPAPHDSR
001700 WORKING-STORAGE SECTION.                                         PPAPHDSR
001800                                                                  PPAPHDSR
001900 01  WS-COMN.                                                     PPAPHDSR
002000     05  WS-START                          PIC X(27) VALUE        PPAPHDSR
002100         'WORKING STORAGE STARTS HERE'.                           PPAPHDSR
002200     05  WS-RESP                           PIC S9(8) COMP.        PPAPHDSR
002300     05  WS-LENGTH                         PIC S9(08) COMP.       PPAPHDSR
002400     05  TS-QUEUE-ID.                                             PPAPHDSR
002500         10  TS-QUEUE-NAME-SS              PIC X(02).             PPAPHDSR
002600         10  TS-QUEUE-NAME-N1              PIC 9(01).             PPAPHDSR
002700         10  TS-QUEUE-NAME-N2              PIC 9(01).             PPAPHDSR
002800         10  TS-QUEUE-TERMID               PIC X(04).             PPAPHDSR
002900     05  TS-QUEUE-LENGTH                   PIC S9(04) COMP.       PPAPHDSR
003000     05  TS-QUEUE-ITEM                     PIC S9(04) COMP.       PPAPHDSR
003100     05  TS-AUDIT-QUEUE-ID.                                       PPAPHDSR
003200         10  TS-AUDIT-Q-SS-NAME            PIC X(02).             PPAPHDSR
003300         10  TS-AUDIT-Q-VARIABLE           PIC X(02).             PPAPHDSR
003400             88  TS-AUDIT-Q-VAR-REGULAR              VALUE 'AU'.  PPAPHDSR
003500             88  TS-AUDIT-Q-VAR-NESTED               VALUE 'AN'.  PPAPHDSR
003600         10  TS-AUDIT-Q-TERM-ID            PIC X(04).             PPAPHDSR
003700     05  TS-AUDIT-QUEUE-LENGTH             PIC S9(04) COMP        PPAPHDSR
003800                                                      VALUE 8.    PPAPHDSR
003900     05  TS-AUDIT-QUEUE-DATA.                                     PPAPHDSR
004000         10  TS-AUDIT-Q-QUEUE-NAME         PIC X(08).             PPAPHDSR
004100                                                                  PPAPHDSR
004200 01  WS-TS-AREA                            PIC X(24000).          PPAPHDSR
004300                                                                  PPAPHDSR
004400 01  PPDB2MSG-INTERFACE.                                          PPAPHDSR
004500     COPY CPLNKDB2.                                               PPAPHDSR
004600                                                                  PPAPHDSR
004700 01  UCCOMMON   EXTERNAL.                                         PPAPHDSR
004800     COPY UCCOMMON.                                               PPAPHDSR
004900                                                                  PPAPHDSR
005000 01  UCPIROSR   EXTERNAL.                                         PPAPHDSR
005100     COPY UCPIROSR.                                               PPAPHDSR
005200                                                                  PPAPHDSR
005300 01  CPWSWORK   EXTERNAL.                                         PPAPHDSR
005400     COPY CPWSWORK.                                               PPAPHDSR
005500                                                                  PPAPHDSR
005600 01  CPWSHDBW   EXTERNAL.                                         PPAPHDSR
005700     COPY CPWSHDBW.                                               PPAPHDSR
005800                                                                  PPAPHDSR
005900 01  CPWSKEYS   EXTERNAL.                                         PPAPHDSR
006000     COPY CPWSKEYS.                                               PPAPHDSR
006100                                                                  PPAPHDSR
006200 01  UCWSABND    EXTERNAL.                                        PPAPHDSR
006300     COPY UCWSABND.                                               PPAPHDSR
006400                                                                  PPAPHDSR
006500 01  IDX-ROW     EXTERNAL.                                        PPAPHDSR
006600     EXEC SQL                                                     PPAPHDSR
006700          INCLUDE PPPVZIDX                                        PPAPHDSR
006800     END-EXEC.                                                    PPAPHDSR
006900                                                                  PPAPHDSR
007000 LINKAGE SECTION.                                                 PPAPHDSR
007100                                                                  PPAPHDSR
007200     EJECT                                                        PPAPHDSR
007300                                                                  PPAPHDSR
007400 PROCEDURE DIVISION.                                              PPAPHDSR
007500                                                                  PPAPHDSR
007600 0000-MAIN-LINE   SECTION.                                        PPAPHDSR
007700**************************************************************    PPAPHDSR
007800*  MAIN LINE PROCESSING SECTION                              *    PPAPHDSR
007900*  PERFORM STORE OR RESTORE FUNCTIONS DEPENDING ON REQUEST   *    PPAPHDSR
008000*  CODE.                                                     *    PPAPHDSR
008100*  THE FOLLOWING CONVENTIONS ARE USED FOR TEMP STORAGE QUEUE *    PPAPHDSR
008200*  NAMES: SSXYTTTT WHERE:                                    *    PPAPHDSR
008300*    SS = SUBSYSTEM ID PASSED FROM UCROUTER                  *    PPAPHDSR
008400*    X  = NESTING LEVEL PASSED FROM UCROUTER                 *    PPAPHDSR
008500*    Y  = QUEUE IDENTIFIER                                   *    PPAPHDSR
008600*    TTTT = TERMINAL ID                                      *    PPAPHDSR
008700*                                                            *    PPAPHDSR
008800**************************************************************    PPAPHDSR
008900                                                                  PPAPHDSR
009000                                                                  PPAPHDSR
009100     MOVE ZERO TO UCCOMMON-RETURN-CODE.                           PPAPHDSR
009200                                                                  PPAPHDSR
009300     IF UCCOMMON-STORE-DATA                                       PPAPHDSR
009400        PERFORM 1000-STORE-DATA                                   PPAPHDSR
009500     ELSE                                                         PPAPHDSR
009600        PERFORM 2000-RESTORE-DATA                                 PPAPHDSR
009700     END-IF.                                                      PPAPHDSR
009800                                                                  PPAPHDSR
009900     GOBACK.                                                      PPAPHDSR
010000                                                                  PPAPHDSR
010100 0000-EXIT.                                                       PPAPHDSR
010200     EXIT.                                                        PPAPHDSR
010300                                                                  PPAPHDSR
010400     EJECT                                                        PPAPHDSR
010500                                                                  PPAPHDSR
010600 1000-STORE-DATA   SECTION.                                       PPAPHDSR
010700**************************************************************    PPAPHDSR
010800*  STORE EXTERNAL AREAS IN TEMP STORAGE                      *    PPAPHDSR
010900**************************************************************    PPAPHDSR
011000                                                                  PPAPHDSR
011100     MOVE EIBTRMID TO TS-QUEUE-TERMID.                            PPAPHDSR
011200                                                                  PPAPHDSR
011300     MOVE UCCOMMON-SUBSYSTEM-ID TO TS-QUEUE-NAME-SS.              PPAPHDSR
011400     MOVE UCPIROSR-NESTED-FUNCTION-SW TO TS-QUEUE-NAME-N1.        PPAPHDSR
011500                                                                  PPAPHDSR
011600**************************************************************    PPAPHDSR
011700*  QUEUE 1 CONTAINS MISC EXTERNAL DATA                       *    PPAPHDSR
011800**************************************************************    PPAPHDSR
011900     MOVE 1 TO TS-QUEUE-NAME-N2.                                  PPAPHDSR
012000                                                                  PPAPHDSR
012100     PERFORM 1100-INITIALIZE-QUEUE.                               PPAPHDSR
012200                                                                  PPAPHDSR
012300     MOVE 1 TO TS-QUEUE-ITEM.                                     PPAPHDSR
012400     MOVE LENGTH OF CPWSWORK TO TS-QUEUE-LENGTH.                  PPAPHDSR
012500                                                                  PPAPHDSR
012600     EXEC CICS                                                    PPAPHDSR
012700          WRITEQ                                                  PPAPHDSR
012800          QUEUE  (TS-QUEUE-ID)                                    PPAPHDSR
012900          FROM   (CPWSWORK)                                       PPAPHDSR
013000          ITEM   (TS-QUEUE-ITEM)                                  PPAPHDSR
013100          LENGTH (TS-QUEUE-LENGTH)                                PPAPHDSR
013200          NOHANDLE                                                PPAPHDSR
013300          RESP   (WS-RESP)                                        PPAPHDSR
013400     END-EXEC.                                                    PPAPHDSR
013500                                                                  PPAPHDSR
013600     IF WS-RESP NOT = DFHRESP(NORMAL)                             PPAPHDSR
013700        PERFORM 9999-ABEND                                        PPAPHDSR
013800     END-IF.                                                      PPAPHDSR
013900                                                                  PPAPHDSR
014000     MOVE 2 TO TS-QUEUE-ITEM.                                     PPAPHDSR
014100     MOVE LENGTH OF CPWSHDBW TO TS-QUEUE-LENGTH.                  PPAPHDSR
014200                                                                  PPAPHDSR
014300     EXEC CICS                                                    PPAPHDSR
014400          WRITEQ                                                  PPAPHDSR
014500          QUEUE  (TS-QUEUE-ID)                                    PPAPHDSR
014600          FROM   (CPWSHDBW)                                       PPAPHDSR
014700          ITEM   (TS-QUEUE-ITEM)                                  PPAPHDSR
014800          LENGTH (TS-QUEUE-LENGTH)                                PPAPHDSR
014900          NOHANDLE                                                PPAPHDSR
015000          RESP   (WS-RESP)                                        PPAPHDSR
015100     END-EXEC.                                                    PPAPHDSR
015200                                                                  PPAPHDSR
015300     IF WS-RESP NOT = DFHRESP(NORMAL)                             PPAPHDSR
015400        PERFORM 9999-ABEND                                        PPAPHDSR
015500     END-IF.                                                      PPAPHDSR
015600                                                                  PPAPHDSR
015700     MOVE 3 TO TS-QUEUE-ITEM.                                     PPAPHDSR
015800     MOVE LENGTH OF CPWSKEYS TO TS-QUEUE-LENGTH.                  PPAPHDSR
015900                                                                  PPAPHDSR
016000     EXEC CICS                                                    PPAPHDSR
016100          WRITEQ                                                  PPAPHDSR
016200          QUEUE  (TS-QUEUE-ID)                                    PPAPHDSR
016300          FROM   (CPWSKEYS)                                       PPAPHDSR
016400          ITEM   (TS-QUEUE-ITEM)                                  PPAPHDSR
016500          LENGTH (TS-QUEUE-LENGTH)                                PPAPHDSR
016600          NOHANDLE                                                PPAPHDSR
016700          RESP   (WS-RESP)                                        PPAPHDSR
016800     END-EXEC.                                                    PPAPHDSR
016900                                                                  PPAPHDSR
017000     IF WS-RESP NOT = DFHRESP(NORMAL)                             PPAPHDSR
017100        PERFORM 9999-ABEND                                        PPAPHDSR
017200     END-IF.                                                      PPAPHDSR
017300                                                                  PPAPHDSR
017400**************************************************************    PPAPHDSR
017500*  QUEUE 2 CONTAINS HDB EXTERNAL DATA                        *    PPAPHDSR
017600**************************************************************    PPAPHDSR
017700     MOVE 2 TO TS-QUEUE-NAME-N2.                                  PPAPHDSR
017800                                                                  PPAPHDSR
017900     PERFORM 1100-INITIALIZE-QUEUE.                               PPAPHDSR
018000                                                                  PPAPHDSR
018100     MOVE 1 TO TS-QUEUE-ITEM.                                     PPAPHDSR
018200     MOVE LENGTH OF IDX-ROW TO TS-QUEUE-LENGTH.                   PPAPHDSR
018300                                                                  PPAPHDSR
018400     EXEC CICS                                                    PPAPHDSR
018500          WRITEQ                                                  PPAPHDSR
018600          QUEUE  (TS-QUEUE-ID)                                    PPAPHDSR
018700          FROM   (IDX-ROW)                                        PPAPHDSR
018800          ITEM   (TS-QUEUE-ITEM)                                  PPAPHDSR
018900          LENGTH (TS-QUEUE-LENGTH)                                PPAPHDSR
019000          NOHANDLE                                                PPAPHDSR
019100          RESP   (WS-RESP)                                        PPAPHDSR
019200     END-EXEC.                                                    PPAPHDSR
019300                                                                  PPAPHDSR
019400     IF WS-RESP NOT = DFHRESP(NORMAL)                             PPAPHDSR
019500        PERFORM 9999-ABEND                                        PPAPHDSR
019600     END-IF.                                                      PPAPHDSR
019700                                                                  PPAPHDSR
019800 1000-EXIT.                                                       PPAPHDSR
019900     EXIT.                                                        PPAPHDSR
020000                                                                  PPAPHDSR
020100     EJECT                                                        PPAPHDSR
020200                                                                  PPAPHDSR
020300 1100-INITIALIZE-QUEUE   SECTION.                                 PPAPHDSR
020400**************************************************************    PPAPHDSR
020500*  DELETE THE TEMP STORAGE QUEUE, IF IT IS THERE. IF IT'S    *    PPAPHDSR
020600*  THE FIRST TIME THROUGH, WRITE THE QUEUE NAME TO THE       *    PPAPHDSR
020700*  TEMP STORAGE AUDIT QUEUE.                                 *    PPAPHDSR
020800**************************************************************    PPAPHDSR
020900                                                                  PPAPHDSR
021000     EXEC CICS                                                    PPAPHDSR
021100          DELETEQ TS                                              PPAPHDSR
021200          QUEUE (TS-QUEUE-ID)                                     PPAPHDSR
021300          NOHANDLE                                                PPAPHDSR
021400          RESP(WS-RESP)                                           PPAPHDSR
021500     END-EXEC.                                                    PPAPHDSR
021600                                                                  PPAPHDSR
021700     IF UCCOMMON-SUBSYSTEM-INIT                                   PPAPHDSR
021800      OR UCPIROSR-NESTED-FUNCTION                                 PPAPHDSR
021900        PERFORM 9000-AUDIT-TS-QUEUE                               PPAPHDSR
022000     END-IF.                                                      PPAPHDSR
022100                                                                  PPAPHDSR
022200 1100-EXIT.                                                       PPAPHDSR
022300     EXIT.                                                        PPAPHDSR
022400                                                                  PPAPHDSR
022500     EJECT                                                        PPAPHDSR
022600                                                                  PPAPHDSR
022700 1500-WRITE-ARRAY-QUEUE   SECTION.                                PPAPHDSR
022800**************************************************************    PPAPHDSR
022900*  WRITE AN ACTION CODE ARRAY QUEUE                          *    PPAPHDSR
023000**************************************************************    PPAPHDSR
023100                                                                  PPAPHDSR
023200     EXEC CICS                                                    PPAPHDSR
023300          WRITEQ                                                  PPAPHDSR
023400          QUEUE  (TS-QUEUE-ID)                                    PPAPHDSR
023500          FROM   (WS-TS-AREA)                                     PPAPHDSR
023600          ITEM   (TS-QUEUE-ITEM)                                  PPAPHDSR
023700          LENGTH (TS-QUEUE-LENGTH)                                PPAPHDSR
023800          NOHANDLE                                                PPAPHDSR
023900          RESP   (WS-RESP)                                        PPAPHDSR
024000     END-EXEC.                                                    PPAPHDSR
024100                                                                  PPAPHDSR
024200     IF WS-RESP NOT = DFHRESP(NORMAL)                             PPAPHDSR
024300        PERFORM 9999-ABEND                                        PPAPHDSR
024400     END-IF.                                                      PPAPHDSR
024500                                                                  PPAPHDSR
024600 1500-EXIT.                                                       PPAPHDSR
024700     EXIT.                                                        PPAPHDSR
024800                                                                  PPAPHDSR
024900     EJECT                                                        PPAPHDSR
025000                                                                  PPAPHDSR
025100 2000-RESTORE-DATA   SECTION.                                     PPAPHDSR
025200**************************************************************    PPAPHDSR
025300*  READ TEMP STORAGE QUEUES AND POPULATE EXTERNAL AREAS      *    PPAPHDSR
025400**************************************************************    PPAPHDSR
025500                                                                  PPAPHDSR
025600     MOVE EIBTRMID TO TS-QUEUE-TERMID.                            PPAPHDSR
025700                                                                  PPAPHDSR
025800     MOVE UCCOMMON-SUBSYSTEM-ID TO TS-QUEUE-NAME-SS.              PPAPHDSR
025900     MOVE UCPIROSR-NESTED-FUNCTION-SW TO TS-QUEUE-NAME-N1.        PPAPHDSR
026000                                                                  PPAPHDSR
026100**************************************************************    PPAPHDSR
026200*  QUEUE 1 CONTAINS THE MISC NON-HDB EXTERNAL DATA           *    PPAPHDSR
026300**************************************************************    PPAPHDSR
026400     MOVE 1 TO TS-QUEUE-NAME-N2.                                  PPAPHDSR
026500                                                                  PPAPHDSR
026600     MOVE 1 TO TS-QUEUE-ITEM.                                     PPAPHDSR
026700                                                                  PPAPHDSR
026800     EXEC CICS                                                    PPAPHDSR
026900          READQ                                                   PPAPHDSR
027000          QUEUE  (TS-QUEUE-ID)                                    PPAPHDSR
027100          INTO   (CPWSWORK)                                       PPAPHDSR
027200          ITEM   (TS-QUEUE-ITEM)                                  PPAPHDSR
027300          NOHANDLE                                                PPAPHDSR
027400          RESP   (WS-RESP)                                        PPAPHDSR
027500     END-EXEC.                                                    PPAPHDSR
027600                                                                  PPAPHDSR
027700     IF WS-RESP = DFHRESP(NORMAL)                                 PPAPHDSR
027800        CONTINUE                                                  PPAPHDSR
027900     ELSE                                                         PPAPHDSR
028000        PERFORM 9999-ABEND                                        PPAPHDSR
028100     END-IF.                                                      PPAPHDSR
028200                                                                  PPAPHDSR
028300     MOVE 2 TO TS-QUEUE-ITEM.                                     PPAPHDSR
028400                                                                  PPAPHDSR
028500     EXEC CICS                                                    PPAPHDSR
028600          READQ                                                   PPAPHDSR
028700          QUEUE  (TS-QUEUE-ID)                                    PPAPHDSR
028800          INTO   (CPWSHDBW)                                       PPAPHDSR
028900          ITEM   (TS-QUEUE-ITEM)                                  PPAPHDSR
029000          NOHANDLE                                                PPAPHDSR
029100          RESP   (WS-RESP)                                        PPAPHDSR
029200     END-EXEC.                                                    PPAPHDSR
029300                                                                  PPAPHDSR
029400     IF WS-RESP = DFHRESP(NORMAL)                                 PPAPHDSR
029500        CONTINUE                                                  PPAPHDSR
029600     ELSE                                                         PPAPHDSR
029700        PERFORM 9999-ABEND                                        PPAPHDSR
029800     END-IF.                                                      PPAPHDSR
029900                                                                  PPAPHDSR
030000     MOVE 3 TO TS-QUEUE-ITEM.                                     PPAPHDSR
030100                                                                  PPAPHDSR
030200     EXEC CICS                                                    PPAPHDSR
030300          READQ                                                   PPAPHDSR
030400          QUEUE  (TS-QUEUE-ID)                                    PPAPHDSR
030500          INTO   (CPWSKEYS)                                       PPAPHDSR
030600          ITEM   (TS-QUEUE-ITEM)                                  PPAPHDSR
030700          NOHANDLE                                                PPAPHDSR
030800          RESP   (WS-RESP)                                        PPAPHDSR
030900     END-EXEC.                                                    PPAPHDSR
031000                                                                  PPAPHDSR
031100     IF WS-RESP = DFHRESP(NORMAL)                                 PPAPHDSR
031200        CONTINUE                                                  PPAPHDSR
031300     ELSE                                                         PPAPHDSR
031400        PERFORM 9999-ABEND                                        PPAPHDSR
031500     END-IF.                                                      PPAPHDSR
031600                                                                  PPAPHDSR
031700**************************************************************    PPAPHDSR
031800*  QUEUE 2 CONTAINS THE HDB EXTERNAL DATA                    *    PPAPHDSR
031900**************************************************************    PPAPHDSR
032000     MOVE 2 TO TS-QUEUE-NAME-N2.                                  PPAPHDSR
032100                                                                  PPAPHDSR
032200     MOVE 1 TO TS-QUEUE-ITEM.                                     PPAPHDSR
032300                                                                  PPAPHDSR
032400     EXEC CICS                                                    PPAPHDSR
032500          READQ                                                   PPAPHDSR
032600          QUEUE  (TS-QUEUE-ID)                                    PPAPHDSR
032700          INTO   (IDX-ROW)                                        PPAPHDSR
032800          ITEM   (TS-QUEUE-ITEM)                                  PPAPHDSR
032900          NOHANDLE                                                PPAPHDSR
033000          RESP   (WS-RESP)                                        PPAPHDSR
033100     END-EXEC.                                                    PPAPHDSR
033200                                                                  PPAPHDSR
033300     IF WS-RESP = DFHRESP(NORMAL)                                 PPAPHDSR
033400        CONTINUE                                                  PPAPHDSR
033500     ELSE                                                         PPAPHDSR
033600        PERFORM 9999-ABEND                                        PPAPHDSR
033700     END-IF.                                                      PPAPHDSR
033800                                                                  PPAPHDSR
033900                                                                  PPAPHDSR
034000 2000-EXIT.                                                       PPAPHDSR
034100     EXIT.                                                        PPAPHDSR
034200                                                                  PPAPHDSR
034300     EJECT                                                        PPAPHDSR
034400                                                                  PPAPHDSR
034500 2100-DELETE-QUEUE   SECTION.                                     PPAPHDSR
034600**************************************************************    PPAPHDSR
034700***DELETE THE TEMP STORAGE QUEUE                             *    PPAPHDSR
034800**************************************************************    PPAPHDSR
034900                                                                  PPAPHDSR
035000     EXEC CICS                                                    PPAPHDSR
035100          DELETEQ TS                                              PPAPHDSR
035200          QUEUE (TS-QUEUE-ID)                                     PPAPHDSR
035300          NOHANDLE                                                PPAPHDSR
035400          RESP(WS-RESP)                                           PPAPHDSR
035500     END-EXEC.                                                    PPAPHDSR
035600                                                                  PPAPHDSR
035700 2100-EXIT.                                                       PPAPHDSR
035800     EXIT.                                                        PPAPHDSR
035900                                                                  PPAPHDSR
036000     EJECT                                                        PPAPHDSR
036100                                                                  PPAPHDSR
036200                                                                  PPAPHDSR
036300 9000-AUDIT-TS-QUEUE   SECTION.                                   PPAPHDSR
036400**************************************************************    PPAPHDSR
036500*  WRITE THE TEMP STORAGE AUDIT QUEUE CONTAINING THE NAME    *    PPAPHDSR
036600*  OF A TEMP STORAGE QUEUE BEING WRITTEN.                    *    PPAPHDSR
036700**************************************************************    PPAPHDSR
036800                                                                  PPAPHDSR
036900     IF UCPIROSR-NESTED-FUNCTION                                  PPAPHDSR
037000        SET TS-AUDIT-Q-VAR-NESTED TO TRUE                         PPAPHDSR
037100     ELSE                                                         PPAPHDSR
037200        SET TS-AUDIT-Q-VAR-REGULAR TO TRUE                        PPAPHDSR
037300     END-IF.                                                      PPAPHDSR
037400                                                                  PPAPHDSR
037500     MOVE UCCOMMON-SUBSYSTEM-ID TO TS-AUDIT-Q-SS-NAME.            PPAPHDSR
037600     MOVE EIBTRMID TO TS-AUDIT-Q-TERM-ID.                         PPAPHDSR
037700     MOVE TS-QUEUE-ID TO TS-AUDIT-Q-QUEUE-NAME.                   PPAPHDSR
037800                                                                  PPAPHDSR
037900     EXEC CICS                                                    PPAPHDSR
038000          WRITEQ TS                                               PPAPHDSR
038100          QUEUE  (TS-AUDIT-QUEUE-ID)                              PPAPHDSR
038200          FROM   (TS-AUDIT-QUEUE-DATA)                            PPAPHDSR
038300          LENGTH (TS-AUDIT-QUEUE-LENGTH)                          PPAPHDSR
038400          NOHANDLE                                                PPAPHDSR
038500          RESP   (WS-RESP)                                        PPAPHDSR
038600     END-EXEC.                                                    PPAPHDSR
038700                                                                  PPAPHDSR
038800     IF WS-RESP NOT = DFHRESP(NORMAL)                             PPAPHDSR
038900        PERFORM 9999-ABEND                                        PPAPHDSR
039000     END-IF.                                                      PPAPHDSR
039100                                                                  PPAPHDSR
039200 9000-EXIT.                                                       PPAPHDSR
039300     EXIT.                                                        PPAPHDSR
039400                                                                  PPAPHDSR
039500     EJECT                                                        PPAPHDSR
039600                                                                  PPAPHDSR
039700 9999-ABEND    SECTION.                                           PPAPHDSR
039800**************************************************************    PPAPHDSR
039900*  COPIED PROCEDURE DIVISION CODE                            *    PPAPHDSR
040000**************************************************************    PPAPHDSR
040100                                                                  PPAPHDSR
040200     MOVE 'PPAPHDSR' TO UCWSABND-ABENDING-PGM.                    PPAPHDSR
040300                                                                  PPAPHDSR
040400     COPY UCPDABND.                                               PPAPHDSR
040500                                                                  PPAPHDSR
040600 9999-EXIT.                                                       PPAPHDSR
040700     EXIT.                                                        PPAPHDSR
040800                                                                  PPAPHDSR
040900     EXIT.                                                        PPAPHDSR
041000                                                                  PPAPHDSR
041100***************    END OF SOURCE - PPAPHDSR    *******************PPAPHDSR
