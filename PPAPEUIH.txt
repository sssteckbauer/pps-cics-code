000000**************************************************************/   36520886
000001*  PROGRAM: PPAPEUIH                                         */   36520886
000002*  RELEASE: ___0886______ SERVICE REQUEST(S): _____3652____  */   36520886
000003*  NAME:_______MLG_______ MODIFICATION DATE:  __03/30/94____ */   36520886
000004*  DESCRIPTION:                                              */   36520886
000005*                                                            */   36520886
000006*    ADD CODE TO ALLOW ENTRY IN SELECTION SCREENS WHEN       */   36520886
000007*    NO UPDATE ACCESS IS PERMITTED.                          */   36520886
000008*                                                            */   36520886
000009**************************************************************/   36520886
000100**************************************************************/   36430795
000200*  PROGRAM: PPAPEUIH                                         */   36430795
000300*  RELEASE # ___0795___   SERVICE REQUEST NO(S)____3643______*/   36430795
000400*  NAME _____EMS_______   MODIFICATION DATE ____07/15/93_____*/   36430795
000500*  DESCRIPTION                                               */   36430795
000600*     PAYROLL ENTRY/UPDATE INTERPRET HEADER/FOOTER PROGRAM   */   36430795
000700*                                                            */   36430795
000800*                                                            */   36430795
000900**************************************************************/   36430795
001000 IDENTIFICATION DIVISION.                                         PPAPEUIH
001100 PROGRAM-ID. PPAPEUIH.                                            PPAPEUIH
001200 ENVIRONMENT DIVISION.                                            PPAPEUIH
001300 CONFIGURATION SECTION.                                           PPAPEUIH
001400 SOURCE-COMPUTER.                                                 PPAPEUIH
001500 COPY  CPOTXUCS.                                                  PPAPEUIH
001600 DATA DIVISION.                                                   PPAPEUIH
001700 WORKING-STORAGE SECTION.                                         PPAPEUIH
001800                                                                  PPAPEUIH
001900 01  WS-COMN.                                                     PPAPEUIH
002000     05  WS-START                          PIC X(27) VALUE        PPAPEUIH
002100         'WORKING STORAGE STARTS HERE'.                           PPAPEUIH
002200     05  WS-KEY-COUNT                    PIC 9(02) VALUE ZEROS.   PPAPEUIH
002300*                                                                 PPAPEUIH
002400 01  WS-MESS-NOS.                                                 PPAPEUIH
002500     05 MP0006                           PIC X(5)  VALUE 'P0006'. PPAPEUIH
002600     05 MP0634                           PIC X(5)  VALUE 'P0634'. PPAPEUIH
002610     05 MU0030                           PIC X(5)  VALUE 'U0030'. PPAPEUIH
002700*                                                                 PPAPEUIH
002800*                                                                 PPAPEUIH
002900 01  UCFTRARE  EXTERNAL.                                          PPAPEUIH
003000     COPY UCFTRARE.                                               PPAPEUIH
003100 01  WS-PPEUFT0 REDEFINES UCFTRARE.                               PPAPEUIH
003200     COPY PPEUFT0.                                                PPAPEUIH
003300                                                                  PPAPEUIH
003400 01  UCCOMMON               EXTERNAL.                             PPAPEUIH
003500     COPY UCCOMMON.                                               PPAPEUIH
003600                                                                  PPAPEUIH
003700 01  UCPIROIH               EXTERNAL.                             PPAPEUIH
003800     COPY UCPIROIH.                                               PPAPEUIH
003900                                                                  PPAPEUIH
004000 01  CPWSWORK               EXTERNAL.                             PPAPEUIH
004100     COPY CPWSWORK.                                               PPAPEUIH
004200                                                                  PPAPEUIH
004300 01  CPWSFOOT               EXTERNAL.                             PPAPEUIH
004400     EXEC SQL                                                     PPAPEUIH
004500          INCLUDE CPWSFOOT                                        PPAPEUIH
004600     END-EXEC.                                                    PPAPEUIH
004700*                                                                 PPAPEUIH
004800 01  UCWSABND               EXTERNAL.                             PPAPEUIH
004900     COPY UCWSABND.                                               PPAPEUIH
005000                                                                  PPAPEUIH
005100     COPY DFHAID.                                                 PPAPEUIH
005200                                                                  PPAPEUIH
005300                                                                  PPAPEUIH
005400******************************************************************PPAPEUIH
005500*          SQL - WORKING STORAGE                                 *PPAPEUIH
005600******************************************************************PPAPEUIH
005700                                                                  PPAPEUIH
005800******************************************************************PPAPEUIH
005900*                                                                *PPAPEUIH
006000*                  SQL - SELECTS                                 *PPAPEUIH
006100*                                                                *PPAPEUIH
006200******************************************************************PPAPEUIH
006300 LINKAGE SECTION.                                                 PPAPEUIH
006400                                                                  PPAPEUIH
006500                                                                  PPAPEUIH
006600 PROCEDURE DIVISION.                                              PPAPEUIH
006700*                                                                 PPAPEUIH
006800*                                                                 PPAPEUIH
006900 0000-MAINLINE                        SECTION.                    PPAPEUIH
007000*                                                                 PPAPEUIH
007100     SET UCCOMMON-RETURN-NO-ERRORS TO TRUE.                       PPAPEUIH
007200     MOVE COMMANDI TO UCCOMMON-COMMAND-DATA.                      PPAPEUIH
007300     MOVE NEXT-FUNCI TO CPWSFOOT-ENTERED-FUNC.                    PPAPEUIH
007400     SET UCPIROIH-KEY-NOT-ENTERED TO TRUE.                        PPAPEUIH
007500                                                                  PPAPEUIH
007600     IF UCCOMMON-UPD-IN-PROGRESS                                  PPAPEUIH
007700      AND UCCOMMON-FUNC-TYPE-INQUIRY                              PPAPEUIH
007800        MOVE MU0030 TO UCCOMMON-HOT-MSG-NUMBER                    PPAPEUIH
007900     END-IF.                                                      PPAPEUIH
008000                                                                  PPAPEUIH
008100     IF UCCOMMON-RECORD-AUTH-VIEW                                 PPAPEUIH
008200      AND (NOT UCCOMMON-FUNC-TYPE-MENU)                           PPAPEUIH
008210      AND (NOT UCCOMMON-FUNC-TYPE-SELE)                           36520886
008300      AND (UCCOMMON-FUNCTION NOT = 'EDEM')                        PPAPEUIH
008301      AND (UCCOMMON-FUNCTION NOT = 'EKEY')                        PPAPEUIH
008310      AND (UCCOMMON-BUN-APPL-DATA NOT = 'H')                      PPAPEUIH
008320      AND (UCCOMMON-BUN-APPL-DATA NOT = 'R')                      PPAPEUIH
008400        MOVE MP0634 TO UCCOMMON-HOT-MSG-NUMBER                    PPAPEUIH
008500     END-IF.                                                      PPAPEUIH
008600                                                                  PPAPEUIH
008700     IF EIBAID = DFHENTER                                         PPAPEUIH
008800        PERFORM 1000-CHECK-KEY-COUNT                              PPAPEUIH
008900        IF UCCOMMON-RETURN-NO-ERRORS                              PPAPEUIH
009000           PERFORM 2000-CHECK-DATA-ENTERED                        PPAPEUIH
009100        END-IF                                                    PPAPEUIH
009200     END-IF.                                                      PPAPEUIH
009300                                                                  PPAPEUIH
009400     GOBACK.                                                      PPAPEUIH
009500*                                                                 PPAPEUIH
009600 0000-EXIT.                                                       PPAPEUIH
009700     EXIT.                                                        PPAPEUIH
009800*                                                                 PPAPEUIH
009900*                                                                 PPAPEUIH
010000 1000-CHECK-KEY-COUNT                 SECTION.                    PPAPEUIH
010100                                                                  PPAPEUIH
010200     MOVE ZERO TO WS-KEY-COUNT.                                   PPAPEUIH
010300                                                                  PPAPEUIH
010400     IF FTR-IDL > ZERO AND                                        PPAPEUIH
010500        FTR-IDI NOT = SPACES                                      PPAPEUIH
010600        ADD 1 TO WS-KEY-COUNT                                     PPAPEUIH
010700     END-IF.                                                      PPAPEUIH
010800                                                                  PPAPEUIH
010900     IF FTR-NAMEL > ZERO AND                                      PPAPEUIH
011000        FTR-NAMEI NOT = SPACES                                    PPAPEUIH
011100        ADD 1 TO WS-KEY-COUNT                                     PPAPEUIH
011200     END-IF.                                                      PPAPEUIH
011300                                                                  PPAPEUIH
011400     IF FTR-SSNL > ZERO AND                                       PPAPEUIH
011500        FTR-SSNI NOT = SPACES                                     PPAPEUIH
011600        ADD 1 TO WS-KEY-COUNT                                     PPAPEUIH
011700     END-IF.                                                      PPAPEUIH
011800                                                                  PPAPEUIH
011900     IF WS-KEY-COUNT > 1                                          PPAPEUIH
012000        SET UCCOMMON-RETURN-ERRORS-FOUND TO TRUE                  PPAPEUIH
012100        MOVE MP0006 TO  UCCOMMON-MSG-NUMBER                       PPAPEUIH
012200        SET UCCOMMON-REDISPLAY-FUNC TO TRUE                       PPAPEUIH
012300        SET CPWSWORK-REDISPLAY-EEID TO TRUE                       PPAPEUIH
012400        SET CPWSWORK-REDISPLAY-NAME TO TRUE                       PPAPEUIH
012500        SET CPWSWORK-REDISPLAY-SSN TO TRUE                        PPAPEUIH
012600     END-IF.                                                      PPAPEUIH
012700                                                                  PPAPEUIH
012800 1000-EXIT.                                                       PPAPEUIH
012900     EXIT.                                                        PPAPEUIH
013000                                                                  PPAPEUIH
013100                                                                  PPAPEUIH
013200 2000-CHECK-DATA-ENTERED              SECTION.                    PPAPEUIH
013300*                                                                 PPAPEUIH
013400     IF (FTR-IDI NOT = SPACES)                                    PPAPEUIH
013500       AND (FTR-IDI NOT = LOW-VALUES)                             PPAPEUIH
013600         MOVE FTR-IDI TO CPWSFOOT-ENTERED-ID                      PPAPEUIH
013700         SET UCPIROIH-KEY-ENTERED TO TRUE                         PPAPEUIH
013800         MOVE UCCOMMON-ATR-ENTRY TO FTR-IDA                       PPAPEUIH
013900     END-IF.                                                      PPAPEUIH
014000                                                                  PPAPEUIH
014100     IF (FTR-NAMEI NOT = SPACES)                                  PPAPEUIH
014200       AND (FTR-NAMEI NOT = LOW-VALUES)                           PPAPEUIH
014300         MOVE FTR-NAMEI TO CPWSFOOT-ENTERED-NAME                  PPAPEUIH
014400         SET UCPIROIH-KEY-ENTERED TO TRUE                         PPAPEUIH
014500         MOVE UCCOMMON-ATR-ENTRY TO FTR-NAMEA                     PPAPEUIH
014600     END-IF.                                                      PPAPEUIH
014700*                                                                 PPAPEUIH
014800     IF (FTR-SSNI NOT = SPACES)                                   PPAPEUIH
014900       AND (FTR-SSNI NOT = LOW-VALUES)                            PPAPEUIH
015000         MOVE FTR-SSNI TO CPWSFOOT-ENTERED-SSN                    PPAPEUIH
015100         SET UCPIROIH-KEY-ENTERED TO TRUE                         PPAPEUIH
015200         MOVE UCCOMMON-ATR-ENTRY TO FTR-SSNA                      PPAPEUIH
015300     END-IF.                                                      PPAPEUIH
015400*                                                                 PPAPEUIH
015500 2000-EXIT.                                                       PPAPEUIH
015600     EXIT.                                                        PPAPEUIH
015700*                                                                 PPAPEUIH
015800                                                                  PPAPEUIH
015900 9999-ABEND    SECTION.                                           PPAPEUIH
016000**************************************************************    PPAPEUIH
016100*  COPIED PROCEDURE DIVISION CODE FOR ABEND HANDLING         *    PPAPEUIH
016200**************************************************************    PPAPEUIH
016300     MOVE 'PPAPEUIH' TO UCWSABND-ABENDING-PGM.                    PPAPEUIH
016400                                                                  PPAPEUIH
016500     COPY UCPDABND.                                               PPAPEUIH
016600                                                                  PPAPEUIH
016700 9999-EXIT.                                                       PPAPEUIH
