000100**************************************************************/   36370967
000200*  PROGRAM: PPAPTFFE                                         */   36370967
000300*  RELEASE: ___0967______ SERVICE REQUEST(S): _____3637____  */   36370967
000400*  NAME:_______PXP_______ MODIFICATION DATE:  __03/10/95____ */   36370967
000500*  DESCRIPTION:                                              */   36370967
000600*    THF ENTRY/UPDATE FINAL EDIT APPLICATION PROCESSOR       */   36370967
000800**************************************************************/   36370967
000900 IDENTIFICATION DIVISION.                                         PPAPTFFE
001000 PROGRAM-ID. PPAPTFFE.                                            PPAPTFFE
001100 ENVIRONMENT DIVISION.                                            PPAPTFFE
001200 CONFIGURATION SECTION.                                           PPAPTFFE
001300 SOURCE-COMPUTER.                                  COPY CPOTXUCS. PPAPTFFE
001400 OBJECT-COMPUTER.                                  COPY CPOTXOBJ. PPAPTFFE
001500 DATA DIVISION.                                                   PPAPTFFE
001600 WORKING-STORAGE SECTION.                                         PPAPTFFE
001700                                                                  PPAPTFFE
001800 01  WS-COMN.                                                     PPAPTFFE
001900     05  WS-START                          PIC X(27) VALUE        PPAPTFFE
002000         'WORKING STORAGE STARTS HERE'.                           PPAPTFFE
002100     05  A-STND-PROG-ID                   PIC X(15) VALUE         PPAPTFFE
002200                                                    'PPAPTFFE'.   PPAPTFFE
002300     05  WS-PGM-NAME                      PIC X(08).              PPAPTFFE
002400     05  WS-EMPLOYEE-ID         PIC X(09).                        PPAPTFFE
002500     05  WS-RESP                           PIC S9(8) COMP.        PPAPTFFE
002600     05  WS-SUB1                           PIC S9(8) COMP.        PPAPTFFE
002700     05  WS-SUB2                           PIC S9(8) COMP.        PPAPTFFE
002800     05  WS-QUEUE-ID.                                             PPAPTFFE
002900         10  WS-QUEUE-NAME-SS              PIC X(02).             PPAPTFFE
003000         10  WS-QUEUE-NAME-XX              PIC X(02).             PPAPTFFE
003100         10  WS-QUEUE-TERMID               PIC X(04).             PPAPTFFE
003200                                                                  PPAPTFFE
003300 01  CPWSSEVX EXTERNAL.                                           PPAPTFFE
003400     COPY CPWSSEVX.                                               PPAPTFFE
003500     EJECT                                                        PPAPTFFE
003600 01  UCCOMMON   EXTERNAL.                                         PPAPTFFE
003700     COPY UCCOMMON.                                               PPAPTFFE
003800                                                                  PPAPTFFE
003900 01  CPWSWORK   EXTERNAL.                                         PPAPTFFE
004000     COPY CPWSWORK.                                               PPAPTFFE
004100                                                                  PPAPTFFE
004200 01  CPWSGETM   EXTERNAL.                                         PPAPTFFE
004300     COPY CPWSGETM.                                               PPAPTFFE
004400                                                                  PPAPTFFE
004500                                                                  PPAPTFFE
004600 01  UCWSABND    EXTERNAL.                                        PPAPTFFE
004700     COPY UCWSABND.                                               PPAPTFFE
004800******************************************************************PPAPTFFE
004900**     E  X  T  E  R  N  A  L  S      S  E  C  T  I  O  N       **PPAPTFFE
005000**--------------------------------------------------------------**PPAPTFFE
005100** THE FOLLOWING EXTERNAL AREAS WILL BE USED BY OTHER PROCESSES **PPAPTFFE
005200******************************************************************PPAPTFFE
005300 01  ONLINE-SIGNALS                      EXTERNAL. COPY CPWSONLI. PPAPTFFE
007100                                                                  PPAPTFFE
007200 LINKAGE SECTION.                                                 PPAPTFFE
007300                                                                  PPAPTFFE
007400     EJECT                                                        PPAPTFFE
007500                                                                  PPAPTFFE
007600 PROCEDURE DIVISION.                                              PPAPTFFE
008000                                                                  PPAPTFFE
008100 0000-MAIN-LINE   SECTION.                                        PPAPTFFE
008110******************************************************************PPAPTFFE
008120*  MAIN LINE PROCESSING                                          *PPAPTFFE
008140******************************************************************PPAPTFFE
008200                                                                  PPAPTFFE
008300     MOVE ZERO TO UCCOMMON-RETURN-CODE.                           PPAPTFFE
008400     PERFORM 1000-INITIALIZE.                                     PPAPTFFE
008600     PERFORM 9000-FINALIZE.                                       PPAPTFFE
008700     GOBACK.                                                      PPAPTFFE
008800                                                                  PPAPTFFE
008900 0000-EXIT.                                                       PPAPTFFE
009000     EXIT.                                                        PPAPTFFE
009100                                                                  PPAPTFFE
009200     EJECT                                                        PPAPTFFE
009300                                                                  PPAPTFFE
009400*----------------------------------------------------------------*PPAPTFFE
009500 1000-INITIALIZE   SECTION.                                       PPAPTFFE
009600*----------------------------------------------------------------*PPAPTFFE
009700                                                                  PPAPTFFE
009800**************************************************************    PPAPTFFE
009900*  THE QUE OF ERRORS WILL BE REBUILT.                        *    PPAPTFFE
010000*  DELETE IT.                                                *    PPAPTFFE
010100**************************************************************    PPAPTFFE
010200     MOVE EIBTRMID TO WS-QUEUE-TERMID.                            PPAPTFFE
010300     MOVE UCCOMMON-SUBSYSTEM-ID TO WS-QUEUE-NAME-SS.              PPAPTFFE
010400     MOVE 'ER' TO WS-QUEUE-NAME-XX.                               PPAPTFFE
010500                                                                  PPAPTFFE
010600     EXEC CICS                                                    PPAPTFFE
010700          DELETEQ TS                                              PPAPTFFE
010800          QUEUE (WS-QUEUE-ID)                                     PPAPTFFE
010900          NOHANDLE                                                PPAPTFFE
011000          RESP(WS-RESP)                                           PPAPTFFE
011100     END-EXEC.                                                    PPAPTFFE
011200                                                                  PPAPTFFE
011300**************************************************************    PPAPTFFE
011400* SETUP MESSAGE SEVERITY INTERFACE:                          *    PPAPTFFE
011500*    SET THE SEVERITIES TO BE USED IN LOWER LEVEL MODULES    *    PPAPTFFE
011600*    PROCESSING                                              *    PPAPTFFE
011700**************************************************************    PPAPTFFE
011800     MOVE SPACE TO CPWSSEVX-ERROR-MSG-SEVERITY                    PPAPTFFE
011900                   CPWSSEVX-ERROR-MSG-FATALITY.                   PPAPTFFE
012000     MOVE 1 TO CPWSSEVX-INFORMATIONAL-SEV.                        PPAPTFFE
012100     MOVE 3 TO CPWSSEVX-WARNING-SEV.                              PPAPTFFE
012200     MOVE 4 TO CPWSSEVX-SERIOUS-SEV.                              PPAPTFFE
012300     MOVE 5 TO CPWSSEVX-SEV-FATAL.                                PPAPTFFE
012400     MOVE 8 TO CPWSSEVX-SEE-SYSTEM-SEV.                           PPAPTFFE
012500                                                                  PPAPTFFE
013100 1000-EXIT.                                                       PPAPTFFE
013200     EXIT.                                                        PPAPTFFE
013300                                                                  PPAPTFFE
013310     EJECT                                                        PPAPTFFE
013320                                                                  PPAPTFFE
013400*----------------------------------------------------------------*PPAPTFFE
013500 9000-FINALIZE SECTION.                                           PPAPTFFE
013600*----------------------------------------------------------------*PPAPTFFE
013700**************************************************************    PPAPTFFE
013800*    SET RETURN INFO FOR UCROUTER                            *    PPAPTFFE
013900**************************************************************    PPAPTFFE
014000                                                                  PPAPTFFE
014100     SET UCCOMMON-NO-EDIT-ERRORS TO TRUE.                         PPAPTFFE
014200                                                                  PPAPTFFE
014300     IF CPWSSEVX-ERRMSG-SEV-FATAL                                 PPAPTFFE
014400        SET UCCOMMON-EDIT-ERRORS-FOUND TO TRUE                    PPAPTFFE
014500     END-IF.                                                      PPAPTFFE
014510 9000-EXIT.                                                       PPAPTFFE
014520     EXIT.                                                        PPAPTFFE
014530                                                                  PPAPTFFE
014540     EJECT                                                        PPAPTFFE
014550                                                                  PPAPTFFE
014600 9999-ABEND    SECTION.                                           PPAPTFFE
014700**************************************************************    PPAPTFFE
014800*  COPIED PROCEDURE DIVISION CODE FOR ABEND HANDLING         *    PPAPTFFE
014900**************************************************************    PPAPTFFE
015000                                                                  PPAPTFFE
015100     MOVE 'PPAPTFFE' TO UCWSABND-ABENDING-PGM.                    PPAPTFFE
015200                                                                  PPAPTFFE
015300     COPY UCPDABND.                                               PPAPTFFE
015400                                                                  PPAPTFFE
015500 9999-EXIT.                                                       PPAPTFFE
015600     EXIT.                                                        PPAPTFFE
017300***************    END OF SOURCE - PPAPTFFE    *******************PPAPTFFE
