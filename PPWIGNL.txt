000040*==========================================================%      UCSDXXXX
000050*= UCSD LOCAL CHANGES HAVE BEEN IMPLEMENTED.              =%      UCSDXXXX
000060*= COLUMN 73 THRU 80 DENOTES CHANGE NUMBER - UCSDXXXX     =%      UCSDXXXX
000070*=                                                        =%      UCSDXXXX
000080*==========================================================%      UCSDXXXX
000090*==========================================================%      UCSD0056
000091*=    PROGRAM: PPWIGNL                                    =%      UCSD0056
000092*=    CHANGE # 0056         PROJ. REQUEST: 0056           =%      UCSD0056
000093*=    NAME: RON BLOCK       MODIFICATION DATE: 12/11/94   =%      UCSD0056
000094*=                                                        =%      UCSD0056
000110*=     ADD LOGIC TO SUPPORT INQUIRY SECURITY; MOVE SPACES =%      UCSD0056
000096*=     TO SURE-TYPEINDO AND SURE-ACCTNUMO IF THE USER     =%      UCSD0056
000096*=     DOES NOT HAVE UNIVERSAL-ACCESS AUTHORITY.          =%      UCSD0056
000104*==========================================================%      UCSD0056
000100**************************************************************/   36430911
000200*  PROGRAM: PPWIGNL                                          */   36430911
000300*  RELEASE # ___0960___   SERVICE REQUEST NO(S)____3643______*/   36430911
000400*  NAME __PHIL THOMPSON__ MODIFICATION DATE ____02/01/95_____*/   36430911
000500*  DESCRIPTION                                               */   36430911
000600*  - ERROR REPORT 1219:                                      */   36430911
000700*    CHANGE SELECT OF PPPSPB TO USE EMPLOYEE'S SUREPAY BANK  */   36430911
000800*    KEY  VALUE IN WHERE CLAUSE.                             */   36430911
000100**************************************************************/   36430911
000200*  PROGRAM: PPWIGNL                                          */   36430911
000300*  RELEASE # ___0911___   SERVICE REQUEST NO(S)____3643______*/   36430911
000400*  NAME __MLG__________   MODIFICATION DATE ____06/30/94_____*/   36430911
000500*  DESCRIPTION                                               */   36430911
000600*                                                            */   36430911
000700*        THIS IS A COMPLETE REPLACEMENT OF PPWIGNL           */   36430911
000800*                AS ISSUED IN RELEASE 0795                   */   36430911
000900*                                                            */   36430911
001000**************************************************************/   36430911
001100 IDENTIFICATION DIVISION.                                         PPWIGNL
001200 PROGRAM-ID. PPWIGNL.                                             PPWIGNL
001300 ENVIRONMENT DIVISION.                                            PPWIGNL
001400 CONFIGURATION SECTION.                                           PPWIGNL
001500 SOURCE-COMPUTER.                                                 PPWIGNL
001600 COPY  CPOTXUCS.                                                  PPWIGNL
001400 DATA DIVISION.                                                   PPWIGNL
001500 WORKING-STORAGE SECTION.                                         PPWIGNL
001600                                                                  PPWIGNL
002000 01  WS-COMN.                                                     PPWIGNL
002100   05  WS-START             PIC X(27) VALUE                       PPWIGNL
002200       'WORKING STORAGE STARTS HERE'.                             PPWIGNL
002300   05  WS-REL-NO            PIC X(05) VALUE 'I0911'.              PPWIGNL
002400   05  WS-PGM-NAME          PIC X(08) VALUE 'PPWIGNL'.            PPWIGNL
001800                                                                  PPWIGNL
003200 01  WS-COMMON-CICS-DATA.                                         PPWIGNL
003300 COPY CPWSCOMN.                                                   PPWIGNL
005302                                                                  PPWIGNL
005303 01  WS-MISC.                                                     PPWIGNL
005308     05  WS-UNIQUE-EE-ID        PIC X(09) VALUE SPACES.           PPWIGNL
006100     05  WS-ACTIVATE-CNT-N      PIC X(04) VALUE ZERO.             PPWIGNL
006200     05  WS-ACTIVATE-CNT.                                         PPWIGNL
006300         10 WS-ACTIVATE-CNT-PR  PIC X(06) VALUE SPACE.            PPWIGNL
006400         10 FILLER              PIC X(02) VALUE SPACE.            PPWIGNL
006500         10 WS-ACTIVATE-CNT-SF  PIC X(02) VALUE SPACE.            PPWIGNL
006600     05  WS-ACTIVATE-CNTO.                                        PPWIGNL
006700         10 WS-ACTIVATE-CNT-PRO PIC X(06) VALUE SPACE.            PPWIGNL
006800         10 WS-ACTIVATE-CNT-SFO PIC X(02) VALUE SPACE.            PPWIGNL
007700                                                                  PPWIGNL
004000 01  PPDB2MSG-INTERFACE.                                          PPWIGNL
004100     COPY CPLNKDB2.                                               PPWIGNL
007800                                                                  PPWIGNL
004300 01  UCDTLARE  EXTERNAL.                                          PPWIGNL
004400     COPY UCDTLARE.                                               PPWIGNL
004500 01  WS-PPIGNL0 REDEFINES UCDTLARE.                               PPWIGNL
004600     COPY PPIGNL0.                                                PPWIGNL
008100                                                                  PPWIGNL
004800 01  UCCOMMON    EXTERNAL.                                        PPWIGNL
008900     EXEC SQL                                                     PPWIGNL
005000      INCLUDE UCCOMMON                                            PPWIGNL
009100     END-EXEC.                                                    PPWIGNL
009200                                                                  PPWIGNL
005300 01  CPWSWORK    EXTERNAL.                                        PPWIGNL
005400     COPY CPWSWORK.                                               PPWIGNL
009500                                                                  PPWIGNL
005600 01  UCPISCRN    EXTERNAL.                                        PPWIGNL
005700     COPY UCPISCRN.                                               PPWIGNL
009800                                                                  PPWIGNL
005900 01  UCWSABND    EXTERNAL.                                        PPWIGNL
006000     COPY UCWSABND.                                               PPWIGNL
010000                                                                  PPWIGNL
010200                                                                  PPWIGNL
010300******************************************************************PPWIGNL
010400*          SQL - WORKING STORAGE                                 *PPWIGNL
010500******************************************************************PPWIGNL
010600                                                                  PPWIGNL
006700 01  PAY-ROW.                                                     PPWIGNL
010800      EXEC SQL                                                    PPWIGNL
006900           INCLUDE PPPVZPAY                                       PPWIGNL
011000      END-EXEC.                                                   PPWIGNL
011100                                                                  PPWIGNL
007200 01  PCM-ROW.                                                     PPWIGNL
011300      EXEC SQL                                                    PPWIGNL
007400           INCLUDE PPPVZPCM                                       PPWIGNL
011500      END-EXEC.                                                   PPWIGNL
011600                                                                  PPWIGNL
007700 01  SPB-ROW.                                                     PPWIGNL
011800      EXEC SQL                                                    PPWIGNL
007900           INCLUDE PPPVZSPB                                       PPWIGNL
012000      END-EXEC.                                                   PPWIGNL
012100                                                                  PPWIGNL
012200      EXEC SQL                                                    PPWIGNL
012300           INCLUDE SQLCA                                          PPWIGNL
012400      END-EXEC.                                                   PPWIGNL
012500                                                                  PPWIGNL
012600******************************************************************PPWIGNL
012700*                                                                *PPWIGNL
012800*                  SQL - SELECTS                                 *PPWIGNL
012900*                                                                *PPWIGNL
013000******************************************************************PPWIGNL
013100*                                                                *PPWIGNL
013200*        THE VIEW USED IN THIS PROGRAM ARE:                      *PPWIGNL
013300*                                                                *PPWIGNL
013400*    VIEW               VIEW DDL AND DCL INCLUDE                 *PPWIGNL
013500*                        MEMBER NAMES                            *PPWIGNL
013600*                                                                *PPWIGNL
009700*  PPPVZSPB_SPB          PPPVZSPB                                *PPWIGNL
014000******************************************************************PPWIGNL
009900                                                                  PPWIGNL
014100 LINKAGE SECTION.                                                 PPWIGNL
014200                                                                  PPWIGNL
014400                                                                  PPWIGNL
014500 PROCEDURE DIVISION.                                              PPWIGNL
014600                                                                  PPWIGNL
010500 0000-MAIN-LINE   SECTION.                                        PPWIGNL
010600**************************************************************    PPWIGNL
010700*  MAIN LINE PROCESSING                                      *    PPWIGNL
010800**************************************************************    PPWIGNL
015200                                                                  PPWIGNL
011000     EXEC SQL                                                     PPWIGNL
011100          INCLUDE CPPDXE99                                        PPWIGNL
011200     END-EXEC.                                                    PPWIGNL
011300                                                                  PPWIGNL
011400     PERFORM 1000-INITIALIZATION.                                 PPWIGNL
011500     PERFORM 2000-INIT-SCREEN                                     PPWIGNL
011600     PERFORM 3000-DISPLAY-DATA.                                   PPWIGNL
011700                                                                  PPWIGNL
011800     GOBACK.                                                      PPWIGNL
011900                                                                  PPWIGNL
012000 0000-EXIT.                                                       PPWIGNL
012100     EXIT.                                                        PPWIGNL
012200                                                                  PPWIGNL
012300 1000-INITIALIZATION   SECTION.                                   PPWIGNL
012400**************************************************************    PPWIGNL
012500*  INITIALIZE WORK AREAS                                     *    PPWIGNL
012600**************************************************************    PPWIGNL
012700                                                                  PPWIGNL
012800     MOVE WS-PGM-NAME TO UCWSABND-ABENDING-PGM.                   PPWIGNL
012900     MOVE WS-PGM-NAME TO DB2MSG-PGM-ID.                           PPWIGNL
013000                                                                  PPWIGNL
013100     MOVE LOW-VALUES TO UCDTLARE.                                 PPWIGNL
013200     MOVE ZERO TO UCCOMMON-RETURN-CODE.                           PPWIGNL
013300     MOVE LENGTH OF PPIGNL0O TO UCPISCRN-DETAIL-MAP-LENGTH.       PPWIGNL
013400     MOVE WS-REL-NO TO UCPISCRN-DTL-REL-NO.                       PPWIGNL
013500                                                                  PPWIGNL
013600     MOVE 'BROWSE' TO UCPISCRN-ADDL-LOGIC-FUNC (1).               PPWIGNL
013700                                                                  PPWIGNL
013800 1000-EXIT.                                                       PPWIGNL
013900     EXIT.                                                        PPWIGNL
014000                                                                  PPWIGNL
014100     EJECT                                                        PPWIGNL
014200                                                                  PPWIGNL
014300                                                                  PPWIGNL
014400 2000-INIT-SCREEN   SECTION.                                      PPWIGNL
014500**************************************************************    PPWIGNL
014600*  SET COLOR AND HIGHLIGHT BYTES FOR ALL FIELDS              *    PPWIGNL
014700**************************************************************    PPWIGNL
014800                                                                  PPWIGNL
014900     MOVE UCCOMMON-CLR-FLD-LABEL TO                               PPWIGNL
020300                            L-CHK-DISP-CODEC                      PPWIGNL
020400                            L-SURE-PRENOTE-STATC                  PPWIGNL
020500                            L-SURE-PRENOTE-CYCLEC                 PPWIGNL
020600                            L-SURE-ACTIVATE-CNTC                  PPWIGNL
020700                            L-SURE-BANK-KEYC                      PPWIGNL
020800                            L-TRANSITC                            PPWIGNL
020900                            L-SURE-TYPEINDC                       PPWIGNL
021000                            L-CHK-STREET1C                        PPWIGNL
015800     MOVE UCCOMMON-CLR-NORM-DISPL TO                              PPWIGNL
015900                            CHK-DISP-CODEC                        PPWIGNL
016000                            SURE-PRENOTE-STATC                    PPWIGNL
016100                            SURE-PRENOTE-CYCLEC                   PPWIGNL
016200                            SURE-ACTIVATE-CNTC                    PPWIGNL
016300                            SURE-BANK-KEYC                        PPWIGNL
016400                            BANK-NAMEC                            PPWIGNL
016500                            TRANSITC                              PPWIGNL
016600                            SURE-TYPEINDC                         PPWIGNL
016700                            SURE-ACCTNUMC                         PPWIGNL
016800                            CHK-STREET1C                          PPWIGNL
016900                            CHK-STREET2C                          PPWIGNL
017000                            CHK-CITYC                             PPWIGNL
017100                            CHK-STATEC                            PPWIGNL
017200                            CHK-ZIPC                              PPWIGNL
017300     MOVE UCCOMMON-HLT-FLD-LABEL TO                               PPWIGNL
017400                            L-CHK-DISP-CODEH                      PPWIGNL
017500                            L-SURE-PRENOTE-STATH                  PPWIGNL
017600                            L-SURE-PRENOTE-CYCLEH                 PPWIGNL
017700                            L-SURE-ACTIVATE-CNTH                  PPWIGNL
017800                            L-SURE-BANK-KEYH                      PPWIGNL
017900                            L-TRANSITH                            PPWIGNL
018000                            L-SURE-TYPEINDH                       PPWIGNL
018100                            L-CHK-STREET1H                        PPWIGNL
018200     MOVE UCCOMMON-HLT-NORM-DISPL TO                              PPWIGNL
018300                            CHK-DISP-CODEH                        PPWIGNL
018400                            SURE-PRENOTE-STATH                    PPWIGNL
018500                            SURE-PRENOTE-CYCLEH                   PPWIGNL
018600                            SURE-ACTIVATE-CNTH                    PPWIGNL
018700                            SURE-BANK-KEYH                        PPWIGNL
018800                            BANK-NAMEH                            PPWIGNL
018900                            TRANSITH                              PPWIGNL
019000                            SURE-TYPEINDH                         PPWIGNL
019100                            SURE-ACCTNUMH                         PPWIGNL
019200                            CHK-STREET1H                          PPWIGNL
019300                            CHK-STREET2H                          PPWIGNL
019400                            CHK-CITYH                             PPWIGNL
019500                            CHK-STATEH                            PPWIGNL
019600                            CHK-ZIPH.                             PPWIGNL
022700                                                                  PPWIGNL
019800 2000-EXIT.                                                       PPWIGNL
019900     EXIT.                                                        PPWIGNL
023000                                                                  PPWIGNL
020100     EJECT                                                        PPWIGNL
025600                                                                  PPWIGNL
020300 3000-DISPLAY-DATA   SECTION.                                     PPWIGNL
020400**************************************************************    PPWIGNL
020500* THIS PARAGRAPH PERFORMS THE SELECTS ON THE PAY, AND PCM    *    PPWIGNL
020600* TABLES.  IT ALSO PICKS UP DATA FROM THE CONTROL FILE.      *    PPWIGNL
020700**************************************************************    PPWIGNL
033000                                                                  PPWIGNL
020900     MOVE CPWSWORK-UNIQUE-EE-ID TO WS-UNIQUE-EE-ID.               PPWIGNL
033200                                                                  PPWIGNL
021100     PERFORM 3100-GET-PAYROLL-DATA.                               PPWIGNL
033202     PERFORM 3200-GET-PCM-DATA.                                   PPWIGNL
021200                                                                  UCSD0056
021200     IF NOT UCCOMMON-UNIVERSAL-ACCESS                             UCSD0056
033209        MOVE SPACES TO SURE-TYPEINDO                              UCSD0056
033210                       SURE-ACCTNUMO                              UCSD0056
021200     END-IF.                                                      UCSD0056
039900                                                                  PPWIGNL
021400 3000-EXIT.                                                       PPWIGNL
021500     EXIT.                                                        PPWIGNL
040500                                                                  PPWIGNL
021700     EJECT                                                        PPWIGNL
040800                                                                  PPWIGNL
021900 3100-GET-PAYROLL-DATA   SECTION.                                 PPWIGNL
022000**************************************************************    PPWIGNL
022100*   SELECT PAY AND BANK TABLE DATA AND MOVE TO MAP           *    PPWIGNL
022200**************************************************************    PPWIGNL
041000                                                                  PPWIGNL
022400     PERFORM 8100-SELECT-PAY.                                     PPWIGNL
041200                                                                  PPWIGNL
022600     MOVE 'Y' TO WS-DATA-FLAG                                     PPWIGNL
022700     PERFORM 3150-OBTAIN-BANK-DATA                                PPWIGNL
041900                                                                  PPWIGNL
022900     MOVE SURE-BANK-KEY      OF PAY-ROW                           PPWIGNL
023000                            TO SURE-BANK-KEYO                     PPWIGNL
023100     MOVE SURE-PRENOTE-STAT OF PAY-ROW                            PPWIGNL
023200                            TO SURE-PRENOTE-STATO                 PPWIGNL
023300     MOVE SURE-PRENOTE-CYCLE OF PAY-ROW                           PPWIGNL
023400                            TO SURE-PRENOTE-CYCLEO                PPWIGNL
023500     MOVE SURE-TYPEIND      OF PAY-ROW                            PPWIGNL
023600                            TO SURE-TYPEINDO                      PPWIGNL
023700     MOVE SURE-ACCTNUM      OF PAY-ROW                            PPWIGNL
023800                            TO SURE-ACCTNUMO                      PPWIGNL
023900     MOVE CHK-STREET1       OF PAY-ROW                            PPWIGNL
024000                            TO CHK-STREET1O                       PPWIGNL
024100     MOVE CHK-STREET2       OF PAY-ROW                            PPWIGNL
024200                            TO CHK-STREET2O                       PPWIGNL
024300     MOVE CHK-CITY          OF PAY-ROW                            PPWIGNL
024400                            TO CHK-CITYO                          PPWIGNL
024500     MOVE CHK-STATE         OF PAY-ROW                            PPWIGNL
024600                            TO CHK-STATEO                         PPWIGNL
024700     MOVE CHK-ZIP           OF PAY-ROW                            PPWIGNL
024800                            TO CHK-ZIPO                           PPWIGNL
024900     IF CPWSWORK-PRENOTE-OPTION = 1                               PPWIGNL
025000         IF SURE-CYCLEDATE EQUAL WS-NULL-DATE                     PPWIGNL
025100             MOVE SPACES TO SURE-ACTIVATE-CNTO                    PPWIGNL
025200         ELSE                                                     PPWIGNL
025300             MOVE SURE-CYCLEDATE OF PAY-ROW                       PPWIGNL
025400                                  TO WS-ACTIVATE-CNT              PPWIGNL
025500             MOVE WS-ACTIVATE-CNT-PR TO WS-ACTIVATE-CNT-PRO       PPWIGNL
025600             MOVE WS-ACTIVATE-CNT-SF TO WS-ACTIVATE-CNT-SFO       PPWIGNL
025700             MOVE WS-ACTIVATE-CNTO TO SURE-ACTIVATE-CNTO          PPWIGNL
025800         END-IF                                                   PPWIGNL
025900     ELSE                                                         PPWIGNL
026000     IF CPWSWORK-PRENOTE-OPTION = 2                               PPWIGNL
026100         MOVE SURE-ACTIVATE-CNT OF PAY-ROW                        PPWIGNL
026200                                  TO WS-ACTIVATE-CNT-N            PPWIGNL
026300         MOVE WS-ACTIVATE-CNT-N                                   PPWIGNL
026400                                  TO WS-ACTIVATE-CNTO             PPWIGNL
026500         MOVE WS-ACTIVATE-CNT-PR TO WS-ACTIVATE-CNT-PRO           PPWIGNL
026600         MOVE WS-ACTIVATE-CNT-SF TO WS-ACTIVATE-CNT-SFO           PPWIGNL
026700         MOVE WS-ACTIVATE-CNTO TO SURE-ACTIVATE-CNTO              PPWIGNL
026800     END-IF                                                       PPWIGNL
026900     END-IF.                                                      PPWIGNL
027000                                                                  PPWIGNL
027100 3100-EXIT.                                                       PPWIGNL
027200     EXIT.                                                        PPWIGNL
027300                                                                  PPWIGNL
027400     EJECT                                                        PPWIGNL
027500                                                                  PPWIGNL
027600 3150-OBTAIN-BANK-DATA   SECTION.                                 PPWIGNL
027700**************************************************************    PPWIGNL
027800*  SELECT BNK TABLE DATA AND MOVE TO MAP                     *    PPWIGNL
027900**************************************************************    PPWIGNL
028000                                                                  PPWIGNL
028100      IF SURE-BANK-KEY = SPACES                                   PPWIGNL
028200         MOVE SPACES TO BANK-NAMEO                                PPWIGNL
028300                        TRANSITO                                  PPWIGNL
028400      ELSE                                                        PPWIGNL
028500         PERFORM 8300-SELECT-BNK                                  PPWIGNL
028600         IF SQLCODE = ZERO                                        PPWIGNL
028700            MOVE SPB-BANK-NAME TO BANK-NAMEO                      PPWIGNL
028800            MOVE SPB-TRANSIT-RTE-NO TO TRANSITO                   PPWIGNL
028900          ELSE                                                    PPWIGNL
029000            MOVE SPACES            TO BANK-NAMEO                  PPWIGNL
029100            MOVE SPACES            TO TRANSITO                    PPWIGNL
029200          END-IF                                                  PPWIGNL
029300      END-IF.                                                     PPWIGNL
029400                                                                  PPWIGNL
029500 3150-EXIT.                                                       PPWIGNL
029600     EXIT.                                                        PPWIGNL
029700                                                                  PPWIGNL
029800     EJECT                                                        PPWIGNL
029900                                                                  PPWIGNL
030000 3200-GET-PCM-DATA   SECTION.                                     PPWIGNL
030100**************************************************************    PPWIGNL
030200*  SELECT PCM TABLE DATA AND MOVE TO THE MAP                 *    PPWIGNL
030300**************************************************************    PPWIGNL
030400                                                                  PPWIGNL
030500     PERFORM 8200-SELECT-PCM.                                     PPWIGNL
030600                                                                  PPWIGNL
030700     IF  SQLCODE = ZERO                                           PPWIGNL
030800         MOVE 'Y'  TO WS-DATA-FLAG                                PPWIGNL
030900         MOVE CHK-DISP-CODE     OF PCM-ROW                        PPWIGNL
031000         TO              CHK-DISP-CODEO                           PPWIGNL
031100     END-IF.                                                      PPWIGNL
031200                                                                  PPWIGNL
031300 3200-EXIT.                                                       PPWIGNL
031400     EXIT.                                                        PPWIGNL
031500                                                                  PPWIGNL
031600     EJECT                                                        PPWIGNL
031700                                                                  PPWIGNL
031800 8100-SELECT-PAY   SECTION.                                       PPWIGNL
031900**************************************************************    PPWIGNL
032000*  SELECT PAY TABLE ROW                                      *    PPWIGNL
032100**************************************************************    PPWIGNL
032200                                                                  PPWIGNL
032300     MOVE 'SELECT PAY' TO DB2MSG-TAG.                             PPWIGNL
032400                                                                  PPWIGNL
042900     EXEC SQL                                                     PPWIGNL
043000        SELECT                                                    PPWIGNL
043100           SURE_ACCTNUM        ,                                  PPWIGNL
043200           SURE_TYPEIND        ,                                  PPWIGNL
043300           SURE_PRENOTE_STAT   ,                                  PPWIGNL
043400           SURE_CYCLEDATE      ,                                  PPWIGNL
043500           SURE_ACTIVATE_CNT   ,                                  PPWIGNL
043600           SURE_PRENOTE_CYCLE  ,                                  PPWIGNL
043700           SURE_BANK_KEY       ,                                  PPWIGNL
043800           CHK_STREET1         ,                                  PPWIGNL
043900           CHK_STREET2         ,                                  PPWIGNL
044000           CHK_CITY            ,                                  PPWIGNL
044100           CHK_STATE           ,                                  PPWIGNL
044200           CHK_ZIP                                                PPWIGNL
044300       INTO: SURE-ACCTNUM      ,                                  PPWIGNL
044400            :SURE-TYPEIND      ,                                  PPWIGNL
044500            :SURE-PRENOTE-STAT ,                                  PPWIGNL
044600            :SURE-CYCLEDATE    ,                                  PPWIGNL
044700            :SURE-ACTIVATE-CNT ,                                  PPWIGNL
044800            :SURE-PRENOTE-CYCLE,                                  PPWIGNL
044900            :SURE-BANK-KEY     ,                                  PPWIGNL
045000            :CHK-STREET1       ,                                  PPWIGNL
045100            :CHK-STREET2       ,                                  PPWIGNL
045200            :CHK-CITY          ,                                  PPWIGNL
045300            :CHK-STATE         ,                                  PPWIGNL
045400            :CHK-ZIP                                              PPWIGNL
045500       FROM PPPVZPAY_PAY                                          PPWIGNL
045600       WHERE EMPLOYEE_ID    = :WS-UNIQUE-EE-ID                    PPWIGNL
045700                                                                  PPWIGNL
045800     END-EXEC.                                                    PPWIGNL
045900                                                                  PPWIGNL
035600     IF SQLCODE NOT = ZERO                                        PPWIGNL
035700        PERFORM 999999-SQL-ERROR                                  PPWIGNL
052100     END-IF.                                                      PPWIGNL
052200                                                                  PPWIGNL
036000 8100-EXIT.                                                       PPWIGNL
036100     EXIT.                                                        PPWIGNL
052400                                                                  PPWIGNL
036300     EJECT                                                        PPWIGNL
036400                                                                  PPWIGNL
036500 8200-SELECT-PCM   SECTION.                                       PPWIGNL
036600**************************************************************    PPWIGNL
036700*  SELECT PCM TABLE ROW                                      *    PPWIGNL
036800**************************************************************    PPWIGNL
036900                                                                  PPWIGNL
037000     MOVE 'SELECT PCM' TO DB2MSG-TAG.                             PPWIGNL
037100                                                                  PPWIGNL
052500     EXEC SQL                                                     PPWIGNL
052600        SELECT                                                    PPWIGNL
052700           CHK_DISP_CODE                                          PPWIGNL
052800       INTO :CHK-DISP-CODE                                        PPWIGNL
052900       FROM PPPVZPCM_PCM                                          PPWIGNL
053000       WHERE EMPLOYEE_ID    = :WS-UNIQUE-EE-ID                    PPWIGNL
053200     END-EXEC.                                                    PPWIGNL
053300                                                                  PPWIGNL
038000 8200-EXIT.                                                       PPWIGNL
038100     EXIT.                                                        PPWIGNL
055300                                                                  PPWIGNL
038300     EJECT                                                        PPWIGNL
055500                                                                  PPWIGNL
038500 8300-SELECT-BNK   SECTION.                                       PPWIGNL
038600**************************************************************    PPWIGNL
038700*  SELECT BNK TABLE ROW                                      *    PPWIGNL
038800**************************************************************    PPWIGNL
056300                                                                  PPWIGNL
039000     MOVE 'SELECT BNK' TO DB2MSG-TAG.                             PPWIGNL
057100                                                                  PPWIGNL
059700     EXEC SQL                                                     PPWIGNL
039300          SELECT SPB_BANK_NAME,                                   PPWIGNL
039400                 SPB_TRANSIT_RTE_NO                               PPWIGNL
039500          INTO  :SPB-BANK-NAME,                                   PPWIGNL
039600                :SPB-TRANSIT-RTE-NO                               PPWIGNL
039700          FROM   PPPVZSPB_SPB                                     PPWIGNL
039800*****     WHERE  SPB_BANK_NUMBER = :SPB-BANK-NUMBER               36430960
039800          WHERE  SPB_BANK_NUMBER = :SURE-BANK-KEY                 36430960
059900     END-EXEC.                                                    PPWIGNL
040000                                                                  PPWIGNL
040100 8300-EXIT.                                                       PPWIGNL
040200     EXIT.                                                        PPWIGNL
059904                                                                  PPWIGNL
040400     EJECT                                                        PPWIGNL
040500                                                                  PPWIGNL
040600*999999-SQL-ERROR SECTION.                                        PPWIGNL
040700**************************************************************    PPWIGNL
040800*  COPIED PROCEDURE DIVISION CODE FOR SQL ERROR HANDLING     *    PPWIGNL
040900**************************************************************    PPWIGNL
041000                                                                  PPWIGNL
041100      EXEC SQL                                                    PPWIGNL
041200           INCLUDE CPPDXP99                                       PPWIGNL
041300      END-EXEC.                                                   PPWIGNL
041400                                                                  PPWIGNL
041500      MOVE 'SQL ERROR - SEE PRECEDING DB2 MESSAGES'               PPWIGNL
041600                                      TO UCWSABND-ABEND-DATA (1). PPWIGNL
041700                                                                  PPWIGNL
041800      PERFORM 9999-ABEND.                                         PPWIGNL
041900                                                                  PPWIGNL
042000 999999-EXIT.                                                     PPWIGNL
042100       EXIT.                                                      PPWIGNL
042200                                                                  PPWIGNL
042300       EJECT                                                      PPWIGNL
042400                                                                  PPWIGNL
042500 9999-ABEND                           SECTION.                    PPWIGNL
042600******************************************************************PPWIGNL
042700*  COPIED PROCEDURE DIVISION CODE FOR ABEND HANDLING             *PPWIGNL
042800******************************************************************PPWIGNL
042900                                                                  PPWIGNL
043000     COPY UCPDABND.                                               PPWIGNL
043100                                                                  PPWIGNL
043200 9999-EXIT.                                                       PPWIGNL
043300     EXIT.                                                        PPWIGNL
043400***************    END OF SOURCE - PPWIGNL     *******************PPWIGNL
