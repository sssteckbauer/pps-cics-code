000800 IDENTIFICATION DIVISION.                                         ULA1SAUP
000900 PROGRAM-ID. ULA1SAUP.                                            ULA1SAUP
001000 ENVIRONMENT DIVISION.                                            ULA1SAUP
001100 CONFIGURATION SECTION.                                           ULA1SAUP
001200 SOURCE-COMPUTER.                         COPY CPOTXUCS.          ULA1SAUP
001300 OBJECT-COMPUTER.                         COPY CPOTXOBJ.          ULA1SAUP
001400 DATA DIVISION.                                                   ULA1SAUP
001500     EJECT                                                        ULA1SAUP
001600 WORKING-STORAGE SECTION.                                         ULA1SAUP
001700 01  WS-COMN.                                                     ULA1SAUP
001800     05  WS-START                         PIC X(27) VALUE         ULA1SAUP
001900         'WORKING STORAGE STARTS HERE'.                           ULA1SAUP
002000     05  A-STND-PROG-ID                   PIC X(15) VALUE         ULA1SAUP
002100                                                    'ULA1SAUP'.   ULA1SAUP
002200     05  WS-QUEUE-ID.                                             ULA1SAUP
002300         10  WS-QUEUE-NAME-SS             PIC X(2).               ULA1SAUP
002400         10  WS-QUEUE-NAME-XX             PIC X(2).               ULA1SAUP
002500         10  WS-QUEUE-TERMID              PIC X(4).               ULA1SAUP
002600     05  WS-RESP                          PIC S9(8) COMP.         ULA1SAUP
002700     05  WS-TIMESTAMP.                                            ULA1SAUP
002800         10  WS-TS-YYYY                   PIC X(4).               ULA1SAUP
002900         10  FILLER                       PIC X(1).               ULA1SAUP
003000         10  WS-TS-MM                     PIC X(2).               ULA1SAUP
003100         10  FILLER                       PIC X(1).               ULA1SAUP
003200         10  WS-TS-DD                     PIC X(2).               ULA1SAUP
003300         10  FILLER                       PIC X(1).               ULA1SAUP
003400         10  WS-TS-HH                     PIC X(2).               ULA1SAUP
003500         10  FILLER                       PIC X(1).               ULA1SAUP
003600         10  WS-TS-MN                     PIC X(2).               ULA1SAUP
003700         10  FILLER                       PIC X(1).               ULA1SAUP
003800         10  WS-TS-SS                     PIC X(2).               ULA1SAUP
003900                                                                  ULA1SAUP
003910 01  WS-MESS-NOS.                                                 ULA1SAUP
003920     05  MU0001                           PIC X(5) VALUE 'U0001'. ULA1SAUP
003920     05  MU0007                           PIC X(5) VALUE 'U0007'. ULA1SAUP
003920     05  MU0104                           PIC X(5) VALUE 'U0104'. ULA1SAUP
003920     05  MU0128                           PIC X(5) VALUE 'U0128'. ULA1SAUP
003920     05  MU0215                           PIC X(5) VALUE 'U0215'. ULA1SAUP
003930                                                                  ULA1SAUP
004800 01 LIMITS-AND-CONSTANTS.                                         ULA1SAUP
004900     05  PGM-RELEASE-NBR                  PIC X(5)                ULA1SAUP
005000                                               VALUE 'DU057'.     ULA1SAUP
004900     05  DIR-PER-SCRN                     PIC S9(5) COMP          ULA1SAUP
005000                                               VALUE +10.         ULA1SAUP
004800 01 HOLD-NAME-EXT.                                                ULA1SAUP
004900     05 HOLD-NAME                         PIC X(22).              ULA1SAUP
004900     05 HOLD-PHONE-EXT                    PIC X(8).               ULA1SAUP
005700     EJECT                                                        ULA1SAUP
005800 01  CLWSEDIR EXTERNAL.                                           ULA1SAUP
005900     EXEC SQL                                                     ULA1SAUP
006000          INCLUDE CLWSEDIR                                        ULA1SAUP
006100     END-EXEC.                                                    ULA1SAUP
006200     EJECT                                                        ULA1SAUP
006300 01  UCCOMMON   EXTERNAL.                                         ULA1SAUP
006400     COPY UCCOMMON.                                               ULA1SAUP
006500     EJECT                                                        ULA1SAUP
006600 01  UCPIROFE   EXTERNAL.                                         ULA1SAUP
006700     COPY UCPIROFE.                                               ULA1SAUP
006800     EJECT                                                        ULA1SAUP
006900 01  UCWSABND    EXTERNAL.                                        ULA1SAUP
007000     COPY UCWSABND.                                               ULA1SAUP
007100     EJECT                                                        ULA1SAUP
007200 01  PPDB2MSG-INTERFACE.            COPY CPLNKDB2.                ULA1SAUP
007300******************************************************************ULA1SAUP
007400**  SQL       AREAS                                             **ULA1SAUP
007500**                                                              **ULA1SAUP
007600** THE VIEWS USED IN THIS PROGRAM ARE:                          **ULA1SAUP
007700**                                                              **ULA1SAUP
007800**    VIEW              VIEW DDL AND DCL INCLUDE                **ULA1SAUP
007900**                       MEMBER NAMES                           **ULA1SAUP
008000**                                                              **ULA1SAUP
008100**  UC0VZDIR_DIR          UC0VZDIR                              **ULA1SAUP
008400**                                                              **ULA1SAUP
008500******************************************************************ULA1SAUP
008600     EXEC SQL                                                     ULA1SAUP
008700          INCLUDE SQLCA                                           ULA1SAUP
008800     END-EXEC.                                                    ULA1SAUP
008900     EJECT                                                        ULA1SAUP
009400     EXEC SQL                                                     ULA1SAUP
009500          INCLUDE UC0VZDIR                                        ULA1SAUP
009600     END-EXEC.                                                    ULA1SAUP
009700 LINKAGE SECTION.                                                 ULA1SAUP
009800     EJECT                                                        ULA1SAUP
009900 PROCEDURE DIVISION.                                              ULA1SAUP
010000****************************************************************  ULA1SAUP
010100*            P R O C E D U R E  D I V I S I O N                   ULA1SAUP
010200****************************************************************  ULA1SAUP
010300     EXEC SQL                                                     ULA1SAUP
010400          INCLUDE CPPDXE99                                        ULA1SAUP
010500     END-EXEC.                                                    ULA1SAUP
010600 0000-PARA-FOR-PRECOMP-PAGING  SECTION.                           ULA1SAUP
010700****************************************************************  ULA1SAUP
010800                                                                  ULA1SAUP
011000 0000-MAINLINE SECTION.                                           ULA1SAUP
011100     SET UCCOMMON-RETURN-NO-ERRORS TO TRUE                        ULA1SAUP
011200     MOVE  A-STND-PROG-ID TO DB2MSG-PGM-ID                        ULA1SAUP
011300                                                                  ULA1SAUP
011400     EVALUATE UCCOMMON-FUNCTION                                   ULA1SAUP
011900       WHEN 'EDIR'                                                ULA1SAUP
012000          PERFORM 2000-EVAL-DIR-TBL                               ULA1SAUP
012100     END-EVALUATE                                                 ULA1SAUP
010800                                                                  ULA1SAUP
011100     SET UCCOMMON-NO-UPD-IN-PROGRESS TO TRUE.                     ULA1SAUP
010800                                                                  ULA1SAUP
012200     GOBACK.                                                      ULA1SAUP
012300     EJECT                                                        ULA1SAUP
014800 2000-EVAL-DIR-TBL   SECTION.                                     ULA1SAUP
014900****************************************************************  ULA1SAUP
017200                                                                  ULA1SAUP
016000     MOVE EDIR-ADDRESS   TO DIR-ADDRESS                           ULA1SAUP
016000     MOVE EDIR-NAME      TO HOLD-NAME                             ULA1SAUP
016000     MOVE EDIR-PHONE-EXT TO HOLD-PHONE-EXT                        ULA1SAUP
016000     MOVE HOLD-NAME-EXT  TO DIR-NAME                              ULA1SAUP
016000     MOVE EDIR-TYPE      TO DIR-MODE                              ULA1SAUP
017200                                                                  ULA1SAUP
016000     EVALUATE EDIR-ACT                                            ULA1SAUP
016100       WHEN 'D'                                                   ULA1SAUP
016200         MOVE 'DELETE FROM DIR TABLE       ' TO DB2MSG-TAG        ULA1SAUP
016300         PERFORM 3200-DEL-DIR-ROW                                 ULA1SAUP
016303         IF SQLCODE = ZERO                                        ULA1SAUP
016304             MOVE MU0128 TO UCCOMMON-MSG-NUMBER                   ULA1SAUP
016303         ELSE                                                     ULA1SAUP
016303             IF SQLCODE = +100                                    ULA1SAUP
016304                MOVE MU0104 TO UCCOMMON-MSG-NUMBER                ULA1SAUP
016305             END-IF                                               ULA1SAUP
016305         END-IF                                                   ULA1SAUP
016400       WHEN 'C'                                                   ULA1SAUP
016500         MOVE 'UPDATE DIR TABLE            ' TO DB2MSG-TAG        ULA1SAUP
016600         PERFORM 3300-UPD-DIR-ROW                                 ULA1SAUP
016303         IF SQLCODE = ZERO                                        ULA1SAUP
016304             MOVE MU0007 TO UCCOMMON-MSG-NUMBER                   ULA1SAUP
016303         ELSE                                                     ULA1SAUP
016303             IF SQLCODE = +100                                    ULA1SAUP
016304                MOVE MU0104 TO UCCOMMON-MSG-NUMBER                ULA1SAUP
016305             END-IF                                               ULA1SAUP
016305         END-IF                                                   ULA1SAUP
017200                                                                  ULA1SAUP
016400       WHEN 'A'                                                   ULA1SAUP
016500         MOVE 'ADD TO DIR TABLE            ' TO DB2MSG-TAG        ULA1SAUP
016600         PERFORM 3100-ADD-DIR-ROW                                 ULA1SAUP
016303         IF SQLCODE = ZERO                                        ULA1SAUP
016304             MOVE MU0007 TO UCCOMMON-MSG-NUMBER                   ULA1SAUP
016303         ELSE                                                     ULA1SAUP
016303             IF SQLCODE = -803                                    ULA1SAUP
016304                 MOVE MU0215 TO UCCOMMON-MSG-NUMBER               ULA1SAUP
016303             END-IF                                               ULA1SAUP
016305         END-IF                                                   ULA1SAUP
017100     END-EVALUATE.                                                ULA1SAUP
017200                                                                  ULA1SAUP
017300 3000-EXIT.                                                       ULA1SAUP
017400     EXIT.                                                        ULA1SAUP
017500     EJECT                                                        ULA1SAUP
017600 3100-ADD-DIR-ROW   SECTION.                                      ULA1SAUP
017700****************************************************************  ULA1SAUP
017800****************************************************************  ULA1SAUP
017900     EXEC SQL                                                     ULA1SAUP
018000        INSERT                                                    ULA1SAUP
018100        INTO UC0VZDIR_DIR                                         ULA1SAUP
018210               ( DIR_ADDRESS                                      ULA1SAUP
018300             ,   DIR_NAME                                         ULA1SAUP
019000             ,   DIR_MODE    )                                    ULA1SAUP
019010        VALUES (:DIR-ADDRESS                                      ULA1SAUP
019020             ,  :DIR-NAME                                         ULA1SAUP
019090             ,  :DIR-MODE         )                               ULA1SAUP
019100     END-EXEC.                                                    ULA1SAUP
019200                                                                  ULA1SAUP
014320     MOVE SPACE TO EDIR-UPD-AREA                                  ULA1SAUP
014320     SET CLWSEDIR-TABLE-UPDATED TO TRUE.                          ULA1SAUP
019200                                                                  ULA1SAUP
019300 3100-EXIT.                                                       ULA1SAUP
019400     EXIT.                                                        ULA1SAUP
019500     EJECT                                                        ULA1SAUP
019600 3200-DEL-DIR-ROW   SECTION.                                      ULA1SAUP
019700****************************************************************  ULA1SAUP
019800                                                                  ULA1SAUP
019900     EXEC SQL                                                     ULA1SAUP
020000        DELETE                                                    ULA1SAUP
020100        FROM UC0VZDIR_DIR                                         ULA1SAUP
020200       WHERE DIR_ADDRESS = :DIR-ADDRESS                           ULA1SAUP
020300     END-EXEC.                                                    ULA1SAUP
020400                                                                  ULA1SAUP
014320     MOVE SPACE TO EDIR-UPD-AREA                                  ULA1SAUP
014320     SET CLWSEDIR-TABLE-UPDATED TO TRUE.                          ULA1SAUP
019800                                                                  ULA1SAUP
020500 3200-EXIT.                                                       ULA1SAUP
020600     EXIT.                                                        ULA1SAUP
020700     EJECT                                                        ULA1SAUP
020800 3300-UPD-DIR-ROW   SECTION.                                      ULA1SAUP
020900****************************************************************  ULA1SAUP
021000                                                                  ULA1SAUP
021100     EXEC SQL                                                     ULA1SAUP
021200        UPDATE                                                    ULA1SAUP
021300        UC0VZDIR_DIR                                              ULA1SAUP
021400        SET DIR_ADDRESS = :DIR-ADDRESS                            ULA1SAUP
021500          , DIR_NAME    = :DIR-NAME                               ULA1SAUP
021900          , DIR_MODE    = :DIR-MODE                               ULA1SAUP
022200       WHERE DIR_ADDRESS = :DIR-ADDRESS                           ULA1SAUP
022300     END-EXEC.                                                    ULA1SAUP
022400                                                                  ULA1SAUP
014320     MOVE SPACE TO EDIR-UPD-AREA                                  ULA1SAUP
014320     SET CLWSEDIR-TABLE-UPDATED TO TRUE.                          ULA1SAUP
022400                                                                  ULA1SAUP
022500 3300-EXIT.                                                       ULA1SAUP
022600     EXIT.                                                        ULA1SAUP
022700     EJECT                                                        ULA1SAUP
022800 9999-ABEND    SECTION.                                           ULA1SAUP
022900**************************************************************    ULA1SAUP
023000*  COPIED PROCEDURE DIVISION CODE FOR ABEND HANDLING         *    ULA1SAUP
023100**************************************************************    ULA1SAUP
023200     MOVE A-STND-PROG-ID  TO UCWSABND-ABENDING-PGM.               ULA1SAUP
023300                                                                  ULA1SAUP
023400     COPY UCPDABND.                                               ULA1SAUP
023500     EJECT                                                        ULA1SAUP
023600*999999-SQL-ERROR SECTION.                                        ULA1SAUP
023700**************************************************************    ULA1SAUP
023800*  COPIED PROCEDURE DIVISION CODE FOR SQL ERROR HANDLING     *    ULA1SAUP
023900**************************************************************    ULA1SAUP
024000      EXEC SQL                                                    ULA1SAUP
024100           INCLUDE CPPDXP99                                       ULA1SAUP
024200      END-EXEC.                                                   ULA1SAUP
024300                                                                  ULA1SAUP
024400      MOVE 'SQL ERROR - SEE PRECEDING DB2 MESSAGES'               ULA1SAUP
024500                                 TO UCWSABND-ABEND-DATA (1).      ULA1SAUP
024600                                                                  ULA1SAUP
024700      PERFORM 9999-ABEND.                                         ULA1SAUP
